/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.w3.geo;

import java.net.URI;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.ontologies.WGS84Pos;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * WGS84 ontology annotator
 *
 * @author ymombrun
 */
public class WGS84Annotator extends BaseAnnotator {


	private static final String PREFIX = WGS84Pos.PREFERRED_PREFIX;


	private static final URI NS_URI = URI.create(WGS84Pos.NAMESPACE);


	private static final String LAT = "lat";


	private static final String LONG = "long";


	private static final String ALT = "alt";


	private static final String LOCATION = "location";


	/**
	 * @param subject
	 *            The default subject of the statements to be read/written
	 * @param pieceOfKnowledge
	 *            The pok holding the RDF
	 */
	public WGS84Annotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * @param resource
	 *            The resource holding the Annotations to be read/written
	 */
	public WGS84Annotator(final Resource resource) {
		super(resource);
	}


	/**
	 * Read the Latitude of the SpatialThing
	 *
	 * @return a Double
	 */
	public Value<Double> readLat() {
		return this.applyOperator(Operator.READ, WGS84Annotator.PREFIX, WGS84Annotator.NS_URI, WGS84Annotator.LAT, Double.class, null);
	}


	/**
	 * Write the Latitude of the SpatialThing
	 *
	 * @param value
	 *            a Double
	 */
	public void writeLat(final Double value) {
		this.applyOperator(Operator.WRITE, WGS84Annotator.PREFIX, WGS84Annotator.NS_URI, WGS84Annotator.LAT, Double.class, value);
	}


	/**
	 * Read the Longitude of the SpatialThing
	 *
	 * @return a Double
	 */
	public Value<Double> readLong() {
		return this.applyOperator(Operator.READ, WGS84Annotator.PREFIX, WGS84Annotator.NS_URI, WGS84Annotator.LONG, Double.class, null);
	}


	/**
	 * Write the Longitude of the SpatialThing
	 *
	 * @param value
	 *            a Double
	 */
	public void writeLong(final Double value) {
		this.applyOperator(Operator.WRITE, WGS84Annotator.PREFIX, WGS84Annotator.NS_URI, WGS84Annotator.LONG, Double.class, value);
	}


	/**
	 * Read the Altitude of the SpatialThing
	 *
	 * @return a Double
	 */
	public Value<Double> readAlt() {
		return this.applyOperator(Operator.READ, WGS84Annotator.PREFIX, WGS84Annotator.NS_URI, WGS84Annotator.ALT, Double.class, null);
	}


	/**
	 * Write the Altitude of the SpatialThing
	 *
	 * @param value
	 *            a Double
	 */
	public void writeAlt(final Double value) {
		this.applyOperator(Operator.WRITE, WGS84Annotator.PREFIX, WGS84Annotator.NS_URI, WGS84Annotator.ALT, Double.class, value);
	}


	/**
	 * Read the location (i.e. the SpatialThing) of a Thing
	 *
	 * @return a Double
	 */
	public Value<URI> readLocation() {
		return this.applyOperator(Operator.READ, WGS84Annotator.PREFIX, WGS84Annotator.NS_URI, WGS84Annotator.LOCATION, URI.class, null);
	}


	/**
	 * Write the location (i.e. the SpatialThing) of a Thing
	 *
	 * @param value
	 *            an URI of a SpatialThing
	 */
	public void writeLocation(final URI value) {
		this.applyOperator(Operator.WRITE, WGS84Annotator.PREFIX, WGS84Annotator.NS_URI, WGS84Annotator.LOCATION, URI.class, value);
	}

}
