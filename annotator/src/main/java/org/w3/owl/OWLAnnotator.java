/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.w3.owl;

import java.net.URI;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * The annotator for OWL properties
 */
public class OWLAnnotator extends BaseAnnotator {


	protected static final String PREFIX = "owl";


	protected static final URI AURI = URI.create("http://www.w3.org/2002/07/owl#");


	protected static final String OWL = "owl";


	protected static final String BOTTOM_DATA_PROPERTY = "bottomDataProperty";


	protected static final String TOP_DATA_PROPERTY = "topDataProperty";


	protected static final String BOTTOM = "bottom";


	protected static final String TOP = "top";


	protected static final String ALL_VALUES_FROM = "allValuesFrom";


	protected static final String ANNOTATED_PROPERTY = "annotatedProperty";


	protected static final String ANNOTATED_SOURCE = "annotatedSource";


	protected static final String ANNOTATED_TARGET = "annotatedTarget";


	protected static final String ASSERTION_PROPERTY = "assertionProperty";


	protected static final String CARDINALITY = "cardinality";


	protected static final String COMPLEMENT_OF = "complementOf";


	protected static final String DATATYPE_COMPLEMENT_OF = "datatypeComplementOf";


	protected static final String DIFFERENT_FROM = "differentFrom";


	protected static final String DISJOINT_UNION_OF = "disjointUnionOf";


	protected static final String DISJOINT_WITH = "disjointWith";


	protected static final String DISTINCT_MEMBERS = "distinctMembers";


	protected static final String EQUIVALENT_CLASS = "equivalentClass";


	protected static final String EQUIVALENT_PROPERTY = "equivalentProperty";


	protected static final String HAS_KEY = "hasKey";


	protected static final String HAS_SELF = "hasSelf";


	protected static final String HAS_VALUE = "hasValue";


	protected static final String INTERSECTION_OF = "intersectionOf";


	protected static final String INVERSE_OF = "inverseOf";


	protected static final String MAX_CARDINALITY = "maxCardinality";


	protected static final String MAX_QUALIFIED_CARDINALITY = "maxQualifiedCardinality";


	protected static final String MEMBERS = "members";


	protected static final String MIN_CARDINALITY = "minCardinality";


	protected static final String MIN_QUALIFIED_CARDINALITY = "minQualifiedCardinality";


	protected static final String ON_CLASS = "onClass";


	protected static final String ON_DATA_RANGE = "onDataRange";


	protected static final String ON_DATATYPE = "onDatatype";


	protected static final String ONE_OF = "oneOf";


	protected static final String ON_PROPERTIES = "onProperties";


	protected static final String ON_PROPERTY = "onProperty";


	protected static final String PROPERTY_CHAIN_AXIOM = "propertyChainAxiom";


	protected static final String PROPERTY_DISJOINT_WITH = "propertyDisjointWith";


	protected static final String QUALIFIED_CARDINALITY = "qualifiedCardinality";


	protected static final String SAME_AS = "sameAs";


	protected static final String SOME_VALUES_FROM = "someValuesFrom";


	protected static final String SOURCE_INDIVIDUAL = "sourceIndividual";


	protected static final String TARGET_INDIVIDUAL = "targetIndividual";


	protected static final String TARGET_VALUE = "targetValue";


	protected static final String UNION_OF = "unionOf";


	protected static final String WITH_RESTRICTIONS = "withRestrictions";


	/**
	 * @param subject
	 *            the subject on which annotate.
	 * @param pieceOfKnowledge
	 *            the piece of knowledge that will contains statements.
	 */
	public OWLAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * @param resource
	 *            The resource to be annotated
	 */
	public OWLAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * Read the
	 *
	 * @return an URI
	 */
	public final Value<URI> readOwl() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.OWL, URI.class, null);
	}


	/**
	 * Write the
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOwl(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.OWL, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The data property that does not relate any individual to any data value.
	 *
	 * @return a String
	 */
	public final Value<String> readBottomDataProperty() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.BOTTOM_DATA_PROPERTY, String.class, null);
	}


	/**
	 * Write the The data property that does not relate any individual to any data value.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeBottomDataProperty(final String value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.BOTTOM_DATA_PROPERTY, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The data property that relates every individual to every data value.
	 *
	 * @return a String
	 */
	public final Value<String> readTopDataProperty() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.TOP_DATA_PROPERTY, String.class, null);
	}


	/**
	 * Write the The data property that relates every individual to every data value.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTopDataProperty(final String value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.TOP_DATA_PROPERTY, String.class, value).getIsAnnotatedOn();
	}



	/**
	 * Read the The object property that does not relate any two individuals.
	 *
	 * @return a String
	 */
	public final Value<String> readBottom() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.BOTTOM, String.class, null);
	}


	/**
	 * Write the The object property that does not relate any two individuals.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeBottom(final String value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.BOTTOM, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The object property that relates every two individuals.
	 *
	 * @return a String
	 */
	public final Value<String> readTop() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.TOP, String.class, null);
	}


	/**
	 * Write the The object property that relates every two individuals.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTop(final String value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.TOP, String.class, value).getIsAnnotatedOn();
	}



	/**
	 * Read the The property that determines the class that a universal property restriction refers to.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAllValuesFrom() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ALL_VALUES_FROM, URI.class, null);
	}


	/**
	 * Write the The property that determines the class that a universal property restriction refers to.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAllValuesFrom(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ALL_VALUES_FROM, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the predicate of an annotated axiom or annotated annotation.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAnnotatedProperty() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ANNOTATED_PROPERTY, URI.class, null);
	}


	/**
	 * Write the The property that determines the predicate of an annotated axiom or annotated annotation.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAnnotatedProperty(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ANNOTATED_PROPERTY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the subject of an annotated axiom or annotated annotation.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAnnotatedSource() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ANNOTATED_SOURCE, URI.class, null);
	}


	/**
	 * Write the The property that determines the subject of an annotated axiom or annotated annotation.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAnnotatedSource(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ANNOTATED_SOURCE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the object of an annotated axiom or annotated annotation.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAnnotatedTarget() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ANNOTATED_TARGET, URI.class, null);
	}


	/**
	 * Write the The property that determines the object of an annotated axiom or annotated annotation.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAnnotatedTarget(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ANNOTATED_TARGET, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the predicate of a negative property assertion.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAssertionProperty() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ASSERTION_PROPERTY, URI.class, null);
	}


	/**
	 * Write the The property that determines the predicate of a negative property assertion.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAssertionProperty(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ASSERTION_PROPERTY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the cardinality of an exact cardinality restriction.
	 *
	 * @return an URI
	 */
	public final Value<URI> readCardinality() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.CARDINALITY, URI.class, null);
	}


	/**
	 * Write the The property that determines the cardinality of an exact cardinality restriction.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCardinality(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.CARDINALITY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines that a given class is the complement of another class.
	 *
	 * @return an URI
	 */
	public final Value<URI> readComplementOf() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.COMPLEMENT_OF, URI.class, null);
	}


	/**
	 * Write the The property that determines that a given class is the complement of another class.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeComplementOf(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.COMPLEMENT_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines that a given data range is the complement of another data range with respect to the data domain.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDatatypeComplementOf() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.DATATYPE_COMPLEMENT_OF, URI.class, null);
	}


	/**
	 * Write the The property that determines that a given data range is the complement of another data range with respect to the data domain.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDatatypeComplementOf(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.DATATYPE_COMPLEMENT_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines that two given individuals are different.
	 *
	 * @return a String
	 */
	public final Value<String> readDifferentFrom() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.DIFFERENT_FROM, String.class, null);
	}


	/**
	 * Write the The property that determines that two given individuals are different.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDifferentFrom(final String value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.DIFFERENT_FROM, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines that a given class is equivalent to the disjoint union of a collection of other classes.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDisjointUnionOf() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.DISJOINT_UNION_OF, URI.class, null);
	}


	/**
	 * Write the The property that determines that a given class is equivalent to the disjoint union of a collection of other classes.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDisjointUnionOf(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.DISJOINT_UNION_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines that two given classes are disjoint.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDisjointWith() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.DISJOINT_WITH, URI.class, null);
	}


	/**
	 * Write the The property that determines that two given classes are disjoint.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDisjointWith(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.DISJOINT_WITH, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the collection of pairwise different individuals in a owl:AllDifferent axiom.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDistinctMembers() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.DISTINCT_MEMBERS, URI.class, null);
	}


	/**
	 * Write the The property that determines the collection of pairwise different individuals in a owl:AllDifferent axiom.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDistinctMembers(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.DISTINCT_MEMBERS, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines that two given classes are equivalent, and that is used to specify datatype definitions.
	 *
	 * @return an URI
	 */
	public final Value<URI> readEquivalentClass() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.EQUIVALENT_CLASS, URI.class, null);
	}


	/**
	 * Write the The property that determines that two given classes are equivalent, and that is used to specify datatype definitions.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeEquivalentClass(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.EQUIVALENT_CLASS, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines that two given properties are equivalent.
	 *
	 * @return an URI
	 */
	public final Value<URI> readEquivalentProperty() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.EQUIVALENT_PROPERTY, URI.class, null);
	}


	/**
	 * Write the The property that determines that two given properties are equivalent.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeEquivalentProperty(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.EQUIVALENT_PROPERTY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the collection of properties that jointly build a key.
	 *
	 * @return an URI
	 */
	public final Value<URI> readHasKey() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.HAS_KEY, URI.class, null);
	}


	/**
	 * Write the The property that determines the collection of properties that jointly build a key.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeHasKey(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.HAS_KEY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the property that a self restriction refers to.
	 *
	 * @return an URI
	 */
	public final Value<URI> readHasSelf() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.HAS_SELF, URI.class, null);
	}


	/**
	 * Write the The property that determines the property that a self restriction refers to.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeHasSelf(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.HAS_SELF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the individual that a has-value restriction refers to.
	 *
	 * @return an URI
	 */
	public final Value<URI> readHasValue() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.HAS_VALUE, URI.class, null);
	}


	/**
	 * Write the The property that determines the individual that a has-value restriction refers to.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeHasValue(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.HAS_VALUE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the collection of classes or data ranges that build an intersection.
	 *
	 * @return an URI
	 */
	public final Value<URI> readIntersectionOf() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.INTERSECTION_OF, URI.class, null);
	}


	/**
	 * Write the The property that determines the collection of classes or data ranges that build an intersection.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeIntersectionOf(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.INTERSECTION_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines that two given properties are inverse.
	 *
	 * @return an URI
	 */
	public final Value<URI> readInverseOf() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.INVERSE_OF, URI.class, null);
	}


	/**
	 * Write the The property that determines that two given properties are inverse.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeInverseOf(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.INVERSE_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the cardinality of a maximum cardinality restriction.
	 *
	 * @return an URI
	 */
	public final Value<URI> readMaxCardinality() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.MAX_CARDINALITY, URI.class, null);
	}


	/**
	 * Write the The property that determines the cardinality of a maximum cardinality restriction.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMaxCardinality(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.MAX_CARDINALITY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the cardinality of a maximum qualified cardinality restriction.
	 *
	 * @return an URI
	 */
	public final Value<URI> readMaxQualifiedCardinality() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.MAX_QUALIFIED_CARDINALITY, URI.class, null);
	}


	/**
	 * Write the The property that determines the cardinality of a maximum qualified cardinality restriction.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMaxQualifiedCardinality(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.MAX_QUALIFIED_CARDINALITY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the collection of members in either a owl:AllDifferent, :AllDisjointClasses or owl:AllDisjointProperties axiom.
	 *
	 * @return an URI
	 */
	public final Value<URI> readMembers() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.MEMBERS, URI.class, null);
	}


	/**
	 * Write the The property that determines the collection of members in either a owl:AllDifferent, :AllDisjointClasses or owl:AllDisjointProperties axiom.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMembers(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.MEMBERS, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the cardinality of a minimum cardinality restriction.
	 *
	 * @return an URI
	 */
	public final Value<URI> readMinCardinality() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.MIN_CARDINALITY, URI.class, null);
	}


	/**
	 * Write the The property that determines the cardinality of a minimum cardinality restriction.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMinCardinality(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.MIN_CARDINALITY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the cardinality of a minimum qualified cardinality restriction.
	 *
	 * @return an URI
	 */
	public final Value<URI> readMinQualifiedCardinality() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.MIN_QUALIFIED_CARDINALITY, URI.class, null);
	}


	/**
	 * Write the The property that determines the cardinality of a minimum qualified cardinality restriction.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMinQualifiedCardinality(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.MIN_QUALIFIED_CARDINALITY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the class that a qualified object cardinality restriction refers to.
	 *
	 * @return an URI
	 */
	public final Value<URI> readOnClass() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ON_CLASS, URI.class, null);
	}


	/**
	 * Write the The property that determines the class that a qualified object cardinality restriction refers to.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOnClass(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ON_CLASS, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the data range that a qualified data cardinality restriction refers to.
	 *
	 * @return an URI
	 */
	public final Value<URI> readOnDataRange() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ON_DATA_RANGE, URI.class, null);
	}


	/**
	 * Write the The property that determines the data range that a qualified data cardinality restriction refers to.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOnDataRange(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ON_DATA_RANGE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the datatype that a datatype restriction refers to.
	 *
	 * @return an URI
	 */
	public final Value<URI> readOnDatatype() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ON_DATATYPE, URI.class, null);
	}


	/**
	 * Write the The property that determines the datatype that a datatype restriction refers to.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOnDatatype(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ON_DATATYPE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the collection of individuals or data values that build an enumeration.
	 *
	 * @return an URI
	 */
	public final Value<URI> readOneOf() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ONE_OF, URI.class, null);
	}


	/**
	 * Write the The property that determines the collection of individuals or data values that build an enumeration.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOneOf(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ONE_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the n-tuple of properties that a property restriction on an n-ary data range refers to.
	 *
	 * @return an URI
	 */
	public final Value<URI> readOnProperties() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ON_PROPERTIES, URI.class, null);
	}


	/**
	 * Write the The property that determines the n-tuple of properties that a property restriction on an n-ary data range refers to.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOnProperties(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ON_PROPERTIES, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the property that a property restriction refers to.
	 *
	 * @return an URI
	 */
	public final Value<URI> readOnProperty() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.ON_PROPERTY, URI.class, null);
	}


	/**
	 * Write the The property that determines the property that a property restriction refers to.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOnProperty(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.ON_PROPERTY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the n-tuple of properties that build a sub property chain of a given property.
	 *
	 * @return an URI
	 */
	public final Value<URI> readPropertyChainAxiom() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.PROPERTY_CHAIN_AXIOM, URI.class, null);
	}


	/**
	 * Write the The property that determines the n-tuple of properties that build a sub property chain of a given property.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePropertyChainAxiom(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.PROPERTY_CHAIN_AXIOM, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines that two given properties are disjoint.
	 *
	 * @return an URI
	 */
	public final Value<URI> readPropertyDisjointWith() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.PROPERTY_DISJOINT_WITH, URI.class, null);
	}


	/**
	 * Write the The property that determines that two given properties are disjoint.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePropertyDisjointWith(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.PROPERTY_DISJOINT_WITH, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the cardinality of an exact qualified cardinality restriction.
	 *
	 * @return an URI
	 */
	public final Value<URI> readQualifiedCardinality() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.QUALIFIED_CARDINALITY, URI.class, null);
	}


	/**
	 * Write the The property that determines the cardinality of an exact qualified cardinality restriction.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeQualifiedCardinality(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.QUALIFIED_CARDINALITY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines that two given individuals are equal.
	 *
	 * @return a URI
	 */
	public final Value<URI> readSameAs() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.SAME_AS, URI.class, null);
	}


	/**
	 * Write the The property that determines that two given individuals are equal.
	 *
	 * @param value
	 *            a URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSameAs(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.SAME_AS, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the class that an existential property restriction refers to.
	 *
	 * @return an URI
	 */
	public final Value<URI> readSomeValuesFrom() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.SOME_VALUES_FROM, URI.class, null);
	}


	/**
	 * Write the The property that determines the class that an existential property restriction refers to.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSomeValuesFrom(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.SOME_VALUES_FROM, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the subject of a negative property assertion.
	 *
	 * @return a String
	 */
	public final Value<String> readSourceIndividual() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.SOURCE_INDIVIDUAL, String.class, null);
	}


	/**
	 * Write the The property that determines the subject of a negative property assertion.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSourceIndividual(final String value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.SOURCE_INDIVIDUAL, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the object of a negative object property assertion.
	 *
	 * @return a String
	 */
	public final Value<String> readTargetIndividual() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.TARGET_INDIVIDUAL, String.class, null);
	}


	/**
	 * Write the The property that determines the object of a negative object property assertion.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTargetIndividual(final String value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.TARGET_INDIVIDUAL, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the value of a negative data property assertion.
	 *
	 * @return a String
	 */
	public final Value<String> readTargetValue() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.TARGET_VALUE, String.class, null);
	}


	/**
	 * Write the The property that determines the value of a negative data property assertion.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTargetValue(final String value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.TARGET_VALUE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the collection of classes or data ranges that build a union.
	 *
	 * @return an URI
	 */
	public final Value<URI> readUnionOf() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.UNION_OF, URI.class, null);
	}


	/**
	 * Write the The property that determines the collection of classes or data ranges that build a union.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeUnionOf(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.UNION_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The property that determines the collection of facet-value pairs that define a datatype restriction.
	 *
	 * @return an URI
	 */
	public final Value<URI> readWithRestrictions() {
		return this.applyOperator(Operator.READ, null, OWLAnnotator.AURI, OWLAnnotator.WITH_RESTRICTIONS, URI.class, null);
	}


	/**
	 * Write the The property that determines the collection of facet-value pairs that define a datatype restriction.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeWithRestrictions(final URI value) {
		return this.applyOperator(Operator.WRITE, OWLAnnotator.PREFIX, OWLAnnotator.AURI, OWLAnnotator.WITH_RESTRICTIONS, URI.class, value).getIsAnnotatedOn();
	}

}
