/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.w3.ma_ont;

import java.net.URI;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * Created by Tobias Buerger, Jean Pierre Evain and Pierre-Antoine Champin with the RDFS Taskforce within the W3C Media Annotation Working Group.
 */
public class MAOntAnnotator extends BaseAnnotator {


	protected static final String APREFIX = "ma-ont";


	protected static final URI AURI = URI.create("http://www.w3.org/ns/ma-ont#");


	protected static final String SAMPLING_RATE = "samplingRate";


	protected static final String HAS_RATING_SYSTEM = "hasRatingSystem";


	protected static final String HAS_CREATOR = "hasCreator";


	protected static final String LOCATION_ALTITUDE = "locationAltitude";


	protected static final String RECORD_DATE = "recordDate";


	protected static final String HAS_FRAGMENT = "hasFragment";


	protected static final String CREATED_IN = "createdIn";


	protected static final String FEATURES = "features";


	protected static final String NUMBER_OF_TRACKS = "numberOfTracks";


	protected static final String DEPICTS_FICTIONAL_LOCATION = "depictsFictionalLocation";


	protected static final String HAS_TRACK = "hasTrack";


	protected static final String HAS_NAMED_FRAGMENT = "hasNamedFragment";


	protected static final String IS_PROVIDED_BY = "isProvidedBy";


	protected static final String FRAME_RATE = "frameRate";


	protected static final String IS_MEMBER_OF = "isMemberOf";


	protected static final String COPYRIGHT = "copyright";


	protected static final String HAS_POLICY = "hasPolicy";


	protected static final String DESCRIPTION = "description";


	protected static final String HAS_LOCATION_COORDINATE_SYSTEM = "hasLocationCoordinateSystem";


	protected static final String DURATION = "duration";


	protected static final String IS_CREATION_LOCATION_OF = "isCreationLocationOf";


	protected static final String HAS_CONTRIBUTOR = "hasContributor";


	protected static final String HAS_CLASSIFICATION = "hasClassification";


	protected static final String IS_CHAPTER_OF = "isChapterOf";


	protected static final String TITLE = "title";


	protected static final String PROVIDES = "provides";


	protected static final String IS_SOURCE_OF = "isSourceOf";


	protected static final String HAS_MEMBER = "hasMember";


	protected static final String HAS_LANGUAGE = "hasLanguage";


	protected static final String HAS_CAPTIONING = "hasCaptioning";


	protected static final String HAS_CREATED = "hasCreated";


	protected static final String HAS_GENRE = "hasGenre";


	protected static final String IS_TARGET_AUDIENCE_OF = "isTargetAudienceOf";


	protected static final String HAS_FORMAT = "hasFormat";


	protected static final String IS_COPYRIGHTED_BY = "isCopyrightedBy";


	protected static final String HAS_PUBLISHED = "hasPublished";


	protected static final String FRAME_HEIGHT = "frameHeight";


	protected static final String HAS_COMPRESSION = "hasCompression";


	protected static final String HAS_SOURCE = "hasSource";


	protected static final String HAS_COPYRIGHT_OVER = "hasCopyrightOver";


	protected static final String LOCATION_LATITUDE = "locationLatitude";


	protected static final String LOCATION_NAME = "locationName";


	protected static final String RELEASE_DATE = "releaseDate";


	protected static final String FRAGMENT_NAME = "fragmentName";


	protected static final String LOCATOR = "locator";


	protected static final String IS_LOCATION_RELATED_TO = "isLocationRelatedTo";


	protected static final String DATE = "date";


	protected static final String IS_NAMED_FRAGMENT_OF = "isNamedFragmentOf";


	protected static final String RATING_VALUE = "ratingValue";


	protected static final String HAS_SIGNING = "hasSigning";


	protected static final String HAS_PUBLISHER = "hasPublisher";


	protected static final String IS_RELATED_TO = "isRelatedTo";


	protected static final String TRACK_NAME = "trackName";


	protected static final String FRAME_WIDTH = "frameWidth";


	protected static final String HAS_RELATED_IMAGE = "hasRelatedImage";


	protected static final String FRAME_SIZE_UNIT = "frameSizeUnit";


	protected static final String HAS_SUBTITLING = "hasSubtitling";


	protected static final String PLAYS_IN = "playsIn";


	protected static final String EDIT_DATE = "editDate";


	protected static final String _IS_RATING_OF = "IsRatingOf";


	protected static final String IS_RATING_OF = "isRatingOf";


	protected static final String HAS_RATING = "hasRating";


	protected static final String HAS_CLASSIFICATION_SYSTEM = "hasClassificationSystem";


	protected static final String AVERAGE_BIT_RATE = "averageBitRate";


	protected static final String IS_TRACK_OF = "isTrackOf";


	protected static final String HAS_RELATED_LOCATION = "hasRelatedLocation";


	protected static final String IS_SIGNING_OF = "isSigningOf";


	protected static final String HAS_RELATED_RESOURCE = "hasRelatedResource";


	protected static final String IS_IMAGE_RELATED_TO = "isImageRelatedTo";


	protected static final String MAIN_ORIGINAL_TITLE = "mainOriginalTitle";


	protected static final String IS_FICTIONAL_LOCATION_DEPICTED_IN = "isFictionalLocationDepictedIn";


	protected static final String IS_CAPTIONING_OF = "isCaptioningOf";


	protected static final String HAS_CHAPTER = "hasChapter";


	protected static final String CREATION_DATE = "creationDate";


	protected static final String HAS_ACCESS_CONDITIONS = "hasAccessConditions";


	protected static final String HAS_PERMISSIONS = "hasPermissions";


	protected static final String HAS_KEYWORD = "hasKeyword";


	protected static final String HAS_TARGET_AUDIENCE = "hasTargetAudience";


	protected static final String ALTERNATIVE_TITLE = "alternativeTitle";


	protected static final String LOCATION_LONGITUDE = "locationLongitude";


	protected static final String IS_FRAGMENT_OF = "isFragmentOf";


	protected static final String COLLECTION_NAME = "collectionName";


	protected static final String HAS_AUDIO_DESCRIPTION = "hasAudioDescription";


	protected static final String RATING_SCALE_MIN = "ratingScaleMin";


	protected static final String HAS_CONTRIBUTED_TO = "hasContributedTo";


	protected static final String RATING_SCALE_MAX = "ratingScaleMax";


	/**
	 * @param subject
	 *            The default subject of upcomming statements
	 * @param pieceOfKnowledge
	 *            The {@link PieceOfKnowledge} holding the statements to be read or written
	 */
	public MAOntAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * @param resource
	 *            The resource to be annotated
	 */
	public MAOntAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * Read the Corresponds to 'samplingRate' in the Ontology for Media Resources, in samples per second.
	 *
	 * @return a Float
	 */
	public final Value<Float> readSamplingRate() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.SAMPLING_RATE, Float.class, null);
	}


	/**
	 * Write the Corresponds to 'samplingRate' in the Ontology for Media Resources, in samples per second.
	 *
	 * @param value
	 *            a Float
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSamplingRate(final Float value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.SAMPLING_RATE, Float.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'rating.type' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readRatingSystem() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_RATING_SYSTEM, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'rating.type' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRatingSystem(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_RATING_SYSTEM, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'creator.creator' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of
	 * 'creator.role'. Note that this property is semantically a subproperty of 'hasContributor'.
	 *
	 * @return an URI
	 */
	public final Value<URI> readCreator() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CREATOR, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'creator.creator' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of
	 * 'creator.role'. Note that this property is semantically a subproperty of 'hasContributor'.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCreator(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CREATOR, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'location.altitude' in the Ontology for Media Resources.
	 *
	 * @return a Float
	 */
	public final Value<Float> readLocationAltitude() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.LOCATION_ALTITUDE, Float.class, null);
	}


	/**
	 * Write the Corresponds to 'location.altitude' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a Float
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLocationAltitude(final Float value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.LOCATION_ALTITUDE, Float.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "recordDate".
	 *
	 * @return a String
	 */
	public final Value<String> readRecordDate() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.RECORD_DATE, String.class, null);
	}


	/**
	 * Write the Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "recordDate".
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRecordDate(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.RECORD_DATE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'fragment' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'fragment.role'.
	 *
	 * @return an URI
	 */
	public final Value<URI> readFragment() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_FRAGMENT, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'fragment' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'fragment.role'.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFragment(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_FRAGMENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A subproperty of 'hasRelatedLocation" used to specify where material shooting took place.
	 *
	 * @return an URI
	 */
	public final Value<URI> readCreatedIn() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.CREATED_IN, URI.class, null);
	}


	/**
	 * Write the A subproperty of 'hasRelatedLocation" used to specify where material shooting took place.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCreatedIn(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.CREATED_IN, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'contributor.contributor' in the Ontology for Media Resources with a 'contributor.role' meaning "actor".
	 *
	 * @return an URI
	 */
	public final Value<URI> readFeatures() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.FEATURES, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'contributor.contributor' in the Ontology for Media Resources with a 'contributor.role' meaning "actor".
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFeatures(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.FEATURES, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'numTracks.number' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of
	 * 'numTracks.type'.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readNumberOfTracks() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.NUMBER_OF_TRACKS, Integer.class, null);
	}


	/**
	 * Write the Corresponds to 'numTracks.number' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of
	 * 'numTracks.type'.
	 *
	 * @param value
	 *            an Integer
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeNumberOfTracks(final Integer value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.NUMBER_OF_TRACKS, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A subproperty of 'hasRelatedLocation' used to specify where the action depicted in the media is supposed to take place, as opposed to the
	 * location where shooting actually took place (see 'createdIn').
	 *
	 * @return an URI
	 */
	public final Value<URI> readDepictsFictionalLocation() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.DEPICTS_FICTIONAL_LOCATION, URI.class, null);
	}


	/**
	 * Write the A subproperty of 'hasRelatedLocation' used to specify where the action depicted in the media is supposed to take place, as opposed to the
	 * location where shooting actually took place (see 'createdIn').
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDepictsFictionalLocation(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.DEPICTS_FICTIONAL_LOCATION, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "track".
	 *
	 * @return an URI
	 */
	public final Value<URI> readTrack() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_TRACK, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "track".
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTrack(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_TRACK, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'namedFragment' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readNamedFragment() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_NAMED_FRAGMENT, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'namedFragment' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeNamedFragment(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_NAMED_FRAGMENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'rating.identifier' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readProvidedBy() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_PROVIDED_BY, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'rating.identifier' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeProvidedBy(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_PROVIDED_BY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'frameRate' in the Ontology for Media Resources, in frame per second.
	 *
	 * @return a Float
	 */
	public final Value<Float> readFrameRate() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.FRAME_RATE, Float.class, null);
	}


	/**
	 * Write the Corresponds to 'frameRate' in the Ontology for Media Resources, in frame per second.
	 *
	 * @param value
	 *            a Float
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFrameRate(final Float value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.FRAME_RATE, Float.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'collection' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readMemberOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_MEMBER_OF, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'collection' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMemberOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_MEMBER_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'copyright.copyright' in the Ontology for Media Resources.
	 *
	 * @return a String
	 */
	public final Value<String> readCopyright() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.COPYRIGHT, String.class, null);
	}


	/**
	 * Write the Corresponds to 'copyright.copyright' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCopyright(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.COPYRIGHT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'policy' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'policy.type'.
	 *
	 * @return an URI
	 */
	public final Value<URI> readPolicy() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_POLICY, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'policy' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'policy.type'.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePolicy(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_POLICY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'description' in the Ontology for Media Resources. This can be specialised by using sub-properties e.g. 'summary' or 'script'.
	 *
	 * @return a String
	 */
	public final Value<String> readDescription() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.DESCRIPTION, String.class, null);
	}


	/**
	 * Write the Corresponds to 'description' in the Ontology for Media Resources. This can be specialised by using sub-properties e.g. 'summary' or 'script'.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDescription(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.DESCRIPTION, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'location.coordinateSystem' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readLocationCoordinateSystem() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_LOCATION_COORDINATE_SYSTEM, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'location.coordinateSystem' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLocationCoordinateSystem(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_LOCATION_COORDINATE_SYSTEM, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'duration' in the Ontology for Media Resources.
	 *
	 * @return a Float
	 */
	public final Value<Float> readDuration() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.DURATION, Float.class, null);
	}


	/**
	 * Write the Corresponds to 'duration' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a Float
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDuration(final Float value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.DURATION, Float.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readCreationLocationOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_CREATION_LOCATION_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCreationLocationOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_CREATION_LOCATION_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'contributor.contributor' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of
	 * 'contributor.role'.
	 *
	 * @return an URI
	 */
	public final Value<URI> readContributor() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CONTRIBUTOR, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'contributor.contributor' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of
	 * 'contributor.role'.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeContributor(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CONTRIBUTOR, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'targetAudience.classification' in the Ontology for Media Resources. This property is used to provide a value characterising the
	 * target audience.
	 *
	 * @return an URI
	 */
	public final Value<URI> readClassification() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CLASSIFICATION, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'targetAudience.classification' in the Ontology for Media Resources. This property is used to provide a value characterising the
	 * target audience.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeClassification(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CLASSIFICATION, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readChapterOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_CHAPTER_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeChapterOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_CHAPTER_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'title.title' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'title.type'.
	 *
	 * @return a String
	 */
	public final Value<String> readTitle() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.TITLE, String.class, null);
	}


	/**
	 * Write the Corresponds to 'title.title' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'title.type'.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTitle(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.TITLE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readProvides() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.PROVIDES, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeProvides(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.PROVIDES, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readSourceOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_SOURCE_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSourceOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_SOURCE_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readMember() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_MEMBER, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMember(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_MEMBER, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'language' in the Ontology for Media Resources. The language used in the resource. A controlled vocabulary such as defined in BCP
	 * 47 SHOULD be used. This property can also be used to identify the presence of sign language (RFC 5646). By inheritance, the hasLanguage property applies
	 * indifferently at the media resource / fragment / track levels. Best practice recommends to use to best possible level of granularity fo describe the
	 * usage of language within a media resource including at fragment and track levels.
	 *
	 * @return an URI
	 */
	public final Value<URI> readLanguage() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_LANGUAGE, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'language' in the Ontology for Media Resources. The language used in the resource. A controlled vocabulary such as defined in
	 * BCP 47 SHOULD be used. This property can also be used to identify the presence of sign language (RFC 5646). By inheritance, the hasLanguage property
	 * applies indifferently at the media resource / fragment / track levels. Best practice recommends to use to best possible level of granularity fo describe
	 * the usage of language within a media resource including at fragment and track levels.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLanguage(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_LANGUAGE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "captioning". This property can for example point
	 * to a spatial fragment, a VideoTrack or a DataTrack. The language of the captioning track can be expressed by attaching a 'hasLanguage' property to the
	 * specific track.
	 *
	 * @return an URI
	 */
	public final Value<URI> readCaptioning() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CAPTIONING, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "captioning". This property can for example point
	 * to a spatial fragment, a VideoTrack or a DataTrack. The language of the captioning track can be expressed by attaching a 'hasLanguage' property to the
	 * specific track.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCaptioning(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CAPTIONING, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readCreated() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CREATED, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCreated(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CREATED, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'genre' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readGenre() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_GENRE, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'genre' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeGenre(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_GENRE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readTargetAudienceOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_TARGET_AUDIENCE_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTargetAudienceOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_TARGET_AUDIENCE_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'format' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readFormat() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_FORMAT, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'format' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFormat(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_FORMAT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'copyright.identifier' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readCopyrightedBy() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_COPYRIGHTED_BY, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'copyright.identifier' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCopyrightedBy(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_COPYRIGHTED_BY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readPublished() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_PUBLISHED, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePublished(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_PUBLISHED, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'frameSize.height' in the Ontology for Media Resources, measured in frameSizeUnit.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readFrameHeight() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.FRAME_HEIGHT, Integer.class, null);
	}


	/**
	 * Write the Corresponds to 'frameSize.height' in the Ontology for Media Resources, measured in frameSizeUnit.
	 *
	 * @param value
	 *            an Integer
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFrameHeight(final Integer value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.FRAME_HEIGHT, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'compression' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readCompression() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_COMPRESSION, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'compression' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCompression(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_COMPRESSION, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'relation' and in the Ontology for Media Resources with a 'relation.type' meaning "source".
	 *
	 * @return an URI
	 */
	public final Value<URI> readSource() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_SOURCE, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'relation' and in the Ontology for Media Resources with a 'relation.type' meaning "source".
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSource(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_SOURCE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readCopyrightOver() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_COPYRIGHT_OVER, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCopyrightOver(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_COPYRIGHT_OVER, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'location.latitude' in the Ontology for Media Resources.
	 *
	 * @return a Float
	 */
	public final Value<Float> readLocationLatitude() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.LOCATION_LATITUDE, Float.class, null);
	}


	/**
	 * Write the Corresponds to 'location.latitude' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a Float
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLocationLatitude(final Float value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.LOCATION_LATITUDE, Float.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'location.name' in the Ontology for Media Resources.
	 *
	 * @return a String
	 */
	public final Value<String> readLocationName() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.LOCATION_NAME, String.class, null);
	}


	/**
	 * Write the Corresponds to 'location.name' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLocationName(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.LOCATION_NAME, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "releaseDate".
	 *
	 * @return a String
	 */
	public final Value<String> readReleaseDate() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.RELEASE_DATE, String.class, null);
	}


	/**
	 * Write the Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "releaseDate".
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeReleaseDate(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.RELEASE_DATE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'namedFragment.label' in the Ontology for Media Resources.
	 *
	 * @return a String
	 */
	public final Value<String> readFragmentName() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.FRAGMENT_NAME, String.class, null);
	}


	/**
	 * Write the Corresponds to 'namedFragment.label' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFragmentName(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.FRAGMENT_NAME, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'locator' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readLocator() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.LOCATOR, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'locator' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLocator(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.LOCATOR, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readLocationRelatedTo() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_LOCATION_RELATED_TO, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLocationRelatedTo(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_LOCATION_RELATED_TO, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to date.date in the ontology for Media Resources. Subproperties can be used to distinguish different values of 'date.type'. The
	 * recommended range is 'xsd:dateTime' (for compliance with OWL2-QL and OWL2-RL) but other time-related datatypes may be used (e.g. 'xsd:gYear',
	 * 'xsd:date'...).
	 *
	 * @return a String
	 */
	public final Value<String> readDate() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.DATE, String.class, null);
	}


	/**
	 * Write the Corresponds to date.date in the ontology for Media Resources. Subproperties can be used to distinguish different values of 'date.type'. The
	 * recommended range is 'xsd:dateTime' (for compliance with OWL2-QL and OWL2-RL) but other time-related datatypes may be used (e.g. 'xsd:gYear',
	 * 'xsd:date'...).
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDate(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.DATE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readNamedFragmentOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_NAMED_FRAGMENT_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeNamedFragmentOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_NAMED_FRAGMENT_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'rating.value' in the Ontology for Media Resources.
	 *
	 * @return a Float
	 */
	public final Value<Float> readRatingValue() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.RATING_VALUE, Float.class, null);
	}


	/**
	 * Write the Corresponds to 'rating.value' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a Float
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRatingValue(final Float value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.RATING_VALUE, Float.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "signing". This property can for example point to a
	 * spatial fragment or a VideoTrack. The sign language of the captioning track can be expressed by attaching a 'hasLanguage' property to the specific track.
	 *
	 * @return an URI
	 */
	public final Value<URI> readSigning() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_SIGNING, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "signing". This property can for example point to
	 * a spatial fragment or a VideoTrack. The sign language of the captioning track can be expressed by attaching a 'hasLanguage' property to the specific
	 * track.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSigning(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_SIGNING, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'publisher' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readPublisher() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_PUBLISHER, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'publisher' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePublisher(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_PUBLISHER, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readRelatedTo() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_RELATED_TO, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRelatedTo(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_RELATED_TO, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'fragment.name' in the Ontology for Media Resources, for Track fragments.
	 *
	 * @return a String
	 */
	public final Value<String> readTrackName() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.TRACK_NAME, String.class, null);
	}


	/**
	 * Write the Corresponds to 'fragment.name' in the Ontology for Media Resources, for Track fragments.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTrackName(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.TRACK_NAME, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'frameSize.width' in the Ontology for Media Resources measured in frameSizeUnit.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readFrameWidth() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.FRAME_WIDTH, Integer.class, null);
	}


	/**
	 * Write the Corresponds to 'frameSize.width' in the Ontology for Media Resources measured in frameSizeUnit.
	 *
	 * @param value
	 *            an Integer
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFrameWidth(final Integer value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.FRAME_WIDTH, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'relation' and in the Ontology for Media Resources with a 'relation.type' meaning "related image".
	 *
	 * @return an URI
	 */
	public final Value<URI> readRelatedImage() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_RELATED_IMAGE, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'relation' and in the Ontology for Media Resources with a 'relation.type' meaning "related image".
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRelatedImage(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_RELATED_IMAGE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'frameSize.unit' in the Ontology for Media Resources.
	 *
	 * @return a String
	 */
	public final Value<String> readFrameSizeUnit() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.FRAME_SIZE_UNIT, String.class, null);
	}


	/**
	 * Write the Corresponds to 'frameSize.unit' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFrameSizeUnit(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.FRAME_SIZE_UNIT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "subtitling".
	 *
	 * @return an URI
	 */
	public final Value<URI> readSubtitling() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_SUBTITLING, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "subtitling".
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSubtitling(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_SUBTITLING, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readPlaysIn() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.PLAYS_IN, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePlaysIn(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.PLAYS_IN, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "editDate".
	 *
	 * @return a String
	 */
	public final Value<String> readEditDate() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.EDIT_DATE, String.class, null);
	}


	/**
	 * Write the Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "editDate".
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeEditDate(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.EDIT_DATE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readIsRatingOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator._IS_RATING_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeIsRatingOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator._IS_RATING_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readIsRatingOfCorrected() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_RATING_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeIsRatingOfCorrected(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_RATING_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'rating' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readRating() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_RATING, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'rating' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRating(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_RATING, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'targetAudience.identifier' in the Ontology for Media Resources. This is used to identify the reference sheme against which the
	 * target audience has been characterised.
	 *
	 * @return an URI
	 */
	public final Value<URI> readClassificationSystem() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CLASSIFICATION_SYSTEM, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'targetAudience.identifier' in the Ontology for Media Resources. This is used to identify the reference sheme against which the
	 * target audience has been characterised.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeClassificationSystem(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CLASSIFICATION_SYSTEM, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'averageBitRate' in the Ontology for Media Resources, expressed in kilobits/second.
	 *
	 * @return a Float
	 */
	public final Value<Float> readAverageBitRate() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.AVERAGE_BIT_RATE, Float.class, null);
	}


	/**
	 * Write the Corresponds to 'averageBitRate' in the Ontology for Media Resources, expressed in kilobits/second.
	 *
	 * @param value
	 *            a Float
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAverageBitRate(final Float value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.AVERAGE_BIT_RATE, Float.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readTrackOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_TRACK_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTrackOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_TRACK_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'location' in the Ontology for Media Resources. Subproperties are provided to specify, when possible, the relation between the
	 * media resource and the location.
	 *
	 * @return an URI
	 */
	public final Value<URI> readRelatedLocation() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_RELATED_LOCATION, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'location' in the Ontology for Media Resources. Subproperties are provided to specify, when possible, the relation between the
	 * media resource and the location.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRelatedLocation(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_RELATED_LOCATION, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readSigningOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_SIGNING_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSigningOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_SIGNING_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'relation' and in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'relation.type'.
	 *
	 * @return an URI
	 */
	public final Value<URI> readRelatedResource() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_RELATED_RESOURCE, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'relation' and in the Ontology for Media Resources. Subproperties can be used to distinguish different values of
	 * 'relation.type'.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRelatedResource(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_RELATED_RESOURCE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readImageRelatedTo() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_IMAGE_RELATED_TO, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeImageRelatedTo(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_IMAGE_RELATED_TO, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'title.title' in the Ontology for Media Resources with a 'title.type' meaning "original".
	 *
	 * @return a String
	 */
	public final Value<String> readMainOriginalTitle() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.MAIN_ORIGINAL_TITLE, String.class, null);
	}


	/**
	 * Write the Corresponds to 'title.title' in the Ontology for Media Resources with a 'title.type' meaning "original".
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMainOriginalTitle(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.MAIN_ORIGINAL_TITLE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readFictionalLocationDepictedIn() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_FICTIONAL_LOCATION_DEPICTED_IN, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFictionalLocationDepictedIn(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_FICTIONAL_LOCATION_DEPICTED_IN, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readCaptioningOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_CAPTIONING_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCaptioningOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_CAPTIONING_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "actor".
	 *
	 * @return an URI
	 */
	public final Value<URI> readChapter() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CHAPTER, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "actor".
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeChapter(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CHAPTER, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "creationDate".
	 *
	 * @return a String
	 */
	public final Value<String> readCreationDate() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.CREATION_DATE, String.class, null);
	}


	/**
	 * Write the Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "creationDate".
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCreationDate(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.CREATION_DATE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'policy' in the Ontology for Media Resources with a 'policy.type' "access conditions".
	 *
	 * @return an URI
	 */
	public final Value<URI> readAccessConditions() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_ACCESS_CONDITIONS, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'policy' in the Ontology for Media Resources with a 'policy.type' "access conditions".
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAccessConditions(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_ACCESS_CONDITIONS, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'policy' in the Ontology for Media Resources with a 'policy.type' meaning "permissions".
	 *
	 * @return an URI
	 */
	public final Value<URI> readPermissions() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_PERMISSIONS, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'policy' in the Ontology for Media Resources with a 'policy.type' meaning "permissions".
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePermissions(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_PERMISSIONS, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'keyword' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readKeyword() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_KEYWORD, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'keyword' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeKeyword(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_KEYWORD, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'targetAudience' in the Ontology for Media Resources.
	 *
	 * @return an URI
	 */
	public final Value<URI> readTargetAudience() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_TARGET_AUDIENCE, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'targetAudience' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTargetAudience(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_TARGET_AUDIENCE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'title.title' in the Ontology for Media Resources with a 'title.type' meaning "alternative".
	 *
	 * @return a String
	 */
	public final Value<String> readAlternativeTitle() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.ALTERNATIVE_TITLE, String.class, null);
	}


	/**
	 * Write the Corresponds to 'title.title' in the Ontology for Media Resources with a 'title.type' meaning "alternative".
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAlternativeTitle(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.ALTERNATIVE_TITLE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'location.longitude' in the Ontology for Media Resources.
	 *
	 * @return a Float
	 */
	public final Value<Float> readLocationLongitude() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.LOCATION_LONGITUDE, Float.class, null);
	}


	/**
	 * Write the Corresponds to 'location.longitude' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a Float
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLocationLongitude(final Float value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.LOCATION_LONGITUDE, Float.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readFragmentOf() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.IS_FRAGMENT_OF, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFragmentOf(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.IS_FRAGMENT_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The name by which a collection (e.g. series) is known.
	 *
	 * @return a String
	 */
	public final Value<String> readCollectionName() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.COLLECTION_NAME, String.class, null);
	}


	/**
	 * Write the The name by which a collection (e.g. series) is known.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCollectionName(final String value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.COLLECTION_NAME, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "audio-description".
	 *
	 * @return an URI
	 */
	public final Value<URI> readAudioDescription() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_AUDIO_DESCRIPTION, URI.class, null);
	}


	/**
	 * Write the Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "audio-description".
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAudioDescription(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_AUDIO_DESCRIPTION, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'rating.min' in the Ontology for Media Resources.
	 *
	 * @return a Float
	 */
	public final Value<Float> readRatingScaleMin() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.RATING_SCALE_MIN, Float.class, null);
	}


	/**
	 * Write the Corresponds to 'rating.min' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a Float
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRatingScaleMin(final Float value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.RATING_SCALE_MIN, Float.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the null
	 *
	 * @return an URI
	 */
	public final Value<URI> readContributedTo() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CONTRIBUTED_TO, URI.class, null);
	}


	/**
	 * Write the null
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeContributedTo(final URI value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.HAS_CONTRIBUTED_TO, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Corresponds to 'rating.max' in the Ontology for Media Resources.
	 *
	 * @return a Float
	 */
	public final Value<Float> readRatingScaleMax() {
		return this.applyOperator(Operator.READ, null, MAOntAnnotator.AURI, MAOntAnnotator.RATING_SCALE_MAX, Float.class, null);
	}


	/**
	 * Write the Corresponds to 'rating.max' in the Ontology for Media Resources.
	 *
	 * @param value
	 *            a Float
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRatingScaleMax(final Float value) {
		return this.applyOperator(Operator.WRITE, MAOntAnnotator.APREFIX, MAOntAnnotator.AURI, MAOntAnnotator.RATING_SCALE_MAX, Float.class, value).getIsAnnotatedOn();
	}

}
