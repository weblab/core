/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.geonames;

import java.net.URI;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.ontologies.Geonames;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * Geonames Ontology annotator.
 *
 * @author ymombrun
 */
public class GeonamesAnnotator extends BaseAnnotator {


	private static final String PREFIX = Geonames.PREFERRED_PREFIX;


	private static final URI AURI = URI.create(Geonames.NAMESPACE);


	private static final String ALTERNATE_NAME = "alternateName";


	private static final String COLLOQUIAL_NAME = "colloquialName";


	private static final String GEONAMES_ID = "geonamesID";


	private static final String COUNTRY_CODE = "countryCode";


	private static final String HISTORICAL_NAME = "historicalName";


	@Deprecated
	private static final String HAS_SPATIAL_THING = "hasSpatialThing";


	private static final String NAME = "name";


	private static final String OFFICIAL_NAME = "officialName";


	private static final String POPULATION = "population";


	private static final String POSTAL_CODE = "postalCode";


	private static final String SHORT_NAME = "shortName";


	private static final String LOCATED_IN = "locatedIn";


	private static final String NEARBY = "nearby";


	private static final String NEIGHBOUR = "neighbour";


	private static final String PARENT_ADM_1 = "parentADM1";


	private static final String PARENT_ADM_2 = "parentADM2";


	private static final String PARENT_ADM_3 = "parentADM3";


	private static final String PARENT_ADM_4 = "parentADM4";


	private static final String PARENT_COUNTRY = "parentCountry";


	private static final String PARENT_FEATURE = "parentFeature";


	private static final String WIKIPEDIA_ARTICLE = "wikipediaArticle";


	/**
	 * @param subject
	 *            The default subject of the statements to be read/written
	 * @param pieceOfKnowledge
	 *            The pok holding the rdf
	 */
	public GeonamesAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * @param resource
	 *            The resource whose annotations should be read/written
	 */
	public GeonamesAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * Read the linked spatial thing (most of the time an instance of geonames:Feature)
	 *
	 * @deprecated HasSpatialThing does not exists in Geonames ontology. We'd better use something else, probably locatedId.
	 *
	 * @return a Value containing URIs of the linked features
	 */
	@Deprecated
	public Value<URI> readSpatialThing() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.HAS_SPATIAL_THING, URI.class, null);
	}


	/**
	 * Write the linked spatial thing (most of the time an instance of geonames:Feature)
	 *
	 * @deprecated HasSpatialThing does not exists in Geonames ontology. We'd better use something else, probably locatedId.
	 * @param value
	 *            an URI
	 */
	@Deprecated
	public void writeSpatialThing(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.HAS_SPATIAL_THING, URI.class, value);
	}


	// Read and write names

	/**
	 * Read the Gives an alternate name for the feature.
	 *
	 * @return a String
	 */
	public Value<String> readAlternateName() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.ALTERNATE_NAME, String.class, null);
	}


	/**
	 * Write the Gives an alternate name for the feature.
	 *
	 * @param value
	 *            a String
	 */
	public void writeAlternateName(final String value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.ALTERNATE_NAME, String.class, value);
	}


	/**
	 * Read the Gives a colloquial name for the feature.
	 *
	 * @return a String
	 */
	public Value<String> readColloquialName() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.COLLOQUIAL_NAME, String.class, null);
	}


	/**
	 * Write the Gives a colloquial name for the feature.
	 *
	 * @param value
	 *            a String
	 */
	public void writeColloquialName(final String value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.COLLOQUIAL_NAME, String.class, value);
	}


	/**
	 * Read the Gives an historical name for the feature.
	 *
	 * @return a String
	 */
	public Value<String> readHistoricalName() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.HISTORICAL_NAME, String.class, null);
	}


	/**
	 * Write the Gives an historical name for the feature.
	 *
	 * @param value
	 *            a String
	 */
	public void writeHistoricalName(final String value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.HISTORICAL_NAME, String.class, value);
	}


	/**
	 * Read the Gives a name for the feature.
	 *
	 * @return a String
	 */
	public Value<String> readName() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.NAME, String.class, null);
	}


	/**
	 * Write the Gives a name for the feature.
	 *
	 * @param value
	 *            a String
	 */
	public void writeName(final String value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.NAME, String.class, value);
	}


	/**
	 * Read the Gives an official name for the feature.
	 *
	 * @return a String
	 */
	public Value<String> readOfficialName() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.OFFICIAL_NAME, String.class, null);
	}


	/**
	 * Write the Gives an official name for the feature.
	 *
	 * @param value
	 *            a String
	 */
	public void writeOfficialName(final String value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.OFFICIAL_NAME, String.class, value);
	}


	/**
	 * Read the Gives a short name for the feature.
	 *
	 * @return a String
	 */
	public Value<String> readShortName() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.SHORT_NAME, String.class, null);
	}


	/**
	 * Write the Gives a short name for the feature.
	 *
	 * @param value
	 *            a String
	 */
	public void writeShortName(final String value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.SHORT_NAME, String.class, value);
	}


	/**
	 * Read the Gives a Geonames ID for the feature.
	 *
	 * @return a String
	 */
	public Value<String> readGeonamesID() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.GEONAMES_ID, String.class, null);
	}


	/**
	 * Write the Gives a Geonames ID for the feature.
	 *
	 * @param value
	 *            a String
	 */
	public void writeGeonamesID(final String value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.GEONAMES_ID, String.class, value);
	}


	/**
	 * Read the Gives a Country Code for the feature.
	 *
	 * @return a String
	 */
	public Value<String> readCountryCode() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.COUNTRY_CODE, String.class, null);
	}


	/**
	 * Write the Gives Country Code for the feature.
	 *
	 * @param value
	 *            a String
	 */
	public void writeCountryCode(final String value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.COUNTRY_CODE, String.class, value);
	}


	/**
	 * Read the Gives a postal Code for the feature.
	 *
	 * @return a String
	 */
	public Value<String> readPostalCode() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.POSTAL_CODE, String.class, null);
	}


	/**
	 * Write the Gives Postal Code for the feature.
	 *
	 * @param value
	 *            a String
	 */
	public void writePostalCode(final String value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.POSTAL_CODE, String.class, value);
	}



	/**
	 * Read the Feature URI associated to a Resource.
	 *
	 * @return the URI of the Feature containing the subject resource
	 */
	public Value<URI> readLocatedIn() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.LOCATED_IN, URI.class, null);
	}


	/**
	 * Write the eature URI associated to a Resource.
	 *
	 * @param value
	 *            the URI of the Feature containing the subject resource
	 */
	public void writeLocatedIn(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.LOCATED_IN, URI.class, value);
	}


	/**
	 * Read the Feature URI associated to another Feature.
	 *
	 * @return the URI of the Feature close to the subject Feature
	 */
	public Value<URI> readNearby() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.NEARBY, URI.class, null);
	}


	/**
	 * Write the Feature URI associated to a Feature.
	 *
	 * @param value
	 *            the URI of the Feature close to the subject Feature
	 */
	public void writeNearby(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.NEARBY, URI.class, value);
	}


	/**
	 * Read the Feature URI associated to another Feature.
	 *
	 * @return the URI of the Feature that shares a boarder with the subject Feature
	 */
	public Value<URI> readNeighbour() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.NEIGHBOUR, URI.class, null);
	}


	/**
	 * Write the Feature URI associated to a Feature.
	 *
	 * @param value
	 *            the URI of the Feature that shares a boarder with the subject Feature
	 */
	public void writeNeighbour(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.NEIGHBOUR, URI.class, value);
	}


	/**
	 * Read the feature that includes this one
	 *
	 * @return a Value containing URIs of the parent features
	 */
	public Value<URI> readParentFeature() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_FEATURE, URI.class, null);
	}


	/**
	 * Write the feature that includes this one
	 *
	 * @param value
	 *            an URI
	 */
	public void writeParentFeature(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_FEATURE, URI.class, value);
	}



	/**
	 * Read the administrative level 1 feature that includes this one
	 *
	 * @return a Value containing URIs of the parent features
	 */
	public Value<URI> readParentADM1() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_ADM_1, URI.class, null);
	}


	/**
	 * Write the administrative level 1 feature that includes this one
	 *
	 * @param value
	 *            an URI
	 */
	public void writeParentADM1(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_ADM_1, URI.class, value);
	}



	/**
	 * Read the administrative level 2 feature that includes this one
	 *
	 * @return a Value containing URIs of the parent features
	 */
	public Value<URI> readParentADM2() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_ADM_2, URI.class, null);
	}


	/**
	 * Write the administrative level 2 feature that includes this one
	 *
	 * @param value
	 *            an URI
	 */
	public void writeParentADM2(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_ADM_2, URI.class, value);
	}



	/**
	 * Read the administrative level 3 feature that includes this one
	 *
	 * @return a Value containing URIs of the parent features
	 */
	public Value<URI> readParentADM3() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_ADM_3, URI.class, null);
	}


	/**
	 * Write the administrative level 3 feature that includes this one
	 *
	 * @param value
	 *            an URI
	 */
	public void writeParentADM3(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_ADM_3, URI.class, value);
	}



	/**
	 * Read the administrative level 4 feature that includes this one
	 *
	 * @return a Value containing URIs of the parent features
	 */
	public Value<URI> readParentADM4() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_ADM_4, URI.class, null);
	}


	/**
	 * Write the administrative level 4 feature that includes this one
	 *
	 * @param value
	 *            an URI
	 */
	public void writeParentADM4(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_ADM_4, URI.class, value);
	}


	/**
	 * Read the country feature that includes this one
	 *
	 * @return a Value containing URIs of the parent features
	 */
	public Value<URI> readCountry() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_COUNTRY, URI.class, null);
	}


	/**
	 * Write the country feature that includes this one
	 *
	 * @param value
	 *            an URI
	 */
	public void writeParentCountry(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.PARENT_COUNTRY, URI.class, value);
	}


	/**
	 * Read the Wikipedia article URI associated to the feature
	 *
	 * @return a Value containing URIs of the associated wikipedia article
	 */
	public Value<URI> readWikipediaArticle() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.WIKIPEDIA_ARTICLE, URI.class, null);
	}


	/**
	 * Write the country feature that includes this one
	 *
	 * @param value
	 *            an URI of the associated wikipedia article
	 */
	public void writeWikipediaArticle(final URI value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.WIKIPEDIA_ARTICLE, URI.class, value);
	}


	/**
	 * Read the the population living in the feature.
	 *
	 * @return an Integer
	 */
	public Value<Integer> readPopulation() {
		return this.applyOperator(Operator.READ, null, GeonamesAnnotator.AURI, GeonamesAnnotator.POPULATION, Integer.class, null);
	}


	/**
	 * Write the the population living in the feature.
	 *
	 * @param value
	 *            an Integer
	 */
	public void writePopulation(final Integer value) {
		this.applyOperator(Operator.WRITE, GeonamesAnnotator.PREFIX, GeonamesAnnotator.AURI, GeonamesAnnotator.POPULATION, Integer.class, value);
	}


}
