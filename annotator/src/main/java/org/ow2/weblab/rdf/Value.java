/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.rdf;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.ow2.weblab.core.model.PieceOfKnowledge;

/**
 * This class simulate several behaviors:
 * <ul>
 * <li>a typed object</li>
 * <li>a list of typed object</li>
 * <li>support a typed object access by language</li>
 * </ul>
 *
 * @param <T>
 *            the type of the object value.
 */
public class Value<T> implements Iterable<T> {


	private final LinkedList<T> list = new LinkedList<>();


	private final Map<String, List<T>> byLang = new HashMap<>();


	private PieceOfKnowledge isAnnotatedOn = null;


	/**
	 * This constructor is made to return empty values;
	 */
	public Value() {

	}


	/**
	 * @param item
	 *            The actual value Object
	 * @param pok
	 *            The pok containing the value
	 */
	public Value(final T item, final PieceOfKnowledge pok) {
		this(item);
		this.isAnnotatedOn = pok;
	}


	/**
	 * If you have rational to grant a public access to the constructor please, explain it at forum.weblab-project.org
	 * 
	 * @param item
	 *            The first value to be added in the value
	 */
	protected Value(final T item) {
		this.addItem(item, null);
	}


	/**
	 * If you have rational to give a grant access to this method please, explain it at forum.weblab-project.org
	 * 
	 * @param item
	 *            The item to be added
	 * @param lang
	 *            The lang or null
	 */
	protected void addItem(final T item, final String lang) {
		this.list.add(item);
		if (lang != null) {
			// add support to several literal with the same language
			List<T> listItems = this.byLang.get(lang);
			if (listItems == null) {
				listItems = new LinkedList<>();
			}
			listItems.add(item);
			this.byLang.put(lang, listItems);
		}
	}


	/**
	 * Returns a list of typed values.
	 * One can note that the association of typed values with language attributes will be lost for the returned list.
	 *
	 * @return a list of typed values
	 */
	public LinkedList<T> getValues() {
		return (LinkedList<T>) this.list.clone();
	}


	/**
	 * Returns a list of typed values for the given language, or null if there is no value with such language
	 *
	 * @param language
	 *            the language code present in the rdf statement
	 *
	 * @return a list of typed values for the given language, can be null
	 */
	public List<T> getValues(final String language) {
		return this.byLang.get(language);
	}


	/**
	 * Returns true if this object contains at least one typed value
	 *
	 * @return true if this object contains at least one typed value else false
	 */
	public boolean hasValue() {
		return !this.list.isEmpty();
	}


	/**
	 * Returns the first typed value.
	 *
	 * @return the first typed value or null if the value could not be parsed to the requested type.
	 */
	public T firstTypedValue() {
		return this.firstTypedValue(null);
	}


	/**
	 * Returns the first typed value that is defined this the language attribute.
	 *
	 * @param lang
	 *            The language attribute value
	 *
	 * @return the first typed value that is defined this the language attribute or null if the value could not be parsed to the requested type.
	 */
	public T firstTypedValue(final String lang) {
		if (this.list.isEmpty()) {
			return null;
		}
		if (lang == null) {
			return this.list.get(0);
		}

		List<T> listItems = this.byLang.get(lang);
		if (listItems != null && listItems.size() > 0) {
			return listItems.get(0);
		}
		return null;
	}


	/**
	 * Returns the number of typed values available
	 *
	 * @return the number of typed values available
	 */
	public int size() {
		return this.list.size();
	}


	/**
	 * Returns the toString() method result on the first typed value or null if there is not value available.
	 *
	 * @return the toString() method result on the first typed value or null if there is not value available.
	 */
	@Override
	public String toString() {
		final T value = this.firstTypedValue();
		if (value == null) {
			return "";
		}
		return value.toString();
	}


	@Override
	public Iterator<T> iterator() {
		return this.list.iterator();
	}


	/**
	 * Aggregate two value
	 *
	 * @param otherValue
	 *            The second value to aggregate with <code>this</code>
	 * @return this after aggregation with <code>otherValue</code>
	 */
	public Value<T> aggregate(final Value<T> otherValue) {
		for (Entry<String, List<T>> entry : otherValue.byLang.entrySet()) {
			List<T> list_l = this.byLang.get(entry.getKey());
			if (list_l != null && entry.getValue() != null) {
				list_l.addAll(entry.getValue());
			} else if (list_l == null) {
				list_l = entry.getValue();
			}
			this.byLang.put(entry.getKey(), list_l);
		}
		this.list.addAll(otherValue.list);
		return this;
	}


	/**
	 * Return the annotation on which this value is defined
	 *
	 * @return the annotation on which this value is defined or null is the Value is not linked to an Annotation
	 */
	public PieceOfKnowledge getIsAnnotatedOn() {
		return this.isAnnotatedOn;
	}
}
