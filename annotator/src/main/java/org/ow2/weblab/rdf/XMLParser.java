/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.rdf;

import java.lang.instrument.IllegalClassFormatException;
import java.lang.reflect.Modifier;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.ow2.weblab.rdf.AbstractTypeParser.DateTypeParser;
import org.slf4j.LoggerFactory;

/**
 * XMLParser class managed type parser to transform String value into an other Type of Object.
 *
 * By default the following final supported types are:
 * <ul>
 * <li>Boolean</li>
 * <li>Byte</li>
 * <li>Integer</li>
 * <li>Double</li>
 * <li>Float</li>
 * <li>Long</li>
 * <li>Character</li>
 * <li>String</li>
 * </ul>
 *
 * Following non final supported types are:
 * <ul>
 * <li>Date</li>
 * </ul>
 *
 * This parser supports auto discovery of RDF datatypes with RDFDatatypeContainer.
 * If a RDF datatype is not explicitly supported, it will use the String datatype to return a result.
 *
 * @author asaval
 * @see RDFDatatypeContainer
 */
public final class XMLParser {


	/**
	 * Parsers for final classes
	 */
	private static Map<Class<?>, AbstractTypeParser<?>> finalParsers = XMLParser.resetParsers();


	/**
	 * Parsers for non final classes
	 */
	private static Map<Class<?>, AbstractTypeParser<?>> genericParsers;


	private XMLParser() {
		throw new UnsupportedOperationException("This class should not been instanciated.");
	}


	/**
	 * Parse the value of a property info an instance of T.
	 *
	 * @param <T>
	 *            the type the value should be returned
	 * @param value
	 *            the value that must be converted into type
	 * @param clazz
	 *            the class of the type
	 * @return the parsed value
	 */
	protected static <T> T xmlParse(final String value, final Class<T> clazz) {
		return XMLParser.xmlParse(value, clazz, null);
	}


	/**
	 * Parse the value of a property info an instance of T.
	 *
	 * @param <T>
	 *            the type the value should be returned
	 * @param value
	 *            the value that must be converted into type
	 * @param clazz
	 *            the class of the type
	 * @param rdfDatatype
	 *            the rdfDatatype parameter for the value
	 * @return the parsed value
	 */
	protected static <T> T xmlParse(final String value, final Class<T> clazz, final String rdfDatatype) {
		T result = null;

		// if class type is RDFDatatypeContainer,
		// we try to automatically resolve the Class from the RDF Datatype
		if (clazz.equals(RDFDatatypeContainer.class)) {
			Class<?> rdfDatatypeClazz = null;
			if (rdfDatatype != null) {
				rdfDatatypeClazz = XMLParser.findClassForRDFDatatype(rdfDatatype);
			}
			if (rdfDatatypeClazz == null) {
				// by default, if there is no datatype or the datatype is not supported, we use a String
				rdfDatatypeClazz = String.class;
			}

			final AbstractTypeParser<?> parser = XMLParser.getParser(rdfDatatypeClazz);
			result = (T) new RDFDatatypeContainer(parser.parse(value), rdfDatatype);
		} else {
			// Since the only way to add a TypeParser is adding it with its corresponding Class, nothing can possibly go wrong...
			final AbstractTypeParser<T> parser = XMLParser.getParser(clazz);
			if (parser == null) {
				throw new IllegalArgumentException("Type " + clazz.getName() + " is not supported by XMLParser. You can add your own parser for " + clazz.getName()
						+ " using XMLParser.addTypeSupport(Class<T> clazz, TypeParser<T> parser) method.");
			}
			try {
				result = parser.parse(value);
			} catch (final Exception e) {
				result = parser.defaultValueOnException(e);
				LoggerFactory.getLogger(XMLParser.class).error("Parser for type " + clazz.getName() + " failed to parse value '" + value + "' return default value : " + result, e);
			}
		}

		return result;
	}


	/**
	 * Add a TypeParser to support or overwrite a Type a Object.
	 *
	 * @param <T>
	 *            the type the parser know how to parse from String
	 * @param clazz
	 *            the type the parser know how to parse from String
	 * @param parser
	 *            the parser that transform a String into an Object instance of class clazz.
	 * @throws IllegalClassFormatException
	 *             if the given Class is of primitive type.
	 */
	public static <T> void addTypeSupport(final Class<T> clazz, final AbstractTypeParser<T> parser) throws IllegalClassFormatException {
		if (clazz.isPrimitive()) {
			throw new IllegalClassFormatException("Primitive type are not supported by the Annotator parser, please use the equivalent Object.");
		}
		if (Modifier.isFinal(clazz.getModifiers())) {
			XMLParser.finalParsers.put(clazz, parser);
		} else {
			XMLParser.genericParsers.put(clazz, parser);
		}
	}


	/**
	 * By default the following final supported types are:
	 * <ul>
	 * <li>Boolean</li>
	 * <li>Byte</li>
	 * <li>Integer</li>
	 * <li>Double</li>
	 * <li>Float</li>
	 * <li>Long</li>
	 * <li>Character</li>
	 * <li>String</li>
	 * </ul>
	 *
	 * Following non final supported types are:
	 * <ul>
	 * <li>Date</li>
	 * </ul>
	 *
	 * @return the map of default parsers.
	 */
	public static Map<Class<?>, AbstractTypeParser<?>> resetParsers() {
		XMLParser.finalParsers = new HashMap<>();
		XMLParser.finalParsers.put(Double.class, new AbstractTypeParser<Double>() {


			@Override
			public Double parse(final String value) {
				return Double.valueOf(value);
			}


			@Override
			public String getDatatypeURI() {
				return "http://www.w3.org/2001/XMLSchema#double";
			}
		});
		XMLParser.finalParsers.put(Integer.class, new AbstractTypeParser<Integer>() {


			@Override
			public Integer parse(final String value) {
				return Integer.valueOf(value);
			}


			@Override
			public String getDatatypeURI() {
				return "http://www.w3.org/2001/XMLSchema#int";
			}
		});
		XMLParser.finalParsers.put(Float.class, new AbstractTypeParser<Float>() {


			@Override
			public Float parse(final String value) {
				return Float.valueOf(value);
			}


			@Override
			public String getDatatypeURI() {
				return "http://www.w3.org/2001/XMLSchema#float";
			}
		});
		XMLParser.finalParsers.put(Long.class, new AbstractTypeParser<Long>() {


			@Override
			public Long parse(final String value) {
				return Long.valueOf(value);
			}


			@Override
			public String getDatatypeURI() {
				return "http://www.w3.org/2001/XMLSchema#long";
			}
		});
		XMLParser.finalParsers.put(Character.class, new AbstractTypeParser<Character>() {


			@Override
			public Character parse(final String value) {
				return Character.valueOf(value.charAt(0));
			}
		});
		XMLParser.finalParsers.put(Boolean.class, new AbstractTypeParser<Boolean>() {


			@Override
			public Boolean parse(final String value) {
				return Boolean.valueOf(value);
			}


			@Override
			public String getDatatypeURI() {
				return "http://www.w3.org/2001/XMLSchema#boolean";
			}

		});
		XMLParser.finalParsers.put(Byte.class, new AbstractTypeParser<Byte>() {


			@Override
			public Byte parse(final String value) {
				return Byte.valueOf(value);
			}


			@Override
			public String getDatatypeURI() {
				return "http://www.w3.org/2001/XMLSchema#byte";
			}
		});
		XMLParser.finalParsers.put(URI.class, new AbstractTypeParser<URI>() {


			@Override
			public URI parse(final String value) {
				return URI.create(value);
			}
		});
		XMLParser.finalParsers.put(String.class, new AbstractTypeParser<String>() {


			@Override
			public String parse(final String value) {
				return value;
			}
		});

		XMLParser.genericParsers = new HashMap<>();
		XMLParser.genericParsers.put(Date.class, new DateTypeParser<Date>());

		return XMLParser.finalParsers;
	}


	/**
	 * Return the parser corresponding to the given class
	 *
	 * @param clazz
	 *            a class to search for the corresponding parser
	 * @return the corresponding parser or null if not found
	 */
	@SuppressWarnings("unchecked")
	private static <T> AbstractTypeParser<T> getParser(final Class<T> clazz) {

		AbstractTypeParser<T> parser = null;

		// first check if class parser is available in final parsers; faster since there is no subclasses to check
		parser = (AbstractTypeParser<T>) XMLParser.finalParsers.get(clazz);

		// if not final, search for not final class parser
		if (parser == null) {
			// try to get parser from class directly
			parser = (AbstractTypeParser<T>) XMLParser.genericParsers.get(clazz);
			if (parser == null) {
				// not found, so we search if this class is an extension of one of the known classes
				// we return the first we find
				for (final Entry<Class<?>, AbstractTypeParser<?>> entry : XMLParser.genericParsers.entrySet()) {
					final Class<?> classToCheck = entry.getKey();
					if (classToCheck.isAssignableFrom(clazz)) {
						// clazz is a subclass of classToCheck, so we return its parser
						return (AbstractTypeParser<T>) entry.getValue();
					}
				}
			}
		}

		return parser;
	}


	/**
	 * Return the parser for the given datatype
	 *
	 * @param <T>
	 *            The type of the value
	 * @param value
	 *            a value
	 * @return the parser for the given datatype
	 */
	public static <T> AbstractTypeParser<T> getParser(final T value) {
		return (AbstractTypeParser<T>) XMLParser.getParser(value.getClass());
	}


	/**
	 * Finds the Java class corresponding to the RDF datatype
	 *
	 * @param rdfDatatype
	 *            the RDF datatype
	 * @return the class corresponding to the RDF datatype or null if this datatype is not supported
	 */
	public static Class<?> findClassForRDFDatatype(final String rdfDatatype) {
		for (final Entry<Class<?>, AbstractTypeParser<?>> entry : XMLParser.finalParsers.entrySet()) {
			if ((entry.getValue() != null) && rdfDatatype.equals(entry.getValue().getDatatypeURI())) {
				return entry.getKey();
			}
		}
		for (final Entry<Class<?>, AbstractTypeParser<?>> entry : XMLParser.genericParsers.entrySet()) {
			if ((entry.getValue() != null) && rdfDatatype.equals(entry.getValue().getDatatypeURI())) {
				return entry.getKey();
			}
		}
		return null;
	}

}
