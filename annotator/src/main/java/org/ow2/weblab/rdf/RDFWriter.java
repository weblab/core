/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.rdf;

import java.net.URI;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.dom.DOMResult;

import org.ow2.weblab.core.annotator.Element;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * This call is a backend class that do all the work on xml writing
 *
 * @author asaval
 *
 */
public final class RDFWriter {


	static final String XML_PARSER_CLASS = "com.sun.xml.internal.stream.XMLOutputFactoryImpl";


	private static final String XML_URI = "http://www.w3.org/XML/1998/namespace";


	private static final String RDF = "RDF";


	private static final String ABOUT = "about";


	private static XMLOutputFactory outputFactory = null;


	private RDFWriter() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Writes or deletes a node in XML.<br>
	 * This method has two behaviors depending on <code>add</code> value:
	 * <ul>
	 * <li>true: append mode =&gt; add a new node.</li>
	 * <li>false: overwrite mode =&gt; replace the value of existing node or remove it if given value is null</li>
	 * </ul>
	 *
	 * @param annotation
	 *            the annotation, if null and delete mode enabled throws exception
	 * @param subject
	 *            The subject of the statement to write
	 * @param prefix
	 *            the namespace prefix of the property
	 * @param namespace
	 *            namespace of the property
	 * @param property
	 *            localname of the property
	 * @param value
	 *            value of the property, for all classes but Date, it use the toString() method.
	 * @param lang
	 *            if not null, support language through xml:lang attribute
	 * @param add
	 *            if true, set 'append mode', else 'overwrite mode'
	 * @param element
	 *            the kind of node to write.
	 * @throws XMLStreamException
	 *             If the value is null in add mode, or if no statement can be removed
	 * @throws ParserConfigurationException
	 *             if a DocumentBuilder cannot be created which satisfies the configuration requested.
	 */
	public static void writeStatement(final PieceOfKnowledge annotation, final URI subject, final String prefix, final URI namespace, final String property, final Object value, final String lang,
			final boolean add, final Element element) throws XMLStreamException, ParserConfigurationException {

		if ((value == null) && add) {
			throw new XMLStreamException("You can not delete a statement using a null value when the annotator is in append mode.");
		}

		if (RDFWriter.isEmptyAnnotation(annotation)) {
			if (value == null) {
				throw new XMLStreamException("You tried to delete a statement (" + subject + ", " + namespace + property + ", null ) that does not exist.");
			}
			RDFWriter.createAnnotation(annotation, subject, prefix, namespace.toString(), property, value, lang, element);
		} else {
			RDFWriter.updateAnnotation(annotation, subject, prefix, namespace.toString(), property, value, lang, add, element);
		}
	}


	/**
	 * @param pok
	 *            The pok to check whether or not the data field contains rdf descriptions.
	 * @return <code>true</code> only if there is a data node with children.
	 */
	private static boolean isEmptyAnnotation(final PieceOfKnowledge pok) {
		final Object data = pok.getData();
		return (data == null) || (data.getClass().equals(Object.class)) || ((data instanceof Node) && !((Node) data).hasChildNodes());
	}


	private static void updateAnnotation(final PieceOfKnowledge annotation, final URI subject, final String prefix, final String namespace, final String property, final Object value,
			final String lang, final boolean add, final Element element) throws XMLStreamException {
		final Node data = (Node) annotation.getData();

		// We search for an rdf description about the subject
		Node description = RDFReader.findElementAbout(data, subject.toString(), element);

		if (description == null) {
			// we create a new description node
			description = RDFWriter.createElementAbout(RDFReader.findRDF(data), subject.toString(), element);
			if (description == null) {
				throw new XMLStreamException("Can not create rdf:description about: " + subject);
			}
		} else if (!add) {
			final List<Node> nodes = RDFReader.findNodesWithProperty(description, namespace, property);

			for (final Node node : nodes) {
				if (lang == null) {
					node.getParentNode().removeChild(node);
				} else {
					final String nlang = RDFReader.getLanguage(node);
					if (lang.equalsIgnoreCase(nlang)) {
						node.getParentNode().removeChild(node);
					}
				}
			}

			// if the value is null
			// we delete the statement
			if (value == null) {
				return;
			}
		}

		final DOMResult domResult = new DOMResult(description);

		final XMLStreamWriter xmlw = RDFWriter.createXMLStreamWriter(domResult);
		RDFWriter.writeAnnotation(xmlw, prefix, namespace, property, value, lang);
		xmlw.close();
		annotation.setData(data);

	}


	/**
	 * Remove all or part of a statement
	 * TODO: add check for null values
	 *
	 * @param annotation
	 *            The annotation to remove statement from
	 * @param subject
	 *            The subject of the statement to remove
	 * @param prefix
	 *            Not used for removal
	 * @param namespace
	 *            The namespace of the property to remove
	 * @param property
	 *            The localname of the property to remove
	 * @param value
	 *            The value of the object of the statement to remove (might be null, i.e. delete all)
	 * @param lang
	 *            The lang code if needed to restrict
	 * @param add
	 *            Not used
	 * @param element
	 *            Current element
	 */
	public static void removeStatement(final PieceOfKnowledge annotation, final URI subject, final String prefix, final URI namespace, final String property, final Object value, final String lang,
			final boolean add, final Element element) {
		if (RDFWriter.isEmptyAnnotation(annotation)) {
			return;
		}
		final Node data = (Node) annotation.getData();
		final Node description = RDFReader.findElementAbout(data, subject.toString(), element);

		if (description != null) {
			if (property == null) {
				description.getParentNode().removeChild(description);
				return;
			}
			final List<Node> nodes = RDFReader.findNodesWithProperty(description, namespace.toString(), property);
			for (final Node node : nodes) {
				final String nodeValue = RDFReader.getNodeLiteralOrResource(node);
				if (lang != null) {
					final String nlang = RDFReader.getLanguage(node);
					if (lang.equalsIgnoreCase(nlang) && nodeValue.equals(value.toString())) {
						node.getParentNode().removeChild(node);
					}
				} else if ((value == null) || nodeValue.equals(value.toString())) {
					node.getParentNode().removeChild(node);
				}
			}
		}
	}


	private static XMLStreamWriter createXMLStreamWriter(final DOMResult domResult) throws XMLStreamException {
		if (RDFWriter.outputFactory == null) {
			try {
				/**
				 * Try to prevent XML API overriding.
				 * Sun XML parser is valid with namespace and xml:lang support in XML.
				 */
				final Class<?> clazz = ClassLoader.getSystemClassLoader().loadClass(RDFWriter.XML_PARSER_CLASS);
				RDFWriter.outputFactory = (XMLOutputFactory) clazz.newInstance();
			} catch (final Exception e) {
				LoggerFactory.getLogger(RDFWriter.class).warn(e.getMessage(), e);
			}
			if (RDFWriter.outputFactory == null) {
				LoggerFactory.getLogger(RDFWriter.class).warn("Your JVM is not the Oracle one, this is not really a problem (we like open source too)"
						+ " but please define an implementation of a XMLOutputFactory in META-INF/services/javax.xml.stream.XMLOutputFactory");
				/**
				 * Your JVM is not the Oracle one, this is not really a problem (we like open source too) but
				 * please define an implementation of a XMLOutputFactory in META-INF/services/javax.xml.stream.XMLOutputFactory
				 * See http://java.sun.com/javase/6/docs/api/javax/xml/stream/XMLOutputFactory.html#newFactory() for more information
				 */
				RDFWriter.outputFactory = XMLOutputFactory.newInstance();
				LoggerFactory.getLogger(RDFWriter.class).info("Loaded factory is: " + RDFWriter.outputFactory.getClass());
			}
		}

		return RDFWriter.outputFactory.createXMLStreamWriter(domResult);
	}


	private static Node createElementAbout(final Node rdf, final String subject, final Element element) throws XMLStreamException {
		final DOMResult domResult = new DOMResult(rdf);

		final XMLStreamWriter xmlw = RDFWriter.createXMLStreamWriter(domResult);

		xmlw.writeStartElement(element.getPrefix(), element.getLocalName(), element.getUri());
		xmlw.writeAttribute(org.ow2.weblab.core.extended.ontologies.RDF.PREFERRED_PREFIX, org.ow2.weblab.core.extended.ontologies.RDF.NAMESPACE, RDFWriter.ABOUT, subject);
		xmlw.writeEndElement();
		xmlw.close();

		return RDFReader.findElementAbout(rdf, subject, element);
	}


	private static void createAnnotation(final PieceOfKnowledge annotation, final URI subject, final String prefix, final String namespace, final String property, final Object value,
			final String lang, final Element element) throws XMLStreamException, ParserConfigurationException {

		final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		final DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
		final Document document = builder.newDocument();
		final DOMResult domResult = new DOMResult(document);

		final XMLStreamWriter xmlw = RDFWriter.createXMLStreamWriter(domResult);

		// Add prefixes
		xmlw.writeStartElement("data");
		xmlw.writeStartElement(org.ow2.weblab.core.extended.ontologies.RDF.PREFERRED_PREFIX, RDFWriter.RDF, org.ow2.weblab.core.extended.ontologies.RDF.NAMESPACE);
		xmlw.writeNamespace(org.ow2.weblab.core.extended.ontologies.RDF.PREFERRED_PREFIX, org.ow2.weblab.core.extended.ontologies.RDF.NAMESPACE);
		xmlw.writeNamespace(prefix, namespace);

		xmlw.writeStartElement(element.getPrefix(), element.getLocalName(), element.getUri());
		xmlw.writeNamespace(prefix, namespace);
		xmlw.writeAttribute(org.ow2.weblab.core.extended.ontologies.RDF.PREFERRED_PREFIX, org.ow2.weblab.core.extended.ontologies.RDF.NAMESPACE, RDFWriter.ABOUT, subject.toString());

		// Add annotation
		RDFWriter.writeAnnotation(xmlw, prefix, namespace, property, value, lang);

		xmlw.writeEndElement();

		xmlw.writeCharacters("\n");
		xmlw.writeCharacters("        ");
		xmlw.writeEndElement();
		xmlw.writeEndElement();
		xmlw.writeEndDocument();
		xmlw.close();

		annotation.setData(document.getDocumentElement());
	}


	private static void writeAnnotation(final XMLStreamWriter xmlw, final String prefix, final String namespace, final String property, final Object value, final String lang)
			throws XMLStreamException {

		// Check of properties
		final String theNamespace;
		final String thePrefix;
		final String theProperty;
		if ((property.indexOf(':') != -1) && (property.indexOf('/') != -1)) {
			int delimiter = property.lastIndexOf('#');
			if (delimiter == -1) {
				delimiter = property.lastIndexOf('/');
			}
			theNamespace = property.substring(0, delimiter + 1);
			thePrefix = "w" + theNamespace.hashCode();
			theProperty = property.substring(delimiter + 1);
		} else {
			theNamespace = namespace;
			thePrefix = prefix;
			theProperty = property;
		}

		xmlw.writeNamespace(thePrefix, theNamespace);

		if (value instanceof URI) {
			RDFWriter.writeResource(xmlw, thePrefix, theNamespace, theProperty, value.toString(), lang);

			// }else if(value == null){
			// TODO: delete the annotation

		} else {
			RDFWriter.writeLiteral(xmlw, thePrefix, theNamespace, theProperty, value, lang);
		}
	}


	private static void writeResource(final XMLStreamWriter xmlw, final String prefix, final String namespace, final String property, final String value, final String lang) throws XMLStreamException {
		xmlw.writeStartElement(prefix, property, namespace);
		xmlw.writeAttribute(org.ow2.weblab.core.extended.ontologies.RDF.PREFERRED_PREFIX, org.ow2.weblab.core.extended.ontologies.RDF.NAMESPACE, "resource", value);
		if (lang != null) {
			xmlw.writeAttribute("xml", RDFWriter.XML_URI, "lang", lang);
		}
		xmlw.writeEndElement();
	}


	private static void writeLiteral(final XMLStreamWriter xmlw, final String prefix, final String namespace, final String property, final Object value, final String lang) throws XMLStreamException {
		final AbstractTypeParser<Object> parser = XMLParser.getParser(value);
		if (parser == null) {
			throw new XMLStreamException("No parser found for " + value + " of type " + value.getClass());
		}
		xmlw.writeStartElement(prefix, property, namespace);
		if (lang != null) {
			xmlw.writeAttribute("xml", RDFWriter.XML_URI, "lang", lang);
		}
		final String dataTypeURI = parser.getDatatypeURI();
		if (dataTypeURI != null) {
			xmlw.writeAttribute(org.ow2.weblab.core.extended.ontologies.RDF.PREFERRED_PREFIX, org.ow2.weblab.core.extended.ontologies.RDF.NAMESPACE, "datatype", dataTypeURI);
		}
		xmlw.writeCharacters(parser.toString(value));

		xmlw.writeEndElement();
	}


	/**
	 * Replace all uris in a pok by another
	 *
	 * @param pok
	 *            The PieceOfKnowledge into which the uri has to be replaced
	 * @param source
	 *            The uri to be replaced
	 * @param dest
	 *            The new value to be used for the source uri
	 * @return The pok
	 */
	public static PieceOfKnowledge replaceURI(final PieceOfKnowledge pok, final URI source, final URI dest) {
		final Node node = (Node) pok.getData();
		// find about element
		final Node about = RDFReader.findElementAbout(node, source.toString(), Element.getDefaultElement());
		if (about != null) {
			final Node abn = about.getAttributes().getNamedItemNS(org.ow2.weblab.core.extended.ontologies.RDF.NAMESPACE, RDFWriter.ABOUT);
			abn.setNodeValue(dest.toString());
		}

		return pok;
	}

}
