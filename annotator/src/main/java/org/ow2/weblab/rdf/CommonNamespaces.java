/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.rdf;

/**
 * Some common namespaces
 * 
 * @author asaval
 *
 */
@Deprecated
public final class CommonNamespaces {


	/**
	 * rdf prefix: rdf
	 */
	@Deprecated
	public static final String RDF_PREFIX = "rdf";


	/**
	 * RDF namespace: http://www.w3.org/1999/02/22-rdf-syntax-ns#
	 */
	@Deprecated
	public static final String RDF_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";


	/**
	 * DublinCore prefix: dc
	 */
	@Deprecated
	public static final String DUBLIN_CORE_PREFIX = "dc";


	/**
	 * DublinCore namespace http://purl.org/dc/elements/1.1/
	 */
	@Deprecated
	public static final String DUBLIN_CORE_URI = "http://purl.org/dc/elements/1.1/";


	/**
	 * rdfs prefix: rdfs
	 */
	@Deprecated
	public static final String RDFS_PREFIX = "rdfs";


	/**
	 * RDFS namespace: http://www.w3.org/2000/01/rdf-schema#
	 */
	@Deprecated
	public static final String RDFS_URI = "http://www.w3.org/2000/01/rdf-schema#";


	/**
	 * CommonNamespaces constructor, should never be called.
	 */
	@Deprecated
	private CommonNamespaces() {
		throw new UnsupportedOperationException("CommonNamespaces should not be instanciated.");
	}

}
