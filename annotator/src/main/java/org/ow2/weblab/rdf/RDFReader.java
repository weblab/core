/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.rdf;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import org.ow2.weblab.core.annotator.Element;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Provides useful methods to deal with RDF structure in DOM nodes.
 *
 * @author asaval
 *
 */
public final class RDFReader {


	private static final String RDF = "RDF";


	private static final String ABOUT = "about";


	private static final String RESOURCE = "resource";


	private static final String XML_LANG = "xml:lang";


	private static final String RDF_DATATYPE = "rdf:datatype";


	private RDFReader() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Read the value of a property described in RDF
	 *
	 * @param <T>
	 *            the type of the value
	 * @param annotation
	 *            the annotation on which the property is written
	 * @param subject
	 *            the subject of the property
	 * @param namespace
	 *            the namespace of the property
	 * @param property
	 *            the localname of the property
	 * @param language
	 *            the language attribute of the property
	 * @param clazz
	 *            the class of the type T
	 * @param element
	 *            The default element to search into
	 * @return A Value object typed with T that contains all values of the property
	 */
	public static <T> Value<T> readValueFromStatement(final PieceOfKnowledge annotation, final URI subject, final URI namespace, final String property, final String language, final Class<T> clazz,
			final Element element) {

		final Value<T> res = new Value<>();

		// Default value let the data be an object instead of a Dom Node or Element
		if ((annotation.getData() == null) || annotation.getData().getClass().equals(Object.class)) {
			return null;
		}
		final Node data = (Node) annotation.getData();

		/**
		 * We retrieve only the first description node of the subject
		 * There could be other description node about this subject
		 * TODO: support several description node about the same subject
		 */
		final Node description = RDFReader.findElementAbout(data, subject.toString(), element);
		if (description == null) {
			return null;
		}

		final String theProperty;
		// Check of properties
		String sNamespace = namespace.toString();
		if ((property.indexOf(':') != -1) && (property.indexOf('/') != -1)) {
			int index = property.lastIndexOf('#');
			if (index == -1) {
				index = property.lastIndexOf('/');
			}
			sNamespace = property.substring(0, index + 1);
			theProperty = property.substring(index + 1);
		} else {
			theProperty = property;
		}

		final List<Node> nodes = RDFReader.findNodesWithProperty(description, sNamespace, theProperty);
		if (nodes.isEmpty()) {
			return null;
		}

		if (language == null) {

			for (final Node node : nodes) {
				String value = RDFReader.getResource(node);
				if (value == null) {
					value = node.getTextContent();
				}
				final NamedNodeMap nnm = node.getAttributes();
				final Node nlang = nnm.getNamedItem(RDFReader.XML_LANG);
				String lang = null;
				if (nlang != null) {
					lang = nlang.getTextContent();
				}
				final Node nRDFType = nnm.getNamedItem(RDFReader.RDF_DATATYPE);
				String rdfDatatype = null;
				if (nRDFType != null) {
					rdfDatatype = nRDFType.getTextContent();
				}
				final T parsed = XMLParser.xmlParse(value, clazz, rdfDatatype);
				if (parsed != null) {
					res.addItem(parsed, lang);
				}
			}

		}

		for (final Node node : nodes) {
			final NamedNodeMap nnm = node.getAttributes();
			final Node nlang = nnm.getNamedItem(RDFReader.XML_LANG);
			if (nlang != null) {
				final String lang = nlang.getTextContent();
				final Node nRDFType = nnm.getNamedItem(RDFReader.RDF_DATATYPE);
				String rdfDatatype = null;
				if (nRDFType != null) {
					rdfDatatype = nRDFType.getTextContent();
				}

				if ((lang.length() > 1) && (language != null) && language.equalsIgnoreCase(lang)) {
					String value = RDFReader.getResource(node);
					if (value == null) {
						value = node.getTextContent();
					}
					final T parsed = XMLParser.xmlParse(value, clazz, rdfDatatype);
					if (parsed != null) {
						res.addItem(parsed, lang);
					}
				}
			}
		}

		return res;
	}


	protected static String getResource(final Node node) {
		final NamedNodeMap nnm = node.getAttributes();
		final Node resource = nnm.getNamedItemNS(org.ow2.weblab.core.extended.ontologies.RDF.NAMESPACE, RDFReader.RESOURCE);
		if (resource == null) {
			return null;
		}
		return resource.getTextContent();
	}


	protected static List<Node> findNodesWithProperty(final Node node, final String namespace, final String property) {
		final List<Node> list = new LinkedList<>();

		if (namespace.equals(node.getNamespaceURI()) && property.equalsIgnoreCase(node.getLocalName())) {
			list.add(node);
		}

		final NamedNodeMap nnm = node.getAttributes();
		if (nnm != null) {
			for (int i = 0; i < nnm.getLength(); i++) {
				list.addAll(RDFReader.findNodesWithProperty(nnm.item(i), namespace, property));
			}
		}

		final NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			list.addAll(RDFReader.findNodesWithProperty(nodeList.item(i), namespace, property));
		}
		return list;
	}


	protected static List<Node> findNodesWithValue(final Node node, final String value) {
		final List<Node> list = new LinkedList<>();
		if (RDFReader.getNodeLiteralOrResource(node).equals(value)) {
			list.add(node);
		}

		final NamedNodeMap nnm = node.getAttributes();
		if (nnm != null) {
			for (int i = 0; i < nnm.getLength(); i++) {
				list.addAll(RDFReader.findNodesWithValue(nnm.item(i), value));
			}
		}

		final NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			list.addAll(RDFReader.findNodesWithValue(nodeList.item(i), value));
		}

		return list;
	}


	protected static String getNodeLiteralOrResource(final Node node) {
		String value = node.getTextContent();
		if ("".equals(value)) {
			// test if it is a resource
			final NamedNodeMap nnm = node.getAttributes();
			if (nnm != null) {
				final Node item = nnm.getNamedItemNS(org.ow2.weblab.core.extended.ontologies.RDF.NAMESPACE, RDFReader.RESOURCE);
				if (item != null) {
					value = item.getTextContent();
				}
			}
		}
		return value;
	}


	protected static String getLanguage(final Node node) {
		final NamedNodeMap nnm = node.getAttributes();
		if (nnm != null) {
			final Node nlang = nnm.getNamedItem(RDFReader.XML_LANG);
			if (nlang != null) {
				return nlang.getTextContent();
			}
		}
		return null;
	}


	/**
	 * Breadth first search should be more efficient than depth first search
	 *
	 * @param node
	 *            a node
	 * @param subject
	 *            the subject we search
	 * @param element
	 *            Current wrapping element
	 * @return the rdf:description node about the subject or null if it does not exist
	 */
	protected static Node findElementAboutIn(final Node node, final String subject, final Element element) {
		if (node == null) {
			return null;
		}
		final NodeList nodeList = node.getChildNodes();
		if (nodeList == null) {
			return null;
		}
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (RDFReader.isElementAbout(nodeList.item(i), subject, element)) {
				return nodeList.item(i);
			}
		}

		for (int i = 0; i < nodeList.getLength(); i++) {
			final Node about = RDFReader.findElementAboutIn(nodeList.item(i), subject, element);
			if (about != null) {
				return about;
			}
		}
		return null;
	}


	protected static Node findElementAbout(final Node node, final String subject, final Element element) {
		if (RDFReader.isElementAbout(node, subject, element)) {
			return node;
		}
		return RDFReader.findElementAboutIn(node, subject, element);
	}


	protected static boolean isElementAbout(final Node node, final String subject, final Element element) {
		if (node == null) {
			return false;
		}
		final NamedNodeMap nnm = node.getAttributes();
		if ((nnm != null) && element.equalsNode(node)) {
			final Node att = nnm.getNamedItemNS(org.ow2.weblab.core.extended.ontologies.RDF.NAMESPACE, RDFReader.ABOUT);
			if (att != null) {
				final String text = att.getTextContent();
				if ((text != null) && text.equalsIgnoreCase(subject)) {
					return true;
				}
			}
		}
		return false;
	}


	protected static Node findRDF(final Node dom) {
		final NamedNodeMap nnm = dom.getAttributes();
		if ((nnm != null) && RDFReader.RDF.equalsIgnoreCase(dom.getLocalName())) {
			return dom;
		}

		final NodeList nodeList = dom.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			final Node node = RDFReader.findRDF(nodeList.item(i));
			if (node != null) {
				return node;
			}
		}
		return null;
	}


	protected static String print(final Node node, final String space) {
		final StringBuffer sbuff = new StringBuffer();
		final NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			sbuff.append(RDFReader.print(nodeList.item(i), space + "\t"));
		}
		return sbuff.toString();
	}

}
