/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.rdf;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

/**
 * A parser to transform String into an Object whose type it  &lt;T&gt;
 *
 * @author asaval
 *
 * @param <T>
 *            the type supported by the parser
 */
public abstract class AbstractTypeParser<T> {


	/**
	 * Parse a String containing data to and Object of the required type.
	 *
	 * @param value
	 *            a String containing data (can be null)
	 * @return an Object that is a representation of the value in the type T
	 */
	public abstract T parse(String value);


	/**
	 * Override this method to return a default value on an exception occurring during parse.
	 * By default, returns null.
	 *
	 * @param exception
	 *            the thrown exception
	 * @return the default value when parse(String) throws an exception
	 */
	public T defaultValueOnException(final Exception exception) {
		return null;
	}


	/**
	 * Return the dataType URI for this type.
	 *
	 * @return the URI referring to the datatype
	 */
	public String getDatatypeURI() {
		return null;
	}


	/**
	 * Return the String representation of the value
	 *
	 * @param value
	 *            a value of type T
	 * @return the String representation of the value
	 */
	public String toString(final T value) {
		return value.toString();
	}



	public static class DateTypeParser<T> extends AbstractTypeParser<Date> {


		@Override
		public Date parse(final String value) {
			final TemporalAccessor parsed = DateTimeFormatter.ISO_DATE_TIME.parse(value);
			final LocalDateTime localDateTime = LocalDateTime.from(parsed);
			final Instant instant = localDateTime.toInstant(ZoneOffset.UTC);
			final Date date = Date.from(instant);
			return date;
		}


		@Override
		public String getDatatypeURI() {
			return "http://www.w3.org/2001/XMLSchema#dateTime";
		}


		@Override
		public String toString(final Date date) {
			return DateTimeFormatter.ISO_INSTANT.format(date.toInstant());
		}
	}



}
