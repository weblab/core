/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.rdf;

/**
 * A container for any supported RDF datatype.
 * See XMLParser for the list of supported datatypes.
 *
 * @author asaval
 * @see XMLParser
 *
 */
public final class RDFDatatypeContainer {


	/**
	 * RDF Datatype for the object
	 */
	private final String rdfDatatype;


	/**
	 * Result of parser corresponding to the rdfDatatype XMLParser
	 */
	private final Object object;


	/**
	 * Creates a container with an object and its rdf datatype
	 *
	 * @param object
	 *            an object resulting from a XML parser, it can not be null
	 * @param rdfDatatype
	 *            a RDF datatype, it can be null (the String datatype will be assumed)
	 */
	public RDFDatatypeContainer(final Object object, final String rdfDatatype) {
		if (object == null) {
			throw new NullPointerException("RDFdatatypeContainer must contain an object, null value is not a valid value. Make sure XMLParser does know how to parse the following datatype: "
					+ rdfDatatype + " See http://weblab.ow2.org/WebLab1.2.5/javadoc/org/ow2/weblab/rdf/XMLParser.html");
		}
		this.object = object;
		this.rdfDatatype = rdfDatatype;
	}


	/**
	 * Returns the object
	 *
	 * @return the object
	 */
	public Object getObject() {
		return this.object;
	}


	/**
	 * Returns the object with the given class
	 *
	 * @param clazz
	 *            the class of the object
	 * @param <T>
	 *            the actual type of the object
	 * @return the object casted in the given class
	 */
	public <T> T getObject(final Class<T> clazz) {
		return (T) this.object;
	}


	/**
	 * Returns the RDF Datatype provided with the object,
	 * if it is null, string datatype must be assumed.
	 *
	 * @return the RDF Datatype provided with the object, it can be null
	 */
	public String getRdfDatatype() {
		return this.rdfDatatype;
	}


	/**
	 * Returns the class of the object
	 *
	 * @return the class of the object
	 */
	public Class<?> getClazz() {
		return this.object.getClass();
	}


	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append('{');
		buffer.append(this.object.toString());
		buffer.append('}');
		return buffer.toString();
	}
}