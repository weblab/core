/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.model;

import java.net.URI;

import javax.annotation.Generated;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.rdf.Value;

/**
 * WebLab Exchange model v1.2.9 - July 2017
 * This version includes annotable object of the model i.e. Resources and their relationships.
 * It also provides some properties to be used in annotations.
 *
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 */
@Generated(value = "org.ow2.weblab.rdf.OWL2Annotator", date = "2015-09-18T16:33:43.395+02:00")
public class WModelAnnotator extends BaseAnnotator {


	protected static final String APREFIX = "model";


	protected static final URI AURI = URI.create("http://weblab.ow2.org/core/1.2/ontology/model#");


	protected static final String IN_RESULT_SET = "inResultSet";


	protected static final String HAS_CONTENT = "hasContent";


	protected static final String HAS_LOW_LEVEL_DESCRIPTOR = "hasLowLevelDescriptor";


	protected static final String HAS_REQUEST = "hasRequest";


	protected static final String HAS_WEIGHT = "hasWeight";


	protected static final String IN_COMPOSED_RESOURCE = "inComposedResource";


	protected static final String HAS_TIME_STAMP = "hasTimeStamp";


	protected static final String HAS_SUB_ELEMENT = "hasSubElement";


	protected static final String HAS_OPERATOR = "hasOperator";


	protected static final String IS_COMPOSED_BY_SEGMENT = "isComposedBySegment";


	protected static final String END_AT = "endAt";


	protected static final String HAS_ORDINATE = "hasOrdinate";


	protected static final String IN_COMPOSED_QUERY = "inComposedQuery";


	protected static final String IS_COMPOSED_BY_QUERY = "isComposedByQuery";


	protected static final String HAS_COORDINATE = "hasCoordinate";


	protected static final String IS_COMPOSED_BY_MEDIA_UNIT = "isComposedByMediaUnit";


	protected static final String IN_DOCUMENT = "inDocument";


	protected static final String HAS_SEGMENT = "hasSegment";


	protected static final String HAS_ANNOTATION = "hasAnnotation";


	protected static final String IS_COMPOSED_BY_RESOURCE_RESULT_SET = "isComposedByResourceResultSet";


	protected static final String HAS_SUPER_ELEMENT = "hasSuperElement";


	protected static final String HAS_DATA = "hasData";


	protected static final String START_AT = "startAt";


	protected static final String IS_COMPOSED_BY_RESOURCE = "isComposedByResource";


	protected static final String HAS_POK = "hasPok";


	protected static final String HAS_ABSCISSA = "hasAbscissa";


	protected static final String HAS_TEXT_CONTENT = "hasTextContent";


	/**
	 * WModelAnnotator constructor on a PieceOfKnowledge using given URI as the default subject.
	 *
	 * @param subject
	 *            The initial subject of first read/written statements
	 * @param pieceOfKnowledge
	 *            The PieceOfKnowledge to be used for reading/writing RDF statements
	 */
	public WModelAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * WModelAnnotator constructor on a resource.
	 *
	 * @param resource
	 *            The resource on which to read/write RDF statements
	 */
	public WModelAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * Read the Links a resource to its parent ResultSet.
	 *
	 * @return an URI
	 */
	public final Value<URI> readInResultSet() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.IN_RESULT_SET, URI.class, null);
	}


	/**
	 * Write the Links a resource to its parent ResultSet.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeInResultSet(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.IN_RESULT_SET, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The optional content as a bytes array.
	 *
	 * @return a String
	 */
	public final Value<String> readContent() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_CONTENT, String.class, null);
	}


	/**
	 * Write the The optional content as a bytes array.
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeContent(final String value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_CONTENT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a Resource to one of its LowLevelDescriptor.
	 *
	 * @return an URI
	 */
	public final Value<URI> readLowLevelDescriptor() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_LOW_LEVEL_DESCRIPTOR, URI.class, null);
	}


	/**
	 * Write the Links a Resource to one of its LowLevelDescriptor.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLowLevelDescriptor(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_LOW_LEVEL_DESCRIPTOR, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The string query in a free text syntax.
	 *
	 * @return a String
	 */
	public final Value<String> readRequest() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_REQUEST, String.class, null);
	}


	/**
	 * Write the The string query in a free text syntax.
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRequest(final String value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_REQUEST, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A weight used for Query combination.
	 *
	 * @return a Float
	 */
	public final Value<Float> readWeight() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_WEIGHT, Float.class, null);
	}


	/**
	 * Write the A weight used for Query combination.
	 *
	 * @param value
	 *            a Float
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeWeight(final Float value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_WEIGHT, Float.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a resource to its parent ComposedResource or SimilarityQuery.
	 *
	 * @return an URI
	 */
	public final Value<URI> readInComposedResource() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.IN_COMPOSED_RESOURCE, URI.class, null);
	}


	/**
	 * Write the Links a resource to its parent ComposedResource or SimilarityQuery.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeInComposedResource(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.IN_COMPOSED_RESOURCE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The timestamp of a SpatioTemporalSegment
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readTimeStamp() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_TIME_STAMP, Integer.class, null);
	}


	/**
	 * Write the The timestamp of a SpatioTemporalSegment
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTimeStamp(final Integer value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_TIME_STAMP, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a Resource or a Segment to one of its content.
	 *
	 * @return an URI
	 */
	public final Value<URI> readSubElement() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_SUB_ELEMENT, URI.class, null);
	}


	/**
	 * Write the Links a Resource or a Segment to one of its content.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSubElement(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_SUB_ELEMENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the This is a simple restriction of String to reduce the values to the three boolean operators AND/OR/NOT. Note that in a n-ary context, the unary
	 * operator NOT is considered as a AND NOT.
	 *
	 * @return a String Operator
	 */
	public final Value<String> readOperator() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_OPERATOR, String.class, null);
	}


	/**
	 * Write the This is a simple restriction of String to reduce the values to the three boolean operators AND/OR/NOT. Note that in a n-ary context, the unary
	 * operator NOT is considered as a AND NOT.
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOperator(final String value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_OPERATOR, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a TrackSegment to one of its Segment
	 *
	 * @return an URI
	 */
	public final Value<URI> readComposedBySegment() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.IS_COMPOSED_BY_SEGMENT, URI.class, null);
	}


	/**
	 * Write the Links a TrackSegment to one of its Segment
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeComposedBySegment(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.IS_COMPOSED_BY_SEGMENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The end value of a LinearSegment (char in UTF-8) or a TemporalSegment (milliseconds).
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readEndAt() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.END_AT, Integer.class, null);
	}


	/**
	 * Write the The end value of a LinearSegment (char in UTF-8) or a TemporalSegment (milliseconds).
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeEndAt(final Integer value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.END_AT, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Ordinate value in pixels, with 0 at the top border. Other measure references are possible.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readOrdinate() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_ORDINATE, Integer.class, null);
	}


	/**
	 * Write the Ordinate value in pixels, with 0 at the top border. Other measure references are possible.
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOrdinate(final Integer value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_ORDINATE, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a Query to its parent ComposedQuery.
	 *
	 * @return an URI
	 */
	public final Value<URI> readInComposedQuery() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.IN_COMPOSED_QUERY, URI.class, null);
	}


	/**
	 * Write the Links a Query to its parent ComposedQuery.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeInComposedQuery(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.IN_COMPOSED_QUERY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a ComposedQuery to one of its Query.
	 *
	 * @return an URI
	 */
	public final Value<URI> readComposedByQuery() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.IS_COMPOSED_BY_QUERY, URI.class, null);
	}


	/**
	 * Write the Links a ComposedQuery to one of its Query.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeComposedByQuery(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.IS_COMPOSED_BY_QUERY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a SpatialSegment or a SpatioTemporalSegment to one of its Coordinate.
	 *
	 * @return an URI
	 */
	public final Value<URI> readCoordinate() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_COORDINATE, URI.class, null);
	}


	/**
	 * Write the Links a SpatialSegment or a SpatioTemporalSegment to one of its Coordinate.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCoordinate(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_COORDINATE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a Document to one of its sub-MediaUnit.
	 *
	 * @return an URI
	 */
	public final Value<URI> readComposedByMediaUnit() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.IS_COMPOSED_BY_MEDIA_UNIT, URI.class, null);
	}


	/**
	 * Write the Links a Document to one of its sub-MediaUnit.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeComposedByMediaUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.IS_COMPOSED_BY_MEDIA_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a MediaUnit to its parent Document.
	 *
	 * @return an URI
	 */
	public final Value<URI> readInDocument() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.IN_DOCUMENT, URI.class, null);
	}


	/**
	 * Write the Links a MediaUnit to its parent Document.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeInDocument(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.IN_DOCUMENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a MediaUnit to one of its Segment.
	 *
	 * @return an URI
	 */
	public final Value<URI> readSegment() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_SEGMENT, URI.class, null);
	}


	/**
	 * Write the Links a MediaUnit to one of its Segment.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSegment(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_SEGMENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a Resource to one of its Annotation.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAnnotation() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_ANNOTATION, URI.class, null);
	}


	/**
	 * Write the Links a Resource to one of its Annotation.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAnnotation(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_ANNOTATION, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a ResultSet to one of its Resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readComposedByResourceResultSet() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.IS_COMPOSED_BY_RESOURCE_RESULT_SET, URI.class, null);
	}


	/**
	 * Write the Links a ResultSet to one of its Resource.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeComposedByResourceResultSet(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.IS_COMPOSED_BY_RESOURCE_RESULT_SET, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a Resource or a Segment to its container.
	 *
	 * @return an URI
	 */
	public final Value<URI> readSuperElement() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_SUPER_ELEMENT, URI.class, null);
	}


	/**
	 * Write the Links a Resource or a Segment to its container.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSuperElement(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_SUPER_ELEMENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Should be an ARFF content for a LowLevelDescriptor and a RDF/XML content for a PoK.
	 *
	 * @return a String
	 */
	public final Value<String> readData() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_DATA, String.class, null);
	}


	/**
	 * Write the Should be an ARFF content for a LowLevelDescriptor and a RDF/XML content for a PoK.
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeData(final String value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_DATA, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The start value of a LinearSegment (char in UTF-8) or a TemporalSegment (milliseconds).
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readStartAt() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.START_AT, Integer.class, null);
	}


	/**
	 * Write the The start value of a LinearSegment (char in UTF-8) or a TemporalSegment (milliseconds).
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeStartAt(final Integer value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.START_AT, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a ComposedResource or a SimilarityQuery to one of its Resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readComposedByResource() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.IS_COMPOSED_BY_RESOURCE, URI.class, null);
	}


	/**
	 * Write the Links a ComposedResource or a SimilarityQuery to one of its Resource.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeComposedByResource(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.IS_COMPOSED_BY_RESOURCE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a ResultSet to its PieceOfKnowledge.
	 *
	 * @return an URI
	 */
	public final Value<URI> readPok() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_POK, URI.class, null);
	}


	/**
	 * Write the Links a ResultSet to its PieceOfKnowledge.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePok(final URI value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_POK, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Abscissa value in pixels, with 0 at the left border. Other measure references are possible.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readAbscissa() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_ABSCISSA, Integer.class, null);
	}


	/**
	 * Write the Abscissa value in pixels, with 0 at the left border. Other measure references are possible.
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAbscissa(final Integer value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_ABSCISSA, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The String content of the text. Might be empty, in that case an annotation referring to the whole large content should be present.
	 *
	 * @return a String
	 */
	public final Value<String> readTextContent() {
		return this.applyOperator(Operator.READ, null, WModelAnnotator.AURI, WModelAnnotator.HAS_TEXT_CONTENT, String.class, null);
	}


	/**
	 * Write the The String content of the text. Might be empty, in that case an annotation referring to the whole large content should be present.
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTextContent(final String value) {
		return this.applyOperator(Operator.WRITE, WModelAnnotator.APREFIX, WModelAnnotator.AURI, WModelAnnotator.HAS_TEXT_CONTENT, String.class, value).getIsAnnotatedOn();
	}


}
