/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.annotator;

import java.net.URI;

import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * This Annotator provides support for RDF:type, RDFS:label, RDFS:comment and RDFS:seeAlso properties.
 *
 * @author asaval
 */
public class BaseAnnotator extends AbstractAnnotator {


	/**
	 * RDF prefix
	 */
	public static final String RDF_PREFIX = "rdf";


	/**
	 * RDF namespace
	 */
	public static final URI RDF_URI = URI.create("http://www.w3.org/1999/02/22-rdf-syntax-ns#");


	/**
	 * RDFS prefix
	 */
	public static final String RDFS_PREFIX = "rdfs";


	/**
	 * RDFS namespace
	 */
	public static final URI RDFS_URI = URI.create("http://www.w3.org/2000/01/rdf-schema#");


	/**
	 * BaseAnnotator on a Resource
	 *
	 * @param resource
	 *            the Resource to annotate
	 */
	public BaseAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * BaseAnnotator constructor on a PieceOfKnowledge using given URI as the default subject.
	 *
	 * @param subject
	 *            the subject to annotate
	 * @param pieceOfKnowledge
	 *            the pok that will contain annotations
	 */
	public BaseAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * Writes the RDF:type
	 *
	 * @param uri
	 *            the RDF:type value
	 * @return the annotation on which the statement has been added
	 */
	public PieceOfKnowledge writeType(final URI uri) {
		return this.applyOperator(Operator.WRITE, BaseAnnotator.RDF_PREFIX, BaseAnnotator.RDF_URI, "type", URI.class, uri).getIsAnnotatedOn();
	}


	/**
	 * Reads the value of the RDF:type of the subject
	 *
	 * @return Value&lt;URI&gt; that contain the result.
	 */
	public Value<URI> readType() {
		return this.applyOperator(Operator.READ, BaseAnnotator.RDF_PREFIX, BaseAnnotator.RDF_URI, "type", URI.class, null);
	}


	/**
	 * Reads the value of the RDFS:label of the subject
	 *
	 * @return Value&lt;String&gt; that contain the result.
	 */
	public Value<String> readLabel() {
		return this.applyOperator(Operator.READ, BaseAnnotator.RDFS_PREFIX, BaseAnnotator.RDFS_URI, "label", String.class, null);
	}


	/**
	 * Writes the RDFS:label
	 *
	 * @param value
	 *            the RDF:label value
	 * @return the annotation on which the statement has been added
	 */
	public PieceOfKnowledge writeLabel(final String value) {
		return this.applyOperator(Operator.WRITE, BaseAnnotator.RDFS_PREFIX, BaseAnnotator.RDFS_URI, "label", String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Reads the value of the RDFS:comment of the subject
	 *
	 * @return Value&lt;String&gt; that contain the result.
	 */
	public Value<String> readComment() {
		return this.applyOperator(Operator.READ, BaseAnnotator.RDFS_PREFIX, BaseAnnotator.RDFS_URI, "comment", String.class, null);
	}


	/**
	 * Writes the RDFS:comment
	 *
	 * @param value
	 *            the RDF:comment value
	 * @return the annotation on which the statement has been added
	 */
	public PieceOfKnowledge writeComment(final String value) {
		return this.applyOperator(Operator.WRITE, BaseAnnotator.RDFS_PREFIX, BaseAnnotator.RDFS_URI, "comment", String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Writes the RDFS:seeAlso
	 *
	 * @param uri
	 *            the RDFS:seeAlso value
	 * @return the annotation on which the statement has been added
	 */
	public PieceOfKnowledge writeSeeAlso(final URI uri) {
		return this.applyOperator(Operator.WRITE, BaseAnnotator.RDFS_PREFIX, BaseAnnotator.RDFS_URI, "seeAlso", URI.class, uri).getIsAnnotatedOn();
	}


	/**
	 * Reads the value of the RDFS:seeAlso of the subject
	 *
	 * @return Value&lt;URI&gt; that contain the result.
	 */
	public Value<URI> readSeeAlso() {
		return this.applyOperator(Operator.READ, BaseAnnotator.RDFS_PREFIX, BaseAnnotator.RDFS_URI, "seeAlso", URI.class, null);
	}

}
