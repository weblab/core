/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.annotator;

import java.net.URI;

import org.ow2.weblab.core.extended.ontologies.RDF;
import org.w3c.dom.Node;

/**
 * This class defines properties used to build the container in which statements are read and written.
 *
 */
public class Element {


	/**
	 * The local name of the element
	 */
	private String localName;


	/**
	 * The namespace of the element
	 */
	private String uri;


	/**
	 * The prefix to use for the uri
	 */
	private String prefix;


	/**
	 * Create an Element that can used as container for properties.
	 * 
	 * @param prefix
	 *            the prefix
	 * @param uri
	 *            the namespace
	 * @param localName
	 *            the localname
	 */
	public Element(final String prefix, final String uri, final String localName) {
		this.localName = localName;
		this.uri = uri;
		this.prefix = prefix;
	}


	/**
	 * returns the local name.
	 * 
	 * @return the local name
	 */
	public final String getLocalName() {
		return this.localName;
	}


	/**
	 * Set the local name.
	 * 
	 * @param localName
	 *            the local name
	 */
	public final void setLocalName(final String localName) {
		this.localName = localName;
	}


	/**
	 * Returns the namespace.
	 * 
	 * @return the namespace
	 */
	public final String getUri() {
		return this.uri;
	}


	/**
	 * Set the namespace
	 * 
	 * @param uri
	 *            the namespace
	 */
	public final void setUri(final String uri) {
		this.uri = uri;
	}


	/**
	 * Returns the prefix
	 * 
	 * @return the prefix
	 */
	public final String getPrefix() {
		return this.prefix;
	}


	/**
	 * Sets the prefix
	 * 
	 * @param prefix The prefix to be used
	 */
	public final void setPrefix(final String prefix) {
		this.prefix = prefix;
	}


	/**
	 * Compares this element to a DOM node.
	 * If namespace and localname match, then it returns true, else false.
	 * 
	 * @param node
	 *            a DOM node
	 * @return true if this element matches namespace and localname of the node
	 */
	public final boolean equalsNode(final Node node) {
		final String name = node.getLocalName();
		final String nuri = node.getNamespaceURI();
		return ((name != null) && (nuri != null) && name.equalsIgnoreCase(this.localName) && nuri.equalsIgnoreCase(this.uri));
	}


	/**
	 * Returns an rdf:description element
	 * 
	 * @return RDF:Description Element
	 */
	public static final Element getDefaultElement() {
		return new Element(RDF.PREFERRED_PREFIX, RDF.NAMESPACE, "Description");
	}


	/**
	 * Returns the complete URI of this element; namespace with localname in an URI form.
	 * 
	 * @return URI constructed from the namespace and localname of this Element.
	 */
	public final URI toURI() {
		return URI.create(this.uri + this.localName);
	}

}