/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.annotator;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.RDFReader;
import org.ow2.weblab.rdf.RDFWriter;
import org.ow2.weblab.rdf.Value;
import org.slf4j.LoggerFactory;

/**
 * Defines base methods to manage annotations on resources.
 * It provides the following functionalities:
 * <ul>
 * <li>Read statement</li>
 * <li>Add new statement</li>
 * <li>Overwrite statement</li>
 * <li>Delete statement</li>
 * </ul>
 *
 * It can switch from an <code>append mode</code> to an <code>overwrite mode</code> to either add new statements or overwrite existing ones, see <code>setAppendMode(boolean)</code>.
 *
 * To add a statement with a given language, define Annotator language with <code>startSpecifyLanguage(String lang)</code>.
 *
 * To change the subject of the annotation, use <code>startInnerAnnotatorOn(URI subject)</code>.
 *
 * @author asaval
 */
public abstract class AbstractAnnotator {


	/**
	 * The element properties used to write statements in.
	 */
	private Element element = Element.getDefaultElement();


	/**
	 * Operator to select action
	 */
	public static enum Operator {
		/**
		 * ADD: removes an existing statement with the same property from the first Annotation found
		 *
		 * <p>
		 * <b>Warning:</b> the name of the operator does not reflect its real action
		 */
		@Deprecated
		ADD,

		/**
		 * REMOVE: to be used when wanting to remove any statement for all annotations using the same property.
		 */
		REMOVE,

		/**
		 * SET: Is not handled
		 */
		@Deprecated
		SET,

		/**
		 * READ: to be used when wanting to read every statements using the same property.
		 */
		READ,

		/**
		 * WRITE: to be used when wanting to add a new statement even if one exists using the same property.
		 */
		WRITE
	}


	/**
	 * The Pok on which we will write and read statements
	 */
	private List<PieceOfKnowledge> poks;


	/**
	 * The WebLab Resource that will be annotated.
	 * It can be null since we can annotate subject which are just URI
	 *
	 * @see #AbstractAnnotator(URI, PieceOfKnowledge)
	 */
	private Resource resource;


	/**
	 * The subject URI that will be used when writing/reading a statement.
	 */
	private URI subjectURI;


	/**
	 * State of the inner Annotator
	 */
	private boolean innerAnnotatorStarted = false;


	/**
	 * The original URI from the constructor (either subject or Resource's uri)
	 */
	private URI originalURI;


	/**
	 * The language currently supported in annotation
	 * If null, all languages are retrieve
	 */
	private String lang = null;


	/**
	 * Set the writing mode of the annotator (ADD/SET)
	 */
	private boolean append = true;


	/**
	 * Creates an annotator on the Resource.
	 * It allows to write and read statement (subject, property, object) about this resource on this resource.
	 *
	 * @param resource
	 *            the resource to wrap.
	 */
	protected AbstractAnnotator(final Resource resource) {
		this.resource = resource;
		try {
			this.subjectURI = new URI(resource.getUri());
		} catch (final URISyntaxException e) {
			LoggerFactory.getLogger(this.getClass()).error("Illegal resource URI " + resource.getUri(), e);
		}
	}


	/**
	 * Creates an annotator for an URI.
	 * It allows to write and read about the subject in this PoK
	 *
	 * @param subject
	 *            the subject of interest
	 * @param pok
	 *            the PieceOfKnowledge on which we manage information about the subject
	 */
	protected AbstractAnnotator(final URI subject, final PieceOfKnowledge pok) {
		this.innerAnnotatorStarted = true;
		this.resource = null;
		this.subjectURI = subject;
		this.originalURI = subject;
		if (pok != null) {
			this.poks = new LinkedList<>();
			this.poks.add(pok);
		}
	}


	/**
	 * Returns the current subject
	 *
	 * @return the current subject
	 */
	public URI getSubject() {
		return this.subjectURI;
	}


	/**
	 * Returns the current resource
	 *
	 * @return the current resource
	 */
	protected Resource getResource() {
		return this.resource;
	}


	/**
	 * Setter for current resource
	 *
	 * @param resource
	 *            update the current resource
	 */
	protected void setResource(final Resource resource) {
		this.resource = resource;
	}


	/**
	 * Set the element that will contain statements.
	 * By default, the element is an RDF:Description object
	 *
	 * @param prefix
	 *            the prefix of the element
	 * @param uri
	 *            the uri of the element
	 * @param localName
	 *            the localName of the element
	 */
	public void setElement(final String prefix, final String uri, final String localName) {
		this.element = new Element(prefix, uri, localName);
	}


	/**
	 * Set the element that will contain statements.
	 * By default, the element is an RDF:Description object
	 *
	 * @param element
	 *            an Element
	 */
	public void setElement(final Element element) {
		this.element = element;
	}


	/**
	 * Set the default statements container that is an RDF:Description object
	 */
	public void setDefaultElement() {
		this.element = Element.getDefaultElement();
	}


	/**
	 * Start a inner annotator on a different subject.
	 * It allows to use an annotator to read/write stament about on other subject.
	 * If an inner annotator is already started, it will be automatically closed
	 *
	 * @param subject
	 *            The subject to focus on
	 */
	public void startInnerAnnotatorOn(final URI subject) {
		this.innerAnnotatorStarted = true;
		this.subjectURI = subject;
	}


	/**
	 * End a inner annotator, the subject will be reseted to the first one
	 * (retrieved when the Annotator has been constructed).
	 */
	public void endInnerAnnotator() {
		if (this.resource == null) {
			this.subjectURI = this.originalURI;
		} else {
			this.innerAnnotatorStarted = false;
			this.subjectURI = URI.create(this.resource.getUri());
		}
	}


	/**
	 * Return the writing mode of the annotator:
	 * if the annotator is in append mode, it will add annotations else the annotator will override existing annotations
	 *
	 * @return true if the annotator will add annotations or false if the annotator will override existing annotations
	 */
	public boolean isAppendMode() {
		return this.append;
	}


	/**
	 * Set the writing mode of the annotator.
	 * If the annotator is in append mode setAppendMode(true), then writing null value will fail and the annotator will do nothing. In this case, the returned
	 * POK will be null.
	 * If the annotator is in override mode setAppendMode(false), then writing null value will delete all the statements with the property ONLY in the FIRST
	 * annotation of the resource.
	 *
	 * @param appendMode
	 *            true if the annotator will add annotations or false if the annotator will override existing annotations
	 *
	 */
	public void setAppendMode(final boolean appendMode) {
		this.append = appendMode;
	}


	/**
	 * Start to support a specific language defined annotation
	 *
	 * @param language
	 *            The langgue to focus on (until {@link #endSpecifyLanguage()}
	 */
	public void startSpecifyLanguage(final String language) {
		this.lang = language;
	}


	/**
	 * End the support for specific language
	 */
	public void endSpecifyLanguage() {
		this.lang = null;
	}


	/**
	 * Returns true if inner annotator is started else false
	 *
	 * @return true if inner annotator is started else false
	 */
	public boolean isInnerAnnotatorStarted() {
		return this.innerAnnotatorStarted;
	}


	protected <T> void retrieveAnnotation(final Operator operator, final T value) {
		if ((this.poks != null) && !this.poks.isEmpty() && this.innerAnnotatorStarted) {
			return;
		}
		this.poks = new LinkedList<>();
		if (this.resource.getAnnotation().isEmpty() && operator != AbstractAnnotator.Operator.READ && value != null) {
			this.poks.add(WebLabResourceFactory.createAndLinkAnnotation(this.resource));
		} else {
			this.poks.addAll(ResourceUtil.getSelectedSubResources(this.resource, Annotation.class));
		}
	}


	/**
	 * Apply given operator to the resource, if a value is not supported, it will be discarded
	 *
	 * @param operator
	 *            the operator
	 * @param uriPrefix
	 *            the prefix
	 * @param propertyUri
	 *            the namespace of the property
	 * @param localname
	 *            the localname of the property
	 * @param clazz
	 *            the class of the value
	 * @param <T>
	 *            the type of the value
	 * @param val
	 *            the value
	 * @return a value containing result of operation.
	 */
	public <T> Value<T> applyOperator(final Operator operator, final String uriPrefix, final URI propertyUri, final String localname, final Class<T> clazz, final T val) {
		Value<T> value = null;
		Operator finalop = operator;
		if (operator == AbstractAnnotator.Operator.WRITE && !this.append && val == null) {
			finalop = AbstractAnnotator.Operator.REMOVE;
		}
		this.retrieveAnnotation(operator, val);

		switch (finalop) {
			case READ:
				for (final PieceOfKnowledge pok : this.poks) {
					final Value<T> ivalue = RDFReader.readValueFromStatement(pok, this.subjectURI, propertyUri, localname, this.lang, clazz, this.element);
					if (value == null) {
						value = ivalue;
					} else if (ivalue != null) {
						value.aggregate(ivalue);
					}
				}
				break;

			case WRITE:
				try {
					final Object object = val;
					final PieceOfKnowledge pok = this.poks.get(0);
					RDFWriter.writeStatement(pok, this.subjectURI, uriPrefix, propertyUri, localname, object, this.lang, this.append, this.element);
					value = new Value<>(val, pok);

				} catch (final Exception e) {
					LoggerFactory.getLogger(this.getClass()).error("Could not write property " + propertyUri + localname + " on " + this.subjectURI + " with value " + val, e);
					value = new Value<>(val, null); // the POK is set to null since the property an not been written nor deleted
				}
				break;

			case ADD:
				final Object object = val;
				final PieceOfKnowledge pok = this.poks.get(0);
				RDFWriter.removeStatement(pok, this.subjectURI, uriPrefix, propertyUri, localname, object, this.lang, this.append, this.element);
				break;

			case REMOVE:
				final Object object1 = val;
				for (final PieceOfKnowledge pok1 : this.poks) {
					RDFWriter.removeStatement(pok1, this.subjectURI, uriPrefix, propertyUri, localname, object1, this.lang, this.append, this.element);
				}
				break;
			default:
				throw new WebLabUncheckedException("Operator " + operator + " was not taken into account.");
		}

		if (value == null) {
			value = new Value<>();
		}

		return value;
	}


	/**
	 * Apply given operator to the resource, if a value is not supported, it will be discarded
	 *
	 * @param operator
	 *            the operator
	 * @param uriPrefix
	 *            the prefix
	 * @param property
	 *            the the property
	 * @param clazz
	 *            the class of the value
	 * @param <T>
	 *            the class of the value
	 * @param val
	 *            the value
	 * @return a value containing result of operation.
	 */
	public <T> Value<T> applyOperator(final Operator operator, final String uriPrefix, final URI property, final Class<T> clazz, final T val) {
		// splitting property URI
		URI baseURI;
		String localname;
		if (property.getFragment() != null) {
			baseURI = URI.create(property.getScheme() + ":" + property.getSchemeSpecificPart() + "#");
			localname = property.getFragment();
		} else {
			final String[] decodedPath = property.getPath().split("/");

			String base = property.getScheme() + "://" + property.getHost();
			for (int i = 1; i < decodedPath.length - 1; i++) {
				base += "/" + decodedPath[i];
			}

			localname = decodedPath[decodedPath.length - 1];
			baseURI = URI.create(base + "/");
		}

		return this.applyOperator(operator, uriPrefix, baseURI, localname, clazz, val);
	}


	/**
	 * @param property
	 *            The URI of a property to read
	 * @param clazz
	 *            The class of the value to be retrieved.
	 * @param <T>
	 *            The class of the value to be retrieved.
	 * @return a value containing the found value(s) or none.
	 */
	public <T> Value<T> read(final URI property, final Class<T> clazz) {
		return this.applyOperator(Operator.READ, null, property, clazz, null);
	}


	/**
	 * @param uriPrefix
	 *            the prefix
	 * @param property
	 *            the the property
	 * @param clazz
	 *            the class of the value
	 * @param <T>
	 *            The clas of the value
	 * @param val
	 *            the value
	 * @return The PoK where the statement has been added.
	 */
	public <T> PieceOfKnowledge write(final String uriPrefix, final URI property, final Class<T> clazz, final T val) {
		return this.applyOperator(Operator.WRITE, uriPrefix, property, clazz, val).getIsAnnotatedOn();
	}


}
