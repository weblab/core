/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.model.structure;

import java.net.URI;

import javax.annotation.Generated;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * WebLab Exchange model used for document structure description v0.1.
 *
 * This version includes relationships used for document structure description
 *
 */
@Generated(value = "org.ow2.weblab.rdf.OWL2Annotator", date = "2013-03-21T16:55:57.558+01:00")
public class WStructureAnnotator extends BaseAnnotator {


	protected static final String APREFIX = "wstructure";


	protected static final URI AURI = URI.create("http://weblab.ow2.org/ontology/structure#");


	protected static final String IS_BOLD = "isBold";


	protected static final String HAS_DESCRIPTION = "hasDescription";


	protected static final String HAS_TOP = "hasTop";


	protected static final String HAS_IMAGE = "hasImage";


	protected static final String HAS_FONT = "hasFont";


	protected static final String HAS_LEFT = "hasLeft";


	protected static final String IS_ITALIC = "isItalic";


	protected static final String HAS_NAME = "hasName";


	protected static final String HAS_HREF = "hasHref";


	protected static final String HAS_STYLE = "hasStyle";


	protected static final String HAS_ALT = "hasAlt";


	protected static final String HAS_PAGE_NUMBER = "hasPageNumber";


	protected static final String HAS_DIV_NUMBER = "hasDivNumber";


	/**
	 * WStructureAnnotator constructor on a PieceOfKnowledge using given URI as the default subject.
	 *
	 * @param subject
	 *            The initial subject of first read/written statements
	 * @param pieceOfKnowledge
	 *            The PieceOfKnowledge to be used for reading/writing RDF statements
	 */
	public WStructureAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * WStructureAnnotator constructor on a resource.
	 *
	 * @param resource
	 *            The resource on which to read/write RDF statements
	 */
	public WStructureAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * Read the Indicates whether or not a Segment is format Bold.
	 *
	 * @return a Boolean
	 */
	public final Value<Boolean> readBold() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.IS_BOLD, Boolean.class, null);
	}


	/**
	 * Write the Indicates whether or not a Segment is format Bold.
	 *
	 * @param value
	 *            a Boolean
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeBold(final Boolean value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.IS_BOLD, Boolean.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A Font has Description
	 *
	 * @return a String
	 */
	public final Value<String> readDescription() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_DESCRIPTION, String.class, null);
	}


	/**
	 * Write the A Font has Description
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDescription(final String value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_DESCRIPTION, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The Image has Top
	 *
	 * @return a String
	 */
	public final Value<String> readTop() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_TOP, String.class, null);
	}


	/**
	 * Write the The Image has Top
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTop(final String value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_TOP, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A Page has Image
	 *
	 * @return an URI
	 */
	public final Value<URI> readImage() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_IMAGE, URI.class, null);
	}


	/**
	 * Write the A Page has Image
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeImage(final URI value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_IMAGE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A Document and a Text have Font
	 *
	 * @return an URI
	 */
	public final Value<URI> readFont() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_FONT, URI.class, null);
	}


	/**
	 * Write the A Document and a Text have Font
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFont(final URI value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_FONT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The Image has Left
	 *
	 * @return a String
	 */
	public final Value<String> readLeft() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_LEFT, String.class, null);
	}


	/**
	 * Write the The Image has Left
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLeft(final String value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_LEFT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Indicates whether or not a Segment is format Italic.
	 *
	 * @return a Boolean
	 */
	public final Value<Boolean> readItalic() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.IS_ITALIC, Boolean.class, null);
	}


	/**
	 * Write the Indicates whether or not a Segment is format Italic.
	 *
	 * @param value
	 *            a Boolean
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeItalic(final Boolean value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.IS_ITALIC, Boolean.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The Image and the Font have Name.
	 *
	 * @return a String
	 */
	public final Value<String> readName() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_NAME, String.class, null);
	}


	/**
	 * Write the The Image and the Font have Name.
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeName(final String value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_NAME, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The Link has href
	 *
	 * @return a URI
	 */
	public final Value<URI> readHref() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_HREF, URI.class, null);
	}


	/**
	 * Write the The Link has href
	 *
	 * @param value
	 *            a URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeHref(final URI value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_HREF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The Image, the Div and the Page have Style
	 *
	 * @return a String
	 */
	public final Value<String> readStyle() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_STYLE, String.class, null);
	}


	/**
	 * Write the The Image, the Div and the Page have Style
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeStyle(final String value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_STYLE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the An Image has Alt
	 *
	 * @return a String
	 */
	public final Value<String> readAlt() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_ALT, String.class, null);
	}


	/**
	 * Write the An Image has Alt
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAlt(final String value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_ALT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A page number (defines the order of the pages in the document)
	 *
	 * @return an Integer
	 */
	public Value<Integer> readPageNumber() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_PAGE_NUMBER, Integer.class, null);
	}


	/**
	 * Write the A page number (defines the order of the pages in the document)
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public PieceOfKnowledge writePageNumber(final Integer value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_PAGE_NUMBER, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A div number (defines the order of the divs in the page)
	 *
	 * @return an Integer
	 */
	public Value<Integer> readDivNumber() {
		return this.applyOperator(Operator.READ, null, WStructureAnnotator.AURI, WStructureAnnotator.HAS_DIV_NUMBER, Integer.class, null);
	}


	/**
	 * Write the A div number (defines the order of the divs in the page)
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public PieceOfKnowledge writeDivNumber(final Integer value) {
		return this.applyOperator(Operator.WRITE, WStructureAnnotator.APREFIX, WStructureAnnotator.AURI, WStructureAnnotator.HAS_DIV_NUMBER, Integer.class, value).getIsAnnotatedOn();
	}

}
