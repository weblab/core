/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.model.processing;

import java.net.URI;
import java.util.Date;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * WebLab Exchange model used for resources processing v0.6.
 *
 * This version includes relationships used for WebLab Resources processing.
 *
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 */
public class WProcessingAnnotator extends BaseAnnotator {


	private static final String APREFIX = WebLabProcessing.PREFERRED_PREFIX;


	private static final URI AURI = URI.create(WebLabProcessing.NAMESPACE);


	@Deprecated
	private static final String AVAILABLE_TRANSLATION = "availableTranslation";


	private static final String CAN_BE_IGNORED = "canBeIgnored";


	private static final String HAS_AVAILABLE_TRANSLATION_IN = "availableTranslation";


	private static final String HAS_BEEN_UPDATED_BY = "hasBeenUpdatedBy";


	private static final String HAS_CLUSTER_PROXIMITY = "hasClusterProximity";


	private static final String HAS_CREDIBILITY = "hasCredibility";


	private static final String HAS_DIARISATION_ID = "hasDiarisationId";


	private static final String HAS_GATHERING_DATE = "hasGatheringDate";


	private static final String HAS_GATHERING_SEED = "hasGatheringSeed";


	private static final String HAS_MAIN_DATE = "hasMainDate";


	private static final String HAS_NATIVE_CONTENT = "hasNativeContent";


	private static final String HAS_NORMALISED_CONTENT = "hasNormalisedContent";


	private static final String HAS_ORIGINAL_FILE_NAME = "hasOriginalFileName";


	private static final String HAS_ORIGINAL_FILE_SIZE = "hasOriginalFileSize";


	private static final String HAS_POLARITY = "hasPolarity";


	private static final String HAS_RATING = "hasRating";


	private static final String HAS_RELIABILITY = "hasReliability";


	private static final String HAS_SENTIMENT = "hasSentiment";


	private static final String HAS_SPEAKER = "hasSpeaker";


	private static final String HAS_TOPIC = "hasTopic";


	private static final String IS_CANDIDATE = "isCandidate";


	private static final String IS_CLEANSING_OF = "isCleansingOf";


	private static final String IS_COMPOSED_BY_CLUSTER = "isComposedByCluster";


	private static final String IS_DELIMITER_OF = "isDelimiterOf";


	private static final String IS_DELIMITER_OF_CLEANSING_UNIT = "isDelimiterOfCleansingUnit";


	private static final String IS_DELIMITER_OF_FRAME_UNIT = "isDelimiterOfFrameUnit";


	private static final String IS_DELIMITER_OF_RECOGNITION_UNIT = "isDelimiterOfRecognitionUnit";


	private static final String IS_DELIMITER_OF_SHOT_UNIT = "isDelimiterOfShotUnit";


	private static final String IS_DELIMITER_OF_SOUNDTRACK_UNIT = "isDelimiterOfSoundtrackUnit";


	private static final String IS_DELIMITER_OF_SUMMARY_UNIT = "isDelimiterOfSummaryUnit";


	private static final String IS_DELIMITER_OF_SYNTHESIS_UNIT = "isDelimiterOfSynthesisUnit";


	private static final String IS_DELIMITER_OF_TRANSCRIPT_UNIT = "isDelimiterOfTranscriptUnit";


	private static final String IS_DELIMITER_OF_TRANSLATION_UNIT = "isDelimiterOfTranslationUnit";


	private static final String IS_EXPOSED_AS = "isExposedAs";


	private static final String IS_EXPOSED_AS_THUMBNAIL = "isExposedAsThumbnail";


	private static final String IS_FRAME_OF = "isFrameOf";


	private static final String IS_GENERATED_FROM = "isGeneratedFrom";


	private static final String IS_IN_CLUSTER = "isInCluster";


	private static final String IS_IN_CLUSTER_LINK = "isInClusterLink";


	private static final String IS_LOCATED_AT = "isLocatedAt";


	private static final String IS_PRODUCED_BY = "isProducedBy";


	private static final String IS_RECOGNITION_OF = "isRecognitionOf";


	private static final String IS_SHOT_OF = "isShotOf";


	private static final String IS_SOUNDTRACK_OF = "isSoundtrackOf";


	private static final String IS_SUMMARY_OF = "isSummaryOf";


	private static final String IS_SYNTHESIS_OF = "isSynthesisOf";


	private static final String IS_TO_KEEP = "isToKeep";


	private static final String IS_TRANSCRIPT_OF = "isTranscriptOf";


	private static final String IS_TRANSLATION_OF = "isTranslationOf";


	private static final String REFERS_TO = "refersTo";


	/**
	 * WProcessingAnnotator constructor on a resource.
	 *
	 * @param resource
	 *            The resource on which to read/write RDF statements
	 */
	public WProcessingAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * WProcessingAnnotator constructor on a PieceOfKnowledge using given URI as the default subject.
	 *
	 * @param subject
	 *            The initial subject of first read/written statements
	 * @param pieceOfKnowledge
	 *            The PieceOfKnowledge to be used for reading/writing RDF statements
	 */
	public WProcessingAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * Read the language of the available translation.
	 *
	 * @return a String
	 */
	public final Value<String> readAvailableTranslation() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.AVAILABLE_TRANSLATION, String.class, null);
	}


	/**
	 * Read the language of the available translation
	 *
	 * @return a String
	 */
	public final Value<String> readAvailableTranslationIn() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_AVAILABLE_TRANSLATION_IN, String.class, null);
	}


	/**
	 * Read the Links a resource to the service that has modified this resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readBeenUpdatedBy() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_BEEN_UPDATED_BY, URI.class, null);
	}


	/**
	 * Read the Indicates whether or not a MediaUnit can be ignored by a service.
	 *
	 * @return a Boolean
	 */
	public final Value<Boolean> readCanBeIgnored() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.CAN_BE_IGNORED, Boolean.class, null);
	}


	/**
	 * Read the Indicates whether or not is the instance extracted a candidate instance.
	 *
	 * @return a Boolean
	 */
	public final Value<Boolean> readCandidate() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_CANDIDATE, Boolean.class, null);
	}


	/**
	 * Read the Links a cleansed MediaUnit to its source MediaUnit.
	 *
	 * @return an URI
	 */
	public final Value<URI> readCleansingOf() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_CLEANSING_OF, URI.class, null);
	}


	/**
	 * Read the Value that corresponds to the proximity between the two clusters implied in a cluster link.
	 *
	 * @return a Double
	 */
	public final Value<Double> readClusterProximity() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_CLUSTER_PROXIMITY, Double.class, null);
	}


	/**
	 * Read the Link a ClusterLink with its two components clusters.
	 *
	 * @return an URI
	 */
	public final Value<URI> readComposedByCluster() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_COMPOSED_BY_CLUSTER, URI.class, null);
	}


	/**
	 * Read the credibility. Should be an integer between 0 and 6 included.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readCredibility() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_CREDIBILITY, Integer.class, null);
	}


	/**
	 * Read the Links a segment to the MediaUnit it represents.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDelimiterOf() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF, URI.class, null);
	}


	/**
	 * Read the Links a segment to the cleansing MediaUnit it represents.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDelimiterOfCleansingUnit() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_CLEANSING_UNIT, URI.class, null);
	}


	/**
	 * Read the Links a segment to the frame MediaUnit it represents.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDelimiterOfFrameUnit() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_FRAME_UNIT, URI.class, null);
	}


	/**
	 * Read the Links a segment to the recognition MediaUnit it represents.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDelimiterOfRecognitionUnit() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_RECOGNITION_UNIT, URI.class, null);
	}


	/**
	 * Read the Links a segment to the shot MediaUnit it represents.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDelimiterOfShotUnit() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_SHOT_UNIT, URI.class, null);
	}


	/**
	 * Read the Links a segment to the soundtrack MediaUnit it represents.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDelimiterOfSoundtrackUnit() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_SOUNDTRACK_UNIT, URI.class, null);
	}


	/**
	 * Read the Links a segment to the summary MediaUnit it represents.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDelimiterOfSummaryUnit() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_SUMMARY_UNIT, URI.class, null);
	}


	/**
	 * Read the Links a segment to the synthesis MediaUnit it represents.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDelimiterOfSynthesisUnit() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_SYNTHESIS_UNIT, URI.class, null);
	}


	/**
	 * Read the Links a segment to the transcript MediaUnit it represents.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDelimiterOfTranscriptUnit() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_TRANSCRIPT_UNIT, URI.class, null);
	}


	/**
	 * Read the Links a segment to the translation MediaUnit it represents.
	 *
	 * @return an URI
	 */
	public final Value<URI> readDelimiterOfTranslationUnit() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_TRANSLATION_UNIT, URI.class, null);
	}


	/**
	 * Read the diarisation id
	 *
	 * @return a String
	 */
	public final Value<String> readDiarisationId() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_DIARISATION_ID, String.class, null);
	}


	/**
	 * Read the Is exposed on a given protocol at a given address (for instance points to an URL).
	 *
	 * @return a String
	 */
	public final Value<String> readExposedAs() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_EXPOSED_AS, String.class, null);
	}


	/**
	 * Read the Is exposed as a given thumbnail at a given address (for instance points to an URL).
	 *
	 * @return a String
	 */
	public final Value<String> readExposedAsThumbnail() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_EXPOSED_AS_THUMBNAIL, String.class, null);
	}


	/**
	 * Read the Links a frame Image to its source Video.
	 *
	 * @return an URI
	 */
	public final Value<URI> readFrameOf() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_FRAME_OF, URI.class, null);
	}


	/**
	 * Read the Gives the date when the Document was gathered.
	 *
	 * @return a Date
	 */
	public final Value<Date> readGatheringDate() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_GATHERING_DATE, Date.class, null);
	}


	/**
	 * Read the gathering seed
	 *
	 * @return a String
	 */
	public final Value<String> readGatheringSeed() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_GATHERING_SEED, String.class, null);
	}


	/**
	 * Read the Links a resource or segment result of an automatic (translation, summarisation, transcription) process to its source resource or segment.
	 *
	 * @return an URI
	 */
	public final Value<URI> readGeneratedFrom() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_GENERATED_FROM, URI.class, null);
	}


	/**
	 * Read the Links a WebLab Document to a Cluster.
	 *
	 * @return an URI
	 */
	public final Value<URI> readInCluster() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_IN_CLUSTER, URI.class, null);
	}


	/**
	 * Read the Links a cluster to a ClusterLink.
	 *
	 * @return an URI
	 */
	public final Value<URI> readInClusterLink() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_IN_CLUSTER_LINK, URI.class, null);
	}


	/**
	 * Read the Links a MediaUnit to the segment from which it has been produced.
	 *
	 * @return an URI
	 */
	public final Value<URI> readLocatedAt() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_LOCATED_AT, URI.class, null);
	}


	/**
	 * Read the Gives the main date of the Document.
	 *
	 * @return a Date
	 */
	public final Value<Date> readMainDate() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_MAIN_DATE, Date.class, null);
	}


	/**
	 * Read the Links a media unit to its native content.
	 *
	 * @return an URI
	 */
	public final Value<URI> readNativeContent() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_NATIVE_CONTENT, URI.class, null);
	}


	/**
	 * Read the Links a media unit to its normalised content.
	 *
	 * @return an URI
	 */
	public final Value<URI> readNormalisedContent() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_NORMALISED_CONTENT, URI.class, null);
	}


	/**
	 * Read the Gives the name of the original file gathered.
	 *
	 * @return a String
	 */
	public final Value<String> readOriginalFileName() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_ORIGINAL_FILE_NAME, String.class, null);
	}


	/**
	 * Read the Gives the number of bytes of the original file gathered.
	 *
	 * @return a Long
	 */
	public final Value<Long> readOriginalFileSize() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_ORIGINAL_FILE_SIZE, Long.class, null);
	}


	/**
	 * Read the polarity. Should be a double between -1.0 and 1.0 included.
	 *
	 * @return a Double
	 */
	public final Value<Double> readPolarity() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_POLARITY, Double.class, null);
	}


	/**
	 * Read the Indicates the service that has produced automatically or semi-automatically the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readProducedBy() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_PRODUCED_BY, URI.class, null);
	}


	/**
	 * Read the rating. Should be an integer between 0 and 5 included.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readRating() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_RATING, Integer.class, null);
	}


	/**
	 * Read the Links an extracted Text to its source Image.
	 *
	 * @return an URI
	 */
	public final Value<URI> readRecognitionOf() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_RECOGNITION_OF, URI.class, null);
	}


	/**
	 * Read the Links a segment to an instance of a recognised entity (given its URI).
	 *
	 * @return an URI
	 */
	public final Value<URI> readRefersTo() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.REFERS_TO, URI.class, null);
	}


	/**
	 * Read the reliability. Should be a single character between A and F included.
	 *
	 * @return a String
	 */
	public final Value<String> readReliability() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_RELIABILITY, String.class, null);
	}


	/**
	 * Read the sentiment
	 *
	 * @return a String
	 */
	public final Value<String> readSentiment() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_SENTIMENT, String.class, null);
	}


	/**
	 * Read the Links a shot Video to its source Video.
	 *
	 * @return an URI
	 */
	public final Value<URI> readShotOf() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_SHOT_OF, URI.class, null);
	}


	/**
	 * Read the Links a soundtrack to its source Video.
	 *
	 * @return an URI
	 */
	public final Value<URI> readSoundtrackOf() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_SOUNDTRACK_OF, URI.class, null);
	}


	/**
	 * Read the speaker URI
	 *
	 * @return an URI
	 */
	public final Value<URI> readSpeaker() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_SPEAKER, URI.class, null);
	}


	/**
	 * Read the Links a summary to its source Text.
	 *
	 * @return an URI
	 */
	public final Value<URI> readSummaryOf() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_SUMMARY_OF, URI.class, null);
	}


	/**
	 * Read the Links a synthesized Audio unit to its source Text.
	 *
	 * @return an URI
	 */
	public final Value<URI> readSynthesisOf() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_SYNTHESIS_OF, URI.class, null);
	}


	/**
	 * Read the Indicates whether or not a resource should be kept.
	 *
	 * @return a Boolean
	 */
	public final Value<Boolean> readToKeep() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_TO_KEEP, Boolean.class, null);
	}


	/**
	 * Read the topic
	 *
	 * @return a String
	 */
	public final Value<String> readTopic() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_TOPIC, String.class, null);
	}


	/**
	 * Read the Links a transcript to its source (Image or Video MediaUnit).
	 *
	 * @return an URI
	 */
	public final Value<URI> readTranscriptOf() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_TRANSCRIPT_OF, URI.class, null);
	}


	/**
	 * Read the Links translated Text or LinearSegment to its source Text or LinearSegment.
	 *
	 * @return an URI
	 */
	public final Value<URI> readTranslationOf() {
		return this.applyOperator(Operator.READ, null, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_TRANSLATION_OF, URI.class, null);
	}


	/**
	 * write the language of the available translation.
	 *
	 * @param value
	 *            a String
	 *
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAvailableTranslation(final String value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.AVAILABLE_TRANSLATION, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the language of the available translation
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAvailableTranslationIn(final String value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_AVAILABLE_TRANSLATION_IN, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a resource to the service that has modified this resource.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeBeenUpdatedBy(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_BEEN_UPDATED_BY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Indicates whether or not a MediaUnit can be ignored by a service.
	 *
	 * @param value
	 *            a Boolean
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCanBeIgnored(final Boolean value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.CAN_BE_IGNORED, Boolean.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Indicates whether or not is the instance extracted a candidate instance.
	 *
	 * @param value
	 *            a Boolean
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCandidate(final Boolean value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_CANDIDATE, Boolean.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a cleansed MediaUnit to its source MediaUnit.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCleansingOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_CLEANSING_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Value that corresponds to the proximity between the two clusters implied in a cluster link.
	 *
	 * @param value
	 *            a Double
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeClusterProximity(final Double value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_CLUSTER_PROXIMITY, Double.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Link a ClusterLink with its two components clusters.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeComposedByCluster(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_COMPOSED_BY_CLUSTER, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the credibility. Should be an integer between 0 and 6 included.
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCredibility(final Integer value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_CREDIBILITY, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to the MediaUnit it represents.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDelimiterOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to the cleansing MediaUnit it represents.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDelimiterOfCleansingUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_CLEANSING_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to the frame MediaUnit it represents.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDelimiterOfFrameUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_FRAME_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to the recognition MediaUnit it represents.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDelimiterOfRecognitionUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_RECOGNITION_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to the shot MediaUnit it represents.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDelimiterOfShotUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_SHOT_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to the soundtrack MediaUnit it represents.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDelimiterOfSoundtrackUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_SOUNDTRACK_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to the summary MediaUnit it represents.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDelimiterOfSummaryUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_SUMMARY_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to the synthesis MediaUnit it represents.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDelimiterOfSynthesisUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_SYNTHESIS_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to the transcript MediaUnit it represents.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDelimiterOfTranscriptUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_TRANSCRIPT_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to the translation MediaUnit it represents.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDelimiterOfTranslationUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_DELIMITER_OF_TRANSLATION_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the diarisation id
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDiarisationId(final String value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_DIARISATION_ID, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Is exposed on a given protocol at a given address (for instance points to an URL).
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeExposedAs(final String value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_EXPOSED_AS, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Is exposed as a given thumbnail at a given address (for instance points to an URL).
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeExposedAsThumbnail(final String value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_EXPOSED_AS_THUMBNAIL, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a frame Image to its source Video.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFrameOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_FRAME_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Gives the date when the Document was gathered.
	 *
	 * @param value
	 *            a Date
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeGatheringDate(final Date value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_GATHERING_DATE, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the gathering seed
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeGatheringSeed(final String value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_GATHERING_SEED, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a resource or segment result of an automatic (translation, summarisation, transcription) process to its source resource or segment.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeGeneratedFrom(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_GENERATED_FROM, URI.class, value).getIsAnnotatedOn();
	}



	/**
	 * Write the Links a WebLab Document to a Cluster.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeInCluster(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_IN_CLUSTER, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a cluster to a ClusterLink.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeInClusterLink(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_IN_CLUSTER_LINK, URI.class, value).getIsAnnotatedOn();
	}



	/**
	 * Write the Links a MediaUnit to the segment from which it has been produced.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLocatedAt(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_LOCATED_AT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Gives the main date of the Document.
	 *
	 * @param value
	 *            a Date
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMainDate(final Date value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_MAIN_DATE, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a media unit to its native content.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeNativeContent(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_NATIVE_CONTENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a media unit to its normalised content.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeNormalisedContent(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_NORMALISED_CONTENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Gives the name of the original file gathered.
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOriginalFileName(final String value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_ORIGINAL_FILE_NAME, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Gives the number of bytes of the original file gathered.
	 *
	 * @param value
	 *            a Long
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOriginalFileSize(final Long value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_ORIGINAL_FILE_SIZE, Long.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the polarity.Should be a double between -1.0 and 1.0 included.
	 *
	 * @param value
	 *            a Double
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePolarity(final Double value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_POLARITY, Double.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Indicates the service that has produced automatically or semi-automatically the resource.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeProducedBy(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_PRODUCED_BY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the rating. Should be an integer between 0 and 5 included.
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRating(final Integer value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_RATING, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links an extracted Text to its source Image.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRecognitionOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_RECOGNITION_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a segment to an instance of a recognised entity (given its URI).
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRefersTo(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.REFERS_TO, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the reliability. Should be a single character between A and F included.
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeReliability(final String value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_RELIABILITY, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the sentiment
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSentiment(final String value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_SENTIMENT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a shot Video to its source Video.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeShotOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_SHOT_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a soundtrack to its source Video.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSoundtrackOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_SOUNDTRACK_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the speaker URI
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSpeaker(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_SPEAKER, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a summary to its source Text.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSummaryOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_SUMMARY_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links a synthesized Audio unit to its source Text.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSynthesisOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_SYNTHESIS_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Indicates whether or not a resource should be kept.
	 *
	 * @param value
	 *            a Boolean
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeToKeep(final Boolean value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_TO_KEEP, Boolean.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the topic
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTopic(final String value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.HAS_TOPIC, String.class, value).getIsAnnotatedOn();
	}



	/**
	 * Write the Links a transcript to its source (Image or Video MediaUnit).
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTranscriptOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_TRANSCRIPT_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Links translated Text or LinearSegment to its source Text or LinearSegment.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTranslationOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WProcessingAnnotator.APREFIX, WProcessingAnnotator.AURI, WProcessingAnnotator.IS_TRANSLATION_OF, URI.class, value).getIsAnnotatedOn();
	}

}
