/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.model.retrieval;

import java.net.URI;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * WebLab Exchange model used for resources retrieval v0.8
 *
 * This version includes relationships used for WebLab Resources retrieval.
 *
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 */
public class WRetrievalAnnotator extends BaseAnnotator {


	protected static final String APREFIX = "wlr";


	protected static final URI AURI = URI.create("http://weblab.ow2.org/core/1.2/ontology/retrieval#");


	protected static final String HAS_ORDERED_QUERY = "hasOrderedQuery";


	protected static final String IN_RESULT_SET_HIT = "inResultSetHit";


	protected static final String HAS_SCORE = "hasScore";


	protected static final String IS_RESULT_OF = "isResultOf";


	protected static final String TO_BE_RANKED_ASCENDING = "toBeRankedAscending";


	protected static final String HAS_RELEVANT_SEGMENT = "hasRelevantSegment";


	protected static final String IS_EXPRESSED_WITH = "isExpressedWith";


	protected static final String HAS_RANK = "hasRank";


	protected static final String HAS_QUERY_OFFSET = "hasQueryOffset";


	protected static final String TO_BE_RANKED_BY = "toBeRankedBy";


	protected static final String IS_LINKED_TO = "isLinkedTo";


	protected static final String HAS_EXPECTED_OFFSET = "hasExpectedOffset";


	protected static final String HAS_HIT = "hasHit";


	protected static final String HAS_RELEVANT_MEDIA_UNIT = "hasRelevantMediaUnit";


	protected static final String IS_RANKED_ASCENDING = "isRankedAscending";


	protected static final String HAS_EXPECTED_LIMIT = "hasExpectedLimit";


	protected static final String HAS_DESCRIPTION = "hasDescription";


	protected static final String HAS_SCOPE = "hasScope";


	protected static final String HAS_FACET_SUGGESTION = "hasFacetSuggestion";


	protected static final String IS_RANKED_BY = "isRankedBy";


	protected static final String HAS_NUMBER_OF_RESULTS = "hasNumberOfResults";


	/**
	 * WRetrievalAnnotator constructor on a PieceOfKnowledge using given URI as the default subject.
	 *
	 * @param subject
	 *            The initial subject of first read/written statements
	 * @param pieceOfKnowledge
	 *            The PieceOfKnowledge to be used for reading/writing RDF statements
	 */
	public WRetrievalAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * WRetrievalAnnotator constructor on a resource.
	 *
	 * @param resource
	 *            The resource on which to read/write RDF statements
	 */
	public WRetrievalAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * Read the Gives a link to the requested query for this order.
	 *
	 * @return an URI
	 */
	public final Value<URI> readOrderedQuery() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_ORDERED_QUERY, URI.class, null);
	}


	/**
	 * Write the Gives a link to the requested query for this order.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeOrderedQuery(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_ORDERED_QUERY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a Hit to its ResultSet.
	 *
	 * @return an URI
	 */
	public final Value<URI> readInResultSetHit() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IN_RESULT_SET_HIT, URI.class, null);
	}


	/**
	 * Write the Links a Hit to its ResultSet.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeInResultSetHit(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IN_RESULT_SET_HIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Gives a score for hit.
	 *
	 * @return a Double
	 */
	public final Value<Double> readScore() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_SCORE, Double.class, null);
	}


	/**
	 * Write the Gives a score for hit.
	 *
	 * @param value
	 *            a Double
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeScore(final Double value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_SCORE, Double.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Links a ResultSet to the Query that has been used to retrieve the set of results.
	 *
	 * @return an URI
	 */
	public final Value<URI> readResultOf() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IS_RESULT_OF, URI.class, null);
	}


	/**
	 * Write the Links a ResultSet to the Query that has been used to retrieve the set of results.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeResultOf(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IS_RESULT_OF, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Default ranking mode is descendant, this property allow to ask for results ranked ascendingly.
	 *
	 * @return a Boolean
	 */
	public final Value<Boolean> readToBeRankedAscending() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.TO_BE_RANKED_ASCENDING, Boolean.class, null);
	}


	/**
	 * Write the Default ranking mode is descendant, this property allow to ask for results ranked ascendingly.
	 *
	 * @param value
	 *            a Boolean
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeToBeRankedAscending(final Boolean value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.TO_BE_RANKED_ASCENDING, Boolean.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Can be used to point out a specific Segment inside the Resource that is linked to a Hit. This is an optional property of a Hit. But if present
	 * it MUST be combined
	 * with #hasRelevantMediaUnit (to locate the MediaUnit that contains the Segment).
	 *
	 *
	 * @return an URI
	 */
	public final Value<URI> readRelevantSegment() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_RELEVANT_SEGMENT, URI.class, null);
	}


	/**
	 * Write the Can be used to point out a specific Segment inside the Resource that is linked to a Hit. This is an optional property of a Hit. But if present
	 * it MUST be combined
	 * with #hasRelevantMediaUnit (to locate the MediaUnit that contains the Segment).
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRelevantSegment(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_RELEVANT_SEGMENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Gives the language of the query .
	 *
	 * @return a String
	 */
	public final Value<String> readExpressedWith() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IS_EXPRESSED_WITH, String.class, null);
	}


	/**
	 * Write the Gives the language of the query .
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeExpressedWith(final String value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IS_EXPRESSED_WITH, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Gives a rank for hit.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readRank() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_RANK, Integer.class, null);
	}


	/**
	 * Write the Gives a rank for hit.
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRank(final Integer value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_RANK, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Provides the offset about the query in the case of a result set returns by a search service.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readQueryOffset() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_QUERY_OFFSET, Integer.class, null);
	}


	/**
	 * Write the Provides the offset about the query in the case of a result set returns by a search service.
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeQueryOffset(final Integer value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_QUERY_OFFSET, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Define what is the expected property to be used to rank the results of a request when the traditional relevance is not the default ranking to
	 * use.
	 *
	 * @return an URI
	 */
	public final Value<URI> readToBeRankedBy() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.TO_BE_RANKED_BY, URI.class, null);
	}


	/**
	 * Write the Define what is the expected property to be used to rank the results of a request when the traditional relevance is not the default ranking to
	 * use.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeToBeRankedBy(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.TO_BE_RANKED_BY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Gives a link to resource for hit.
	 *
	 * @return an URI
	 */
	public final Value<URI> readLinkedTo() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IS_LINKED_TO, URI.class, null);
	}


	/**
	 * Write the Gives a link to resource for hit.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLinkedTo(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IS_LINKED_TO, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Gives the expected offset for the requested query for this order.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readExpectedOffset() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_EXPECTED_OFFSET, Integer.class, null);
	}


	/**
	 * Write the Gives the expected offset for the requested query for this order.
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeExpectedOffset(final Integer value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_EXPECTED_OFFSET, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Gives a hit for ResultSet.
	 *
	 * @return an URI
	 */
	public final Value<URI> readHit() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_HIT, URI.class, null);
	}


	/**
	 * Write the Gives a hit for ResultSet.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeHit(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_HIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Can be used to point out a specific MediaUnit inside the Resource that is linked to a Hit. This is an optional property of a Hit.
	 *
	 * @return an URI
	 */
	public final Value<URI> readRelevantMediaUnit() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_RELEVANT_MEDIA_UNIT, URI.class, null);
	}


	/**
	 * Write the Can be used to point out a specific MediaUnit inside the Resource that is linked to a Hit. This is an optional property of a Hit.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRelevantMediaUnit(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_RELEVANT_MEDIA_UNIT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Default ranking mode is descending, this property allow to state that the result set has been ranked ascendingly.
	 *
	 * @return a Boolean
	 */
	public final Value<Boolean> readRankedAscending() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IS_RANKED_ASCENDING, Boolean.class, null);
	}


	/**
	 * Write the Default ranking mode is descending, this property allow to state that the result set has been ranked ascendingly.
	 *
	 * @param value
	 *            a Boolean
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRankedAscending(final Boolean value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IS_RANKED_ASCENDING, Boolean.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Gives the limit for the requested query for this order.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readExpectedLimit() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_EXPECTED_LIMIT, Integer.class, null);
	}


	/**
	 * Write the Gives the limit for the requested query for this order.
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeExpectedLimit(final Integer value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_EXPECTED_LIMIT, Integer.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Gives a description for a hit.
	 *
	 * @return a String
	 */
	public final Value<String> readDescription() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_DESCRIPTION, String.class, null);
	}


	/**
	 * Write the Gives a description for a hit.
	 *
	 * @param value
	 *            a String
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDescription(final String value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_DESCRIPTION, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Defines on which property values the query applies. For instance, having dc:title has value, means that the query aims at searching a Resource
	 * whose title matches the query.
	 *
	 * @return an URI
	 */
	public final Value<URI> readScope() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_SCOPE, URI.class, null);
	}


	/**
	 * Write the Defines on which property values the query applies. For instance, having dc:title has value, means that the query aims at searching a Resource
	 * whose title matches the query.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeScope(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_SCOPE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the suggested facet.
	 *
	 * @return an URI
	 */
	public final Value<URI> readFacetSuggestion() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_FACET_SUGGESTION, URI.class, null);
	}


	/**
	 * Write the suggested facet.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFacetSuggestion(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_FACET_SUGGESTION, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Defines what property is used to rank the results when the traditional relevance is not the default ranking to use.
	 *
	 * @return an URI
	 */
	public final Value<URI> readRankedBy() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IS_RANKED_BY, URI.class, null);
	}


	/**
	 * Write the Defines what property is used to rank the results when the traditional relevance is not the default ranking to use.
	 *
	 * @param value
	 *            an URI
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRankedBy(final URI value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.IS_RANKED_BY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Gives the total number of results for this result set.
	 *
	 * @return an Integer
	 */
	public final Value<Integer> readNumberOfResults() {
		return this.applyOperator(Operator.READ, null, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_NUMBER_OF_RESULTS, Integer.class, null);
	}


	/**
	 * Write the Gives the total number of results for this result set.
	 *
	 * @param value
	 *            an Integer
	 * @return a PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeNumberOfResults(final Integer value) {
		return this.applyOperator(Operator.WRITE, WRetrievalAnnotator.APREFIX, WRetrievalAnnotator.AURI, WRetrievalAnnotator.HAS_NUMBER_OF_RESULTS, Integer.class, value).getIsAnnotatedOn();
	}

}
