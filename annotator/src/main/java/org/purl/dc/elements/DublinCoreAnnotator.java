/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.purl.dc.elements;

import java.net.URI;
import java.util.Date;

import org.ow2.weblab.core.annotator.AbstractAnnotator;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * Dublin Core WebLab annotator
 *
 * @author asaval
 */
public class DublinCoreAnnotator extends AbstractAnnotator {


	protected static final String APREFIX = "dc";


	protected static final URI AURI = URI.create("http://purl.org/dc/elements/1.1/");


	protected static final String CONTRIBUTOR = "contributor";


	protected static final String COVERAGE = "coverage";


	protected static final String CREATOR = "creator";


	protected static final String DATE = "date";


	protected static final String DESCRIPTION = "description";


	protected static final String FORMAT = "format";


	protected static final String IDENTIFIER = "identifier";


	protected static final String LANGUAGE = "language";


	protected static final String PUBLISHER = "publisher";


	protected static final String RELATION = "relation";


	protected static final String RIGHTS = "rights";


	protected static final String SOURCE = "source";


	protected static final String SUBJECT = "subject";


	protected static final String TITLE = "title";


	protected static final String TYPE = "type";


	/**
	 * DublinCoreAnnotator constructor on a resource.
	 * 
	 * @param resource
	 *            The resource on which to read/write (in its annotations) and being the default subject of the statements
	 */
	public DublinCoreAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * DublinCoreAnnotator constructor on a PieceOfKnowledge using given URI as the default subject.
	 * 
	 * @param subject
	 *            the default subject of the properties to read/write
	 * @param pieceOfKnowledge
	 *            The PoK to read/write into
	 */
	public DublinCoreAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * Read the An entity responsible for making contributions to the resource.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readContributor() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.CONTRIBUTOR, String.class, null);
	}


	/**
	 * Read the The spatial or temporal topic of the resource, the spatial applicability of the resource, or the jurisdiction under which the resource is relevant.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readCoverage() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.COVERAGE, String.class, null);
	}


	/**
	 * Read the An entity primarily responsible for making the resource.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readCreator() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.CREATOR, String.class, null);
	}


	/**
	 * Read the A point or period of time associated with an event in the lifecycle of the resource.
	 *
	 * @return a Value&lt;Date&gt;
	 */
	public final Value<Date> readDate() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.DATE, Date.class, null);
	}


	/**
	 * Read the An account of the resource.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readDescription() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.DESCRIPTION, String.class, null);
	}


	/**
	 * Read the The file format, physical medium, or dimensions of the resource.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readFormat() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.FORMAT, String.class, null);
	}


	/**
	 * Read the An unambiguous reference to the resource within a given context.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readIdentifier() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.IDENTIFIER, String.class, null);
	}


	/**
	 * Read the A language of the resource.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readLanguage() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.LANGUAGE, String.class, null);
	}


	/**
	 * Read the An entity responsible for making the resource available.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readPublisher() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.PUBLISHER, String.class, null);
	}


	/**
	 * Read the A related resource.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readRelation() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.RELATION, String.class, null);
	}


	/**
	 * Read the Information about rights held in and over the resource.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readRights() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.RIGHTS, String.class, null);
	}


	/**
	 * Read the A related resource from which the described resource is derived.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readSource() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.SOURCE, String.class, null);
	}


	/**
	 * Read the The topic of the resource.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readSubject() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.SUBJECT, String.class, null);
	}


	/**
	 * Read the A name given to the resource.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readTitle() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.TITLE, String.class, null);
	}


	/**
	 * Read the The nature or genre of the resource.
	 *
	 * @return a Value&lt;String&gt;
	 */
	public final Value<String> readType() {
		return this.applyOperator(Operator.READ, null, DublinCoreAnnotator.AURI, DublinCoreAnnotator.TYPE, String.class, null);
	}


	/**
	 * Write the An entity responsible for making contributions to the resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeContributor(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.CONTRIBUTOR, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the The spatial or temporal topic of the resource, the spatial applicability of the resource, or the jurisdiction under which the resource is relevant.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCoverage(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.COVERAGE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the An entity primarily responsible for making the resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCreator(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.CREATOR, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the A point or period of time associated with an event in the lifecycle of the resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDate(final Date value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.DATE, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the An account of the resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDescription(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.DESCRIPTION, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the The file format, physical medium, or dimensions of the resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFormat(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.FORMAT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the An unambiguous reference to the resource within a given context.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeIdentifier(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.IDENTIFIER, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the A language of the resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLanguage(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.LANGUAGE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the An entity responsible for making the resource available.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePublisher(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.PUBLISHER, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the A related resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRelation(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.RELATION, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the Information about rights held in and over the resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRights(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.RIGHTS, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the A related resource from which the described resource is derived.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSource(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.SOURCE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the The topic of the resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSubject(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.SUBJECT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the A name given to the resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTitle(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.TITLE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Write the The nature or genre of the resource.
	 *
	 * @param value
	 *            a String
	 * @return the modified PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeType(final String value) {
		return this.applyOperator(Operator.WRITE, DublinCoreAnnotator.APREFIX, DublinCoreAnnotator.AURI, DublinCoreAnnotator.TYPE, String.class, value).getIsAnnotatedOn();
	}

}
