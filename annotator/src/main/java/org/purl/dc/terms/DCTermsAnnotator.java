/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.purl.dc.terms;

import java.net.URI;
import java.util.Date;

import org.ow2.weblab.core.annotator.AbstractAnnotator;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * DC Terms WebLab annotator
 *
 * @author asaval
 *
 */
public class DCTermsAnnotator extends AbstractAnnotator {


	protected static final String APREFIX = "dcterms";


	protected static final URI AURI = URI.create("http://purl.org/dc/terms/");


	protected static final String IS_REQUIRED_BY = "isRequiredBy";


	protected static final String IDENTIFIER = "identifier";


	protected static final String IS_FORMAT_OF = "isFormatOf";


	protected static final String VALID = "valid";


	protected static final String INSTRUCTIONAL_METHOD = "instructionalMethod";


	protected static final String MEDIUM = "medium";


	protected static final String MEDIATOR = "mediator";


	protected static final String FORMAT = "format";


	protected static final String RELATION = "relation";


	protected static final String SOURCE = "source";


	protected static final String IS_REFERENCED_BY = "isReferencedBy";


	protected static final String RIGHTS = "rights";


	protected static final String ABSTRACT = "abstract";


	protected static final String AUDIENCE = "audience";


	protected static final String BIBLIOGRAPHIC_CITATION = "bibliographicCitation";


	protected static final String LANGUAGE = "language";


	protected static final String LICENSE = "license";


	protected static final String CONFORMS_TO = "conformsTo";


	protected static final String ACCRUAL_POLICY = "accrualPolicy";


	protected static final String DATE_SUBMITTED = "dateSubmitted";


	protected static final String IS_VERSION_OF = "isVersionOf";


	protected static final String RIGHTS_HOLDER = "rightsHolder";


	protected static final String CREATED = "created";


	protected static final String ACCRUAL_METHOD = "accrualMethod";


	protected static final String CREATOR = "creator";


	protected static final String TEMPORAL = "temporal";


	protected static final String ACCESS_RIGHTS = "accessRights";


	protected static final String ISSUED = "issued";


	protected static final String DESCRIPTION = "description";


	protected static final String IS_REPLACED_BY = "isReplacedBy";


	protected static final String AVAILABLE = "available";


	protected static final String REPLACES = "replaces";


	protected static final String DATE = "date";


	protected static final String TABLE_OF_CONTENTS = "tableOfContents";


	protected static final String REQUIRES = "requires";


	protected static final String ACCRUAL_PERIODICITY = "accrualPeriodicity";


	protected static final String HAS_FORMAT = "hasFormat";


	protected static final String TITLE = "title";


	protected static final String DATE_ACCEPTED = "dateAccepted";


	protected static final String HAS_VERSION = "hasVersion";


	protected static final String MODIFIED = "modified";


	protected static final String PUBLISHER = "publisher";


	protected static final String SUBJECT = "subject";


	protected static final String CONTRIBUTOR = "contributor";


	protected static final String TYPE = "type";


	protected static final String EDUCATION_LEVEL = "educationLevel";


	protected static final String DATE_COPYRIGHTED = "dateCopyrighted";


	protected static final String REFERENCES = "references";


	protected static final String PROVENANCE = "provenance";


	protected static final String EXTENT = "extent";


	protected static final String ALTERNATIVE = "alternative";


	protected static final String IS_PART_OF = "isPartOf";


	protected static final String HAS_PART = "hasPart";


	protected static final String COVERAGE = "coverage";


	protected static final String SPATIAL = "spatial";


	/**
	 * DCTermsAnnotator constructor on a PieceOfKnowledge using given URI as the default subject.
	 *
	 * @param subject
	 *            the subject on which annotate.
	 * @param pieceOfKnowledge
	 *            the piece of knowledge that will contains statements.
	 */
	public DCTermsAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * DCTermsAnnotator constructor on a resource.
	 *
	 * @param resource
	 *            the resource to annotate.
	 */
	public DCTermsAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * Read the A related resource that requires the described resource to support its function, delivery, or coherence.
	 *
	 * @return a String
	 */
	public final Value<String> readRequiredBy() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_REQUIRED_BY, String.class, null);
	}


	/**
	 * Write the A related resource that requires the described resource to support its function, delivery, or coherence.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRequiredBy(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_REQUIRED_BY, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the An unambiguous reference to the resource within a given context.
	 *
	 * @return a String
	 */
	public final Value<String> readIdentifier() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.IDENTIFIER, String.class, null);
	}


	/**
	 * Write the An unambiguous reference to the resource within a given context.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeIdentifier(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.IDENTIFIER, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource that is substantially the same as the described resource, but in another format.
	 *
	 * @return a String
	 */
	public final Value<String> readFormatOf() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_FORMAT_OF, String.class, null);
	}


	/**
	 * Write the A related resource that is substantially the same as the described resource, but in another format.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFormatOf(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_FORMAT_OF, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Date (often a range) of validity of a resource.
	 *
	 * @return a String
	 */
	public final Value<Date> readValid() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.VALID, Date.class, null);
	}


	/**
	 * Write the Date (often a range) of validity of a resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeValid(final Date value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.VALID, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A process, used to engender knowledge, attitudes and skills, that the described resource is designed to support.
	 *
	 * @return an URI
	 */
	public final Value<URI> readInstructionalMethod() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.INSTRUCTIONAL_METHOD, URI.class, null);
	}


	/**
	 * Write the A process, used to engender knowledge, attitudes and skills, that the described resource is designed to support.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeInstructionalMethod(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.INSTRUCTIONAL_METHOD, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The material or physical carrier of the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readMedium() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.MEDIUM, URI.class, null);
	}


	/**
	 * Write the The material or physical carrier of the resource.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMedium(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.MEDIUM, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the An entity that mediates access to the resource and for whom the resource is intended or useful.
	 *
	 * @return an URI
	 */
	public final Value<URI> readMediator() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.MEDIATOR, URI.class, null);
	}


	/**
	 * Write the An entity that mediates access to the resource and for whom the resource is intended or useful.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeMediator(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.MEDIATOR, URI.class, value).getIsAnnotatedOn();
	}

	/**
	 * Read the file format, physical medium, or dimensions of the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readFormat() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.FORMAT, URI.class, null);
	}
	
	
	/**
	 * Write the The file format, physical medium, or dimensions of the resource.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeFormat(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.FORMAT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource.
	 *
	 * @return a String
	 */
	public final Value<String> readRelation() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.RELATION, String.class, null);
	}


	/**
	 * Write the A related resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRelation(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.RELATION, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource from which the described resource is derived.
	 *
	 * @return a String
	 */
	public final Value<String> readSource() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.SOURCE, String.class, null);
	}


	/**
	 * Write the A related resource from which the described resource is derived.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSource(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.SOURCE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource that references, cites, or otherwise points to the described resource.
	 *
	 * @return a String
	 */
	public final Value<String> readReferencedBy() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_REFERENCED_BY, String.class, null);
	}


	/**
	 * Write the A related resource that references, cites, or otherwise points to the described resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeReferencedBy(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_REFERENCED_BY, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Information about rights held in and over the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readRights() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.RIGHTS, URI.class, null);
	}


	/**
	 * Write the Information about rights held in and over the resource.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRights(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.RIGHTS, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A summary of the resource.
	 *
	 * @return a String
	 */
	public final Value<String> readAbstract() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.ABSTRACT, String.class, null);
	}


	/**
	 * Write the A summary of the resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAbstract(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.ABSTRACT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A class of entity for whom the resource is intended or useful.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAudience() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.AUDIENCE, URI.class, null);
	}


	/**
	 * Write the A class of entity for whom the resource is intended or useful.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAudience(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.AUDIENCE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A bibliographic reference for the resource.
	 *
	 * @return a String
	 */
	public final Value<String> readBibliographicCitation() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.BIBLIOGRAPHIC_CITATION, String.class, null);
	}


	/**
	 * Write the A bibliographic reference for the resource.
	 *
	 * @param value
	 *            a PieceOfKnowledge
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeBibliographicCitation(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.BIBLIOGRAPHIC_CITATION, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A language of the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readLanguage() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.LANGUAGE, URI.class, null);
	}


	/**
	 * Write the A language of the resource.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLanguage(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.LANGUAGE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A legal document giving official permission to do something with the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readLicense() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.LICENSE, URI.class, null);
	}


	/**
	 * Write the A legal document giving official permission to do something with the resource.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeLicense(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.LICENSE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the An established standard to which the described resource conforms.
	 *
	 * @return an URI
	 */
	public final Value<URI> readConformsTo() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.CONFORMS_TO, URI.class, null);
	}


	/**
	 * Write the An established standard to which the described resource conforms.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeConformsTo(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.CONFORMS_TO, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The policy governing the addition of items to a collection.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAccrualPolicy() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.ACCRUAL_POLICY, URI.class, null);
	}


	/**
	 * Write the The policy governing the addition of items to a collection.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAccrualPolicy(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.ACCRUAL_POLICY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Date of submission of the resource.
	 *
	 * @return a Date
	 */
	public final Value<Date> readDateSubmitted() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.DATE_SUBMITTED, Date.class, null);
	}


	/**
	 * Write the Date of submission of the resource.
	 *
	 * @param value
	 *            a Date
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDateSubmitted(final Date value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.DATE_SUBMITTED, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource of which the described resource is a version, edition, or adaptation.
	 *
	 * @return a String
	 */
	public final Value<String> readVersionOf() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_VERSION_OF, String.class, null);
	}


	/**
	 * Write the A related resource of which the described resource is a version, edition, or adaptation.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeVersionOf(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_VERSION_OF, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A person or organization owning or managing rights over the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readRightsHolder() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.RIGHTS_HOLDER, URI.class, null);
	}


	/**
	 * Write the A person or organization owning or managing rights over the resource.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRightsHolder(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.RIGHTS_HOLDER, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Date of creation of the resource.
	 *
	 * @return a Date
	 */
	public final Value<Date> readCreated() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.CREATED, Date.class, null);
	}


	/**
	 * Write the Date of creation of the resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCreated(final Date value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.CREATED, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The method by which items are added to a collection.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAccrualMethod() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.ACCRUAL_METHOD, URI.class, null);
	}


	/**
	 * Write the The method by which items are added to a collection.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAccrualMethod(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.ACCRUAL_METHOD, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the An entity primarily responsible for making the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readCreator() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.CREATOR, URI.class, null);
	}


	/**
	 * Write the An entity primarily responsible for making the resource.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCreator(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.CREATOR, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Temporal characteristics of the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readTemporal() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.TEMPORAL, URI.class, null);
	}


	/**
	 * Write the Temporal characteristics of the resource.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTemporal(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.TEMPORAL, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Information about who can access the resource or an indication of its security status.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAccessRights() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.ACCESS_RIGHTS, URI.class, null);
	}


	/**
	 * Write the Information about who can access the resource or an indication of its security status.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAccessRights(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.ACCESS_RIGHTS, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Date of formal issuance (e.g., publication) of the resource.
	 *
	 * @return a Date
	 */
	public final Value<Date> readIssued() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.ISSUED, Date.class, null);
	}


	/**
	 * Write the Date of formal issuance (e.g., publication) of the resource.
	 *
	 * @param value
	 *            a Date
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeIssued(final Date value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.ISSUED, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the An account of the resource.
	 *
	 * @return a String
	 */
	public final Value<String> readDescription() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.DESCRIPTION, String.class, null);
	}


	/**
	 * Write the An account of the resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDescription(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.DESCRIPTION, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource that supplants, displaces, or supersedes the described resource.
	 *
	 * @return a String
	 */
	public final Value<String> readReplacedBy() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_REPLACED_BY, String.class, null);
	}


	/**
	 * Write the A related resource that supplants, displaces, or supersedes the described resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeReplacedBy(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_REPLACED_BY, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Date (often a range) that the resource became or will become available.
	 *
	 * @return a Date
	 */
	public final Value<Date> readAvailable() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.AVAILABLE, Date.class, null);
	}


	/**
	 * Write the Date (often a range) that the resource became or will become available.
	 *
	 * @param value
	 *            a Date
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAvailable(final Date value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.AVAILABLE, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource that is supplanted, displaced, or superseded by the described resource.
	 *
	 * @return a String
	 */
	public final Value<String> readReplaces() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.REPLACES, String.class, null);
	}


	/**
	 * Write the A related resource that is supplanted, displaced, or superseded by the described resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeReplaces(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.REPLACES, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A point or period of time associated with an event in the lifecycle of the resource.
	 *
	 * @return a Date
	 */
	public final Value<Date> readDate() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.DATE, Date.class, null);
	}


	/**
	 * Write the A point or period of time associated with an event in the lifecycle of the resource.
	 *
	 * @param value
	 *            a Date
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDate(final Date value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.DATE, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A list of subunits of the resource.
	 *
	 * @return a String
	 */
	public final Value<String> readTableOfContents() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.TABLE_OF_CONTENTS, String.class, null);
	}


	/**
	 * Write the A list of subunits of the resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTableOfContents(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.TABLE_OF_CONTENTS, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource that is required by the described resource to support its function, delivery, or coherence.
	 *
	 * @return a String
	 */
	public final Value<String> readRequires() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.REQUIRES, String.class, null);
	}


	/**
	 * Write the A related resource that is required by the described resource to support its function, delivery, or coherence.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeRequires(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.REQUIRES, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The frequency with which items are added to a collection.
	 *
	 * @return an URI
	 */
	public final Value<URI> readAccrualPeriodicity() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.ACCRUAL_PERIODICITY, URI.class, null);
	}


	/**
	 * Write the The frequency with which items are added to a collection.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAccrualPeriodicity(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.ACCRUAL_PERIODICITY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the related resource that is substantially the same as the pre-existing described resource, but in another format.
	 *
	 * @return an URI
	 */
	public final Value<URI> readHasFormat() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.HAS_FORMAT, URI.class, null);
	}


	/**
	 * Write the A related resource that is substantially the same as the pre-existing described resource, but in another format.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeHasFormat(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.HAS_FORMAT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A name given to the resource.
	 *
	 * @return a String
	 */
	public final Value<String> readTitle() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.TITLE, String.class, null);
	}


	/**
	 * Write the A name given to the resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeTitle(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.TITLE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Date of acceptance of the resource.
	 *
	 * @return a Date
	 */
	public final Value<Date> readDateAccepted() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.DATE_ACCEPTED, Date.class, null);
	}


	/**
	 * Write the Date of acceptance of the resource.
	 *
	 * @param value
	 *            a Date
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDateAccepted(final Date value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.DATE_ACCEPTED, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource that is a version, edition, or adaptation of the described resource.
	 *
	 * @return a String
	 */
	public final Value<String> readVersion() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.HAS_VERSION, String.class, null);
	}


	/**
	 * Write the A related resource that is a version, edition, or adaptation of the described resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeVersion(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.HAS_VERSION, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Date on which the resource was changed.
	 *
	 * @return a Date
	 */
	public final Value<Date> readModified() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.MODIFIED, Date.class, null);
	}


	/**
	 * Write the Date on which the resource was changed.
	 *
	 * @param value
	 *            a Date
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeModified(final Date value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.MODIFIED, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the An entity responsible for making the resource available.
	 *
	 * @return an URI
	 */
	public final Value<URI> readPublisher() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.PUBLISHER, URI.class, null);
	}


	/**
	 * Write the An entity responsible for making the resource available.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePublisher(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.PUBLISHER, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The topic of the resource.
	 *
	 * @return a String
	 */
	public final Value<String> readSubject() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.SUBJECT, String.class, null);
	}


	/**
	 * Write the The topic of the resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSubject(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.SUBJECT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the An entity responsible for making contributions to the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readContributor() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.CONTRIBUTOR, URI.class, null);
	}


	/**
	 * Write the An entity responsible for making contributions to the resource.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeContributor(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.CONTRIBUTOR, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read The nature or genre of the resource.
	 *
	 * @return an URI
	 */
	public final Value<String> readType() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.TYPE, String.class, null);
	}


	/**
	 * Write The nature or genre of the resource.
	 *
	 * @param value
	 *            a String
	 * @return The PieceOfKnowledge that has been written
	 */
	public final PieceOfKnowledge writeType(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.TYPE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read A class of entity, defined in terms of progression through an educational or training context, for which the described resource is intended.
	 *
	 * @return a String
	 */
	public final Value<URI> readEducationLevel() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.EDUCATION_LEVEL, URI.class, null);
	}


	/**
	 * Write the A class of entity, defined in terms of progression through an educational or training context, for which the described resource is intended.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeEducationLevel(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.EDUCATION_LEVEL, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Date of copyright.
	 *
	 * @return a Date
	 */
	public final Value<Date> readDateCopyrighted() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.DATE_COPYRIGHTED, Date.class, null);
	}


	/**
	 * Write the Date of copyright.
	 *
	 * @param value
	 *            a Date
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeDateCopyrighted(final Date value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.DATE_COPYRIGHTED, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource that is referenced, cited, or otherwise pointed to by the described resource.
	 *
	 * @return a String
	 */
	public final Value<String> readReferences() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.REFERENCES, String.class, null);
	}


	/**
	 * Write the A related resource that is referenced, cited, or otherwise pointed to by the described resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeReferences(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.REFERENCES, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A statement of any changes in ownership and custody of the resource since its creation that are significant for its authenticity, integrity, and
	 * interpretation.
	 *
	 * @return an URI
	 */
	public final Value<URI> readProvenance() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.PROVENANCE, URI.class, null);
	}


	/**
	 * Write the A statement of any changes in ownership and custody of the resource since its creation that are significant for its authenticity, integrity,
	 * and interpretation.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeProvenance(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.PROVENANCE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The size or duration of the resource.
	 *
	 * @return an String
	 */
	public final Value<String> readExtent() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.EXTENT, String.class, null);
	}


	/**
	 * Write the The size or duration of the resource.
	 *
	 * @param value
	 *            an String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeExtent(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.EXTENT, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the An alternative name for the resource.
	 *
	 * @return a String
	 */
	public final Value<String> readAlternative() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.ALTERNATIVE, String.class, null);
	}


	/**
	 * Write the An alternative name for the resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeAlternative(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.ALTERNATIVE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource in which the described resource is physically or logically included.
	 *
	 * @return a String
	 */
	public final Value<String> readPartOf() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_PART_OF, String.class, null);
	}


	/**
	 * Write the A related resource in which the described resource is physically or logically included.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePartOf(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.IS_PART_OF, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the A related resource that is included either physically or logically in the described resource.
	 *
	 * @return a String
	 */
	public final Value<String> readPart() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.HAS_PART, String.class, null);
	}


	/**
	 * Write the A related resource that is included either physically or logically in the described resource.
	 *
	 * @param value
	 *            a String
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writePart(final String value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.HAS_PART, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the The spatial or temporal topic of the resource, the spatial applicability of the resource, or the jurisdiction under which the resource is
	 * relevant.
	 *
	 * @return an URI
	 */
	public final Value<URI> readCoverage() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.COVERAGE, URI.class, null);
	}


	/**
	 * Write the The spatial or temporal topic of the resource, the spatial applicability of the resource, or the jurisdiction under which the resource is
	 * relevant.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeCoverage(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.COVERAGE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Spatial characteristics of the resource.
	 *
	 * @return an URI
	 */
	public final Value<URI> readSpatial() {
		return this.applyOperator(Operator.READ, null, DCTermsAnnotator.AURI, DCTermsAnnotator.SPATIAL, URI.class, null);
	}


	/**
	 * Write the Spatial characteristics of the resource.
	 *
	 * @param value
	 *            an URI
	 * @return PieceOfKnowledge
	 */
	public final PieceOfKnowledge writeSpatial(final URI value) {
		return this.applyOperator(Operator.WRITE, DCTermsAnnotator.APREFIX, DCTermsAnnotator.AURI, DCTermsAnnotator.SPATIAL, URI.class, value).getIsAnnotatedOn();
	}



}
