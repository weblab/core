/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.rdf;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.UserDataHandler;

public class DNode implements Node {


	@Override
	public Node appendChild(final Node newChild) throws DOMException {
		return null;
	}


	@Override
	public Node cloneNode(final boolean deep) {
		return null;
	}


	@Override
	public short compareDocumentPosition(final Node other) throws DOMException {
		return 0;
	}


	@Override
	public NamedNodeMap getAttributes() {
		return null;
	}


	@Override
	public String getBaseURI() {
		return null;
	}


	@Override
	public NodeList getChildNodes() {
		return null;
	}


	@Override
	public Object getFeature(final String feature, final String version) {
		return null;
	}


	@Override
	public Node getFirstChild() {
		return null;
	}


	@Override
	public Node getLastChild() {
		return null;
	}


	@Override
	public String getLocalName() {
		return null;
	}


	@Override
	public String getNamespaceURI() {
		return null;
	}


	@Override
	public Node getNextSibling() {
		return null;
	}


	@Override
	public String getNodeName() {
		return null;
	}


	@Override
	public short getNodeType() {
		return 0;
	}


	@Override
	public String getNodeValue() throws DOMException {
		return null;
	}


	@Override
	public Document getOwnerDocument() {
		return null;
	}


	@Override
	public Node getParentNode() {
		return null;
	}


	@Override
	public String getPrefix() {
		return null;
	}


	@Override
	public Node getPreviousSibling() {
		return null;
	}


	@Override
	public String getTextContent() throws DOMException {
		return null;
	}


	@Override
	public Object getUserData(final String key) {
		return null;
	}


	@Override
	public boolean hasAttributes() {
		return false;
	}


	@Override
	public boolean hasChildNodes() {
		return false;
	}


	@Override
	public Node insertBefore(final Node newChild, final Node refChild) throws DOMException {
		return null;
	}


	@Override
	public boolean isDefaultNamespace(final String namespaceURI) {
		return false;
	}


	@Override
	public boolean isEqualNode(final Node arg) {
		return false;
	}


	@Override
	public boolean isSameNode(final Node other) {
		return false;
	}


	@Override
	public boolean isSupported(final String feature, final String version) {
		return false;
	}


	@Override
	public String lookupNamespaceURI(final String prefix) {
		return null;
	}


	@Override
	public String lookupPrefix(final String namespaceURI) {
		return null;
	}


	@Override
	public void normalize() {
		// do nothing
	}


	@Override
	public Node removeChild(final Node oldChild) throws DOMException {
		return null;
	}


	@Override
	public Node replaceChild(final Node newChild, final Node oldChild) throws DOMException {
		return null;
	}


	@Override
	public void setNodeValue(final String nodeValue) throws DOMException {
		// do nothing
	}


	@Override
	public void setPrefix(final String prefix) throws DOMException {
		// do nothing
	}


	@Override
	public void setTextContent(final String textContent) throws DOMException {
		// do nothing
	}


	@Override
	public Object setUserData(final String key, final Object data, final UserDataHandler handler) {
		return null;
	}


	public class DNamedNodeMap implements NamedNodeMap {


		@Override
		public int getLength() {
			return 0;
		}


		@Override
		public Node getNamedItem(final String name) {
			return null;
		}


		@Override
		public Node getNamedItemNS(final String namespaceURI, final String localName) throws DOMException {
			return null;
		}


		@Override
		public Node item(final int index) {
			return null;
		}


		@Override
		public Node removeNamedItem(final String name) throws DOMException {
			return null;
		}


		@Override
		public Node removeNamedItemNS(final String namespaceURI, final String localName) throws DOMException {
			return null;
		}


		@Override
		public Node setNamedItem(final Node arg) throws DOMException {
			return null;
		}


		@Override
		public Node setNamedItemNS(final Node arg) throws DOMException {
			return null;
		}

	}

}
