/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.rdf;

import java.io.OutputStream;
import java.io.Writer;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Result;


public class LazyXMLParser extends XMLOutputFactory {


	@Override
	public XMLEventWriter createXMLEventWriter(final Result result) {
		return null;
	}


	@Override
	public XMLEventWriter createXMLEventWriter(final OutputStream stream) {
		return null;
	}


	@Override
	public XMLEventWriter createXMLEventWriter(final Writer stream) {
		return null;
	}


	@Override
	public XMLEventWriter createXMLEventWriter(final OutputStream stream, final String encoding) {
		return null;
	}


	@Override
	public XMLStreamWriter createXMLStreamWriter(final Writer stream) {
		// do nothing
		return null;
	}


	@Override
	public XMLStreamWriter createXMLStreamWriter(final OutputStream stream) {
		return null;
	}


	@Override
	public XMLStreamWriter createXMLStreamWriter(final Result result) {
		return new XMLStreamWriter() {


			@Override
			public void writeStartElement(final String prefix, final String localName, final String namespaceURI) {
				// Do nothing
			}


			@Override
			public void writeStartElement(final String namespaceURI, final String localName) {
				// do nothing
			}


			@Override
			public void writeStartElement(final String localName) {
				// do nothing
			}


			@Override
			public void writeStartDocument(final String encoding, final String version) {
				// do nothing
			}


			@Override
			public void writeStartDocument(final String version) {
				// do nothing
			}


			@Override
			public void writeStartDocument() {
				// do nothing
			}


			@Override
			public void writeProcessingInstruction(final String target, final String data) {
				// do nothing
			}


			@Override
			public void writeProcessingInstruction(final String target) {
				// do nothing
			}


			@Override
			public void writeNamespace(final String prefix, final String namespaceURI) {
				// do nothing
			}


			@Override
			public void writeEntityRef(final String name) {
				// do nothing
			}


			@Override
			public void writeEndElement() {
				// do nothing
			}


			@Override
			public void writeEndDocument() {
				// do nothing
			}


			@Override
			public void writeEmptyElement(final String prefix, final String localName, final String namespaceURI) {
				// do nothing
			}


			@Override
			public void writeEmptyElement(final String namespaceURI, final String localName) {
				// do nothing
			}


			@Override
			public void writeEmptyElement(final String localName) {
				// do nothing
			}


			@Override
			public void writeDefaultNamespace(final String namespaceURI) {
				// do nothing
			}


			@Override
			public void writeDTD(final String dtd) {
				// do nothing
			}


			@Override
			public void writeComment(final String data) {
				// do nothing
			}


			@Override
			public void writeCharacters(final char[] text, final int start, final int len) {
				// do nothing
			}


			@Override
			public void writeCharacters(final String text) {
				// do nothing
			}


			@Override
			public void writeCData(final String data) {
				// do nothing
			}


			@Override
			public void writeAttribute(final String prefix, final String namespaceURI, final String localName, final String value) {
				// do nothing
			}


			@Override
			public void writeAttribute(final String namespaceURI, final String localName, final String value) {
				// do nothing
			}


			@Override
			public void writeAttribute(final String localName, final String value) {
				// do nothing
			}


			@Override
			public void setPrefix(final String prefix, final String uri) {
				// do nothing
			}


			@Override
			public void setNamespaceContext(final NamespaceContext context) {
				// do nothing
			}


			@Override
			public void setDefaultNamespace(final String uri) {
				// do nothing
			}


			@Override
			public Object getProperty(final String name) throws IllegalArgumentException {
				return null;
			}


			@Override
			public String getPrefix(final String uri) {
				return null;
			}


			@Override
			public NamespaceContext getNamespaceContext() {
				return null;
			}


			@Override
			public void flush() {
				// do nothing
			}


			@Override
			public void close() {
				// do nothing
			}
		};
	}


	@Override
	public XMLStreamWriter createXMLStreamWriter(final OutputStream stream, final String encoding) {
		return null;
	}


	@Override
	public Object getProperty(final String name) throws IllegalArgumentException {
		return null;
	}


	@Override
	public boolean isPropertySupported(final String name) {
		return false;
	}


	@Override
	public void setProperty(final String name, final Object value) throws IllegalArgumentException {
		// do nothing
	}

}
