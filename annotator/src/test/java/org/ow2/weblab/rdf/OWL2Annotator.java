/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.rdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.ontology.Ontology;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OWL2Annotator {


	private static final String WRITE = "return applyOperator(Operator.WRITE, !!PREFIX!!,!!URI!!, !!FIELD!!, !!CLASS!!.class, value).getIsAnnotatedOn();";


	private static final String READ = "return applyOperator(Operator.READ, null,!!URI!!, !!FIELD!!, !!CLASS!!.class, null);";


	private static final String FIRST_VALUE_COMMENT = "";


	private static final Logger LOGGER = LoggerFactory.getLogger(OWL2Annotator.class);


	private static final String WEBLAB_HEADER = "/**\n * WEBLAB: Service oriented integration platform for media mining and intelligence applications\n * \n *"
			+ " Copyright (C) 2004 - 2017 Airbus Defence and Space\n * \n *" + " This library is free software; you can redistribute it and/or modify it under the terms of\n *"
			+ " the GNU Lesser General Public License as published by the Free Software Foundation; either\n *" + " version 2.1 of the License, or (at your option) any later version.\n * \n *"
			+ " This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;\n *"
			+ " without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n *" + " See the GNU Lesser General Public License for more details.\n * \n *"
			+ " You should have received a copy of the GNU Lesser General Public License along with this\n *"
			+ " library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth\n *" + " Floor, Boston, MA 02110-1301 USA\n */\n\n";


	private static Set<OntProperty> properties = new HashSet<>();


	public static void main(final String[] args) throws WebLabCheckedException {
		if (args == null) {
			OWL2Annotator.LOGGER.error("Please provide an ontology and an optional base URI.");
			throw new WebLabCheckedException("Please provide an ontology and an optional base URI.");
		}
		if (args.length <= 0) {
			OWL2Annotator.LOGGER.error("Please provide an ontology and an optional base URI.");
			throw new WebLabCheckedException("Please provide an ontology and an optional base URI.");
		}
		final String ontology = args[0];
		final String pbase;
		if (args.length > 1) {
			pbase = args[1];
		} else {
			pbase = null;
		}

		final String dir;
		if (args.length > 2) {
			dir = args[2];
		} else {
			dir = "";
		}

		OWL2Annotator.parseOntology(ontology, pbase, dir);
	}


	private static void parseOntology(final String ontology, final String pbase, final String dir) throws WebLabCheckedException {

		OWL2Annotator.LOGGER.debug("Starting OW2Annotator for ontology '" + ontology + "' and base '" + pbase + "'.");

		OWL2Annotator.properties.clear();

		InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(ontology);

		final OntModel ontmodel;
		try {

			if (is == null) {
				OWL2Annotator.LOGGER.debug(ontology + " not found within classpath, try with a new file.");
				final File f = new File(ontology);
				if (f.exists()) {
					try {
						is = new FileInputStream(f);
					} catch (final FileNotFoundException fnfe) {
						OWL2Annotator.LOGGER.error("Unable to open a stream on file " + f.getPath() + ".", fnfe);
						throw new WebLabCheckedException("Unable to open a stream on file " + f.getPath() + ".", fnfe);
					}
				} else {
					OWL2Annotator.LOGGER.error("File " + f.getPath() + " does not exist.");
					throw new WebLabCheckedException("File " + f.getPath() + " does not exist.");
				}
			}

			ontmodel = ModelFactory.createOntologyModel();
			// ontmodel.setStrictMode(false);
			// ontmodel.setDynamicImports(true);

			final org.apache.jena.rdf.model.RDFReader rr = ontmodel.getReader();
			// rr.setProperty("http://apache.org/xml/features/validation/schema",false);
			// rr.setProperty("http://xml.org/sax/features/validation",false);
			// rr.setProperty("http://xml.org/sax/features/external-general-entities",false);
			// rr.setProperty("http://xml.org/sax/features/external-parameter-entities",false);
			// rr.setProperty("http://apache.org/xml/features/nonvalidating/load-dtd-grammar",false);
			// rr.setProperty("http://apache.org/xml/features/nonvalidating/load-external-dtd",false);

			rr.read(ontmodel, is, pbase);

		} finally {
			IOUtils.closeQuietly(is);
		}

		final Map<String, String> namespaces = ontmodel.getNsPrefixMap();

		String base = namespaces.get("");

		if ((base == null) && (pbase == null)) {
			OWL2Annotator.LOGGER.error("No base URI found in the ontology '" + ontology + "'" + ". Please provide one as pbase.");
			throw new WebLabCheckedException("No base URI found in the ontology '" + ontology + "'" + ". Please provide one as pbase.");
		} else if (base == null) {
			OWL2Annotator.LOGGER.debug("No base URI found in the ontology '" + ontology + "'" + ". Using the pbase provided (" + pbase + ").");
			base = pbase;
		}

		OWL2Annotator.loadOWL(base, ontmodel, dir);
	}


	private static void loadOWL(final String base, final OntModel ontmodel, final String dir) throws WebLabCheckedException {
		final ExtendedIterator<OntProperty> it = ontmodel.listAllOntProperties();
		while (it.hasNext()) {
			final OntProperty op = it.next();
			if (!op.isAnon()) {
				if (op.getNameSpace().equals(base)) {
					// Property defined in the namespace of the ontology
					OWL2Annotator.LOGGER.trace("Add property " + op);
					OWL2Annotator.properties.add(op);
				} else {
					// Property not defined in the namespace of the ontology but whose range/domain is.
					if (((op.getRange() != null) && op.getRange().isURIResource() && op.getRange().getURI().startsWith(base))
							|| ((op.getDomain() != null) && op.getDomain().isURIResource() && op.getDomain().getURI().startsWith(base))) {
						OWL2Annotator.LOGGER.debug("Add property " + op + ". Thanks to its range or domain.");
						OWL2Annotator.properties.add(op);
					} else {
						OWL2Annotator.LOGGER.debug("Ignore property " + op);
					}
				}
			} else {
				OWL2Annotator.LOGGER.debug("Ignore anonymous property: " + op);
			}
		}

		final ExtendedIterator<Ontology> io = ontmodel.listOntologies();
		String comment = "";
		if (io.hasNext()) {
			final Ontology o = io.next();
			comment = o.getComment(null);
		}

		try {
			OWL2Annotator.writeAnnotator(base, comment, dir);
		} catch (final IOException ioe) {
			OWL2Annotator.LOGGER.error("An error occur writing the annotator from base " + base + ".", ioe);
			throw new WebLabCheckedException("An error occur writing the annotator from base " + base + ".", ioe);
		}

	}


	private static void writeAnnotator(final String base, final String comment, final String dir) throws IOException {


		final String prefix = "w" + OWL2Annotator.prefixFrom(base);
		final String className = prefix.substring(0, 2).toUpperCase() + prefix.substring(2) + "Annotator";
		final String packageName = OWL2Annotator.packageFrom(base);

		try (final Writer fw = new OutputStreamWriter(new FileOutputStream(dir + className + ".java"), "UTF8")) {
			fw.write(OWL2Annotator.WEBLAB_HEADER);
			fw.write("package " + packageName + ";\n\n");
			fw.write("import java.net.URI;\n");
			fw.write("import java.util.Date;\n\n");

			fw.write("import javax.annotation.Generated;\n\n");

			fw.write("import org.ow2.weblab.rdf.Value;\n");
			fw.write("import org.ow2.weblab.core.model.*;\n");
			fw.write("import org.ow2.weblab.core.annotator.*;\n\n");

			fw.write(OWL2Annotator.generateJavaComment(comment, null, null).replaceAll("\t", " "));
			fw.write(OWL2Annotator.generateClassAnnotation());
			fw.write("public class " + className + " extends BaseAnnotator {\n\n");



			fw.write("\tprotected static final String APREFIX = \"" + prefix + "\";\n");
			fw.write("\tprotected static final URI AURI = URI.create(\"" + base + "\");\n\n");

			final StringBuffer constructors = new StringBuffer();

			constructors.append("\t/**\n\t * " + className + " constructor on a PieceOfKnowledge using given URI as the default subject.\n\t*/\n");
			constructors.append("\tpublic " + className + "(URI subject, PieceOfKnowledge pieceOfKnowledge){\n");
			constructors.append("\t\tsuper(subject, pieceOfKnowledge);\n");
			constructors.append("\t}\n\n");

			constructors.append("\t/**\n\t * " + className + " constructor on a resource.\n\t*/\n");
			constructors.append("\tpublic " + className + "(Resource resource){\n");
			constructors.append("\t\tsuper(resource);\n");
			constructors.append("\t}\n\n");

			OWL2Annotator.writeProperties(prefix, base, fw, constructors.toString());

			fw.write("\n}\n\n");
		}
	}


	private static String generateClassAnnotation() {
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("@Generated(value = \"");
		stringBuilder.append(OWL2Annotator.class.getCanonicalName());
		stringBuilder.append("\", date = \"");
		stringBuilder.append(DateTimeFormatter.ISO_DATE.format(LocalDate.now()));
		stringBuilder.append("\")\n");
		return stringBuilder.toString();
	}


	private static void writeProperties(final String prefix, final String base, final Writer fw, final String contructor) throws IOException {

		final StringWriter sw = new StringWriter();

		final StringWriter pw = new StringWriter();

		for (final OntProperty property : OWL2Annotator.properties) {
			final String type = OWL2Annotator.retrieveType(property, base);

			final String value = property.getLocalName();
			final String field = OWL2Annotator.generateStaticField(OWL2Annotator.cleanProperty(value));
			final String cleanedValue = value.replaceAll("&", "").replace(';', '_');
			final String comment = property.getComment(null);

			final String getter = OWL2Annotator.generateGetter("AURI", cleanedValue, field, type, comment);
			final String setter = OWL2Annotator.generateSetter("APREFIX", "AURI", cleanedValue, field, type, comment);

			pw.append(getter + "\n");
			pw.append(setter + "\n");

			sw.append("\tprotected static final String " + field + " = \"" + value + "\";\n");
		}

		sw.append("\n");

		fw.write(sw.toString());

		fw.write(contructor);

		fw.write(pw.toString());
	}


	private static String cleanProperty(String property) {
		String property2 = property.replaceAll("(\\.|-)", "_");
		if (property2.indexOf("\"") != -1) {
			final int e = property2.indexOf("\"");
			return property2.substring(0, e);
		}
		return property2;
	}


	private static String generateStaticField(final String value) {
		final StringBuffer res = new StringBuffer();
		for (int i = 0; i < value.length(); i++) {
			final char c = value.charAt(i);
			if (!(c + "").toLowerCase().equals("" + c)) {
				res.append("_");
			}
			res.append(c);
		}
		return res.toString().replaceAll("(&|;)", "").toUpperCase();
	}


	private static String retrieveType(final OntProperty property, final String base) {
		final OntResource or = property.getRange();

		final String clazzName;
		if (or == null) {
			OWL2Annotator.LOGGER.debug("No range guess for property " + property);
			// We should consider include inference, this will reduce this case.
			if (property.isDatatypeProperty()) {
				OWL2Annotator.LOGGER.debug("Assuming range of " + property + " is String since its a DatatypeProperty.");
				clazzName = "String";
			} else if (property.isObjectProperty()) {
				OWL2Annotator.LOGGER.debug("Assuming range of " + property + " is URI since its an ObjectProperty.");
				clazzName = "URI";
			} else {
				OWL2Annotator.LOGGER.warn("Property " + property + " is neither an ObjectProperty nor a DatatypeProperty. Unable to guess its type; using String.");
				clazzName = "String";
			}
		} else if (or.isAnon()) {
			OWL2Annotator.LOGGER.warn("Range of property " + property + " is an anonymous class. Using an URI instead");
			clazzName = "URI";
		} else if (or.getNameSpace().endsWith(base)) {
			// It is an object defined in the same namespace. Would probably crash with custom datatype.
			clazzName = "URI";
		} else {
			final String r = or.getLocalName();
			if (r.matches("boolean|byte|date|double|float|long|string|integer")) {
				// Type that exists in java.lang
				clazzName = ((char) (r.charAt(0) + ('A' - 'a'))) + r.substring(1);
			} else if (r.matches("int|negativeInteger|nonNegativeInteger|nonPositiveInteger|positiveInteger|short|unsignedInt|unsignedShort")) {
				clazzName = "Integer";
			} else if (r.matches("decimal|rational|real")) {
				clazzName = "Float";
			} else if (r.matches("dateTime|dateTimeStamp|duration|gYearMonth|gYear")) {
				clazzName = "Date";
			} else if (r.matches("unsignedByte")) {
				clazzName = "Byte";
			} else if (r.matches("unsignedLong")) {
				clazzName = "Long";
			} else if (r.matches("language|Literal|PlainLiteral|XMLLiteral|token|gMonthDay|gDay|gMonth|name|NCName")) {
				clazzName = "String";
			} else if (r.matches("anyURI")) {
				clazzName = "URI";
			} else {
				if (r.matches("base64Binary|hexBinary")) {
					// clazzName = "Byte[]";
					OWL2Annotator.LOGGER.debug("Range " + or + " of property " + property + " cannot easily be mapped. Ignore it for the moment.");
				} else if (r.matches("anyType")) {
					// clazzName = "Object";
					OWL2Annotator.LOGGER.debug("Range " + or + " of property " + property + " cannot easily be mapped. Ignore it for the moment.");
				} else {
					OWL2Annotator.LOGGER.debug("Unmapple range " + or + " for property " + property);
				}
				if (property.isDatatypeProperty()) {
					OWL2Annotator.LOGGER.debug("Assuming range of " + property + " is String since its a DatatypeProperty.");
					clazzName = "String";
				} else if (property.isObjectProperty()) {
					OWL2Annotator.LOGGER.debug("Assuming range of " + property + " is URI since its an ObjectProperty.");
					clazzName = "URI";
				} else {
					OWL2Annotator.LOGGER.warn("Property " + property + " is neither an ObjectProperty nor a DatatypeProperty. Unable to guess its type. Using String.");
					clazzName = "String";
				}
			}
		}
		return clazzName;
	}


	private static String generateGetter(final String baseURI, String value, final String field, final String type, final String comment) {
		final String value2;
		if (value.startsWith("has")) {
			value2 = "read" + value.substring(3, 4).toUpperCase() + value.substring(4);
		} else if (value.startsWith("is") && (value.charAt(3) + "").equals(value.charAt(3) + "".toUpperCase())) {
			value2 = "read" + value.substring(2, 3).toUpperCase() + value.substring(3);
		} else {
			value2 = "read" + value.substring(0, 1).toUpperCase() + value.substring(1);
		}

		final String res = "\tpublic final Value<" + type + "> " + value2 + "(){\n" + "\t\t"
				+ OWL2Annotator.READ.replaceFirst("!!URI!!", baseURI).replaceFirst("!!FIELD!!", field).replaceFirst("!!CLASS!!", type) + "\n" + "\t}\n\n";
		return OWL2Annotator.generateJavaComment("Read the " + comment + " \n" + OWL2Annotator.FIRST_VALUE_COMMENT, null, type) + res;
	}


	private static String generateSetter(final String prefixURI, final String baseURI, String value, final String field, final String type, final String comment) {
		final String value2;
		if (value.startsWith("has")) {
			value2 = "write" + value.substring(3, 4).toUpperCase() + value.substring(4);
		} else if (value.startsWith("is")) {
			value2 = "write" + value.substring(2, 3).toUpperCase() + value.substring(3);
		} else {
			value2 = "write" + value.substring(0, 1).toUpperCase() + value.substring(1);
		}

		final String res = "\tpublic final PieceOfKnowledge " + value2 + "(" + type + " value){\n" + "\t\t"
				+ OWL2Annotator.WRITE.replaceFirst("!!URI!!", baseURI).replaceFirst("!!PREFIX!!", prefixURI).replaceFirst("!!FIELD!!", field).replaceFirst("!!CLASS!!", type) + "\n" + "\t}\n\n";
		return OWL2Annotator.generateJavaComment("Write the " + comment, type, "PieceOfKnowledge") + res;
	}


	private static String generateJavaComment(final String comment, final String param, final String ret) {
		String res = "\t/**\n";
		res += "\t * " + comment.replaceAll("( |\\t)+", " ").replaceAll("\\n", "\n\t * ");
		if (param != null) {
			final String des = OWL2Annotator.desFrom(param);
			res += "\n\t * @param value " + des + param;
		}
		if (ret != null) {
			if (ret.equalsIgnoreCase("PieceOfKnowledge")) {
				res += "\n\t * @return the modified " + ret;
			} else {
				res += "\n\t * @return a Value&lt;" + ret + "&gt;";
			}
		}
		return res + "\n\t */\n";
	}


	private static String desFrom(final String param) {
		final String article;
		switch (param.toLowerCase().charAt(0)) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
			case 'y':
				article = "an ";
				break;
			default:
				article = "a ";
				break;
		}
		return article;
	}


	private static String packageFrom(final String baseURI) {
		final String[] split = baseURI.split("/");
		final StringBuffer res = new StringBuffer();
		for (final String s : split) {
			if (s.matches(".*\\..*")) {
				final String[] parts = s.split("\\.");
				String sr = "";
				for (final String p : parts) {
					sr = p + "." + sr;
				}
				res.append("." + sr);
			} else {
				res.append("." + s);
			}
		}
		return res.substring(8, res.length() - 1).replaceAll("\\.+", ".").replaceAll("(-|\\(|\\))", "_");
	}


	private static String prefixFrom(final String baseURI) {
		final int l = baseURI.lastIndexOf('/', baseURI.length() - 2);
		String prefix = baseURI.substring(l + 1, baseURI.length() - 1);
		if (prefix.equalsIgnoreCase("ontology") || prefix.matches("\\d+(\\.\\d+)*")) {
			final int s = baseURI.lastIndexOf('/', l - 1);
			prefix = baseURI.substring(s + 1, l);
			prefix = prefix.replaceAll("(www\\.|\\.(org|net|com|fr))", "");
		}
		return prefix;
	}

}
