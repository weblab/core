/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.rdf;

import java.io.File;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URI;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.annotator.AbstractAnnotator;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.annotator.Element;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.RDF;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * @author asaval
 */
public class RDFTest {


	static final Element element = Element.getDefaultElement();


	@Test
	public void testConstructors() {
		this.testConstructor(RDFWriter.class.getDeclaredConstructors()[0]);
		this.testConstructor(RDFReader.class.getDeclaredConstructors()[0]);
		this.testConstructor(XMLParser.class.getDeclaredConstructors()[0]);
	}


	@Test
	@Deprecated
	public void testDeprecatedConstructor() {
		this.testConstructor(CommonNamespaces.class.getDeclaredConstructors()[0]);
	}


	private void testConstructor(final Constructor<?> c) {
		c.setAccessible(true);
		try {
			c.newInstance(new Object[] {});
			Assert.fail(c.getName() + " should have thrown an exception.");
		} catch (final Exception e) {
			// Normal
			Assert.assertNotNull(e);
		}
	}



	private void setXMLParser(final String parserName) throws Exception {
		final Field field = RDFWriter.class.getDeclaredField("XML_PARSER_CLASS");
		field.setAccessible(true);

		final Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

		field.set(null, parserName);
	}


	@Test
	public void testWrite() throws Exception {

		final Annotation annotation = WebLabResourceFactory.createResource("idfact", "myAnnot", Annotation.class);
		final String RDF_PREFIX = "rdf";
		final String RDF_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		final String DUBLIN_CORE_PREFIX = "dc";
		final String DUBLIN_CORE_URI = "http://purl.org/dc/elements/1.1/";

		RDFWriter.writeStatement(annotation, URI.create("http://myobject#1"), RDF_PREFIX, URI.create(RDF_URI), "type", URI.create("weblab://myWeblab/uri"), null, true, RDFTest.element);
		RDFWriter.writeStatement(annotation, URI.create("http://myobject#1"), DUBLIN_CORE_PREFIX, URI.create(DUBLIN_CORE_URI), "title", "test", "fr", true, RDFTest.element);
		RDFWriter.writeStatement(annotation, URI.create("http://myobject#1"), DUBLIN_CORE_PREFIX, URI.create(DUBLIN_CORE_URI), "title", "test", null, false, RDFTest.element);
		RDFWriter.writeStatement(annotation, URI.create("http://myobject#1"), DUBLIN_CORE_PREFIX, URI.create(DUBLIN_CORE_URI), "title", "test_en", "en", true, RDFTest.element);
		RDFWriter.writeStatement(annotation, URI.create(annotation.getUri()), DUBLIN_CORE_PREFIX, URI.create(DUBLIN_CORE_URI), "creator", "me", null, true, RDFTest.element);
		RDFWriter.writeStatement(annotation, URI.create(annotation.getUri()), DUBLIN_CORE_PREFIX, URI.create(DUBLIN_CORE_URI), "creator", "me2", null, true, RDFTest.element);
		RDFWriter.writeStatement(annotation, URI.create(annotation.getUri()), DUBLIN_CORE_PREFIX, URI.create(DUBLIN_CORE_URI), "author", URI.create("weblab://myWeblab/uri"), "de", true,
				RDFTest.element);

		// For test purpose open the right on RDFWriter.outputFactory
		final Field field = RDFWriter.class.getDeclaredField("outputFactory");
		field.setAccessible(true);

		final Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);



		// not found
		field.set(null, null);
		this.setXMLParser("org.ow2.weblab.rdf.DummyXMLParser");
		RDFWriter.writeStatement(annotation, URI.create(annotation.getUri()), DUBLIN_CORE_PREFIX, URI.create(DUBLIN_CORE_URI), "creator", "me2", null, true, RDFTest.element);

		// instantiation
		field.set(null, null);
		this.setXMLParser("org.ow2.weblab.rdf.UnInstantiableXMLParser");
		RDFWriter.writeStatement(annotation, URI.create(annotation.getUri()), DUBLIN_CORE_PREFIX, URI.create(DUBLIN_CORE_URI), "creator", "me2", null, true, RDFTest.element);

		// Illegal
		field.set(null, null);
		this.setXMLParser("org.ow2.weblab.rdf.IllegalXMLParser");
		RDFWriter.writeStatement(annotation, URI.create(annotation.getUri()), DUBLIN_CORE_PREFIX, URI.create(DUBLIN_CORE_URI), "creator", "me2", null, true, RDFTest.element);


		// woodstock
		field.set(null, null);
		this.setXMLParser("com.ctc.wstx.stax.WstxOutputFactory");
		RDFWriter.writeStatement(annotation, URI.create(annotation.getUri()), DUBLIN_CORE_PREFIX, URI.create(DUBLIN_CORE_URI), "creator", "me2", null, true, RDFTest.element);


		final BaseAnnotator bas = new BaseAnnotator(annotation);
		bas.applyOperator(Operator.WRITE, "no:test", URI.create("no:test"), null, String.class, "");

		Assert.assertNull(RDFReader.readValueFromStatement(new PieceOfKnowledge(), URI.create("no:test"), null, null, null, null, RDFTest.element));



		final DNode ni = new DNode();
		Assert.assertNull(RDFReader.getLanguage(ni));
		Assert.assertNull(RDFReader.getLanguage(new DNode() {


			@Override
			public NamedNodeMap getAttributes() {
				return new DNamedNodeMap() {


					@Override
					public Node getNamedItem(final String name) {
						return null;
					}
				};
			}
		}));
		Assert.assertNull(RDFReader.findElementAboutIn(null, "test", RDFTest.element));

		final Node n = new DNode() {


			@Override
			public NodeList getChildNodes() {
				return new NodeList() {


					@Override
					public Node item(final int index) {
						return new DNode() {


							@Override
							public NodeList getChildNodes() {
								return new NodeList() {


									@Override
									public int getLength() {
										return 0;
									}


									@Override
									public Node item(final int index1) {
										return null;
									}

								};
							}
						};
					}


					@Override
					public int getLength() {
						return 1;
					}
				};
			}
		};
		RDFReader.print(n, "");
		Assert.assertNull(RDFReader.findRDF(n));
		Assert.assertFalse(RDFReader.isElementAbout(null, "dummy", RDFTest.element));


		final Node nd = new DNode() {


			@Override
			public String getLocalName() {
				return "description";
			}


			@Override
			public String getNamespaceURI() {
				return RDF.NAMESPACE;
			}


			@Override
			public NamedNodeMap getAttributes() {
				return new DNamedNodeMap() {


					@Override
					public Node getNamedItemNS(final String uri, final String name) {
						return new DNode() {


							@Override
							public String getTextContent() {
								return "some";
							}
						};
					}
				};
			}
		};
		Assert.assertEquals(nd, RDFReader.findElementAbout(nd, "some", RDFTest.element));

		final Node nfd = new DNode() {


			@Override
			public String getLocalName() {
				return "description";
			}


			@Override
			public String getNamespaceURI() {
				return RDF.NAMESPACE;
			}


			@Override
			public NamedNodeMap getAttributes() {
				return new DNamedNodeMap() {
					//
				};
			}
		};
		Assert.assertFalse(RDFReader.isElementAbout(nfd, "some", RDFTest.element));

		final Node nulld = new DNode() {


			@Override
			public String getLocalName() {
				return "description";
			}


			@Override
			public String getNamespaceURI() {
				return RDF.NAMESPACE;
			}


			@Override
			public NamedNodeMap getAttributes() {
				return new DNamedNodeMap() {


					@Override
					public Node getNamedItemNS(final String uri, final String name) {
						return new DNode();
					}
				};
			}
		};
		Assert.assertFalse(RDFReader.isElementAbout(nulld, "some", RDFTest.element));

		Value<String> result = RDFReader.readValueFromStatement(annotation, URI.create(annotation.getUri()), URI.create(DUBLIN_CORE_URI), "creator", null, String.class, RDFTest.element);
		Assert.assertEquals("me", result.toString());
		result = RDFReader.readValueFromStatement(annotation, URI.create(annotation.getUri()), URI.create(DUBLIN_CORE_URI), "title", null, String.class, RDFTest.element);
		Assert.assertNull(result);
		result = RDFReader.readValueFromStatement(annotation, URI.create("http://myobject#1"), URI.create(DUBLIN_CORE_URI), "title", "en", String.class, RDFTest.element);
		Assert.assertEquals("test_en", result.toString());


		Assert.assertTrue(result.hasValue());
		Assert.assertEquals("test_en", result.toString());
		Assert.assertEquals("test_en", result.firstTypedValue());
		Assert.assertEquals(1, result.size());
		Assert.assertTrue(result.iterator().hasNext());
		// new WebLabMarshaler().marshalResource(annotation, System.out);

		Assert.assertEquals(null, XMLParser.xmlParse("", Date.class));
		Assert.assertNull(XMLParser.xmlParse(null, String.class));
		Assert.assertEquals(12.4d, XMLParser.xmlParse("12.4", Double.class).doubleValue(), Math.ulp(12.4d));
		Assert.assertEquals(12.4, XMLParser.xmlParse("12.4", Float.class).floatValue(), Math.ulp(12.4f));
		Assert.assertEquals(12213543543l, XMLParser.xmlParse("12213543543", Long.class).longValue());
		Assert.assertEquals(12, XMLParser.xmlParse("12", Integer.class).intValue());
		Assert.assertFalse(XMLParser.xmlParse("false", Boolean.class).booleanValue());
		Assert.assertEquals(null, XMLParser.xmlParse("not an uri", URI.class));
		final Value<?> v = new Value<>();
		Assert.assertFalse(v.hasValue());
		Assert.assertNull(v.firstTypedValue("fr"));
		Assert.assertNull(v.firstTypedValue(null));
		Assert.assertTrue("".equals(v.toString()));
		final Value<String> v2 = new Value<>();
		v2.addItem("item", "fr");
		Assert.assertEquals("item", v2.firstTypedValue("fr"));
		Assert.assertNull(v2.firstTypedValue("en"));
		Assert.assertEquals("item", v2.firstTypedValue());

	}


	@Test
	public void testCheckOrderOnAttributesAutoclosedTags() throws XMLStreamException, ParserConfigurationException {
		final Document document = WebLabResourceFactory.createResource("idfact", "myDoc", Document.class);
		final BaseAnnotator ban = new BaseAnnotator(document);
		ban.writeLabel("test1");
		ban.startSpecifyLanguage("fr");
		ban.writeLabel("test");

		RDFWriter.writeStatement(document.getAnnotation().get(0), URI.create(document.getUri()), "dc", URI.create("http://purl.org/dc/elements/1.1/"), "date", Integer.valueOf(0), null, true,
				Element.getDefaultElement());
		RDFWriter.writeStatement(document.getAnnotation().get(0), URI.create(document.getUri()), "dc", URI.create("http://purl.org/dc/elements/1.1/"), "res", URI.create("test:test"), "fr", true,
				Element.getDefaultElement());

		Assert.assertNotNull(ban.readLabel().firstTypedValue("fr"));

	}


	@Test
	public void testXMLParser() throws XMLStreamException, ParserConfigurationException, IllegalClassFormatException {
		final Document document = WebLabResourceFactory.createResource("idfact", "myDoc", Document.class);
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(document);
		final BaseAnnotator ban = new BaseAnnotator(document);
		final PieceOfKnowledge pok = ban.writeLabel("test");

		RDFWriter.writeStatement(pok, URI.create(document.getUri()), "dc", URI.create("http://purl.org/dc/elements/1.1/"), "date", "Hey you !", null, true, Element.getDefaultElement());

		final Value<Date> date = dca.readDate();
		Assert.assertNotNull(date);
		Assert.assertFalse(date.hasValue());

		RDFWriter.writeStatement(pok, URI.create(document.getUri()), "dc", URI.create("http://purl.org/dc/elements/1.1/"), "date", Integer.valueOf(0), null, true, Element.getDefaultElement());
		try {
			RDFReader.readValueFromStatement(pok, URI.create(document.getUri()), URI.create("http://purl.org/dc/elements/1.1/"), "date", null, int.class, Element.getDefaultElement());
			Assert.fail("int.class should not be found in parser's list.");
		} catch (IllegalArgumentException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		try {
			XMLParser.xmlParse("0", int.class);
			Assert.fail("int.class should be refused by parser itself.");
		} catch (IllegalArgumentException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		try {
			XMLParser.addTypeSupport(int.class, new AbstractTypeParser<Integer>() {


				@Override
				public Integer parse(String value) {
					return Integer.valueOf(value);
				}


				@Override
				public String getDatatypeURI() {
					return null;
				}

			});
			Assert.fail("Should not add a primitive parser.");
		} catch (IllegalClassFormatException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		final String default_value = "default value";
		XMLParser.addTypeSupport(String.class, new AbstractTypeParser<String>() {


			@Override
			public String parse(String value) {
				throw new IllegalArgumentException("to test default value");
			}


			@Override
			public String defaultValueOnException(Exception exception) {
				return default_value;
			}


			@Override
			public String getDatatypeURI() {
				return null;
			}
		});

		Assert.assertEquals(default_value, XMLParser.xmlParse(null, String.class));
		Assert.assertEquals(default_value, XMLParser.xmlParse("anything", String.class));

		XMLParser.resetParsers();

		Assert.assertEquals('q', XMLParser.xmlParse("qslihdqs", Character.class).charValue());
		Assert.assertEquals((byte) 42, XMLParser.xmlParse("42", Byte.class).byteValue());
	}


	@Test
	public void testDelete() {
		final Document document = WebLabResourceFactory.createResource("idfact", "myDoc", Document.class);

		final BaseAnnotator ba = new BaseAnnotator(document);

		// Set several type and label
		ba.writeType(URI.create("my:uri:is:mine"));

		ba.applyOperator(AbstractAnnotator.Operator.REMOVE, BaseAnnotator.RDF_PREFIX, BaseAnnotator.RDF_URI, "Type", URI.class, URI.create("my:uri:is:mine"));

		ba.writeLabel("my label");

		ba.applyOperator(AbstractAnnotator.Operator.REMOVE, BaseAnnotator.RDFS_PREFIX, BaseAnnotator.RDFS_URI, "Label", String.class, "my label");

		Assert.assertFalse(ba.readType().hasValue());
		Assert.assertFalse(ba.readLabel().hasValue());

	}


	@Test
	public void testReplaceURIs() throws WebLabCheckedException {
		final PieceOfKnowledge pok = new WebLabMarshaller().unmarshal(new File("src/test/resources/test.xml"), PieceOfKnowledge.class);
		RDFWriter.replaceURI(pok, URI.create("weblab://collections#96673"), URI.create("weblab://collections#72"));
		new WebLabMarshaller().marshalResource(pok, new File("target/replaceUris.xml"));
	}


	@Test
	public void testRDFWriter() throws XMLStreamException, ParserConfigurationException {
		try {
			RDFWriter.writeStatement(null, null, null, null, null, null, null, true, null);
			Assert.fail("Can not remove statement using null value");
		} catch (XMLStreamException e) {
			// Normal
			Assert.assertNotNull(e);
		}
		try {
			RDFWriter.writeStatement(new PieceOfKnowledge(), null, null, null, null, null, null, false, null);
			Assert.fail("Can not delete statement that does not exist");
		} catch (XMLStreamException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		final Document document = WebLabResourceFactory.createResource("idfact", "myDoc", Document.class);
		final BaseAnnotator ban = new BaseAnnotator(document);
		ban.writeLabel("test");

		ban.setAppendMode(false);
		ban.writeLabel(null);
		Value<String> value = ban.readLabel();

		Assert.assertFalse(value.hasValue());
		Assert.assertNull(value.firstTypedValue());

		final PieceOfKnowledge pok = ban.writeLabel("test");
		value = ban.readLabel();
		Assert.assertTrue(value.hasValue());
		Assert.assertEquals("test", value.firstTypedValue());

		RDFWriter.writeStatement(pok, ban.getSubject(), "rdfs", BaseAnnotator.RDFS_URI, "label", null, null, false, Element.getDefaultElement());

		value = ban.readLabel();
		Assert.assertFalse(value.hasValue());
		Assert.assertNull(value.firstTypedValue());

		ban.writeLabel("test");
		value = ban.readLabel();
		Assert.assertTrue(value.hasValue());
		Assert.assertEquals("test", value.firstTypedValue());

		RDFWriter.replaceURI(pok, URI.create("nop://nothind"), ban.getSubject());

		ban.startInnerAnnotatorOn(URI.create("nop://nothind"));
		value = ban.readLabel();
		Assert.assertFalse(value.hasValue());
		Assert.assertNull(value.firstTypedValue());
		value = ban.readLabel();
		Assert.assertFalse(value.hasValue());
		Assert.assertNull(value.firstTypedValue());

		try {
			RDFWriter.writeStatement(pok, ban.getSubject(), "nuts", BaseAnnotator.RDFS_URI, "label/", "none", null, false, Element.getDefaultElement());
			Assert.fail("Name of the property must be valid");
		} catch (DOMException dome) {
			// Normal
			Assert.assertNotNull(dome);
		}

		try {
			RDFWriter.writeStatement(pok, ban.getSubject(), "nuts", BaseAnnotator.RDFS_URI, "label:yopnou", "none", null, false, Element.getDefaultElement());
			Assert.fail("Name of the property must be valid");
		} catch (DOMException dome) {
			// Normal
			Assert.assertNotNull(dome);
		}

		RDFWriter.writeStatement(pok, ban.getSubject(), "nuts", BaseAnnotator.RDFS_URI, "label:yop/nou", "none", null, false, Element.getDefaultElement());

		RDFWriter.removeStatement(pok, URI.create("nop://nop"), null, null, null, null, null, false, Element.getDefaultElement());
		// does nothing
		ban.writeLabel("test");
		RDFWriter.removeStatement(pok, ban.getSubject(), null, BaseAnnotator.RDFS_URI, "label", "test2", null, true, Element.getDefaultElement());
		value = ban.readLabel();
		Assert.assertTrue(value.hasValue());

		RDFWriter.removeStatement(pok, ban.getSubject(), null, BaseAnnotator.RDFS_URI, "label", "test", "fre", true, Element.getDefaultElement());
		value = ban.readLabel();
		Assert.assertTrue(value.hasValue());

		RDFWriter.removeStatement(pok, ban.getSubject(), null, BaseAnnotator.RDFS_URI, null, "test", null, true, Element.getDefaultElement());

		ban.startSpecifyLanguage("fre");
		ban.writeLabel("test_fr");

		RDFWriter.removeStatement(pok, ban.getSubject(), null, BaseAnnotator.RDFS_URI, "label", "test_fr", "fre", true, Element.getDefaultElement());
		value = ban.readLabel();
		Assert.assertFalse(value.hasValue());

		ban.writeLabel("test_fr");
		RDFWriter.removeStatement(pok, ban.getSubject(), null, BaseAnnotator.RDFS_URI, "label", "test_not_fr", "fre", true, Element.getDefaultElement());
		value = ban.readLabel();
		Assert.assertTrue(value.hasValue());


	}


	@Test
	public void testRDFReader() {
		Assert.assertNull(RDFReader.findElementAboutIn(new DNode(), null, null));
		Assert.assertNotNull(RDFReader.getNodeLiteralOrResource(new DNode() {


			@Override
			public String getTextContent() {
				return "";
			}
		}));
		Assert.assertNotNull(RDFReader.getNodeLiteralOrResource(new DNode() {


			@Override
			public String getTextContent() {
				return "";
			}


			@Override
			public NamedNodeMap getAttributes() {
				return new NamedNodeMap() {


					@Override
					public Node setNamedItemNS(Node arg) throws DOMException {
						return null;
					}


					@Override
					public Node setNamedItem(Node arg) throws DOMException {
						return null;
					}


					@Override
					public Node removeNamedItemNS(String namespaceURI, String localName) throws DOMException {
						return null;
					}


					@Override
					public Node removeNamedItem(String name) throws DOMException {
						return null;
					}


					@Override
					public Node item(int index) {
						return null;
					}


					@Override
					public Node getNamedItemNS(String namespaceURI, String localName) throws DOMException {
						return null;
					}


					@Override
					public Node getNamedItem(String name) {
						return null;
					}


					@Override
					public int getLength() {
						return 0;
					}
				};
			}
		}));

		final Document document = WebLabResourceFactory.createResource("idfact", "myDoc", Document.class);
		final BaseAnnotator ban = new BaseAnnotator(document);
		final PieceOfKnowledge pok = ban.writeLabel("test");

		Assert.assertEquals(3, RDFReader.findNodesWithValue((Node) pok.getData(), "test").size());

		Value<String> value = RDFReader.readValueFromStatement(pok, ban.getSubject(), BaseAnnotator.RDFS_URI, "test:you/s", "fre", String.class, element);

		value = RDFReader.readValueFromStatement(pok, ban.getSubject(), BaseAnnotator.RDFS_URI, "testyou:s", null, String.class, element);

		ban.startSpecifyLanguage("f");
		ban.writeLabel("test");

		value = RDFReader.readValueFromStatement(pok, ban.getSubject(), BaseAnnotator.RDFS_URI, "label", "f", String.class, element);
		Assert.assertFalse("test".equals(value.firstTypedValue()));

		ban.startSpecifyLanguage("f2");
		ban.writeType(URI.create("my://type"));

		Value<URI> uri = RDFReader.readValueFromStatement(pok, ban.getSubject(), BaseAnnotator.RDF_URI, "type", "f2", URI.class, element);
		Assert.assertEquals(URI.create("my://type"), uri.firstTypedValue());

		Value<String> v1 = new Value<>("test2");
		value.aggregate(v1);
		Assert.assertEquals(1, value.size());

	}

}
