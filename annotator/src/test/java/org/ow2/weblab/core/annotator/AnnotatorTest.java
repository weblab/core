/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.annotator;

import java.io.File;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;

import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.geonames.GeonamesAnnotator;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.rdf.DNode;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.w3.geo.WGS84Annotator;
import org.w3.owl.OWLAnnotator;
import org.w3c.dom.Node;

public class AnnotatorTest {



	@Test
	public void testDublinCoreAnnotator() throws URISyntaxException {
		final Document document = WebLabResourceFactory.createResource("idfact", "myDoc", Document.class);

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(document);
		final BaseAnnotator rra = new BaseAnnotator(document);

		// Set several title
		dca.writeTitle("My New Document");
		dca.startSpecifyLanguage("fr");
		dca.writeTitle("Mon Nouveau Document");
		dca.endSpecifyLanguage();

		// Retrieve all title
		Assert.assertEquals("My New Document", dca.readTitle().toString());

		final URI docType = new URI("weblab://weblab/type#document");
		// Set the type of the document as a rdf:resource
		rra.writeType(docType);
		Assert.assertEquals(docType, rra.readType().firstTypedValue());

		// get french title
		dca.startSpecifyLanguage("fr");
		Assert.assertEquals("Mon Nouveau Document", dca.readTitle().toString());
		dca.endSpecifyLanguage();

		Assert.assertNotNull(dca.readCoverage());

		final Date d = new Date();
		dca.writeDate(d);
		final Date d2 = dca.readDate().firstTypedValue();
		Assert.assertEquals(d.toString(), d2.toString());


	}


	@Test
	public void testWProcessingAnnotator() throws URISyntaxException, WebLabCheckedException {
		final Text text = WebLabResourceFactory.createResource("idfact", "myDoc", Text.class);
		text.setContent("London.");

		// We can use several Annotator on a Resource at the same time
		final WProcessingAnnotator wpa = new WProcessingAnnotator(text);
		final GeonamesAnnotator geoa = new GeonamesAnnotator(text);
		final WGS84Annotator wgsa = new WGS84Annotator(text);

		wpa.writeProducedBy(URI.create("myservice"));

		// We create URI
		final URI segmentURI = new URI("weblab://idfact/mySegment");
		final URI londonGeonamesURI = new URI("http://sws.geonames.org/2643743/");

		// Create a geographic entity in the document from an external ontology
		geoa.setElement("gn", "http://www.geonames.org/ontology#", "Feature");
		geoa.startInnerAnnotatorOn(londonGeonamesURI);

		// We set RDFS labels
		// We set the language support in Official Aramaic ISO format
		geoa.startSpecifyLanguage("arc");
		// We write label in the Official Aramaic language
		geoa.writeLabel("ܠܘܢܕܘܢ ");

		// Wet the language support in French ISO format
		geoa.startSpecifyLanguage("fr");
		// We write label in the French language
		geoa.writeLabel("La ville de Londres");

		// We set the overwrite mode to reset the french label
		geoa.setAppendMode(false);

		// We overwrite a label
		geoa.writeLabel("Londres");
		Assert.assertEquals("Londres", geoa.readLabel().toString());

		// We set back the append mode of the annotator
		geoa.setAppendMode(true);

		// We remove language specification
		geoa.endSpecifyLanguage();
		// we write a label without language attribute
		geoa.writeLabel("London");

		// We set geonames specific latitude/longitude properties
		wgsa.writeLat(Double.valueOf(51.50051));
		wgsa.writeLong(Double.valueOf(-0.12883));

		// Create add segment on the Text
		final LinearSegment segment = new LinearSegment();
		segment.setStart(0);
		segment.setEnd(6);
		segment.setUri(segmentURI.toString());
		text.getSegment().add(segment);

		// Create a semantic link between the segment and the geographical entity
		wpa.startInnerAnnotatorOn(segmentURI);
		wpa.writeRefersTo(londonGeonamesURI);


		final OWLAnnotator oa = new OWLAnnotator(text);
		oa.writeSameAs(URI.create("test"));

		new WebLabMarshaller().marshalResource(text, new File("target/annotator-test.xml"));

		wpa.writeCandidate(Boolean.TRUE);
		Assert.assertNotNull(geoa.readPopulation());
		Assert.assertFalse(geoa.readPopulation().hasValue());

		Assert.assertEquals(Double.valueOf(51.50051), new WGS84Annotator(text).readLat().firstTypedValue());
		Assert.assertEquals(Double.valueOf(-0.12883), new WGS84Annotator(text).readLong().firstTypedValue());

		final Value<String> v = wpa.applyOperator(Operator.READ, "none", URI.create("none:uri"), "none;prop", String.class, "testValue");
		Assert.assertNotNull(v);
		Assert.assertFalse(v.hasValue());

		Assert.assertNotNull(wpa.getResource());
		Assert.assertEquals(text, wpa.getResource());
	}


	@Test
	public void testElements() {

		final Resource resource = WebLabResourceFactory.createResource("id", "res", Resource.class);

		final BaseAnnotator ba = new BaseAnnotator(resource);
		ba.setDefaultElement();
		final Element element = new Element("prefix", "uri", "localName");
		element.setLocalName("localName");
		element.setUri("uri");
		element.setPrefix("prefix");
		ba.setElement(element);

		Assert.assertEquals("urilocalName", element.toURI().toString());


		final Node node1 = new DNode() {


			@Override
			public String getLocalName() {
				return "localName";
			}


			@Override
			public String getNamespaceURI() {
				return "uri";
			}

		};
		final Node node2 = new DNode() {


			@Override
			public String getLocalName() {
				return "localName";
			}


			@Override
			public String getNamespaceURI() {
				return "anotheruri";
			}

		};
		final Node node3 = new DNode() {


			@Override
			public String getLocalName() {
				return "alocalName";
			}


			@Override
			public String getNamespaceURI() {
				return "uri";
			}

		};
		final Node node4 = new DNode() {


			@Override
			public String getLocalName() {
				return "alocalName";
			}


			@Override
			public String getNamespaceURI() {
				return "auri";
			}

		};
		final Node node5 = new DNode() {


			@Override
			public String getLocalName() {
				return null;
			}


			@Override
			public String getNamespaceURI() {
				return "uri";
			}

		};
		final Node node6 = new DNode() {


			@Override
			public String getLocalName() {
				return "localName";
			}


			@Override
			public String getNamespaceURI() {
				return null;
			}

		};
		final Node node7 = new DNode() {


			@Override
			public String getLocalName() {
				return null;
			}


			@Override
			public String getNamespaceURI() {
				return "auri";
			}

		};
		final Node node8 = new DNode() {


			@Override
			public String getLocalName() {
				return "alocalName";
			}


			@Override
			public String getNamespaceURI() {
				return null;
			}

		};
		Assert.assertTrue(element.equalsNode(node1));
		Assert.assertFalse(element.equalsNode(node2));
		Assert.assertFalse(element.equalsNode(node3));
		Assert.assertFalse(element.equalsNode(node4));
		Assert.assertFalse(element.equalsNode(node5));
		Assert.assertFalse(element.equalsNode(node6));
		Assert.assertFalse(element.equalsNode(node7));
		Assert.assertFalse(element.equalsNode(node8));
		Assert.assertFalse(element.equalsNode(new DNode()));
	}


	@Test
	public void testInnerAnnotator() {
		/**
		 * check if we still have the same URI one innerAnotator is ended.
		 */
		final Resource resource = WebLabResourceFactory.createResource("id", "res", Resource.class);

		URI resourceURI = URI.create(resource.getUri());

		final BaseAnnotator ba = new BaseAnnotator(resource);

		Assert.assertEquals(resourceURI, ba.getSubject());

		ba.writeLabel("test");

		Assert.assertEquals(resourceURI, ba.getSubject());

		URI tempURI = URI.create("test://test");
		ba.startInnerAnnotatorOn(tempURI);
		ba.writeLabel("test2");

		Assert.assertEquals(tempURI, ba.getSubject());

		ba.endInnerAnnotator();

		Assert.assertEquals(resourceURI, ba.getSubject());

		ba.writeLabel("test3");
		ba.endInnerAnnotator();

	}


	@Test
	public void testDatatypes() {
		final Resource resource = WebLabResourceFactory.createResource("id", "res2", Resource.class);

		final WProcessingAnnotator wpa = new WProcessingAnnotator(resource);

		wpa.writeCanBeIgnored(Boolean.TRUE);
		long longValue = 100000000000000l;
		wpa.writeOriginalFileSize(Long.valueOf(longValue));
		Date date = new Date();
		Calendar c1 = Calendar.getInstance();
		c1.setTime(date);

		wpa.writeGatheringDate(date);

		String data = PoKUtil.getPoKData(resource.getAnnotation().get(0));
		final StringReader reader = new StringReader(data);
		Model model = ModelFactory.createDefaultModel();
		model.read(reader, null, "RDF/XML");

		org.apache.jena.rdf.model.Resource res = model.createResource(resource.getUri());
		Property propIgnored = model.createProperty("http://weblab.ow2.org/core/1.2/ontology/processing#canBeIgnored");
		Property propDate = model.createProperty("http://weblab.ow2.org/core/1.2/ontology/processing#hasGatheringDate");
		Property propSize = model.createProperty("http://weblab.ow2.org/core/1.2/ontology/processing#hasOriginalFileSize");

		Assert.assertTrue(model.containsLiteral(res, propIgnored, true));
		Assert.assertTrue(model.containsLiteral(res, propSize, longValue));

		NodeIterator ni = model.listObjectsOfProperty(res, propDate);
		while (ni.hasNext()) {
			RDFNode node = ni.next();
			Assert.assertNotNull(node);
			Assert.assertTrue(node.isLiteral());
			Literal lit = (Literal) node;

			Object object = lit.getValue();
			Assert.assertTrue(object instanceof XSDDateTime);
			XSDDateTime dateTime = (XSDDateTime) object;
			Calendar c2 = dateTime.asCalendar();

			Assert.assertEquals(c1.get(Calendar.YEAR), c2.get(Calendar.YEAR));
			Assert.assertEquals(c1.get(Calendar.MONTH), c2.get(Calendar.MONTH));
			Assert.assertEquals(c1.get(Calendar.DAY_OF_MONTH), c2.get(Calendar.DAY_OF_MONTH));
			Assert.assertEquals(c1.get(Calendar.MINUTE), c2.get(Calendar.MINUTE));
			Assert.assertEquals(c1.get(Calendar.SECOND), c2.get(Calendar.SECOND));

		}

	}

}
