/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.annotator;

import java.io.File;

import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.rdf.OWL2Annotator;


public class TestGeneration {


	@Test
	public void testgenerate() throws WebLabCheckedException {
		final String[][] ontologiesAndBase = new String[][] { new String[] { "src/test/resources/little_geonames.owl", null, "target" + File.separator },
				new String[] { "src/test/resources/dcterms.rdf", "http://purl.org/dc/terms/", "target" + File.separator },
				new String[] { "src/test/resources/ma-ont.rdf", "http://www.w3.org/ns/ma-ont", "target" + File.separator },
				new String[] { "src/test/resources/dcelements.rdf", "http://purl.org/dc/elements/1.1/", "target" + File.separator },
				new String[] { "ontologies/model.owl", null, "target" + File.separator },
				new String[] { "ontologies/processing.owl", null, "target" + File.separator },
				new String[] { "ontologies/retrieval.owl", null, "target" + File.separator } };

		for (final String[] ontologyAndBase : ontologiesAndBase) {
			OWL2Annotator.main(ontologyAndBase);
		}
	}

}
