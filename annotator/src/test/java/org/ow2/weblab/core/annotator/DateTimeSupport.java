/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.annotator;

import java.net.URI;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.rdf.RDFWriter;

public class DateTimeSupport {


	@Test
	public void testISO8601support() throws Exception {
		
		Date minutePrecision = Date.from(LocalDateTime.of(2011, 12, 03, 10, 15).toInstant(ZoneOffset.UTC));
		Date secondPrecision = Date.from(LocalDateTime.of(2011, 12, 03, 10, 15, 30).toInstant(ZoneOffset.UTC));
		
		final String[] datesToTest = new String[] { "2011-12-03T10:15", "2011-12-03T10:15Z", "2011-12-03T10:15:30", "2011-12-03T10:15:30Z", "2011-12-03T10:15:30", "2011-12-03T10:15:30+01:00", "2011-12-03T10:15+01:00",
				"2011-12-03T10:15:30+01:00[Europe/Paris]" };

		for (final String date : datesToTest) {
			final PieceOfKnowledge pok = WebLabResourceFactory.createResource("pok", "res", PieceOfKnowledge.class);
			final URI pokURI = URI.create(pok.getUri());

			RDFWriter.writeStatement(pok, pokURI, "wp", URI.create("http://weblab.ow2.org/core/1.2/ontology/processing#"), "hasGatheringDate", date, null, true, Element.getDefaultElement());
			final WProcessingAnnotator wpa = new WProcessingAnnotator(pokURI, pok);
			final Date d = wpa.readGatheringDate().firstTypedValue();
			Assert.assertNotNull(date, d);
			if (date.contains("30")) {
				Assert.assertEquals(secondPrecision, d);
			} else {
				Assert.assertEquals(minutePrecision, d);
			}
			
		}

	}


	@Test
	public void testTimestampSupport() throws Exception {
		Timestamp date = new Timestamp(System.currentTimeMillis());

		final PieceOfKnowledge pok = WebLabResourceFactory.createResource("pok", "res", PieceOfKnowledge.class);
		final URI pokURI = URI.create(pok.getUri());

		RDFWriter.writeStatement(pok, pokURI, "wp", URI.create("http://weblab.ow2.org/core/1.2/ontology/processing#"), "hasGatheringDate", date, null, true, Element.getDefaultElement());
		final WProcessingAnnotator wpa = new WProcessingAnnotator(pokURI, pok);

		final Date d = wpa.readGatheringDate().firstTypedValue();
		Assert.assertNotNull(d);
		Assert.assertEquals(date.getTime(), d.getTime());
	}

}