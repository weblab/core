/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.annotator;

import java.io.File;
import java.net.URI;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.rdf.RDFWriter;
import org.purl.dc.elements.DublinCoreAnnotator;

public class RemoveStatement {


	@Test
	public void testRemoveProperty() throws WebLabCheckedException {
		final Annotation pok = WebLabResourceFactory.createResource("pok", "res", Annotation.class);
		final URI pokURI = URI.create(pok.getUri());

		final WProcessingAnnotator wpa = new WProcessingAnnotator(pokURI, pok);


		// try to delete a statement that does not exist
		wpa.setAppendMode(false);
		PieceOfKnowledge p = wpa.writeExposedAs(null);

		// it should fail
		Assert.assertNull(p);

		// write a property
		p = wpa.writeExposedAs("test");

		Assert.assertNotNull(p);
		Assert.assertEquals("test", wpa.readExposedAs().firstTypedValue());

		// force append mode
		wpa.setAppendMode(true);

		// append mode is not compatible with statement deletion
		// This will throw a catched exception
		// the result of a deletion/write should be checked by the returned value
		// if the returned POK is null, then the operation failed,
		// else all went well.
		p = wpa.writeExposedAs(null);

		// Thus the return POK should be null
		Assert.assertNull(p);

		// force override mode
		wpa.setAppendMode(false);

		// try to remove all properties on this POK
		p = wpa.writeExposedAs(null);

		// This must work
		Assert.assertNull(p);
		Assert.assertNotNull(wpa.readExposedAs());
		Assert.assertFalse(wpa.readExposedAs().hasValue());


		// This tests, that in absolute should fail
		// show that the implementation of deletion functionality is
		// still not complete at the moment

		// We create a resource
		final Resource res = WebLabResourceFactory.createResource("res", "res", Resource.class);
		final Annotation pok2 = WebLabResourceFactory.createResource("pok2", "res", Annotation.class);

		// with 2 annotations
		res.getAnnotation().add(pok);
		res.getAnnotation().add(pok2);

		// let's check we got nothing in annotations
		final WProcessingAnnotator wpa2 = new WProcessingAnnotator(res);

		Assert.assertNotNull(wpa2.readExposedAs());
		Assert.assertFalse(wpa2.readExposedAs().hasValue());

		// add a test with a WPA on the second annotation
		final WProcessingAnnotator wpa3 = new WProcessingAnnotator(URI.create(res.getUri()), pok2);
		wpa3.writeExposedAs("testNotdeletable");

		new WebLabMarshaller().marshalResource(res, new File("target/removeProperty.xml"));

		// check if something has been written with the direct WPA on the resource
		Assert.assertNotNull(wpa2.readExposedAs());

		// Try to delete it using
		wpa2.setAppendMode(false);
		p = wpa2.writeExposedAs(null);

		// Check
		Assert.assertNull(p);
	}


	@Test
	public void testRemoveAllDescriptionAbout() {
		final Annotation pok = WebLabResourceFactory.createResource("pok", "res", Annotation.class);
		final URI pokURI = URI.create(pok.getUri());

		final WProcessingAnnotator wpa = new WProcessingAnnotator(pokURI, pok);

		// add some properties about the pok
		wpa.writeCandidate(Boolean.TRUE);
		wpa.writeExposedAs("blablabla");
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(pokURI, pok);
		dca.writeDate(new Date(System.currentTimeMillis()));
		dca.writeFormat("test");

		RDFWriter.removeStatement(pok, pokURI, "none", null, null, null, null, false, Element.getDefaultElement());

		Assert.assertFalse(dca.readFormat().hasValue());
	}


	@Test
	public void testRemove() {
		final StringQuery sq = WebLabResourceFactory.createResource("test", "test", StringQuery.class);
		final Annotation pok1 = WebLabResourceFactory.createAndLinkAnnotation(sq);
		final Annotation pok2 = WebLabResourceFactory.createAndLinkAnnotation(sq);
		final Annotation pok3 = WebLabResourceFactory.createAndLinkAnnotation(sq);
		final Annotation pok4 = WebLabResourceFactory.createAndLinkAnnotation(sq);

		try {
			RDFWriter.writeStatement(pok1, URI.create(sq.getUri()), "wrt", URI.create(WebLabRetrieval.NAMESPACE), "toBeRankedBy", URI.create("1"), null, true,
					Element.getDefaultElement());
			RDFWriter.writeStatement(pok2, URI.create(sq.getUri()), "wrt", URI.create(WebLabRetrieval.NAMESPACE), "toBeRankedBy", URI.create("2"), null, true,
					Element.getDefaultElement());
			RDFWriter.writeStatement(pok3, URI.create(sq.getUri()), "wrt", URI.create(WebLabRetrieval.NAMESPACE), "toBeRankedBy", URI.create("3"), null, true,
					Element.getDefaultElement());
			RDFWriter.writeStatement(pok4, URI.create(sq.getUri()), "wrt", URI.create(WebLabRetrieval.NAMESPACE), "toBeRankedBy", URI.create("4"), null, true,
					Element.getDefaultElement());
		} catch (final XMLStreamException e) {
			e.printStackTrace();
		} catch (final ParserConfigurationException e) {
			e.printStackTrace();
		}

		final WRetrievalAnnotator wRetrievalAnnotator = new WRetrievalAnnotator(sq);

		wRetrievalAnnotator.setAppendMode(false);
		wRetrievalAnnotator.writeToBeRankedBy(null);

		Assert.assertFalse(wRetrievalAnnotator.readToBeRankedBy().hasValue());

	}
}
