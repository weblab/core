/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.annotator;

import java.net.URI;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Document;
import org.purl.dc.elements.DublinCoreAnnotator;

public class EmptyAnnotation {


	@Test
	public void testDoNotCreateEmptyAnnotation() {

		final Document document = WebLabResourceFactory.createResource("idfact", "myDoc", Document.class);

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(document);

		dca.readContributor();

		Assert.assertTrue(document.getAnnotation().isEmpty());

		dca.writeContributor(null);

		Assert.assertTrue(document.getAnnotation().isEmpty());

		dca.applyOperator(Operator.WRITE, "test", URI.create("test://test"), "test", String.class, null);

		Assert.assertTrue(document.getAnnotation().isEmpty());

		dca.applyOperator(Operator.WRITE, "test", URI.create("test://test"), "test", String.class, "test");

		dca.applyOperator(Operator.REMOVE, "test", URI.create("test://test"), "test", String.class, "test");
	}

}
