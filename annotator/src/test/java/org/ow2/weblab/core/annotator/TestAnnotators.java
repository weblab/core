/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.annotator;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.geonames.GeonamesAnnotator;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.WModelAnnotator;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.model.structure.WStructureAnnotator;
import org.ow2.weblab.rdf.AbstractTypeParser;
import org.ow2.weblab.rdf.RDFDatatypeContainer;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.rdf.XMLParser;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.purl.dc.terms.DCTermsAnnotator;
import org.slf4j.LoggerFactory;
import org.w3.geo.WGS84Annotator;
import org.w3.ma_ont.MAOntAnnotator;
import org.w3.owl.OWLAnnotator;

public class TestAnnotators {


	@Test
	public void testAnnotators() throws Exception {
		final Document doc = WebLabResourceFactory.createResource("ref", "test", Document.class);
		final AbstractAnnotator[] bas = new AbstractAnnotator[] { new WProcessingAnnotator(doc), new WRetrievalAnnotator(doc), new GeonamesAnnotator(doc), new WGS84Annotator(doc),
				new BaseAnnotator(doc), new DCTermsAnnotator(doc), new WModelAnnotator(doc), new MAOntAnnotator(doc), new OWLAnnotator(doc), new DublinCoreAnnotator(doc), new WStructureAnnotator(doc) };
		final URI testuri = URI.create("test:test");
		final WProcessingAnnotator wpa = new WProcessingAnnotator(testuri, new PieceOfKnowledge());
		new WRetrievalAnnotator(testuri, new PieceOfKnowledge());
		new GeonamesAnnotator(testuri, new PieceOfKnowledge());
		new WGS84Annotator(testuri, new PieceOfKnowledge());
		new BaseAnnotator(testuri, new PieceOfKnowledge());
		new DCTermsAnnotator(testuri, new PieceOfKnowledge());
		new WModelAnnotator(testuri, new PieceOfKnowledge());
		new OWLAnnotator(testuri, new PieceOfKnowledge());
		new MAOntAnnotator(testuri, new PieceOfKnowledge());
		new DublinCoreAnnotator(testuri, new PieceOfKnowledge());
		new WStructureAnnotator(testuri, new PieceOfKnowledge());

		for (final AbstractAnnotator base : bas) {
			this.browseAnnotator(base);
		}

		Assert.assertTrue(wpa.isAppendMode());
		wpa.startInnerAnnotatorOn(URI.create("none:none"));
		wpa.endInnerAnnotator();
		wpa.setResource(doc);
		wpa.endInnerAnnotator();
		Assert.assertFalse(wpa.isInnerAnnotatorStarted());
		new WProcessingAnnotator(URI.create("none:er"), null);
		doc.setUri("not an uri");
		new WProcessingAnnotator(doc);
	}


	@Test
	public void testDCAnnotatorOnOneDoc() throws WebLabCheckedException {
		final Document doc = WebLabResourceFactory.createResource("test", "doc", Document.class);
		final DublinCoreAnnotator annot = new DublinCoreAnnotator(doc);
		annot.writeLanguage("eng");
		new WebLabMarshaller().marshalResource(doc, new File("target/test.xml"));
		Assert.assertEquals("eng", annot.readLanguage().firstTypedValue());
	}


	private void browseAnnotator(final AbstractAnnotator base) throws Exception {
		final List<Method> write = new LinkedList<>();
		final List<Method> read = new LinkedList<>();
		for (final Method f : base.getClass().getDeclaredMethods()) {
			if (f.getName().startsWith("read")) {
				read.add(f);
			} else if (f.getName().startsWith("write")) {
				write.add(f);
			}
		}

		for (final Method wm : write) {
			try {
				wm.invoke(base, this.getParameter(wm.getParameterTypes()[0]));
			} catch (final Exception e) {
				Assert.fail("Method " + wm.getName() + " is not valid.");
			}
		}

		for (final Method wm : read) {
			final Value<?> o = (Value<?>) wm.invoke(base);

			Assert.assertNotNull("Error for ppt " + wm.getName() + " in class " + base.getClass().getName(), o);
			Assert.assertTrue("Error for ppt " + wm.getName() + " in class " + base.getClass().getName(), o.getValues().size() > 0);
			final Object z = o.getValues().getFirst();
			final Object res = this.getParameter(z.getClass());
			Assert.assertEquals("Error for ppt " + wm.getName() + " in class " + base.getClass().getName(), res, z);
		}
	}


	private Object getParameter(final Class<?> clazz) throws URISyntaxException, ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH);
		final Date now = sdf.parse("2010-09-14T14:17:49+0200");
		final String test = "test";
		final URI uri = new URI("test:test");

		final Byte by = Byte.valueOf((byte) 3);
		final Byte[] bytes = new Byte[] { by, by, by };

		final Boolean b = Boolean.TRUE;
		final Integer t = Integer.valueOf(12);
		final Double d = Double.valueOf(0.241);
		final Long l = Long.valueOf(1235435431453453152l);
		final Float f = Float.valueOf(12354.54f);
		if (clazz.isInstance(now)) {
			return now;
		} else if (clazz.isInstance(uri)) {
			return uri;
		} else if (clazz.isInstance(test)) {
			return test;
		} else if (clazz.isInstance(l)) {
			return l;
		} else if (clazz.isInstance(t)) {
			return t;
		} else if (clazz.isInstance(d)) {
			return d;
		} else if (clazz.isInstance(b)) {
			return b;
		} else if (clazz.isInstance(by)) {
			return by;
		} else if (clazz.isInstance(f)) {
			return f;
		} else if (clazz.isInstance(bytes)) {
			return bytes;
		}
		LoggerFactory.getLogger(this.getClass()).warn("Range type not taken into account in the test: " + clazz);
		return null;
	}


	@Test
	public void testT() throws WebLabCheckedException {
		final Resource res = new WebLabMarshaller().unmarshal(new File("src/test/resources/test.xml"), Resource.class);
		final DublinCoreAnnotator annotator = new DublinCoreAnnotator(res);
		Assert.assertTrue(annotator.readLanguage().hasValue());
		Assert.assertEquals("eng", annotator.readLanguage().firstTypedValue());
		Assert.assertFalse(annotator.readFormat().hasValue());
		final BaseAnnotator ba = new BaseAnnotator(res);
		Assert.assertTrue(ba.read(URI.create(DublinCore.LANGUAGE_PROPERTY_NAME), String.class).hasValue());
		Assert.assertEquals("eng", ba.read(URI.create(DublinCore.LANGUAGE_PROPERTY_NAME), String.class).firstTypedValue());
		Assert.assertFalse(ba.read(URI.create(DublinCore.DATE_PROPERTY_NAME), String.class).hasValue());
		Assert.assertFalse(ba.read(URI.create(DublinCore.LANGUAGE_PROPERTY_NAME), Date.class).hasValue());
		Assert.assertFalse(ba.read(URI.create(DublinCore.LANGUAGE_PROPERTY_NAME), Double.class).hasValue());
		Assert.assertFalse(ba.read(URI.create(DublinCore.LANGUAGE_PROPERTY_NAME), Integer.class).hasValue());
	}


	@Test
	@SuppressWarnings("deprecation")
	public void testAbstractAnnotator() {
		final Document doc = new Document();
		doc.setUri("test://test");
		final BaseAnnotator ba = new BaseAnnotator(doc);
		Assert.assertEquals(doc, ba.getResource());
		ba.retrieveAnnotation(Operator.READ, null);
		ba.writeLabel("test");
		ba.retrieveAnnotation(Operator.READ, null);
		ba.retrieveAnnotation(Operator.ADD, null);
		ba.retrieveAnnotation(Operator.ADD, "test");

		ba.setAppendMode(false);
		ba.applyOperator(Operator.WRITE, "none", URI.create("nop://nop"), "none", String.class, null);
		ba.applyOperator(Operator.ADD, "none", URI.create("nop://nop"), "none", String.class, "test");

		try {
			ba.applyOperator(Operator.SET, "none", URI.create("nop://nop"), "none", String.class, "test");
			Assert.fail("SET Operator is defined");
		} catch (final WebLabUncheckedException e) {
			Assert.assertNotNull(e);
		}

		ba.applyOperator(Operator.READ, "none", URI.create("nop://nop"), "none", String.class, "test");

		doc.getAnnotation().clear();
		final BaseAnnotator ba2 = new BaseAnnotator(doc);
		ba2.startInnerAnnotatorOn(URI.create("dummy://dummy"));
		ba2.retrieveAnnotation(Operator.WRITE, null);

		doc.getAnnotation().clear();
		final BaseAnnotator ba3 = new BaseAnnotator(doc);
		ba3.startInnerAnnotatorOn(URI.create("dummy://dummy"));
		ba3.retrieveAnnotation(Operator.WRITE, "none");
	}


	@Test
	public void testRDFDatatypeContainer() throws Exception {
		final Document doc = new Document();
		doc.setUri("test://test");
		final BaseAnnotator ba = new BaseAnnotator(doc);
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		final Date date = new Date();
		dca.writeDate(date);
		final String creator = "test";
		dca.writeCreator(creator);

		Value<RDFDatatypeContainer> value = ba.applyOperator(Operator.READ, "dc", URI.create("http://purl.org/dc/elements/1.1/"), "date", RDFDatatypeContainer.class, null);
		Assert.assertTrue(value.hasValue());
		Assert.assertEquals(Date.class, value.firstTypedValue().getClazz());
		Assert.assertEquals("http://www.w3.org/2001/XMLSchema#dateTime", value.firstTypedValue().getRdfDatatype());
		Assert.assertEquals(date, value.firstTypedValue().getObject());

		value = ba.applyOperator(Operator.READ, "dc", URI.create("http://purl.org/dc/elements/1.1/"), "creator", RDFDatatypeContainer.class, null);
		Assert.assertTrue(value.hasValue());
		Assert.assertEquals(String.class, value.firstTypedValue().getClazz());
		Assert.assertEquals(creator, value.firstTypedValue().getObject());

		final String creator2 = "test2";
		dca.writeCreator(creator2);

		value = ba.applyOperator(Operator.READ, "dc", URI.create("http://purl.org/dc/elements/1.1/"), "creator", RDFDatatypeContainer.class, null);
		Assert.assertTrue(value.hasValue());
		Assert.assertEquals(2, value.size());
		Assert.assertEquals(creator, value.getValues().get(0).getObject());
		Assert.assertNull(value.getValues().get(0).getRdfDatatype());
		Assert.assertEquals(creator2, value.getValues().get(1).getObject());

		try {
			// temporary remove Date parser
			final AbstractTypeParser<Date> parser = XMLParser.getParser(date);
			XMLParser.addTypeSupport(Date.class, null);

			// try to retrieve a Date
			value = ba.applyOperator(Operator.READ, "dc", URI.create("http://purl.org/dc/elements/1.1/"), "date", RDFDatatypeContainer.class, null);
			Assert.assertTrue(value.hasValue());
			Assert.assertEquals(String.class, value.firstTypedValue().getClazz());
			Assert.assertEquals("http://www.w3.org/2001/XMLSchema#dateTime", value.firstTypedValue().getRdfDatatype());
			Assert.assertEquals(date, parser.parse(value.firstTypedValue().getObject(String.class)));
		} finally {
			// make sure we reset parsers
			XMLParser.resetParsers();
		}

		try {
			new RDFDatatypeContainer(null, null);
			Assert.fail("A RDFDatatypeContainer must not contain a null object.");
		} catch (final NullPointerException npe) {
			Assert.assertNotNull(npe);
		}

		final RDFDatatypeContainer container = new RDFDatatypeContainer("test", null);
		final Value<RDFDatatypeContainer> result = ba.applyOperator(Operator.WRITE, "dc", URI.create("http://purl.org/dc/elements/1.1/"), "date", RDFDatatypeContainer.class, container);
		// value must not be written, so the pok must be null
		Assert.assertNull(result.getIsAnnotatedOn());

		// retrieve a type with a language
		final String lang = "en";
		dca.startSpecifyLanguage(lang);
		final String creator_en = "test_en";
		dca.writeCreator(creator_en);

		value = ba.applyOperator(Operator.READ, "dc", URI.create("http://purl.org/dc/elements/1.1/"), "creator", RDFDatatypeContainer.class, null);
		Assert.assertTrue(value.hasValue());
		Assert.assertEquals(3, value.size());
		Assert.assertEquals(creator_en, value.firstTypedValue(lang).getObject(String.class));

	}


	@Test
	public void testValuesLang() {
		final Document doc = new Document();
		doc.setUri("test://test");
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		final Date date = new Date();
		dca.writeDate(date);
		final String nbcreator = "nbtest";
		dca.writeCreator(nbcreator);
		dca.startSpecifyLanguage("fr");
		final String creator = "test";
		dca.writeCreator(creator);
		final String creator2 = "test2";
		dca.writeCreator(creator2);

		// search with fr language filter
		Assert.assertEquals(2, dca.readCreator().getValues().size());

		// check order and values
		Assert.assertEquals(creator, dca.readCreator().getValues().get(0));
		Assert.assertEquals(creator2, dca.readCreator().getValues().get(1));

		// remove language filter
		dca.endSpecifyLanguage();

		// all results
		Assert.assertEquals(3, dca.readCreator().getValues().size());

		// check order and values
		Assert.assertEquals(nbcreator, dca.readCreator().getValues().get(0));
		Assert.assertEquals(creator, dca.readCreator().getValues().get(1));
		Assert.assertEquals(creator2, dca.readCreator().getValues().get(2));

		// set new en language filter
		dca.startSpecifyLanguage("en");

		// no result
		Assert.assertEquals(0, dca.readCreator().getValues().size());

		// remove language filter
		dca.endSpecifyLanguage();

		// all results
		Assert.assertEquals(3, dca.readCreator().getValues().size());

		// check order and values
		Assert.assertEquals(nbcreator, dca.readCreator().getValues().get(0));
		Assert.assertEquals(creator, dca.readCreator().getValues().get(1));
		Assert.assertEquals(creator2, dca.readCreator().getValues().get(2));

		// select fr language results
		Assert.assertEquals(2, dca.readCreator().getValues("fr").size());

		// check order and values
		Assert.assertEquals(creator, dca.readCreator().getValues("fr").get(0));
		Assert.assertEquals(creator2, dca.readCreator().getValues("fr").get(1));

		// check no values
		Assert.assertNull(dca.readCreator().getValues("en"));
	}


	@Test
	public void testFullPropertyURI() {
		// test new method supporting URI instead of separated namespace/localname v1.2.7
		final Document doc = new Document();
		doc.setUri("test://test");
		final BaseAnnotator ban = new BaseAnnotator(doc);
		final Value<String> writeResult = ban.applyOperator(Operator.WRITE, "dc", URI.create("http://purl.org/dc/elements/1.1/creator"), String.class, "test");
		Assert.assertNotNull(writeResult.getIsAnnotatedOn());
		final Value<String> readResult = ban.applyOperator(Operator.READ, null, URI.create("http://purl.org/dc/elements/1.1/creator"), String.class, null);
		Assert.assertEquals(1, readResult.size());
		Assert.assertEquals("test", readResult.firstTypedValue());

		final Value<Boolean> writeResult2 = ban.applyOperator(Operator.WRITE, "wlp", URI.create("http://weblab.ow2.org/core/1.2/ontology/processing#canBeIgnored"), Boolean.class, Boolean.TRUE);
		Assert.assertNotNull(writeResult2.getIsAnnotatedOn());

		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		Assert.assertTrue(wpa.readCanBeIgnored().hasValue());
		Assert.assertTrue(wpa.readCanBeIgnored().firstTypedValue().booleanValue());
	}


	@Test
	public void testSeparatedAnnotations() {
		// add a test to make sure annotator retrieves data from all annotations on a resource
		final Document doc = new Document();
		doc.setUri("test://test");
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeCreator("creator");

		final Annotation annotation = WebLabResourceFactory.createAndLinkAnnotation(doc);
		final DublinCoreAnnotator dca2 = new DublinCoreAnnotator(URI.create(doc.getUri()), annotation);
		dca2.writeCreator("creator2");

		final DublinCoreAnnotator dca3 = new DublinCoreAnnotator(doc);
		Assert.assertEquals(2, dca.readCreator().getValues().size());
		Assert.assertEquals("creator", dca.readCreator().getValues().get(0));
		Assert.assertEquals("creator2", dca.readCreator().getValues().get(1));
		Assert.assertEquals(1, dca2.readCreator().getValues().size());
		Assert.assertEquals("creator2", dca2.readCreator().firstTypedValue());
		Assert.assertEquals(2, dca3.readCreator().getValues().size());
		Assert.assertEquals("creator", dca3.readCreator().getValues().get(0));
		Assert.assertEquals("creator2", dca3.readCreator().getValues().get(1));

		// we create a Text mediaunit
		final Text text = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final DublinCoreAnnotator dca4 = new DublinCoreAnnotator(text);
		// we write info about the doc in an annotation on a text mediaunit
		dca4.startInnerAnnotatorOn(URI.create(doc.getUri()));
		dca4.writeCreator("creator in text");

		final DublinCoreAnnotator dca5 = new DublinCoreAnnotator(doc);

		Assert.assertEquals(3, dca5.readCreator().getValues().size());
		Assert.assertEquals("creator", dca5.readCreator().getValues().get(0));
		Assert.assertEquals("creator2", dca5.readCreator().getValues().get(1));
		Assert.assertEquals("creator in text", dca5.readCreator().getValues().get(2));
	}

}