/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.factory;

import java.util.LinkedList;
import java.util.List;

import org.ow2.weblab.core.extended.exception.WebLabResourceCreationException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.model.Coordinate;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.SpatioTemporalSegment;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.TrackSegment;


/**
 * To create <code>Segment</code>s we strongly encourage to use this class. It will enable the creation of a <code>Segment</code> having an <code>URI</code>
 * compliant with the <code>WebLabRI</code> recommendations. <br>
 * Since an <code>Segment</code> hasn't any sense without being attached to a <code>MediaUnit</code>, we only provide a
 * method for creating <code>Segment</code> s that takes a <code>MediaUnit</code> in parameter. As a result, the created <code>Segment</code> (an instance of
 * the <code>Segment</code> class in
 * parameter) is added to the <code>Segment</code> <code>List</code> of the <code>MediaUnit</code>.
 *
 * For creating {@link MediaUnit} and other kind of {@link org.ow2.weblab.core.model.Resource} you shoud used {@link WebLabResourceFactory}.
 *
 * @see WebLabResourceFactory
 * @author Cassidian WebLab Team
 */
public final class SegmentFactory {


	/**
	 * Constructors Do not use it.
	 */
	private SegmentFactory() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Looks in <code>text</code> if a <code>LinearSegment</code> already exists at <code>start</code> and <code>end</code>. If it's the case, returns it.
	 * Otherwise, it creates a <code>LinearSegment</code> using {@link #createAndLinkSegment(MediaUnit, Class)} method, and setting the <code>start</code> and
	 * <code>end</code>.
	 *
	 * @param text
	 *            The <code>Text</code> containing the <code>Segment</code>
	 * @param start
	 *            char index of the <code>Segment</code> start
	 * @param end
	 *            char index of the <code>Segment</code> end
	 * @return Either a newly created <code>LinearSegment</code> or the existing one at the same position in the <code>Text</code>.
	 * @see #createAndLinkLinearSegment(Text, int, int, boolean)
	 */
	public static LinearSegment createAndLinkLinearSegment(final Text text, final int start, final int end) {
		return SegmentFactory.createAndLinkLinearSegment(text, start, end, false);
	}


	/**
	 * Creates a <code>LinearSegment</code> at <code>start</code> and <code>end</code>, or returns an existing one.
	 *
	 * @param text
	 *            The <code>Text</code> containing the <code>Segment</code>
	 * @param start
	 *            char index of the <code>Segment</code> start
	 * @param end
	 *            char index of the <code>Segment</code> end
	 * @param enableCollocatedSegment
	 *            Whether or not to prevent two segment (with two URIs) tobe at the same position. If <code>false</code>, it looks in <code>text</code> if a
	 *            <code>LinearSegment</code> already exists at <code>start</code> and <code>end</code>. If it's the case, returns it. Otherwise, it creates a
	 *            <code>LinearSegment</code> using {@link #createAndLinkSegment(MediaUnit, Class)} method, and setting the <code>start</code> and
	 *            <code>end</code>.
	 * @return Either a newly created <code>LinearSegment</code> or an existing one at the same position in the <code>Text</code> if
	 *         <code>enableCollocatedSegment</code> is false.
	 */
	public static LinearSegment createAndLinkLinearSegment(final Text text, final int start, final int end, final boolean enableCollocatedSegment) {
		SegmentFactory.checkArgsForLinearAndTemporal(text, start, end);
		LinearSegment theSeg = null;
		if (!enableCollocatedSegment) {
			theSeg = SegmentFactory.getExistingSegment(text.getSegment(), start, end, LinearSegment.class);
		}
		if (theSeg == null) {
			theSeg = SegmentFactory.createAndLinkSegment(text, LinearSegment.class);
			theSeg.setStart(start);
			theSeg.setEnd(end);
		}
		return theSeg;
	}


	/**
	 * @param track
	 *            The TrackSegment in which to add a Linear
	 * @param start
	 *            The starting offset of the linear
	 * @param end
	 *            The ending offset of the linear
	 * @return The created linear
	 */
	public static LinearSegment createAndLinkLinearSegment(final TrackSegment track, final int start, final int end) {
		SegmentFactory.checkArgsForLinearAndTemporal(track, start, end);
		final LinearSegment lSeg = SegmentFactory.createAndLinkSegment(track, LinearSegment.class);
		lSeg.setStart(start);
		lSeg.setEnd(end);
		return lSeg;
	}


	/**
	 * Automatically set a valid <code>WebLabRI</code> to the created <code>Segment</code> using the <code>MediaUnit</code> to generate an unique id. Created
	 * <code>Segment</code> will be added to the <code>MediaUnit</code>.
	 *
	 * @param <T>
	 *            The subclass of <code>Segment</code> to be created
	 * @param mediaUnit
	 *            the <code>MediaUnit</code> that will contains the created <code>Segment</code>
	 * @param segClass
	 *            The class of the <code>Segment</code> to be created
	 * @return a new instance of <code>segClass</code> with a valid <code>WebLabRI</code> and added to the <code>MediaUnit</code>
	 */
	public static <T extends Segment> T createAndLinkSegment(final MediaUnit mediaUnit, final Class<T> segClass) {
		final T segment;
		try {
			segment = segClass.newInstance();
		} catch (final InstantiationException ie) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + segClass.getName(), ie);
		} catch (final IllegalAccessException iae) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + segClass.getName(), iae);
		}
		segment.setUri(AbstractFactory.createUniqueURIFrom(mediaUnit, true, false));
		AbstractFactory.addSegment(segment, mediaUnit, mediaUnit);

		return segment;
	}


	/**
	 * Automatically set a valid <code>WebLabRI</code> to the created <code>Segment</code> using the <code>TrackSegment</code> to generate an unique id. Created
	 * <code>Segment</code> will be added to the <code>TrackSegment</code>.
	 *
	 * @param <T>
	 *            The subclass of <code>Segment</code> to be created
	 * @param track
	 *            the <code>TrackSegment</code> that will contains the created <code>Segment</code>
	 * @param segClass
	 *            The class of the <code>Segment</code> to be created
	 * @return a new instance of <code>segClass</code> with a valid <code>WebLabRI</code> and added to the <code>TrackSegment</code>
	 */
	public static <T extends Segment> T createAndLinkSegment(final TrackSegment track, final Class<T> segClass) {
		final T segment;
		try {
			segment = segClass.newInstance();
		} catch (final InstantiationException ie) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + segClass.getName(), ie);
		} catch (final IllegalAccessException iae) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + segClass.getName(), iae);
		}
		segment.setUri(AbstractFactory.createSegmentURIFor(track));
		track.getSegment().add(segment);

		return segment;
	}


	/**
	 * Creates a <code>SpatioTemporalSegment</code> at <code>timestamp</code>. A best practice is that timestamp is expressed in millisecond.
	 *
	 * @param mediaUnit
	 *            The <code>MediaUnit</code> containing the <code>Segment</code>
	 * @param shape
	 *            An array of int containing coordinates to be affected to this segment. The smallest array is <code>{x1, y1, x2, y2}</code>. In this case
	 *            coordinate 1 (x1;y1) is the center of a disk, where coordinate 2 (x2;y2) is a point of the border. When shape contains more than tree
	 *            coordinates, it defines a closed polygon.
	 * @return The newly created <code>SpatialSegment</code>
	 */
	public static SpatialSegment createAndLinkSpatialSegment(final MediaUnit mediaUnit, final int... shape) {
		final List<Coordinate> coordinates = SegmentFactory.checkAndcreateCoordinates(mediaUnit, shape);
		final SpatialSegment theSeg = SegmentFactory.createAndLinkSegment(mediaUnit, SpatialSegment.class);
		theSeg.getCoordinate().addAll(coordinates);
		return theSeg;
	}


	/**
	 * @param track
	 *            The track segment in which to add a spatio temporal
	 * @param shape
	 *            An array of int containing coordinates to be affected to this segment. The smallest array is <code>{x1, y1, x2, y2}</code>. In this case
	 *            coordinate 1 (x1;y1) is the center of a disk, where coordinate 2 (x2;y2) is a point of the border. When shape contains more than tree
	 *            coordinates, it defines a closed polygon.
	 * @return The newly created <code>SpatialSegment</code>
	 */
	public static SpatialSegment createAndLinkSpatialSegment(final TrackSegment track, final int... shape) {
		final List<Coordinate> coordinates = SegmentFactory.checkAndcreateCoordinates(track, shape);
		final SpatialSegment theSeg = SegmentFactory.createAndLinkSegment(track, SpatialSegment.class);
		theSeg.getCoordinate().addAll(coordinates);
		return theSeg;
	}


	/**
	 * Creates a <code>SpatioTemporalSegment</code> at <code>timestamp</code>. A best practice is that timestamp is expressed in millisecond.
	 *
	 * @param mediaUnit
	 *            The <code>MediaUnit</code> containing the <code>Segment</code>
	 * @param timestamp
	 *            index of the <code>Segment</code> start
	 * @param shape
	 *            An array of int containing coordinates to be affected to this segment. The smallest array is <code>{x1, y1, x2, y2}</code>. In this case
	 *            coordinate 1 (x1;y1) is the center of a disk, where coodinate 2 (x2;y2) is a point of the border. When shape contains more than tree
	 *            coordinates, it defines a closed polygon.
	 * @return The newly created <code>SpatioTemporalSegment</code>
	 */
	public static SpatioTemporalSegment createAndLinkSpatioTemporalSegment(final MediaUnit mediaUnit, final int timestamp, final int... shape) {
		final List<Coordinate> coordinates = SegmentFactory.checkAndcreateCoordinates(mediaUnit, shape);
		final SpatioTemporalSegment theSeg = SegmentFactory.createAndLinkSegment(mediaUnit, SpatioTemporalSegment.class);
		theSeg.setTimestamp(timestamp);
		theSeg.getCoordinate().addAll(coordinates);
		return theSeg;
	}


	/**
	 * @param track
	 *            The track segment in which to add a spatio temporal
	 * @param timestamp
	 *            The index of the <code>Segment</code> start
	 * @param shape
	 *            An array of int containing coordinates to be affected to this segment. The smallest array is <code>{x1, y1, x2, y2}</code>. In this case
	 *            coordinate 1 (x1;y1) is the centre of a disk, where coordinate 2 (x2;y2) is a point of the border. When shape contains more than tree
	 *            coordinates, it defines a closed polygon.
	 * @return The newly created <code>SpatioTemporalSegment</code>
	 */
	public static SpatioTemporalSegment createAndLinkSpatioTemporalSegment(final TrackSegment track, final int timestamp, final int... shape) {
		final List<Coordinate> coordinates = SegmentFactory.checkAndcreateCoordinates(track, shape);
		final SpatioTemporalSegment theSeg = SegmentFactory.createAndLinkSegment(track, SpatioTemporalSegment.class);
		theSeg.setTimestamp(timestamp);
		theSeg.getCoordinate().addAll(coordinates);
		return theSeg;
	}


	/**
	 * Look in <code>MediaUnit</code> if a <code>TemporalSegment</code> already exists at <code>start</code> and <code>end</code>. If it's the case, returns it.
	 * Otherwise, it creates a <code>TemporalSegment</code> using {@link #createAndLinkSegment(MediaUnit, Class)} method,
	 * and setting the <code>start</code> and <code>end</code>.
	 *
	 * A best practice is that indexes are expressed in millisecond.
	 *
	 * @param mediaUnit
	 *            The <code>MediaUnit</code> containing the <code>Segment</code>
	 * @param start
	 *            index of the <code>Segment</code> start
	 * @param end
	 *            index of the <code>Segment</code> end
	 * @return Either a newly created <code>TemporalSegment</code> or the existing one at the same position in the <code>MediaUnit</code>.
	 */
	public static TemporalSegment createAndLinkTemporalSegment(final MediaUnit mediaUnit, final int start, final int end) {
		return SegmentFactory.createAndLinkTemporalSegment(mediaUnit, start, end, false);
	}


	/**
	 * Creates a <code>TemporalSegment</code> at <code>start</code> and <code>end</code>, or returns an existing one.
	 * A best practice is that indexes are expressed in millisecond.
	 *
	 * @param mediaUnit
	 *            The <code>MediaUnit</code> containing the <code>Segment</code>
	 * @param start
	 *            index of the <code>Segment</code> start
	 * @param end
	 *            index of the <code>Segment</code> end
	 * @param enableCollocatedSegment
	 *            Whether or not to prevent two segment (with two URIs) to be at the same position. If <code>false</code>, it looks in <code>mu</code> if a
	 *            <code>TemporalSegment</code> already exists at <code>start</code> and <code>end</code>. If it's the case, returns it. Otherwise, it creates a
	 *            <code>TemporalSegment</code> using {@link #createAndLinkSegment(MediaUnit, Class)} method, and setting the <code>start</code> and
	 *            <code>end</code>.
	 * @return Either a newly created <code>TemporalSegment</code> or the existing one at the same position in the <code>MediaUnit</code> if
	 *         <code>enableCollocatedSegment</code> is false.
	 */
	public static TemporalSegment createAndLinkTemporalSegment(final MediaUnit mediaUnit, final int start, final int end, final boolean enableCollocatedSegment) {
		SegmentFactory.checkArgsForLinearAndTemporal(mediaUnit, start, end);
		TemporalSegment theSeg = null;
		if (!enableCollocatedSegment) {
			theSeg = SegmentFactory.getExistingSegment(mediaUnit.getSegment(), start, end, TemporalSegment.class);
		}
		if (theSeg == null) {
			theSeg = SegmentFactory.createAndLinkSegment(mediaUnit, TemporalSegment.class);
			theSeg.setStart(start);
			theSeg.setEnd(end);
		}
		return theSeg;
	}


	/**
	 * @param track
	 *            The TrackSegment in which to add a Linear
	 * @param start
	 *            The starting offset of the linear
	 * @param end
	 *            The ending offset of the linear
	 * @return The created temporal
	 */
	public static TemporalSegment createAndLinkTemporalSegment(final TrackSegment track, final int start, final int end) {
		SegmentFactory.checkArgsForLinearAndTemporal(track, start, end);
		final TemporalSegment tSeg = SegmentFactory.createAndLinkSegment(track, TemporalSegment.class);
		tSeg.setStart(start);
		tSeg.setEnd(end);
		return tSeg;
	}


	/**
	 * @param container
	 *            An instance of TrackSegment or of MediaUnit in which the segment will be create.
	 * @param shape
	 *            The array of coordinates to be converted. It should be an even number of dimension, not smaller than four
	 * @return The created list of coordinate
	 */
	private static List<Coordinate> checkAndcreateCoordinates(final Object container, final int... shape) {
		if ((container == null) || (shape == null)) {
			throw new WebLabUncheckedException("Argument was null.");
		}
		if ((shape.length % 2) != 0) {
			throw new WebLabUncheckedException("Coordinate array not valid (should be of an even length).");
		}
		if (shape.length < 4) {
			throw new WebLabUncheckedException("Not enougth coordinates in the shape array (at least two points should be described, i.e. 4 int).");
		}

		final List<Coordinate> coordinates = new LinkedList<>();
		for (int i = 0; i < (shape.length / 2); i++) {
			final Coordinate coordoI = new Coordinate();
			coordoI.setX(shape[2 * i]);
			coordoI.setY(shape[(2 * i) + 1]);
			coordinates.add(coordoI);
		}
		return coordinates;
	}


	/**
	 * @param container
	 *            An instance of TrackSegment or of MediaUnit in which the segment will be create.
	 * @param start
	 *            The starting offset of the linear
	 * @param end
	 *            The ending offset of the linear
	 */
	private static void checkArgsForLinearAndTemporal(final Object container, final int start, final int end) {
		if (container == null) {
			throw new WebLabUncheckedException("Argument was null.");
		}
		if (start > end) {
			throw new WebLabUncheckedException("Starting index was bigger than ending one.");
		}
	}


	/**
	 * @param segments
	 *            The list of segment to check in
	 * @param start
	 *            The starting offset of the linear
	 * @param end
	 *            The ending offset of the linear
	 * @param linearOrTemporalSegment
	 *            The type of segment to check existence
	 * @return An existing instance of segment located at start/end or null
	 */
	private static <T extends Segment> T getExistingSegment(final List<Segment> segments, final int start, final int end, final Class<T> linearOrTemporalSegment) {
		final boolean isLinear;
		if (linearOrTemporalSegment.equals(LinearSegment.class)) {
			isLinear = true;
		} else {
			isLinear = false;
		}

		T theSeg = null;
		for (final Segment seg : segments) {
			if ((seg instanceof LinearSegment) && isLinear) {
				final LinearSegment linSeg = (LinearSegment) seg;
				if ((linSeg.getStart() == start) && (linSeg.getEnd() == end)) {
					theSeg = linearOrTemporalSegment.cast(linSeg);
					break;
				}
			} else if ((seg instanceof TemporalSegment) && !isLinear) {
				final TemporalSegment tempSeg = (TemporalSegment) seg;
				if ((tempSeg.getStart() == start) && (tempSeg.getEnd() == end)) {
					theSeg = linearOrTemporalSegment.cast(tempSeg);
					break;
				}
			}
		}
		return theSeg;
	}

}
