/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.ontologies;

/**
 * Just a useful class containing the <code>String</code> URIs of the Media Ontology (http://www.w3.org/ns/ma-ont)
 * 
 * @author Cassidian WebLab Team
 */
public final class MediaOntology {


	/**
	 * The namespace of all those classes and properties.
	 */
	public static final String NAMESPACE = "http://www.w3.org/ns/ma-ont#";


	/**
	 * Preferred prefix to be used to represent the MediaOntology ontology namespace.
	 */
	public static final String PREFERRED_PREFIX = "ma-ont";


	/**
	 * A person or organisation contributing to the media resource.
	 */
	public static final String AGENT = MediaOntology.NAMESPACE + "Agent";


	/**
	 * Corresponds to 'title.title' in the Ontology for Media Resources with a 'title.type' meaning "alternative".
	 */
	public static final String ALTERNATIVE_TITLE = MediaOntology.NAMESPACE + "alternativeTitle";


	/**
	 * A specialisation of Track for Audio to provide a link to specific data properties such as sampleRate, etc. Specialisation is defined through object
	 * properties.
	 */
	public static final String AUDIO_TRACK = MediaOntology.NAMESPACE + "AudioTrack";


	/**
	 * Corresponds to 'averageBitRate' in the Ontology for Media Resources, expressed in kilobits/second.
	 */
	public static final String AVERAGE_BIT_RATE = MediaOntology.NAMESPACE + "averageBitRate";


	/**
	 * Any group of media resource e.g. a series.
	 */
	public static final String COLLECTION = MediaOntology.NAMESPACE + "Collection";


	/**
	 * The name by which a collection (e.g. series) is known.
	 */
	public static final String COLLECTION_NAME = MediaOntology.NAMESPACE + "collectionName";


	/**
	 * Corresponds to 'copyright.copyright' in the Ontology for Media Resources.
	 */
	public static final String COPYRIGHT = MediaOntology.NAMESPACE + "copyright";


	/**
	 * A subproperty of 'hasRelatedLocation" used to specify where material shooting took place.
	 */
	public static final String CREATED_IN = MediaOntology.NAMESPACE + "createdIn";



	/**
	 * Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "creationDate".
	 */
	public static final String CREATION_DATE = MediaOntology.NAMESPACE + "creationDate";


	/**
	 * Ancillary data track e.g. ¨captioning" in addition to video and audio tracks. Specialisation is made through the use of appropriate object properties.
	 */
	public static final String DATA_TRACK = MediaOntology.NAMESPACE + "DataTrack";


	/**
	 * Corresponds to date.date in the ontology for Media Resources. Subproperties can be used to distinguish different values of 'date.type'. The recommended
	 * range is 'xsd:dateTime' (for compliance with OWL2-QL and OWL2-RL) but other time-related datatypes may be used (e.g. 'xsd:gYear', 'xsd:date'...).
	 */
	public static final String DATE = MediaOntology.NAMESPACE + "date";


	/**
	 * A subproperty of 'hasRelatedLocation' used to specify where the action depicted in the media is supposed to take place, as opposed to the location where
	 * shooting actually took place (see 'createdIn').
	 */
	public static final String DEPICTS_FICTIONAL_LOCATION = MediaOntology.NAMESPACE + "depictsFictionalLocation";


	/**
	 * Corresponds to 'description' in the Ontology for Media Resources. This can be specialised by using sub-properties e.g. 'summary' or 'script'.
	 */
	public static final String DESCRIPTION = MediaOntology.NAMESPACE + "description";


	/**
	 * Corresponds to 'duration' in the Ontology for Media Resources.
	 */
	public static final String DURATION = MediaOntology.NAMESPACE + "duration";


	/**
	 * Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "editDate".
	 */
	public static final String EDIT_DATE = MediaOntology.NAMESPACE + "editDate";


	/**
	 * Corresponds to 'contributor.contributor' in the Ontology for Media Resources with a 'contributor.role' meaning "actor".
	 */
	public static final String FEATURES = MediaOntology.NAMESPACE + "features";


	/**
	 * Corresponds to 'namedFragment.label' in the Ontology for Media Resources.
	 */
	public static final String FRAGMENT_NAME = MediaOntology.NAMESPACE + "fragmentName";


	/**
	 * Corresponds to 'frameSize.height' in the Ontology for Media Resources, measured in frameSizeUnit.
	 */
	public static final String FRAME_HEIGHT = MediaOntology.NAMESPACE + "frameHeight";


	/**
	 * Corresponds to 'frameRate' in the Ontology for Media Resources, in frame per second.
	 */
	public static final String FRAME_RATE = MediaOntology.NAMESPACE + "frameRate";


	/**
	 * Corresponds to 'frameSize.unit' in the Ontology for Media Resources.
	 */
	public static final String FRAME_SIZE_UNIT = MediaOntology.NAMESPACE + "frameSizeUnit";


	/**
	 * Corresponds to 'frameSize.width' in the Ontology for Media Resources measured in frameSizeUnit.
	 */
	public static final String FRAME_WIDTH = MediaOntology.NAMESPACE + "frameWidth";


	/**
	 * Corresponds to 'policy' in the Ontology for Media Resources with a 'policy.type' "access conditions".
	 */
	public static final String HAS_ACCESS_CONDITIONS = MediaOntology.NAMESPACE + "hasAccessConditions";


	/**
	 * Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "audio-description".
	 */
	public static final String HAS_AUDIO_DESCRIPTION = MediaOntology.NAMESPACE + "hasAudioDescription";


	/**
	 * Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "captioning". This property can for example point to a
	 * spatial fragment, a VideoTrack or a DataTrack. The language of the captioning track can be expressed by attaching a 'hasLanguage' property to the
	 * specific track.
	 */
	public static final String HAS_CAPTIONING = MediaOntology.NAMESPACE + "hasCaptioning";


	/**
	 * Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "actor".
	 */
	public static final String HAS_CHAPTER = MediaOntology.NAMESPACE + "hasChapter";


	/**
	 * Corresponds to 'targetAudience.classification' in the Ontology for Media Resources. This property is used to provide a value characterising the target
	 * audience.
	 */
	public static final String HAS_CLASSIFICATION = MediaOntology.NAMESPACE + "hasClassification";


	/**
	 * Corresponds to 'targetAudience.identifier' in the Ontology for Media Resources. This is used to identify the reference sheme against which the target
	 * audience has been characterised.
	 */
	public static final String HAS_CLASSIFICATION_SYSTEM = MediaOntology.NAMESPACE + "hasClassificationSystem";


	/**
	 * Corresponds to 'compression' in the Ontology for Media Resources.
	 */
	public static final String HAS_COMPRESSION = MediaOntology.NAMESPACE + "hasCompression";


	/**
	 * Inverse of hasContributor.
	 */
	public static final String HAS_CONTRIBUTED_TO = MediaOntology.NAMESPACE + "hasContributedTo";


	/**
	 * Corresponds to 'contributor.contributor' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of
	 * 'contributor.role'.
	 */
	public static final String HAS_CONTRIBUTOR = MediaOntology.NAMESPACE + "hasContributor";


	/**
	 * Inverse of isCopyrightedBy.
	 */
	public static final String HAS_COPYRIGHT_OVER = MediaOntology.NAMESPACE + "hasCopyrightOver";


	/**
	 * Inverse of hasCreator
	 */
	public static final String HAS_CREATED = MediaOntology.NAMESPACE + "hasCreated";


	/**
	 * Corresponds to 'creator.creator' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'creator.role'. Note
	 * that this property is semantically a subproperty of 'hasContributor'.
	 */
	public static final String HAS_CREATOR = MediaOntology.NAMESPACE + "hasCreator";


	/**
	 * Corresponds to 'format' in the Ontology for Media Resources.
	 */
	public static final String HAS_FORMAT = MediaOntology.NAMESPACE + "hasFormat";


	/**
	 * Corresponds to 'fragment' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'fragment.role'.
	 */
	public static final String HAS_FRAGMENT = MediaOntology.NAMESPACE + "hasFragment";


	/**
	 * Corresponds to 'genre' in the Ontology for Media Resources.
	 */
	public static final String HAS_GENRE = MediaOntology.NAMESPACE + "hasGenre";


	/**
	 * Corresponds to 'keyword' in the Ontology for Media Resources.
	 */
	public static final String HAS_KEYWORD = MediaOntology.NAMESPACE + "hasKeyword";


	/**
	 * Corresponds to 'language' in the Ontology for Media Resources. The language used in the resource. A controlled vocabulary such as defined in BCP 47
	 * SHOULD be used. This property can also be used to identify the presence of sign language (RFC 5646). By inheritance, the hasLanguage property applies
	 * indifferently at the media resource / fragment / track levels. Best practice recommends to use to best possible level of granularity for describe the
	 * usage of language within a media resource including at fragment and track levels.
	 */
	public static final String HAS_LANGUAGE = MediaOntology.NAMESPACE + "hasLanguage";


	/**
	 * Corresponds to 'location.coordinateSystem' in the Ontology for Media Resources.
	 */
	public static final String HAS_LOCATION_COORDINATESYSTEM = MediaOntology.NAMESPACE + "hasLocationCoordinateSystem";


	/**
	 * Inverse of isMemberOf.
	 */
	public static final String HAS_MEMBER = MediaOntology.NAMESPACE + "hasMember";


	/**
	 * Corresponds to 'namedFragment' in the Ontology for Media Resources.
	 */
	public static final String HAS_NAMED_FRAGMENT = MediaOntology.NAMESPACE + "hasNamedFragment";


	/**
	 * Corresponds to 'policy' in the Ontology for Media Resources with a 'policy.type' meaning "permissions".
	 */
	public static final String HAS_PERMISSIONS = MediaOntology.NAMESPACE + "hasPermissions";


	/**
	 * Corresponds to 'policy' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'policy.type'.
	 */
	public static final String HAS_POLICY = MediaOntology.NAMESPACE + "hasPolicy";


	/**
	 * Inverse of hasPublisher.
	 */
	public static final String HAS_PUBLISHED = MediaOntology.NAMESPACE + "hasPublished";


	/**
	 * Corresponds to 'publisher' in the Ontology for Media Resources.
	 */
	public static final String HAS_PUBLISHER = MediaOntology.NAMESPACE + "hasPublisher";


	/**
	 * Corresponds to 'rating' in the Ontology for Media Resources.
	 */
	public static final String HAS_RATING = MediaOntology.NAMESPACE + "hasRating";


	/**
	 * Corresponds to 'rating.type' in the Ontology for Media Resources.
	 */
	public static final String HAS_RATING_SYSTEM = MediaOntology.NAMESPACE + "hasRatingSystem";


	/**
	 * Corresponds to 'relation' and in the Ontology for Media Resources with a 'relation.type' meaning "related image".
	 */
	public static final String HAS_RELATED_IMAGE = MediaOntology.NAMESPACE + "hasRelatedImage";


	/**
	 * Corresponds to 'location' in the Ontology for Media Resources. Subproperties are provided to specify, when possible, the relation between the media
	 * resource and the location.
	 */
	public static final String HAS_RELATED_LOCATION = MediaOntology.NAMESPACE + "hasRelatedLocation";


	/**
	 * Corresponds to 'relation' and in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'relation.type'.
	 */
	public static final String HAS_RELATED_RESOURCE = MediaOntology.NAMESPACE + "hasRelatedResource";


	/**
	 * Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "signing". This property can for example point to a spatial
	 * fragment or a VideoTrack. The sign language of the captioning track can be expressed by attaching a 'hasLanguage' property to the specific track.
	 */
	public static final String HAS_SIGNING = MediaOntology.NAMESPACE + "hasSigning";


	/**
	 * Corresponds to 'relation' and in the Ontology for Media Resources with a 'relation.type' meaning "source".
	 */
	public static final String HAS_SOURCE = MediaOntology.NAMESPACE + "hasSource";


	/**
	 * Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "subtitling".
	 */
	public static final String HAS_SUBTITLING = MediaOntology.NAMESPACE + "hasSubtitling";


	/**
	 * Corresponds to 'targetAudience' in the Ontology for Media Resources.
	 */
	public static final String HAS_TARGET_AUDIENCE = MediaOntology.NAMESPACE + "hasTargetAudience";


	/**
	 * Corresponds to 'fragment' in the Ontology for Media Resources with a 'fragment.role' meaning "track".
	 */
	public static final String HAS_TRACK = MediaOntology.NAMESPACE + "hasTrack";


	/**
	 * A still image / thumbnail / key frame related to the media resource or being the media resource itself.
	 */
	public static final String IMAGE = MediaOntology.NAMESPACE + "Image";


	/**
	 * Inverse of hasCaptioning.
	 */
	public static final String IS_CAPTIONING_OF = MediaOntology.NAMESPACE + "isCaptioningOf";


	/**
	 * Inverse of hasChapter.
	 */
	public static final String IS_CHAPTER_OF = MediaOntology.NAMESPACE + "isChapterOf";


	/**
	 * Corresponds to 'copyright.identifier' in the Ontology for Media Resources.
	 */
	public static final String IS_COPYRIGHTED_BY = MediaOntology.NAMESPACE + "isCopyrightedBy";


	/**
	 * Inverse of createdIn.
	 */
	public static final String IS_CREATION_LOCATION_OF = MediaOntology.NAMESPACE + "isCreationLocationOf";


	/**
	 * Inverse of depictsFictionalLocation.
	 */
	public static final String IS_FICTIONAL_LOCATION_DEPICTED_IN = MediaOntology.NAMESPACE + "isFictionalLocationDepictedIn";


	/**
	 * Inverse of hasFragment.
	 */
	public static final String IS_FRAGMENT_OF = MediaOntology.NAMESPACE + "isFragmentOf";


	/**
	 * Inverse of hasRelatedImage.
	 */
	public static final String IS_IMAGE_RELATED_TO = MediaOntology.NAMESPACE + "isImageRelatedTo";


	/**
	 * Inverse of hasRelatedLocation.
	 */
	public static final String IS_LOCATION_RELATED_TO = MediaOntology.NAMESPACE + "isLocationRelatedTo";


	/**
	 * Corresponds to 'collection' in the Ontology for Media Resources.
	 */
	public static final String IS_MEMBER_OF = MediaOntology.NAMESPACE + "isMemberOf";


	/**
	 * Inverse of hasNamedFragment.
	 */
	public static final String IS_NAMED_FRAGMENT_OF = MediaOntology.NAMESPACE + "isNamedFragmentOf";


	/**
	 * Corresponds to 'rating.identifier' in the Ontology for Media Resources.
	 */
	public static final String IS_PROVIDED_BY = MediaOntology.NAMESPACE + "isProvidedBy";


	/**
	 * Inverse of property hasRating. Note that there is an error in the real ontology file.
	 */
	public static final String IS_RATING_OF = MediaOntology.NAMESPACE + "IsRatingOf";


	/**
	 * Inverse of property hasRating. Note that there is an error in the real ontology file. This one does not define the property has specified but has it
	 * should.
	 */
	public static final String IS_RATING_OF_CORRECTED = MediaOntology.NAMESPACE + "isRatingOf";


	/**
	 * Inverse of hasRelatedResource.
	 */
	public static final String IS_RELATED_TO = MediaOntology.NAMESPACE + "isRelatedTo";


	/**
	 * Inverse of hasSigning.
	 */
	public static final String IS_SIGNING_OF = MediaOntology.NAMESPACE + "isSigningOf";


	/**
	 * Inverse of hasSource.
	 */
	public static final String IS_SOURCE_OF = MediaOntology.NAMESPACE + "isSourceOf";


	/**
	 * Inverse of hasTargetAudience.
	 */
	public static final String IS_TARGET_AUDIENCE_OF = MediaOntology.NAMESPACE + "isTargetAudienceOf";


	/**
	 * Inverse of hasTrack.
	 */
	public static final String IS_TRACK_OF = MediaOntology.NAMESPACE + "isTrackOf";


	/**
	 * A location related to the media resource, e.g. depicted in the resource (possibly fictional) or where the resource was created (shooting location), etc.
	 */
	public static final String LOCATION = MediaOntology.NAMESPACE + "Location";


	/**
	 * Corresponds to 'location.altitude' in the Ontology for Media Resources.
	 */
	public static final String LOCATION_ALTITUDE = MediaOntology.NAMESPACE + "locationAltitude";


	/**
	 * Corresponds to 'location.latitude' in the Ontology for Media Resources.
	 */
	public static final String LOCATION_LATITUDE = MediaOntology.NAMESPACE + "locationLatitude";


	/**
	 * Corresponds to 'location.longitude' in the Ontology for Media Resources.
	 */
	public static final String LOCATION_LONGITUDE = MediaOntology.NAMESPACE + "locationLongitude";


	/**
	 * Corresponds to 'location.name' in the Ontology for Media Resources.
	 */
	public static final String LOCATION_NAME = MediaOntology.NAMESPACE + "locationName";


	/**
	 * Corresponds to 'locator' in the Ontology for Media Resources.
	 */
	public static final String LOCATOR = MediaOntology.NAMESPACE + "locator";


	/**
	 * Corresponds to 'title.title' in the Ontology for Media Resources with a 'title.type' meaning "original".
	 */
	public static final String MAIN_ORIGINAL_TITLE = MediaOntology.NAMESPACE + "mainOriginalTitle";


	/**
	 * A media fragment (spatial, temporal, track...) composing a media resource. In other ontologies fragment is sometimes referred to as a 'part' or
	 * 'segment'.
	 */
	public static final String MEDIA_FRAGMENT = MediaOntology.NAMESPACE + "MediaFragment";


	/**
	 * An image or an audiovisual media resource, which can be composed of one or more fragment / track.
	 */
	public static final String MEDIA_RESOURCE = MediaOntology.NAMESPACE + "MediaResource";


	/**
	 * Corresponds to 'numTracks.number' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'numTracks.type'.
	 */
	public static final String NUMBER_OF_TRACKS = MediaOntology.NAMESPACE + "numberOfTracks";


	/**
	 * An organisation or moral agent
	 */
	public static final String ORGANISATION = MediaOntology.NAMESPACE + "Organisation";


	/**
	 * A physical person.
	 */
	public static final String PERSON = MediaOntology.NAMESPACE + "Person";


	/**
	 * Inverse of features.
	 */
	public static final String PLAYS_IN = MediaOntology.NAMESPACE + "playsIn";


	/**
	 * Inverse of isProvidedBy.
	 */
	public static final String PROVIDES = MediaOntology.NAMESPACE + "provides";


	/**
	 * Information about the rating given to a media resource.
	 */
	public static final String RATING = MediaOntology.NAMESPACE + "Rating";


	/**
	 * Corresponds to 'rating.max' in the Ontology for Media Resources.
	 */
	public static final String RATING_SCALE_MAX = MediaOntology.NAMESPACE + "ratingScaleMax";


	/**
	 * Corresponds to 'rating.min' in the Ontology for Media Resources.
	 */
	public static final String RATING_SCALE_MIN = MediaOntology.NAMESPACE + "ratingScaleMin";


	/**
	 * Corresponds to 'rating.value' in the Ontology for Media Resources.
	 */
	public static final String RATING_VALUE = MediaOntology.NAMESPACE + "ratingValue";


	/**
	 * Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "recordDate".
	 */
	public static final String RECORD_DATE = MediaOntology.NAMESPACE + "recordDate";


	/**
	 * Corresponds to 'date.date' in the Ontology for Media Resources with a 'date.type' meaning "releaseDate".
	 */
	public static final String RELEASE_DATE = MediaOntology.NAMESPACE + "releaseDate";


	/**
	 * Corresponds to 'samplingRate' in the Ontology for Media Resources, in samples per second.
	 */
	public static final String SAMPLING_RATE = MediaOntology.NAMESPACE + "samplingRate";


	/**
	 * Information about The target audience (target region, target audience category but also parental guidance recommendation) for which a media resource is
	 * intended.
	 */
	public static final String TARGET_AUDIENCE = MediaOntology.NAMESPACE + "TargetAudience";


	/**
	 * Corresponds to 'title.title' in the Ontology for Media Resources. Subproperties can be used to distinguish different values of 'title.type'.
	 */
	public static final String TITLE = MediaOntology.NAMESPACE + "title";


	/**
	 * A specialisation of MediaFragment for audiovisual content.
	 */
	public static final String TRACK = MediaOntology.NAMESPACE + "Track";


	/**
	 * Corresponds to 'fragment.name' in the Ontology for Media Resources, for Track fragments.
	 */
	public static final String TRACK_NAME = MediaOntology.NAMESPACE + "trackName";


	/**
	 * A specialisation of Track for Video to provide a link to specific data properties such as frameRate, etc. Signing is another possible example of video
	 * track. Specialisation is defined through object properties.
	 */
	public static final String VIDEO_TRACK = MediaOntology.NAMESPACE + "VideoTrack";


	/**
	 * Constructors
	 * Do not use it.
	 */
	private MediaOntology() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}

}
