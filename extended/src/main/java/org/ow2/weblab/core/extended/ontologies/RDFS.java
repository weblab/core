/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.ontologies;

/**
 * Just a useful class containing the <code>String</code> URIs of the RDFS specification.
 * 
 * @author Cassidian WebLab Team
 */
public final class RDFS {


	/**
	 * The namespace of all those classes and properties.
	 */
	public static final String NAMESPACE = "http://www.w3.org/2000/01/rdf-schema#";


	/**
	 * The class of classes.
	 */
	public static final String CLASS = RDFS.NAMESPACE + "Class";


	/**
	 * A description of the subject resource.
	 */
	public static final String COMMENT = RDFS.NAMESPACE + "comment";


	/**
	 * The class of RDF containers.
	 */
	public static final String CONTAINER = RDFS.NAMESPACE + "Container";


	/**
	 * The class of container membership properties, rdf:_1, rdf:_2, ..., all of which are sub-properties of 'member'.
	 */
	public static final String CONTAINER_MEMBERSHIP_PROPERTY = RDFS.NAMESPACE + "ContainerMembershipProperty";


	/**
	 * The class of RDF datatypes.
	 */
	public static final String DATATYPE = RDFS.NAMESPACE + "Datatype";


	/**
	 * A domain of the subject property.
	 */
	public static final String DOMAIN = RDFS.NAMESPACE + "domain";


	/**
	 * The definition of the subject resource.
	 */
	public static final String IS_DEFINED_BY = RDFS.NAMESPACE + "isDefinedBy";


	/**
	 * A human-readable name for the subject.
	 */
	public static final String LABEL = RDFS.NAMESPACE + "label";


	/**
	 * The class of literal values, e.g. textual strings and integers.
	 */
	public static final String LITERAL = RDFS.NAMESPACE + "Literal";


	/**
	 * A member of the subject resource.
	 */
	public static final String MEMBER = RDFS.NAMESPACE + "member";


	/**
	 * A range of the subject property.
	 */
	public static final String RANGE = RDFS.NAMESPACE + "range";


	/**
	 * The class resource, everything.
	 */
	public static final String RESOURCE = RDFS.NAMESPACE + "Resource";


	/**
	 * Further information about the subject resource.
	 */
	public static final String SEE_ALSO = RDFS.NAMESPACE + "seeAlso";


	/**
	 * The subject is a subclass of a class.
	 */
	public static final String SUB_CLASS_OF = RDFS.NAMESPACE + "subClassOf";


	/**
	 * The subject is a subproperty of a property.
	 */
	public static final String SUB_PROPERTY_OF = RDFS.NAMESPACE + "subPropertyOf";


	/**
	 * Constructors
	 * Do not use it.
	 */
	private RDFS() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}

}
