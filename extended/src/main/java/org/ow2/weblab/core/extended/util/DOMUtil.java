/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.util;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Utility to convert back and forth XML from String to DOM. The requirement is that the root tag is data
 *
 * @author Airbus WebLab Team
 */
public final class DOMUtil {


	private static final String END_ELEMENT = "</data>";


	private static final String START_ELEMENT = "<data>";


	/**
	 * Constructors Do not use it.
	 */
	private DOMUtil() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * @param dataXmlContent
	 *            The XML content as a String between the start and end of the data Element.
	 * @return The data DOM Element enriched with the <code>dataXmlContent</code>
	 */
	public static Element stringToDataDOMElement(final String dataXmlContent) {
		final String tmp = (DOMUtil.START_ELEMENT + dataXmlContent.trim() + DOMUtil.END_ELEMENT).replaceAll(">\\s+<", "><");
		final DOMResult domRes = new DOMResult();
		final StringReader strW = new StringReader(tmp);
		final Transformer trans;
		try {
			trans = TransformerFactory.newInstance().newTransformer();
			trans.transform(new StreamSource(strW), domRes);
		} catch (final TransformerConfigurationException tce) {
			throw new WebLabUncheckedException(tce);
		} catch (final TransformerException te) {
			throw new WebLabUncheckedException("Unvalid String parameter:' " + tmp + "'.", te);
		}
		return ((Document) domRes.getNode()).getDocumentElement();
	}


	/**
	 * @param domElement
	 *            The node that should be in RDF/XML
	 * @return The String representation of the RDF/XML
	 * @throws TransformerException If an error occ
	 */
	public static String extractXMLAsString(final Node domElement) throws TransformerException {
		final Transformer trans = TransformerFactory.newInstance().newTransformer();
		trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		trans.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		trans.setOutputProperty(OutputKeys.INDENT, "yes");

		final StringWriter strW = new StringWriter();
		trans.transform(new DOMSource(domElement), new StreamResult(strW));
		return strW.toString();
	}

}
