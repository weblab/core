/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.ontologies;

/**
 * The DublinCore properties according to <tt>http://purl.org/dc/elements/1.1</tt>, the DCMI Namespace for the Dublin
 * Core Metadata Element Set, Version 1.1.
 * 
 * @author Cassidian WebLab Team
 */
public final class DublinCore {


	/**
	 * http://purl.org/dc/elements/1.1/
	 */
	public static final String NAMESPACE = "http://purl.org/dc/elements/1.1/";


	/**
	 * An entity responsible for making contributions to the resource.<br>
	 * Examples of a Contributor include a person, an organization, or a service. Typically, the name of a Contributor should be used to indicate the entity.
	 */
	public static final String CONTRIBUTOR_PROPERTY_NAME = DublinCore.NAMESPACE + "contributor";


	/**
	 * The spatial or temporal topic of the resource, the spatial applicability of the resource, or the jurisdiction under which the resource is relevant.<br>
	 * Spatial topic and spatial applicability may be a named place or a location specified by its geographic coordinates. Temporal topic may be a named period,
	 * date, or date range. A jurisdiction may be a named administrative entity or a geographic place to which the resource applies. Recommended best practice
	 * is to use a controlled vocabulary such as the Thesaurus of Geographic Names [TGN]. Where appropriate, named places or time periods can be used in
	 * preference to numeric identifiers such as sets of coordinates or date ranges.
	 */
	public static final String COVERAGE_PROPERTY_NAME = DublinCore.NAMESPACE + "coverage";


	/**
	 * An entity primarily responsible for making the resource.<br>
	 * Examples of a Creator include a person, an organization, or a service. Typically, the name of a Creator should be used to indicate the entity.
	 */
	public static final String CREATOR_PROPERTY_NAME = DublinCore.NAMESPACE + "creator";


	/**
	 * A point or period of time associated with an event in the lifecycle of the resource.<br>
	 * Date may be used to express temporal information at any level of granularity. Recommended best practice is to use an encoding scheme, such as the W3CDTF
	 * profile of ISO 8601 [W3CDTF].
	 */
	public static final String DATE_PROPERTY_NAME = DublinCore.NAMESPACE + "date";


	/**
	 * An account of the resource.<br>
	 * Description may include but is not limited to: an abstract, a table of contents, a graphical representation, or a free-text account of the resource.
	 */
	public static final String DESCRIPTION_PROPERTY_NAME = DublinCore.NAMESPACE + "description";


	/**
	 * The file format, physical medium, or dimensions of the resource.<br>
	 * Examples of dimensions include size and duration. Recommended best practice is to use a controlled vocabulary such as the list of Internet Media Types
	 * [MIME].
	 */
	public static final String FORMAT_PROPERTY_NAME = DublinCore.NAMESPACE + "format";


	/**
	 * An unambiguous reference to the resource within a given context.<br>
	 * Recommended best practice is to identify the resource by means of a string conforming to a formal identification system.
	 */
	public static final String IDENTIFIER_PROPERTY_NAME = DublinCore.NAMESPACE + "identifier";


	/**
	 * A language of the resource.<br>
	 * Recommended best practice is to use a controlled vocabulary such as RFC 4646 [RFC4646].
	 */
	public static final String LANGUAGE_PROPERTY_NAME = DublinCore.NAMESPACE + "language";


	/**
	 * An entity responsible for making the resource available.<br>
	 * Examples of a Publisher include a person, an organization, or a service. Typically, the name of a Publisher should be used to indicate the entity.
	 */
	public static final String PUBLISHER_PROPERTY_NAME = DublinCore.NAMESPACE + "publisher";


	/**
	 * A related resource.<br>
	 * Recommended best practice is to identify the related resource by means of a string conforming to a formal identification system.
	 */
	public static final String RELATION_PROPERTY_NAME = DublinCore.NAMESPACE + "relation";


	/**
	 * Information about rights held in and over the resource.<br>
	 * Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights.
	 */
	public static final String RIGHTS_PROPERTY_NAME = DublinCore.NAMESPACE + "rights";


	/**
	 * A related resource from which the described resource is derived.<br>
	 * The described resource may be derived from the related resource in whole or in part. Recommended best practice is to identify the related resource by
	 * means of a string conforming to a formal identification system.
	 */
	public static final String SOURCE_PROPERTY_NAME = DublinCore.NAMESPACE + "source";


	/**
	 * The topic of the resource.<br>
	 * Typically, the subject will be represented using keywords, key phrases, or classification codes. Recommended best practice is to use a controlled
	 * vocabulary. To describe the spatial or temporal topic of the resource, use the Coverage element.
	 */
	public static final String SUBJECT_PROPERTY_NAME = DublinCore.NAMESPACE + "subject";


	/**
	 * A name given to the resource.
	 */
	public static final String TITLE_PROPERTY_NAME = DublinCore.NAMESPACE + "title";


	/**
	 * The nature or genre of the resource.<br>
	 * Recommended best practice is to use a controlled vocabulary such as the DCMI Type Vocabulary [DCMITYPE]. To describe the file format, physical medium, or
	 * dimensions of the resource, use the Format element.
	 */
	public static final String TYPE_PROPERTY_NAME = DublinCore.NAMESPACE + "type";


	/**
	 * Constructors Do not use it.
	 */
	private DublinCore() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}
}
