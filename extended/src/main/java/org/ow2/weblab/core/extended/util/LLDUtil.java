/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.util;

import javax.xml.transform.TransformerException;

import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.model.LowLevelDescriptor;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Uses to manipulate <code>LowLevelDescriptor</code> data field with a <code>String</code>.
 *
 * @author Airbus WebLab Team
 */
public final class LLDUtil {


	/**
	 * Constructors Do not use it.
	 */
	private LLDUtil() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Returns a <code>String</code> containing the XML content of the data node of the <code>LowLevelDescriptor</code>.
	 *
	 * @param lld
	 *            <code>LowLevelDescriptor</code> you want to get the <code>String</code> content
	 * @return The <code>String</code> content of the <code>LowLevelDescriptor</code>
	 */
	public static String getLLDData(final LowLevelDescriptor lld) {
		if (lld.getData() == null) {
			return "";
		}
		// If the lld is the default Object instance, then return the empty String
		if (lld.getData().getClass().equals(Object.class)) {
			return "";
		}

		final Element elem = ((Element) lld.getData());
		if (elem.getFirstChild() == null) {
			return "";
		}

		final StringBuilder strBuilder = new StringBuilder();
		final NodeList nodes = elem.getChildNodes();
		for (int k = 0; k < nodes.getLength(); k++) {
			final Node domElement = nodes.item(k);
			if ((domElement.getNodeType() == Node.TEXT_NODE) && (domElement.getNodeValue().trim().length() == 0)) {
				// Just a bad indentation. No matter. Skipping it.
				continue;
			}
			if (domElement.getNodeType() == Node.TEXT_NODE) {
				// Someone added texts directly in the Node ?
				throw new WebLabUncheckedException("LowLevelDescriptor contains a text node.");
			}
			try {
				strBuilder.append(DOMUtil.extractXMLAsString(domElement));
			} catch (final TransformerException te) {
				throw new WebLabUncheckedException("Unable to transform data field into a String. LowLevelDescriptor is corrupted!", te);
			}
		}
		return strBuilder.toString();
	}


	/**
	 * Same as <code>setLLDData(lld, pokStr, true);</code>
	 *
	 * @param lld
	 *            the <code>LowLevelDescriptor</code> you want to modify
	 * @param lldStr
	 *            contains the RDF/XML <code>String</code>
	 * @see #setLLDData(LowLevelDescriptor , String, boolean)
	 */
	public static void setLLDData(final LowLevelDescriptor lld, final String lldStr) {
		LLDUtil.setLLDData(lld, lldStr, true);
	}


	/**
	 * Set the <code>LowLevelDescriptor</code> data using a <code>String</code>, the parameter have to be a valid XML <code>String</code>. If the parameter don't start with '&lt;data' it add it
	 * automatically.
	 *
	 * @param lld
	 *            the <code>PieceOfKnowledge</code> you want to modify
	 * @param lldStr
	 *            contains the XML <code>String</code>
	 * @param strict
	 *            whether or not to be string and block some bad XML data
	 */
	public static void setLLDData(final LowLevelDescriptor lld, final String lldStr, final boolean strict) {
		lld.setData(DOMUtil.stringToDataDOMElement(lldStr));
		if (strict) {
			LLDUtil.getLLDData(lld);
		}
	}

}
