/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.ontologies;

/**
 * Just a useful class containing the <code>String</code> URIs of the Geonames ontology.
 * 
 * The Geonames ontologies provides elements of description for geographical features, in particular those defined in the geonames.org data base.
 * 
 * NB: Instances are not part of that utility class.
 * 
 * @see "http://www.geonames.org/ontology/ontology_v3.1.rdf"
 * @author Airbus WebLab Team
 */
public final class Geonames {


	/**
	 * The namespace of all those classes and properties.
	 */
	public static final String NAMESPACE = "http://www.geonames.org/ontology#";


	/**
	 * Preferred prefix to be used to represent the WGS 84 positioning geo ontology namespace.
	 */
	public static final String PREFERRED_PREFIX = "gn";


	/**
	 * A class of features.
	 */
	public static final String CLASS = Geonames.NAMESPACE + "Class";


	/**
	 * A feature code.
	 */
	public static final String CODE = Geonames.NAMESPACE + "Code";


	/**
	 * A feature described in geonames database, uniquely defined by its geonames identifier
	 */
	public static final String GEONAMES_FEATURE = Geonames.NAMESPACE + "GeonamesFeature";


	/**
	 * A geographical feature
	 * 
	 * Modified in version 3.1 : The cardinality constraints on feature code, feature class are relaxed on this class, and put on the specific subclass Geonames Feature. This class and attached
	 * properties can therefore be used for features not necessarily identified in the Geonames data base
	 */
	public static final String FEATURE = Geonames.NAMESPACE + "Feature";


	/**
	 * A Web page displaying a map
	 */
	public static final String MAP = Geonames.NAMESPACE + "Map";


	/**
	 * A Document containing RDF description of one or several features.
	 */
	public static final String RDF_DATA = Geonames.NAMESPACE + "RDFData";


	/**
	 * A Wikipedia article
	 */
	public static final String WIKIPEDIA_ARTICLE = Geonames.NAMESPACE + "WikipediaArticle";


	/**
	 * An alternate name
	 */
	public static final String ALTERNATE_NAME = Geonames.NAMESPACE + "alternateName";


	/**
	 * A colloquial name
	 */
	public static final String COLLOQUIAL_NAME = Geonames.NAMESPACE + "colloquialName";


	/**
	 * Geonames identifier
	 */
	public static final String GEONAMES_ID = Geonames.NAMESPACE + "geonamesID";


	/**
	 * A two letters country code in the ISO 3166 list.
	 * 
	 * The countryCode value for a Geoname Feature is equal to the countryCode value of the parentCountry value.
	 */
	public static final String COUNTRY_CODE = Geonames.NAMESPACE + "countryCode";


	/**
	 * historical name
	 */
	public static final String HISTORICAL_NAME = Geonames.NAMESPACE + "historicalName";


	/**
	 * The main international name of a feature. The value has no xml:lang tag.
	 */
	public static final String NAME = Geonames.NAMESPACE + "name";


	/**
	 * A name in an official local language
	 */
	public static final String OFFICIAL_NAME = Geonames.NAMESPACE + "officialName";


	/**
	 * population
	 */
	public static final String POPULATION = Geonames.NAMESPACE + "population";


	/**
	 * postalCode
	 */
	public static final String POSTAL_CODE = Geonames.NAMESPACE + "postalCode";


	/**
	 * shortName
	 */
	public static final String SHORT_NAME = Geonames.NAMESPACE + "shortName";


	/**
	 * Links to an RDF document containing the descriptions of children features
	 */
	public static final String CHILDREN_FEATURES = Geonames.NAMESPACE + "childrenFeatures";


	/**
	 * The main category of the feature, as defined in geonames taxonomy.
	 */
	public static final String FEATURECLASS = Geonames.NAMESPACE + "featureClass";


	/**
	 * Type of the feature, as defined in geonames taxonomy.
	 */
	public static final String FEATURE_CODE = Geonames.NAMESPACE + "featureCode";


	/**
	 * Indicates that the subject resource is located in the object feature
	 */
	public static final String LOCATED_IN = Geonames.NAMESPACE + "locatedIn";


	/**
	 * A geonames map centered on the feature.
	 */
	public static final String LOCATION_MAP = Geonames.NAMESPACE + "locationMap";


	/**
	 * A feature close to the reference feature
	 */
	public static final String NEARBY = Geonames.NAMESPACE + "nearby";


	/**
	 * Links to an RDF document containing the descriptions of nearby features
	 */
	public static final String NEARBY_FEATURES = Geonames.NAMESPACE + "nearbyFeatures";


	/**
	 * A feature sharing a common boarder with the reference feature
	 */
	public static final String NEIGHBOUR = Geonames.NAMESPACE + "neighbour";


	/**
	 * Links to an RDF document containing the descriptions of neighbouring features. Applies when the feature has definite boarders.
	 */
	public static final String NEIGHBOURING_FEATURES = Geonames.NAMESPACE + "neighbouringFeatures";


	/**
	 * level 1 administrative parent
	 */
	public static final String PARENT_ADM_1 = Geonames.NAMESPACE + "parentADM1";


	/**
	 * level 2 administrative parent
	 */
	public static final String PARENT_ADM_2 = Geonames.NAMESPACE + "parentADM2";


	/**
	 * level 3 administrative parent
	 */
	public static final String PARENT_ADM_3 = Geonames.NAMESPACE + "parentADM3";


	/**
	 * level 4 administrative parent
	 */
	public static final String PARENT_ADM_4 = Geonames.NAMESPACE + "parentADM4";


	/**
	* 
	*/
	public static final String PARENT_COUNTRY = Geonames.NAMESPACE + "parentCountry";


	/**
	 * A feature parent of the current one, in either administrative or physical subdivision.
	 */
	public static final String PARENT_FEATURE = Geonames.NAMESPACE + "parentFeature";


	/**
	 * A Wikipedia article of which subject is the resource.
	 */
	public static final String WIKIPEDIA_ARTICLE_PROP = Geonames.NAMESPACE + "wikipediaArticle";


	/**
	 * Constructors
	 * Do not use it.
	 */
	private Geonames() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}

}
