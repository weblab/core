/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.jaxb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ResourceBundle;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.io.FileUtils;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.messages.Messages;
import org.ow2.weblab.core.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;


/**
 * A wrapping class for a JAXB <code>Marshaller</code> and a JAXB <code>Unmarshaler</code>. Many functions are available on Writer since it's easy for the use
 * to create a Writer from a file or from an output stream. <code>new OutputStreamWriter(new FileOutputStream(file)), "UTF-8");</code> Note that you need to
 * close the I/O streams, readers and writers that you gave to this class. When you use a file, we close the opened stream.
 *
 * @author Cassidian WebLab Team
 */
public final class WebLabMarshaller {


	/**
	 * The ResourceBundle that contains internationalised messages for the WebLab marshaller.
	 */
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle(WebLabMarshaller.class.getCanonicalName());


	private static final String KEY_ERROR_CAST_UNMARSHALL_2 = "WebLabMarshaller.Error_Cast_Unmarshall_2";


	private static final String KEY_ERROR_FNFE_2 = "WebLabMarshaller.Error_FNFE_2";


	private static final String KEY_ERROR_INIT_CONTEXT = "WebLabMarshaller.Error_Init_Context";


	private static final String KEY_ERROR_INIT_MARSHALLER = "WebLabMarshaller.Error_Init_Marshaller";


	private static final String KEY_ERROR_INIT_UNMARSHALLER = "WebLabMarshaller.Error_Init_Unmarshaller";


	private static final String KEY_ERROR_MARSHALLING_2 = "WebLabMarshaller.Error_Marshalling_2";


	private static final String KEY_ERROR_UNMARSHALLING_1 = "WebLabMarshaller.Error_Unmarshalling_1";


	private static final String KEY_ERROR_UTF8 = "WebLabMarshaller.Error_UTF8";


	private static final String KEY_WARN_CLEANED_STRING = "WebLabMarshaller.Warn_Cleaned_String";


	private static final String KEY_WARN_FLUSH_WRITER = "WebLabMarshaller.Warn_Flush_Writer";


	private static final String KEY_WARN_SET_PROPERTY = "WebLabMarshaller.Warn_Set_Property";


	private static final String KEY_WARN_SET_PROPERTY_1 = "WebLabMarshaller.Warn_Set_Property_1";


	private static final String KEY_WARN_SCHEMA_LOADING = "WebLabMarshaller.Warn_Schema_Loading";


	private static final String RESOURCE = "resource";


	private static final String UTF8 = "UTF-8";


	/**
	 * The <code>JAXBContext</code> to be used
	 */
	private JAXBContext jaxbContext;


	/**
	 * The logger used inside the code.
	 */
	private final Logger logger;


	/**
	 * The WebLab Model schema to be used for validation. If null (i.e. default case, then no validation is done).
	 */
	private Schema schema;


	/**
	 * Constructors of a marshaller that does not validate read/written XML files.
	 *
	 * @see #WebLabMarshaller(boolean)
	 */
	public WebLabMarshaller() {
		this(false);
	}


	/**
	 * Constructor of a marshaller that could use schema validation to ensure correctness of read/written XML files.
	 *
	 * @param validateWithSchema
	 *            Whether or not to validate read/written XML files using the WebLab Model XSD.
	 */
	public WebLabMarshaller(final boolean validateWithSchema) {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass());
		if (validateWithSchema) {
			try (final InputStream schemaStream = Resource.class.getResourceAsStream("/model/model.xsd")) {
				if (schemaStream == null) {
					this.logger.warn(WebLabMarshaller.KEY_WARN_SCHEMA_LOADING);
					this.schema = null;
				} else {
					try {
						this.schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new StreamSource(schemaStream));
					} catch (final SAXException saxe) {
						this.logger.warn(WebLabMarshaller.KEY_WARN_SCHEMA_LOADING, saxe);
						this.schema = null;
					}
				}
			} catch (final IOException ioe) {
				this.logger.warn("An error occured when closing the stream. This is strange but non blocking. Ignoring it.", ioe);
			}
		} else {
			this.schema = null;
		}
	}


	/**
	 * @param qname
	 *            The <code>QName</code> of the field containing <code>resource</code>.
	 * @param resource
	 *            The <code>Resource</code> to be marshaled.
	 * @return a <code>JAXBElement</code> containing <code>resource</code>.
	 */
	private static JAXBElement<Resource> createJAXBElement(final QName qname, final Resource resource) {
		return new JAXBElement<>(qname, Resource.class, resource);
	}


	/**
	 * @param qNameLocalPart
	 *            The name of the field containing <code>resource</code>.
	 * @param resource
	 *            The <code>Resource</code> to be marshaled.
	 * @return a <code>JAXBElement</code> containing <code>resource</code>.
	 */
	private static JAXBElement<Resource> createJAXBElement(final String qNameLocalPart, final Resource resource) {
		return new JAXBElement<>(new QName(qNameLocalPart), Resource.class, resource);
	}


	/**
	 * @param resource
	 *            The <code>Resource</code> to be marshaled.
	 * @return a <code>JAXBElement</code> containing <code>resource</code>.
	 */
	private static JAXBElement<Resource> createResourceJAXBElement(final Resource resource) {
		return WebLabMarshaller.createJAXBElement(new QName(WebLabMarshaller.RESOURCE), resource);
	}


	/**
	 * @param resource
	 *            The <code>Resource</code> to be marshaled.
	 * @param writer
	 *            The <code>Writer</code> to be written. Note that you need to
	 *            close the writer after using this method.
	 * @param qname
	 *            The <code>QName</code> of the field containing <code>resource</code>.
	 * @throws WebLabCheckedException
	 *             if a <code>JAXBException</code> occurred.
	 */
	public void marshal(final Resource resource, final Writer writer, final QName qname) throws WebLabCheckedException {
		this.marshal(WebLabMarshaller.createJAXBElement(qname, resource), writer, false);
	}


	/**
	 * @param resource
	 *            The <code>Resource</code> to be marshaled.
	 * @param writer
	 *            The <code>Writer</code> to be written. Note that you need to
	 *            close the writer after using this method.
	 * @param qnameLocalPart
	 *            The name of the field containing <code>resource</code>.
	 * @throws WebLabCheckedException
	 *             if a <code>JAXBException</code> occurred.
	 */
	public void marshal(final Resource resource, final Writer writer, final String qnameLocalPart) throws WebLabCheckedException {
		this.marshal(WebLabMarshaller.createJAXBElement(qnameLocalPart, resource), writer, false);
	}


	/**
	 * @param resource
	 *            The resource to be marshaled.
	 * @param file
	 *            The <code>File</code> to be written.
	 * @throws WebLabCheckedException
	 *             if a <code>JAXBException</code> occurred.
	 */
	public void marshalResource(final Resource resource, final File file) throws WebLabCheckedException {
		try (final FileOutputStream fos = FileUtils.openOutputStream(file)){
			this.marshalResource(resource, fos);
		} catch (final IOException ioe) {
			final String msg = Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_ERROR_FNFE_2, resource.getUri(), file.getAbsolutePath());
			this.logger.error(msg, ioe);
			throw new WebLabCheckedException(msg, ioe);
		}
	}


	/**
	 * @param resource
	 *            The <code>Resource</code> to be marshaled.
	 * @param stream
	 *            The <code>OutputStream</code> to be written. Note that you
	 *            need to close the stream after using this method.
	 * @throws WebLabCheckedException
	 *             if a <code>JAXBException</code> occurred.
	 */
	public void marshalResource(final Resource resource, final OutputStream stream) throws WebLabCheckedException {
		try {
			final OutputStreamWriter osw = new OutputStreamWriter(stream, WebLabMarshaller.UTF8);
			this.marshalResource(resource, osw);
		} catch (final UnsupportedEncodingException uee) {
			final String msg = Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_ERROR_UTF8);
			this.logger.error(msg, uee);
			throw new WebLabCheckedException(msg, uee);
		}
	}


	/**
	 * @param resource
	 *            The <code>Resource</code> to be marshaled.
	 * @param writer
	 *            The <code>Writer</code> to be written. Note that you need to
	 *            close the writer after using this method.
	 * @throws WebLabCheckedException
	 *             if a <code>JAXBException</code> occurred.
	 */
	public void marshalResource(final Resource resource, final Writer writer) throws WebLabCheckedException {
		this.marshal(WebLabMarshaller.createResourceJAXBElement(resource), writer, false);
	}


	/**
	 * @param resource
	 *            The <code>Resource</code> to be marshaled.
	 * @param writer
	 *            The <code>Writer</code> to be written. Note that you need to
	 *            close the writer after using this method.
	 * @param checkXML
	 *            Whether or not to clean the Resource from any not recommended
	 *            XML char. Time of the process will be increased.
	 * @throws WebLabCheckedException
	 *             if a <code>JAXBException</code> occurred.
	 * @see XMLStringCleaner
	 */
	public void marshalResource(final Resource resource, final Writer writer, final boolean checkXML) throws WebLabCheckedException {
		this.marshal(WebLabMarshaller.createResourceJAXBElement(resource), writer, checkXML);
	}


	/**
	 * @see Marshaller for key values.
	 * @param key
	 *            The String key of the property to set.
	 * @param value
	 *            The Object value of the property to set.
	 * @return <code>false</code> if an <code>PropertyException</code> occurred
	 *         while setting the property.
	 */
	public boolean setMarshallerProperty(final String key, final Object value) {
		final Marshaller marshaler = this.createMarshaller();
		boolean set;
		try {
			marshaler.setProperty(key, value);
			set = true;
		} catch (final PropertyException pe) {
			set = false;
			this.logger.error(Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_WARN_SET_PROPERTY_1, key), pe);
		}
		return set;
	}


	/**
	 * @see Marshaller for key values
	 * @param key
	 *            The String key of the property to set
	 * @param value
	 *            The Object value of the property to set
	 * @return false if an PropertyException occurred while setting the property
	 */
	public boolean setUnmarshallerProperty(final String key, final Object value) {
		final Unmarshaller unmarshaler = this.createUnmarshaller();
		boolean set;
		try {
			unmarshaler.setProperty(key, value);
			set = true;
		} catch (final PropertyException pe) {
			set = false;
			this.logger.error(Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_WARN_SET_PROPERTY_1, key), pe);
		}
		return set;
	}


	/**
	 * @param <T>
	 *            Class of the <code>Resource</code> to be unmarshaled
	 * @param file
	 *            <code>File</code> to be read.
	 * @param resourceClass
	 *            Class of the <code>Resource</code> to be unmarshaled. Using <code>Resource</code> will prevent from
	 *            cast errors when the
	 *            content of the <code>File</code> isn't compatible with <code>resourceClass</code>.
	 * @return an instance of <code>resourceClass</code> from the content of <code>File</code>.
	 * @throws WebLabCheckedException
	 *             if a <code>JAXBException</code>, an <code>IOException</code> or a <code>ClassCastException</code> occurred.
	 */
	public <T extends Resource> T unmarshal(final File file, final Class<T> resourceClass) throws WebLabCheckedException {
		try (final FileInputStream fis = FileUtils.openInputStream(file)) {
			return this.unmarshal(fis, resourceClass);
		} catch (final IOException ioe) {
			final String msg = Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_ERROR_UNMARSHALLING_1, file.getAbsolutePath());
			this.logger.error(msg, ioe);
			throw new WebLabCheckedException(msg, ioe);
		}
	}


	/**
	 * @param <T>
	 *            Class of the <code>Resource</code> to be unmarshaled
	 * @param stream
	 *            <code>InputStream</code> to be read. Note that you need to
	 *            close the stream after using this method.
	 * @param resourceClass
	 *            Class of the <code>Resource</code> to be unmarshaled. Using <code>Resource</code> will prevent from
	 *            cast errors when the
	 *            content of the <code>InputStream</code> isn't compatible with <code>resourceClass</code>.
	 * @return an instance of <code>resourceClass</code> from the content of the <code>InputStream</code>.
	 * @throws WebLabCheckedException
	 *             if a <code>JAXBException</code> or a <code>ClassCastException</code> occurred.
	 */
	public <T extends Resource> T unmarshal(final InputStream stream, final Class<T> resourceClass) throws WebLabCheckedException {
		final InputStreamReader isr;
		try {
			isr = new InputStreamReader(stream, WebLabMarshaller.UTF8);
		} catch (final UnsupportedEncodingException uee) {
			final String msg = Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_ERROR_UTF8);
			this.logger.error(msg, uee);
			throw new WebLabCheckedException(msg, uee);
		}
		return this.unmarshal(isr, resourceClass);
	}


	/**
	 * @param <T>
	 *            Class of the <code>Resource</code> to be unmarshaled
	 * @param reader
	 *            <code>Reader</code> to be read. Note that you need to close
	 *            the reader after using this method.
	 * @param resourceClass
	 *            Class of the <code>Resource</code> to be unmarshaled. Using <code>Resource</code> will prevent from
	 *            cast errors when the
	 *            content of the <code>Reader</code> isn't compatible with <code>resourceClass</code>.
	 * @return an instance of <code>resourceClass</code> from the content of
	 *         source.
	 * @throws WebLabCheckedException
	 *             if a <code>JAXBException</code> or a ClassCastException
	 *             occurred.
	 */
	public <T extends Resource> T unmarshal(final Reader reader, final Class<T> resourceClass) throws WebLabCheckedException {
		final Unmarshaller unmarshaler = this.createUnmarshaller();
		final JAXBElement<T> jaxbElement;
		try {
			jaxbElement = unmarshaler.unmarshal(new StreamSource(reader), resourceClass);
		} catch (final JAXBException jaxbe) {
			final String msg = Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_ERROR_UNMARSHALLING_1, reader);
			this.logger.error(msg, jaxbe);
			this.jaxbContext = null;
			throw new WebLabCheckedException(msg, jaxbe);
		}
		final T res = jaxbElement.getValue();
		if (resourceClass.isInstance(res)) {
			resourceClass.cast(res);
		} else {
			final String msg = Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_ERROR_CAST_UNMARSHALL_2, resourceClass.getCanonicalName(), res
					.getClass().getCanonicalName());
			this.logger.error(msg);
			throw new WebLabCheckedException(new ClassCastException(msg));
		}
		return res;
	}


	/**
	 * Initialises the <code>JAXBContext</code> and a marshaler with packages as context, UTF-8 encoding and formatted output.
	 *
	 * @return the JAXB Marshaller
	 */
	private Marshaller createMarshaller() {
		this.initJC();
		final Marshaller marshaller;
		try {
			marshaller = this.jaxbContext.createMarshaller();
		} catch (final JAXBException jaxbe) {
			final String msg = Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_ERROR_INIT_MARSHALLER);
			this.logger.error(msg, jaxbe);
			this.jaxbContext = null;
			throw new WebLabUncheckedException(msg, jaxbe);
		}
		try {
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, WebLabMarshaller.UTF8);
		} catch (final PropertyException pe) {
			this.logger.warn(Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_WARN_SET_PROPERTY), pe);
		}
		if (this.schema != null) {
			marshaller.setSchema(this.schema);
		}
		return marshaller;
	}


	/**
	 * Initialises the <code>JAXBContext</code> with packages as context path if needed and initialises the unmarshaler if needed
	 *
	 * @return the JAXB Unmarshaller
	 */
	private Unmarshaller createUnmarshaller() {
		this.initJC();
		final Unmarshaller unmarshaller;
		try {
			unmarshaller = this.jaxbContext.createUnmarshaller();
		} catch (final JAXBException jaxbe) {
			final String msg = Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_ERROR_INIT_UNMARSHALLER);
			this.logger.error(msg, jaxbe);
			this.jaxbContext = null;
			throw new WebLabUncheckedException(msg, jaxbe);
		}
		if (this.schema != null) {
			unmarshaller.setSchema(this.schema);
		}
		return unmarshaller;
	}


	/**
	 * Initialises <code>jaxbContext</code> with packages as context path if needed.
	 */
	private void initJC() {
		if (this.jaxbContext == null) {
			try {
				this.jaxbContext = JAXBContext.newInstance(Resource.class);
			} catch (final JAXBException jaxbe) {
				final String msg = Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_ERROR_INIT_CONTEXT);
				this.logger.error(msg, jaxbe);
				this.jaxbContext = null;
				throw new WebLabUncheckedException(msg, jaxbe);
			}
		}
	}


	/**
	 * @param jaxbElement
	 *            The <code>JAXBElement</code> to be marshaled
	 * @param writer
	 *            <code>Writer</code> to be written. Note that the writer need
	 *            to be closed after using this method.
	 * @param checkXML
	 *            Whether or not to clean the JAXBElement from any not
	 *            recommended XML char. Time of the process will be increased.
	 * @throws WebLabCheckedException
	 *             if a <code>JAXBException</code> occurred
	 * @see XMLStringCleaner
	 */
	private void marshal(final JAXBElement<?> jaxbElement, final Writer writer, final boolean checkXML) throws WebLabCheckedException {
		final Marshaller marshaler = this.createMarshaller();
		try {
			if (checkXML) {
				final StringWriter strWriter = new StringWriter();
				try {
					marshaler.marshal(jaxbElement, strWriter);
					writer.write(XMLStringCleaner.getXMLRecommendedString(strWriter.toString()));
				} catch (final IOException ioe) {
					this.logger.warn(Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_WARN_CLEANED_STRING), ioe);
					marshaler.marshal(jaxbElement, writer);
				} catch (final JAXBException jaxbe) {
					this.jaxbContext = null;
					this.logger.warn(Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_WARN_CLEANED_STRING), jaxbe);
					final Marshaller otherMarshaler = this.createMarshaller();
					otherMarshaler.marshal(jaxbElement, writer);
				} finally {
					try {
						writer.flush();
					} catch (final IOException ioe) {
						this.logger.warn(Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_WARN_FLUSH_WRITER), ioe);
					}
				}
			} else {
				marshaler.marshal(jaxbElement, writer);
			}
		} catch (final JAXBException jaxbe) {
			final String msg = Messages.getString(WebLabMarshaller.BUNDLE, WebLabMarshaller.KEY_ERROR_MARSHALLING_2, jaxbElement.getValue(), writer);
			this.logger.error(msg, jaxbe);
			this.jaxbContext = null;
			throw new WebLabCheckedException(msg, jaxbe);
		}
	}

}
