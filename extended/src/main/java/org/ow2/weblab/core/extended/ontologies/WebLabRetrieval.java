/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.extended.ontologies;


/**
 * Contains every properties that are defined in the retrieval.owl file of the WebLab model project.
 *
 * This version includes relationships used for WebLab Resources retrieval.
 *
 * @author WebLab Cassidian team
 */
public final class WebLabRetrieval {


	/**
	 * Namespace of the WebLab Retrieval ontology.
	 */
	public static final String NAMESPACE = "http://weblab.ow2.org/core/1.2/ontology/retrieval#";


	/**
	 * Preferred prefix of the WebLab Retrieval Ontology.
	 */
	public static final String PREFERRED_PREFIX = "wlr";


	/**
	 * URI of the class FacetSuggestion.
	 */
	public static final String FACET_SUGGESTION = WebLabRetrieval.NAMESPACE + "FacetSuggestion";


	/**
	 * URI of the property hasDescription. Gives a description for a hit.
	 */
	public static final String HAS_DESCRIPTION = WebLabRetrieval.NAMESPACE + "hasDescription";


	/**
	 * URI of the property hasExpectedLimit. Gives the limit for the requested query for this order.
	 */
	public static final String HAS_EXPECTED_LIMIT = WebLabRetrieval.NAMESPACE + "hasExpectedLimit";


	/**
	 * URI of the property hasExpectedOffset. Gives the expected offset for the requested query for this order.
	 */
	public static final String HAS_EXPECTED_OFFSET = WebLabRetrieval.NAMESPACE + "hasExpectedOffset";


	/**
	 * URI of the property hasFacetSuggestion. Links a query to a facet suggestion.
	 */
	public static final String HAS_FACET_SUGGESTION = WebLabRetrieval.NAMESPACE + "hasFacetSuggestion";


	/**
	 * URI of the property hasHit. Gives a hit for ResultSet.
	 */
	public static final String HAS_HIT = WebLabRetrieval.NAMESPACE + "hasHit";


	/**
	 * URI of the property hasNumberOfResults. Gives the total number of results for this result set.
	 */
	public static final String HAS_NUMBER_OF_RESULTS = WebLabRetrieval.NAMESPACE + "hasNumberOfResults";


	/**
	 * URI of the property hasOrderedQuery. Gives a link to the requested query for this order.
	 */
	public static final String HAS_ORDERED_QUERY = WebLabRetrieval.NAMESPACE + "hasOrderedQuery";


	/**
	 * URI of the property hasQueryOffset. Provides the offset about the query in the case of a result set returns by a search service.
	 */
	public static final String HAS_QUERY_OFFSET = WebLabRetrieval.NAMESPACE + "hasQueryOffset";


	/**
	 * URI of the property hasRank. Gives a rank for hit.
	 */
	public static final String HAS_RANK = WebLabRetrieval.NAMESPACE + "hasRank";


	/**
	 * URI of the property hasRelevantMediaUnit. Can be used to point out a specific MediaUnit inside the Resource that is linked to a Hit. This is an optional
	 * property of a Hit.
	 */
	public static final String HAS_RELEVANT_MEDIA_UNIT = WebLabRetrieval.NAMESPACE + "hasRelevantMediaUnit";


	/**
	 * URI of the property hasRelevantSegment. Can be used to point out a specific Segment inside the Resource that is linked to a Hit. This is an optional
	 * property of a Hit. But if present it MUST be combined with #hasRelevantMediaUnit (to locate the MediaUnit that contains the Segment).
	 */
	public static final String HAS_RELEVANT_SEGMENT = WebLabRetrieval.NAMESPACE + "hasRelevantSegment";


	/**
	 * URI of the property hasScope. Defines on which property values the query applies. For instance, having dc:title has value, means that the query aims at
	 * searching a Resource whose title matches the query.
	 */
	public static final String HAS_SCOPE = WebLabRetrieval.NAMESPACE + "hasScope";


	/**
	 * URI of the property hasScore. Gives a score for hit.
	 */
	public static final String HAS_SCORE = WebLabRetrieval.NAMESPACE + "hasScore";


	/**
	 * URI of the OWL class Hit. A hit describes media document properties corresponding to a request.
	 */
	public static final String HIT = WebLabRetrieval.NAMESPACE + "Hit";


	/**
	 * URI of the property inResultSetHit. Links a Hit to its ResultSet.
	 */
	public static final String IN_RESULT_SET_HIT = WebLabRetrieval.NAMESPACE + "inResultSetHit";


	/**
	 * URI of the property isExpressedWith. Gives the language of the query.
	 */
	public static final String IS_EXPRESSED_WITH = WebLabRetrieval.NAMESPACE + "isExpressedWith";


	/**
	 * URI of the property isLinkedTo. Gives a link to resource for hit.
	 */
	public static final String IS_LINKED_TO = WebLabRetrieval.NAMESPACE + "isLinkedTo";


	/**
	 * URI of the property isRankedAscending. Default ranking mode is descending, this property allow to state that the result set has been ranked ascendingly.
	 */
	public static final String IS_RANKED_ASCENDING = WebLabRetrieval.NAMESPACE + "isRankedAscending";


	/**
	 * URI of the property isRankedBy. Define what property is used to order the results when the traditional relevance is not the default order to use.
	 */
	public static final String IS_RANKED_BY = WebLabRetrieval.NAMESPACE + "isRankedBy";


	/**
	 * URI of the property isResultOf. Links a ResultSet to the Query that has been used to retrieve the set of results.
	 */
	public static final String IS_RESULT_OF = WebLabRetrieval.NAMESPACE + "isResultOf";


	/**
	 * URI of the OWL class SearchOrder. A SearchOrder describe a search order requested by a user or a service.
	 */
	public static final String SEARCH_ORDER = WebLabRetrieval.NAMESPACE + "SearchOrder";


	/**
	 * URI of the property toBeRankedAscending. Default ranking mode is descendant, this property allow to ask for results ranked ascendingly.
	 */
	public static final String TO_BE_RANKED_ASCENDING = WebLabRetrieval.NAMESPACE + "toBeRankedAscending";


	/**
	 * URI of the property toBeRankedBy. Define what is the expected property to be used to order the results of a request when the traditional relevance is
	 * not the default order to use.
	 */
	public static final String TO_BE_RANKED_BY = WebLabRetrieval.NAMESPACE + "toBeRankedBy";


	/**
	 * Constructors Do not use it.
	 */
	private WebLabRetrieval() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


}
