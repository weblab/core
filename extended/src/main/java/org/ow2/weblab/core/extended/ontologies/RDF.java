/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.ontologies;

/**
 * Just a useful class containing the <code>String</code> URIs of the RDF specification.
 * 
 * @author Airbus WebLab Team
 */
public final class RDF {


	/**
	 * The namespace of all those classes and properties.
	 */
	public static final String NAMESPACE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";


	/**
	 * The preferred prefix for this ontology
	 */
	public static final String PREFERRED_PREFIX = "rdf";


	/**
	 * The class of containers of alternatives.
	 */
	public static final String ALT = RDF.NAMESPACE + "Alt";


	/**
	 * The class of unordered containers.
	 */
	public static final String BAG = RDF.NAMESPACE + "Bag";


	/**
	 * The first item in the subject RDF list.
	 */
	public static final String FIRST = RDF.NAMESPACE + "first";


	/**
	 * The class of RDF Lists.
	 */
	public static final String LIST = RDF.NAMESPACE + "List";


	/**
	 * The object of the subject RDF statement.
	 */
	public static final String OBJECT = RDF.NAMESPACE + "object";


	/**
	 * The predicate of the subject RDF statement.
	 */
	public static final String PREDICATE = RDF.NAMESPACE + "predicate";


	/**
	 * The class of RDF properties.
	 */
	public static final String PROPERTY = RDF.NAMESPACE + "Property";


	/**
	 * The rest of the subject RDF list after the first item.
	 */
	public static final String REST = RDF.NAMESPACE + "rest";


	/**
	 * The class of ordered containers.
	 */
	public static final String SEQ = RDF.NAMESPACE + "Seq";


	/**
	 * The class of RDF statements.
	 */
	public static final String STATEMENT = RDF.NAMESPACE + "Statement";


	/**
	 * The subject of the subject RDF statement.
	 */
	public static final String SUBJECT = RDF.NAMESPACE + "subject";


	/**
	 * The subject is an instance of a class.
	 */
	public static final String TYPE = RDF.NAMESPACE + "type";


	/**
	 * Idiomatic property used for structured values (see the RDF Primer for an example of its usage).
	 */
	public static final String VALUE = RDF.NAMESPACE + "value";


	/**
	 * The class of XML literals values.
	 */
	public static final String XML_LITERAL = RDF.NAMESPACE + "XMLLiteral";


	/**
	 * Constructors
	 * Do not use it.
	 */
	private RDF() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}
}
