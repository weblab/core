/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.extended.ontologies;


/**
 * Contains every properties that are defined in the processing.owl file of the WebLab model project.
 *
 * This version includes relationships used for WebLab Resources processing.
 *
 * @author WebLab Cassidian team
 */
public final class WebLabProcessing {


	/**
	 * Namespace of the WebLab Processing ontology.
	 */
	public static final String NAMESPACE = "http://weblab.ow2.org/core/1.2/ontology/processing#";


	/**
	 * Preferred prefix of the WebLab Processing ontology.
	 */
	public static final String PREFERRED_PREFIX = "wlp";


	/**
	 * URI of the property canBeIgnored. Indicates whether or not a MediaUnit can be ignored by a service.
	 */
	public static final String CAN_BE_IGNORED = WebLabProcessing.NAMESPACE + "canBeIgnored";


	/**
	 * URI of the OWL class Cluster. A cluster is a set of resources that share some characteristics.
	 */
	public static final String CLUSTER = WebLabProcessing.NAMESPACE + "Cluster";


	/**
	 * URI of the OWL class ClusterLink. A link between exactly two clusters.
	 */
	public static final String CLUSTER_LINK = WebLabProcessing.NAMESPACE + "ClusterLink";


	/**
	 * URI of the property hasAvailableTranslationIn. Links a Document to the language of its available translations.
	 */
	public static final String HAS_AVAILABLE_TRANSLATION_IN = WebLabProcessing.NAMESPACE + "hasAvailableTranslationIn";


	/**
	 * URI of the property hasBeenUpdatedBy. Links a resource to the service that has modified this resource.
	 */
	public static final String HAS_BEEN_UPDATED_BY = WebLabProcessing.NAMESPACE + "hasBeenUpdatedBy";


	/**
	 * URI of the property hasClusterProximity. Value that corresponds to the proximity between the two clusters implied in a cluster link.
	 */
	public static final String HAS_CLUSTER_PROXIMITY = WebLabProcessing.NAMESPACE + "hasClusterProximity";


	/**
	 * URI of the property hasCredibility. Credibility of information is designated by a number between 1 and 6 signifying varying degrees of confidence.
	 */
	public static final String HAS_CREDIBILITY = WebLabProcessing.NAMESPACE + "hasCredibility";


	/**
	 * URI of the property hasDiarisationId. Gives the generated diarisation id of the speaker of a segment.
	 */
	public static final String HAS_DIARISATION_ID = WebLabProcessing.NAMESPACE + "hasDiarisationId";


	/**
	 * URI of the property hasGatheringDate. Gives the date when the Document was gathered.
	 */
	public static final String HAS_GATHERING_DATE = WebLabProcessing.NAMESPACE + "hasGatheringDate";


	/**
	 * URI of the property hasGatheringSeed. Gives the gathering seed information used to fall on the dc:source of this Document.
	 */
	public static final String HAS_GATHERING_SEED = WebLabProcessing.NAMESPACE + "hasGatheringSeed";


	/**
	 * URI of the property hasMainDate. Gives the main date of the Document (ideally its publication date, but if not available its creation/modified date and finally its gathering date).
	 */
	public static final String HAS_MAIN_DATE = WebLabProcessing.NAMESPACE + "hasMainDate";


	/**
	 * URI of the property hasNativeContent. Links a media unit to its native content.
	 */
	public static final String HAS_NATIVE_CONTENT = WebLabProcessing.NAMESPACE + "hasNativeContent";


	/**
	 * URI of the property hasNormalisedContent. Links a media unit to its normalised content.
	 */
	public static final String HAS_NORMALISED_CONTENT = WebLabProcessing.NAMESPACE + "hasNormalisedContent";


	/**
	 * URI of the property hasOriginalFileName. Gives the name of the original file gathered.
	 */
	public static final String HAS_ORIGINAL_FILE_NAME = WebLabProcessing.NAMESPACE + "hasOriginalFileName";


	/**
	 * URI of the property hasOriginalFileSize. Gives the number of bytes of the original file gathered.
	 */
	public static final String HAS_ORIGINAL_FILE_SIZE = WebLabProcessing.NAMESPACE + "hasOriginalFileSize";


	/**
	 * URI of the property hasRating. Gives the associated number of stars of the Document.
	 */
	public static final String HAS_RATING = WebLabProcessing.NAMESPACE + "hasRating";


	/**
	 * URI of the property hasReliability. Reliability of the source is designated by a letter between 1 and 6 signifying varying degrees of confidence.
	 */
	public static final String HAS_RELIABILITY = WebLabProcessing.NAMESPACE + "hasReliability";


	/**
	 * URI of the property hasPolarity. Gives the polarity expressed in or about the Resource (WebLab or RDF) as a double value, positive or negative.
	 */
	public static final String HAS_POLARITY = WebLabProcessing.NAMESPACE + "hasPolarity";


	/**
	 * URI of the property hasSentiment. Gives the sentiment expressed in or about the Resource (WebLab or RDF) as a String value (often positive, neutral or negative).
	 */
	public static final String HAS_SENTIMENT = WebLabProcessing.NAMESPACE + "hasSentiment";


	/**
	 * URI of the property hasSpeaker. Links a segment to an instance of a recognised speaker entity (given its URI).
	 */
	public static final String HAS_SPEAKER = WebLabProcessing.NAMESPACE + "hasSpeaker";


	/**
	 * URI of the property hasTopic. Gives the detected topic of the Document.
	 */
	public static final String HAS_TOPIC = WebLabProcessing.NAMESPACE + "hasTopic";


	/**
	 * URI of the property isCandidate. Indicates whether or not is the instance extracted a candidate instance.
	 */
	public static final String IS_CANDIDATE = WebLabProcessing.NAMESPACE + "isCandidate";


	/**
	 * URI of the property isCleansingOf. Links a cleansed MediaUnit to its source MediaUnit.
	 */
	public static final String IS_CLEANSING_OF = WebLabProcessing.NAMESPACE + "isCleansingOf";


	/**
	 * URI of the property isComposedByCluster. Link a ClusterLink with its two components clusters.
	 */
	public static final String IS_COMPOSED_BY_CLUSTER = WebLabProcessing.NAMESPACE + "isComposedByCluster";


	/**
	 * URI of the property isDelimiterOf. Links a segment to the MediaUnit it represents.
	 */
	public static final String IS_DELIMITER_OF = WebLabProcessing.NAMESPACE + "isDelimiterOf";


	/**
	 * URI of the property isDelimiterOfCleansingUnit. Links a segment to the cleansing MediaUnit it represents.
	 */
	public static final String IS_DELIMITER_OF_CLEANSING_UNIT = WebLabProcessing.NAMESPACE + "isDelimiterOfCleansingUnit";


	/**
	 * URI of the property isDelimiterOfFrameUnit. Links a segment to the frame MediaUnit it represents.
	 */
	public static final String IS_DELIMITER_OF_FRAME_UNIT = WebLabProcessing.NAMESPACE + "isDelimiterOfFrameUnit";


	/**
	 * URI of the property isDelimiterOfRecognitionUnit. Links a segment to the recognition MediaUnit it represents.
	 */
	public static final String IS_DELIMITER_OF_RECOGNITION_UNIT = WebLabProcessing.NAMESPACE + "isDelimiterOfRecognitionUnit";


	/**
	 * URI of the property isDelimiterOfShotUnit. Links a segment to the shot MediaUnit it represents.
	 */
	public static final String IS_DELIMITER_OF_SHOT_UNIT = WebLabProcessing.NAMESPACE + "isDelimiterOfShotUnit";


	/**
	 * URI of the property isDelimiterOfSoundtrackUnit. Links a segment to the soundtrack MediaUnit it represents.
	 */
	public static final String IS_DELIMITER_OF_SOUNDTRACK_UNIT = WebLabProcessing.NAMESPACE + "isDelimiterOfSoundtrackUnit";


	/**
	 * URI of the property isDelimiterOfSummaryUnit. Links a segment to the summary MediaUnit it represents.
	 */
	public static final String IS_DELIMITER_OF_SUMMARY_UNIT = WebLabProcessing.NAMESPACE + "isDelimiterOfSummaryUnit";


	/**
	 * URI of the property isDelimiterOfSynthesisUnit. Links a segment to the synthesis MediaUnit it represents.
	 */
	public static final String IS_DELIMITER_OF_SYNTHESIS_UNIT = WebLabProcessing.NAMESPACE + "isDelimiterOfSynthesisUnit";


	/**
	 * URI of the property isDelimiterOfTranscriptUnit. Links a segment to the transcript MediaUnit it represents.
	 */
	public static final String IS_DELIMITER_OF_TRANSCRIPT_UNIT = WebLabProcessing.NAMESPACE + "isDelimiterOfTranscriptUnit";


	/**
	 * URI of the property isDelimiterOfTranslationUnit. Links a segment to the translation MediaUnit it represents.
	 */
	public static final String IS_DELIMITER_OF_TRANSLATION_UNIT = WebLabProcessing.NAMESPACE + "isDelimiterOfTranslationUnit";


	/**
	 * URI of the property isExposedAs. Is exposed on a given protocol at a given address (for instance points to an URL).
	 */
	public static final String IS_EXPOSED_AS = WebLabProcessing.NAMESPACE + "isExposedAs";


	/**
	 * URI of the property isExposedAsThumbnail. Is exposed on a given protocol at a given address (for instance points to an URL).
	 */
	public static final String IS_EXPOSED_AS_THUMBNAIL = WebLabProcessing.NAMESPACE + "isExposedAsThumbnail";


	/**
	 * URI of the property isFrameOf. Links a frame Image to its source Video.
	 */
	public static final String IS_FRAME_OF = WebLabProcessing.NAMESPACE + "isFrameOf";


	/**
	 * URI of the property isGeneratedFrom. Links a resource result of an automatic (translation, summarisation, transcription) process to its source resource.
	 */
	public static final String IS_GENERATED_FROM = WebLabProcessing.NAMESPACE + "isGeneratedFrom";


	/**
	 * URI of the property isInCluster. Links a WebLab Document to a Cluster.
	 */
	public static final String IS_IN_CLUSTER = WebLabProcessing.NAMESPACE + "isInCluster";


	/**
	 * URI of the property isInClusterLink. Links a cluster to a ClusterLink.
	 */
	public static final String IS_IN_CLUSTER_LINK = WebLabProcessing.NAMESPACE + "isInClusterLink";


	/**
	 * URI of the property isLocatedAt. Links a MediaUnit to the segment from which it has been produced.
	 */
	public static final String IS_LOCATED_AT = WebLabProcessing.NAMESPACE + "isLocatedAt";


	/**
	 * URI of the property isProducedBy. Indicates the service that has produced automatically or semi-automatically the resource.
	 */
	public static final String IS_PRODUCED_BY = WebLabProcessing.NAMESPACE + "isProducedBy";


	/**
	 * URI of the property isRecognitionOf. Links an extracted Text to its source Image.
	 */
	public static final String IS_RECOGNITION_OF = WebLabProcessing.NAMESPACE + "isRecognitionOf";


	/**
	 * URI of the property isShotOf. Links a shot Video to its source Video.
	 */
	public static final String IS_SHOT_OF = WebLabProcessing.NAMESPACE + "isShotOf";


	/**
	 * URI of the property isSoundtrackOf. Links a soundtrack to its source Video.
	 */
	public static final String IS_SOUNDTRACK_OF = WebLabProcessing.NAMESPACE + "isSoundtrackOf";


	/**
	 * URI of the property isSummaryOf. Links a summary to its source Text.
	 */
	public static final String IS_SUMMARY_OF = WebLabProcessing.NAMESPACE + "isSummaryOf";


	/**
	 * URI of the property isSynthesisOf. Links a synthesized Audio unit to its source Text.
	 */
	public static final String IS_SYNTHESIS_OF = WebLabProcessing.NAMESPACE + "isSynthesisOf";


	/**
	 * URI of the property isToKeep. Indicates whether or not a Document has to be kept even after the retention delay.
	 */
	public static final String IS_TO_KEEP = WebLabProcessing.NAMESPACE + "isToKeep";


	/**
	 * URI of the property isTranscriptOf. Links a transcript to its source (Image or Video MediaUnit).
	 */
	public static final String IS_TRANSCRIPT_OF = WebLabProcessing.NAMESPACE + "isTranscriptOf";


	/**
	 * URI of the property isTranslationOf. Links translated Text or LinearSegment to its source Text or LinearSegment.
	 */
	public static final String IS_TRANSLATION_OF = WebLabProcessing.NAMESPACE + "isTranslationOf";


	/**
	 * URI of the property refersTo. Links a segment to an instance of a recognised entity (given its URI).
	 */
	public static final String REFERS_TO = WebLabProcessing.NAMESPACE + "refersTo";


	/**
	 * URI of the OWL class Service. A service able to deal with WebLab resources.
	 */
	public static final String SERVICE = WebLabProcessing.NAMESPACE + "Service";


	/**
	 * Constructors Do not use it.
	 */
	private WebLabProcessing() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


}
