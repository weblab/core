/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.util;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.SimilarityQuery;


/**
 * A class that contain a useful method when wanting to visualise the content and the structure of a <code>Resource</code>.
 *
 * @author Cassidian WebLab Team
 */
public final class ResourceUtil {


	/**
	 * The gap used in printed tree.
	 */
	private static final String GAP = "  ";


	/**
	 * Constructor
	 *
	 * Do not use it since it's a utility class.
	 */
	private ResourceUtil() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Retrieve the direct sub <code>Resource</code>s of <code>res</code>. In fact, it retrieves:
	 * <ul>
	 * <li>the <code>Annotations</code>,</li>
	 * <li>the <code>LowLevelDescriptors</code>,</li>
	 * <li>the <code>Resources</code> if it's a <code>ComposedResource, a Similarity or a ResultSet</code>,</li>
	 * <li>the <code>MediaUnits</code> if it's a <code>Document</code></li>
	 * <li>the <code>Queries</code> if it's a <code>ComposedQuery</code></li>
	 * <li>the <code>PieceOfKnowledge</code> if it's a <code>ResultSet</code>.</li>
	 * </ul>
	 *
	 * @param res
	 *            The <code>Resource</code> the resource to process.
	 * @return The <code>List</code> containing <code>Resource</code>s defined just before.
	 */
	public static List<Resource> getDirectSubResources(final Resource res) {
		final List<Resource> subResources = new ArrayList<>();
		if (res.isSetAnnotation()) {
			subResources.addAll(res.getAnnotation());
		}
		if (res.isSetDescriptor()) {
			subResources.addAll(res.getDescriptor());
		}
		if (res instanceof ResultSet) {
			final ResultSet resultSet = (ResultSet) res;
			if (resultSet.isSetPok()) {
				subResources.add(resultSet.getPok());
			}
			if (resultSet.isSetResource()) {
				subResources.addAll(resultSet.getResource());
			}
		} else if (res instanceof ComposedResource) {
			subResources.addAll(((ComposedResource) res).getResource());
		} else if (res instanceof ComposedQuery) {
			subResources.addAll(((ComposedQuery) res).getQuery());
		} else if (res instanceof SimilarityQuery) {
			subResources.addAll(((SimilarityQuery) res).getResource());
		} else if (res instanceof Document) {
			final Document doc = (Document) res;
			if (doc.isSetMediaUnit()) {
				subResources.addAll(doc.getMediaUnit());
			}
		}
		return subResources;
	}


	/**
	 * Retrieves the URI of every resource in res (including res) and a links them to the Resource having this as URI.
	 *
	 * @param res
	 *            The resource to look inside
	 * @return a map containing ethe URI of every resource in res (including res) and a link to the Resource having this URI.
	 */
	public static Map<String, Resource> getResourceUriMap(final Resource res) {
		final Map<String, Resource> result = new LinkedHashMap<>();
		result.put(res.getUri(), res);
		for (final Resource innerRes : ResourceUtil.getSubResources1(res).keySet()) {
			result.put(innerRes.getUri(), innerRes);
		}
		return result;
	}



	/**
	 * Retrieves the URI of every Segment in res and a links them to the Segment having this as URI.
	 *
	 * @param res
	 *            The resource to look inside
	 * @return a map containing the URI of every segment in res (including res) and a link to the segment having this URI.
	 */
	public static Map<String, Segment> getSegmentUriMap(final Resource res) {
		final Map<String, Segment> result = new LinkedHashMap<>();
		for (final Segment seg : ResourceUtil.getSelectedSegmentsMap(res, Segment.class).keySet()) {
			result.put(seg.getUri(), seg);
		}
		return result;
	}


	/**
	 * Filters the list of Segment of <code>mediaUnit</code> that are instance of <code>T</code>.
	 *
	 * Note that the content of {@link org.ow2.weblab.core.model.TrackSegment} is not analysed.
	 *
	 * @see #getSelectedSegmentsMap(Resource, Class)
	 * @param mediaUnit
	 *            The MediaUnit to retrieve the segments in.
	 * @param segClass
	 *            The specific class of the segment to retrieve
	 * @param <T>
	 *            The <code>Class</code> of the type of Segment needed in result.
	 * @return A filtered list of T instances.
	 */
	public static <T extends Segment> List<T> getSelectedSegments(final MediaUnit mediaUnit, final Class<T> segClass) {
		final List<T> result = new LinkedList<>();
		if (mediaUnit.isSetSegment()) {
			for (final Segment seg : mediaUnit.getSegment()) {
				if (segClass.isInstance(seg)) {
					final T segAsT = segClass.cast(seg);
					result.add(segAsT);
				}
			}
		}
		return result;
	}



	/**
	 * Retrieves every Segments in <code>res</code> that are instance of T. The map enables to have a reference to the MediaUnit that contains every segments.
	 *
	 * Note that the content of {@link org.ow2.weblab.core.model.TrackSegment} is not analysed.
	 *
	 * @see #getSelectedSegments(MediaUnit, Class)
	 * @param res
	 *            The <code>Resource</code> to retrieve children.
	 * @param segClass
	 *            The type of segments to be retrieved.
	 * @param <T>
	 *            The <code>Class</code> of the type of Segment needed in result.
	 * @return A Map that associates each Segment (of type T) with the MediaUnit that contains it.
	 */
	public static <T extends Segment> Map<T, MediaUnit> getSelectedSegmentsMap(final Resource res, final Class<T> segClass) {
		final Map<T, MediaUnit> result = new LinkedHashMap<>();
		// Get segments on every sub mediaUnit
		for (final MediaUnit mu : ResourceUtil.getSelectedSubResources(res, MediaUnit.class)) {
			for (final T t : ResourceUtil.getSelectedSegments(mu, segClass)) {
				result.put(t, mu);
			}
		}
		// Get segment on this resource if its a mediaUnit
		if (res instanceof MediaUnit) {
			final MediaUnit mediaUnit = (MediaUnit) res;
			for (final T t : ResourceUtil.getSelectedSegments(mediaUnit, segClass)) {
				result.put(t, mediaUnit);
			}
		}
		return result;
	}


	/**
	 * The same as <code>ResourceUtil.getSubResources(Resource)</code>, but with a filter on the <code>Resource</code> class to be retrieved by the
	 * <code>List</code>. I.e. the only retrieved <code>Resource</code>s are those that are instances of <code>resClass</code>.
	 *
	 * @param <T>
	 *            Subclass of Resource to be retrieved
	 * @param res
	 *            The <code>Resource</code> to retrieve children.
	 * @param resClass
	 *            The <code>Class</code> of the only children you need in result <code>List</code>.
	 * @see ResourceUtil#getSubResources(Resource)
	 * @return The subResource <code>List</code>.
	 */
	public static <T extends Resource> List<T> getSelectedSubResources(final Resource res, final Class<T> resClass) {
		final List<T> result = new ArrayList<>();
		for (final Resource r : ResourceUtil.getSubResources1(res).keySet()) {
			if (resClass.isInstance(r)) {
				final T muAsT = resClass.cast(r);
				result.add(muAsT);
			}
		}
		return result;
	}


	/**
	 * The same as <code>ResourceUtil.getSubResources(Resource, Class&lt;T&gt;)</code>; But in addition to the sub-resources by themselves, this method return an
	 * reference to the resource containing every subresource.
	 *
	 * @param <T>
	 *            Subclass of Resource to be retrieved
	 * @param res
	 *            The <code>Resource</code> to retrieve children.
	 * @param resClass
	 *            The <code>Class</code> of the only children you need in result <code>List</code>.
	 * @see ResourceUtil#getSelectedSubResources(Resource, Class)
	 * @return The subResource <code>Map</code> with their parents.
	 */
	public static <T extends Resource> Map<T, Resource> getSelectedSubResourcesMap(final Resource res, final Class<T> resClass) {
		final Map<T, Resource> result = new LinkedHashMap<>();
		for (final Entry<Resource, Resource> entry : ResourceUtil.getSubResources1(res).entrySet()) {
			if (resClass.isInstance(entry.getKey())) {
				final T muAsT = resClass.cast(entry.getKey());
				result.put(muAsT, entry.getValue());
			}
		}
		return result;
	}


	/**
	 * Get a list of every resources included in the <code>Resource</code> in parameter. It goes recursively down the resource, whatever WebLab Resource it is.
	 *
	 * @see #getSelectedSubResources(Resource, Class)
	 * @param res
	 *            The <code>Resource</code> to retrieve children.
	 * @return The sub<code>Resource</code> <code>List</code> without <code>res</code> it self.
	 */
	public static List<Resource> getSubResources(final Resource res) {
		return new ArrayList<>(ResourceUtil.getSubResources1(res).keySet());
	}


	/**
	 * Simply transform the XML <code>String</code> into a <code>Resource</code>. Use the {@link WebLabMarshaller} for more configuration, and usage with files
	 * or streams.
	 *
	 * @see WebLabMarshaller
	 * @param xml
	 *            The XML <code>String</code> representing the Resource
	 * @return The <code>Resource</code> defined in the XML <code>String</code>.
	 * @throws WebLabCheckedException
	 *             If a <code>JAXBException</code> occurred when marshaling the resource.
	 */
	public static Resource loadFromXMLString(final String xml) throws WebLabCheckedException {
		return new WebLabMarshaller().unmarshal(new StringReader(xml), Resource.class);
	}


	/**
	 * Simple visualisation method that enable to print a user friendly (non-xml) description of <code>res</code>. Uses a tree representation. For each type of
	 * <code>Resource</code> that might me contained by the given <code>Resource</code> and it's contained <code>Resource</code>s print their <code>URI</code>
	 * and their class simple name. For any <code>Resource</code>, it's contained <code>Resource</code> s might be:
	 * <ul>
	 * <li>the <code>Annotations</code>,</li>
	 * <li>the <code>LowLevelDescriptors</code>,</li>
	 * <li>the <code>Resources</code> if it's a <code>ComposedResource, a Similarity or a ResultSet</code>,</li>
	 * <li>the <code>MediaUnits</code> if it's a <code>Document</code></li>
	 * <li>the <code>Queries</code> if it's a <code>ComposedQuery</code></li>
	 * <li>the <code>PieceOfKnowledge</code> if it's a <code>ResultSet</code>.</li>
	 * </ul>
	 *
	 * @param res
	 *            The <code>Resource</code> to be printed
	 * @param writer
	 *            The <code>Writer</code> to write into
	 */
	public static void printTree(final Resource res, final Writer writer) {
		ResourceUtil.printTree(res, writer, false);
	}


	/**
	 * Simple visualisation method that enable to print a user friendly (non-xml) description of <code>res</code>. Uses a tree representation. For each type of
	 * <code>Resource</code> that might me contained by the given <code>Resource</code> and it's contained <code>Resource</code>s print their <code>URI</code>
	 * and their class simple name. For any <code>Resource</code>, it's contained <code>Resource</code> s might be:
	 * <ul>
	 * <li>the <code>Annotations</code>,</li>
	 * <li>the <code>LowLevelDescriptors</code>,</li>
	 * <li>the <code>Resources</code> if it's a <code>ComposedResource, a Similarity or a ResultSet</code>,</li>
	 * <li>the <code>MediaUnits</code> if it's a <code>Document</code></li>
	 * <li>the <code>Queries</code> if it's a <code>ComposedQuery</code></li>
	 * <li>the <code>PieceOfKnowledge</code> if it's a <code>ResultSet</code>.</li>
	 * </ul>
	 * In addition, if include Segments is <code>true</code>, URI of <code>Segment</code> are also printed.
	 *
	 * @param includeSegments
	 *            Whether or not to include segments in the <code>strean</code>
	 * @param res
	 *            The <code>Resource</code> to be printed
	 * @param writer
	 *            The <code>Writer</code> to write into
	 */
	public static void printTree(final Resource res, final Writer writer, final boolean includeSegments) {
		final PrintWriter printWriter = new PrintWriter(writer, true);
		ResourceUtil.printTree(res, printWriter, "", includeSegments);
	}


	/**
	 * Simply transforms <code>resource</code> into an XML <code>String</code>. Use the {@link WebLabMarshaller} for more configuration, and usage with files or
	 * streams.
	 *
	 * @see WebLabMarshaller
	 * @param resource
	 *            The <code>Resource</code> to be transformed into an XML <code>String</code>.
	 * @return A <code>String</code> containing the XML view of the <code>Resource</code>
	 * @throws WebLabCheckedException
	 *             If a <code>JAXBException</code> occurred when marshaling the resource.
	 */
	public static String saveToXMLString(final Resource resource) throws WebLabCheckedException {
		final StringWriter strWrtr = new StringWriter();
		new WebLabMarshaller().marshalResource(resource, strWrtr);
		return strWrtr.toString();
	}


	/**
	 * @param res
	 *            The <code>Resource</code> to retrieve children.
	 * @return An ordered Map containing every subresource of <code>res</code> as keys and parent of those resources as values.
	 */
	private static Map<Resource, Resource> getSubResources1(final Resource res) {
		final Map<Resource, Resource> resources = new LinkedHashMap<>();
		final List<Resource> subs = ResourceUtil.getDirectSubResources(res);
		for (final Resource sub : subs) {
			resources.put(sub, res);
			resources.putAll(ResourceUtil.getSubResources1(sub));
		}
		return resources;
	}



	/**
	 * @param includeSegments
	 *            Whether or not to include segments in the printed tree.
	 * @param res
	 *            The <code>Resource</code> to be printed.
	 * @param writer
	 *            The <code>PrintWriter</code> to write into.
	 * @param gap
	 *            The space to be added at the beginning of the lines.
	 */
	private static void printTree(final Resource res, final PrintWriter writer, final String gap, final boolean includeSegments) {
		writer.println(gap + res.getUri() + ResourceUtil.GAP + res.getClass().getSimpleName());
		if (includeSegments && (res instanceof MediaUnit) && ((MediaUnit) res).isSetSegment()) {
			for (final Segment seg : ((MediaUnit) res).getSegment()) {
				writer.println(gap + ResourceUtil.GAP + seg.getUri() + ResourceUtil.GAP + seg.getClass().getSimpleName());
			}
		}
		for (final Resource subResource : ResourceUtil.getDirectSubResources(res)) {
			ResourceUtil.printTree(subResource, writer, gap + ResourceUtil.GAP, includeSegments);
		}
	}


}
