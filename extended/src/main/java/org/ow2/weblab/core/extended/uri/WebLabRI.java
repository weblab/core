/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.uri;

import java.net.URI;
import java.net.URISyntaxException;

import org.ow2.weblab.core.extended.exception.WebLabRISyntaxException;

/**
 * Just a Wrapper for URI. It enable to ensure the <code>uri</code> field of the <code>Resource</code> are compliant with URI (since in the generated classes
 * it's a <code>String</code>).
 * 
 * @author Cassidian WebLab Team
 */
public final class WebLabRI {


	/**
	 * The wrapped URI
	 */
	private URI uri;


	/**
	 * Create a <code>WebLabRI</code> with tests.
	 * 
	 * @param uriStr
	 *            a <code>String</code> representation of the <code>WebLabRI</code>.
	 */
	public WebLabRI(final String uriStr) {
		super();
		// A valid WebLabRI must be a valid URI
		try {
			this.uri = new URI(uriStr);
		} catch (final URISyntaxException urise) {
			throw new WebLabRISyntaxException(urise);
		}
	}


	/**
	 * Add a fragment to a <code>WebLabRI</code> .
	 * 
	 * @param fragment
	 *            Added to the <code>WebLabRI</code> fragment
	 */
	public void addFragment(final String fragment) {
		if (this.uri.getFragment() == null) {
			try {
				this.uri = new URI(this.uri.toString() + '#' + fragment);
			} catch (final URISyntaxException urise) {
				throw new WebLabRISyntaxException(urise);
			}
		} else {
			try {
				this.uri = new URI(this.uri.toString() + '-' + fragment);
			} catch (final URISyntaxException urise) {
				throw new WebLabRISyntaxException(urise);
			}
		}
	}


	/**
	 * @return the fragment part of the URI.
	 */
	public String getFragment() {
		return this.uri.getFragment();
	}


	@Override
	public String toString() {
		return this.uri.toString();
	}

}
