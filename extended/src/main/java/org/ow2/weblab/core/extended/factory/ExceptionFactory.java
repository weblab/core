/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.extended.factory;

import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.EmptyQueueException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;

/**
 * This class enables the creation of Exception to be thrown by the various WebLab methods.<br>
 * It enables to send only one time the String parameter in order to send it to the two string paramter constructors.
 * 
 * @author ymombrun
 */
public final class ExceptionFactory {


	/**
	 * Constructors Do not use it.
	 */
	private ExceptionFactory() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Creates an instance of AccessDeniedException to be thrown when an error due to some security restriction.
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @return An instance of AccessDeniedException
	 */
	public static AccessDeniedException createAccessDeniedException(final String message) {
		return new AccessDeniedException(message, message);
	}


	/**
	 * Creates an instance of AccessDeniedException to be thrown when an error due to some security restriction.
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @param cause
	 *            The cause of the exception
	 * @return An instance of AccessDeniedException
	 */
	public static AccessDeniedException createAccessDeniedException(final String message, final Throwable cause) {
		return new AccessDeniedException(message, message, cause);
	}



	/**
	 * Creates an instance of ContentNotAvailableException to be thrown when the content of a resource is needed and not available.
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @return An instance of ContentNotAvailableException
	 */
	public static ContentNotAvailableException createContentNotAvailableException(final String message) {
		return new ContentNotAvailableException(message, message);
	}


	/**
	 * Creates an instance of ContentNotAvailableException to be thrown when the content of a resource is needed and not available.
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @param cause
	 *            The cause of the exception
	 * @return An instance of ContentNotAvailableException
	 */
	public static ContentNotAvailableException createContentNotAvailableException(final String message, final Throwable cause) {
		return new ContentNotAvailableException(message, message, cause);
	}


	/**
	 * Creates an instance of EmptyQueueException to be thrown when no more resources are available in the queue.
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @return An instance of EmptyQueueException
	 */
	public static EmptyQueueException createEmptyQueueException(final String message) {
		return new EmptyQueueException(message, message);
	}


	/**
	 * Creates an instance of EmptyQueueException to be thrown when no more resources are available in the queue.
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @param cause
	 *            The cause of the exception
	 * @return An instance of EmptyQueueException
	 */
	public static EmptyQueueException createEmptyQueueException(final String message, final Throwable cause) {
		return new EmptyQueueException(message, message, cause);
	}


	/**
	 * Creates an instance of InsufficientResourcesException to be thrown when an error occurs due to the lack of resources (e.g. unable to allocate the disk or
	 * memory space needed).
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @return An instance of InsufficientResourcesException
	 */
	public static InsufficientResourcesException createInsufficientResourcesException(final String message) {
		return new InsufficientResourcesException(message, message);
	}


	/**
	 * Creates an instance of InsufficientResourcesException to be thrown when an error occurs due to the lack of resources (e.g. unable to allocate the disk or
	 * memory space needed).
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @param cause
	 *            The cause of the exception
	 * @return An instance of InsufficientResourcesException
	 */
	public static InsufficientResourcesException createInsufficientResourcesException(final String message, final Throwable cause) {
		return new InsufficientResourcesException(message, message, cause);
	}


	/**
	 * Creates an instance of InvalidParameterException to be thrown when the parameter sent to the service is not valid (e.g. empty, null, bad type, missing
	 * one required property).
	 * 
	 * @see #createUnsupportedRequestException(String) When the resource is almost valid
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @return An instance of InvalidParameterException
	 */
	public static InvalidParameterException createInvalidParameterException(final String message) {
		return new InvalidParameterException(message, message);
	}


	/**
	 * Creates an instance of InvalidParameterException to be thrown when the parameter sent to the service is not valid (e.g. empty, null, bad type, missing
	 * one required property).
	 * 
	 * @see #createUnsupportedRequestException(String, Throwable) When the resource is almost valid
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @param cause
	 *            The cause of the exception
	 * @return An instance of InvalidParameterException
	 */
	public static InvalidParameterException createInvalidParameterException(final String message, final Throwable cause) {
		return new InvalidParameterException(message, message, cause);
	}


	/**
	 * Creates an instance of ServiceNotConfiguredException to be thrown when the service needs to be configured and either your usageContext is null or has not
	 * been configured.
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @return An instance of ServiceNotConfiguredException
	 */
	public static ServiceNotConfiguredException createServiceNotConfiguredException(final String message) {
		return new ServiceNotConfiguredException(message, message);
	}


	/**
	 * Creates an instance of ServiceNotConfiguredException to be thrown when the service needs to be configured and either your usageContext is null or has not
	 * been configured.
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @param cause
	 *            The cause of the exception
	 * @return An instance of ServiceNotConfiguredException
	 */
	public static ServiceNotConfiguredException createServiceNotConfiguredException(final String message, final Throwable cause) {
		return new ServiceNotConfiguredException(message, message, cause);
	}


	/**
	 * Creates an instance of UnexpectedException to be thrown when an internal error occurs.
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @return An instance of UnexpectedException
	 */
	public static UnexpectedException createUnexpectedException(final String message) {
		return new UnexpectedException(message, message);
	}


	/**
	 * Creates an instance of UnexpectedException to be thrown when an internal error occurs.
	 * 
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @param cause
	 *            The cause of the exception
	 * @return An instance of UnexpectedException
	 */
	public static UnexpectedException createUnexpectedException(final String message, final Throwable cause) {
		return new UnexpectedException(message, message, cause);
	}


	/**
	 * Creates an instance of UnsupportedRequestException to be thrown when parameters are good but some specific process are not supported (e.g.: given
	 * language or video format not handled).
	 * 
	 * @see #createInvalidParameterException(String) When the resource is absolutly bad
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @return An instance of UnsupportedRequestException
	 */
	public static UnsupportedRequestException createUnsupportedRequestException(final String message) {
		return new UnsupportedRequestException(message, message);
	}


	/**
	 * Creates an instance of UnsupportedRequestException to be thrown when parameters are good but some specific process are not supported (e.g.: given
	 * language or video format not handled).
	 * 
	 * @see #createInvalidParameterException(String, Throwable) When the resource is absolutly bad
	 * @param message
	 *            The message of the exception and the soap fault message
	 * @param cause
	 *            The cause of the exception
	 * @return An instance of UnsupportedRequestException
	 */
	public static UnsupportedRequestException createUnsupportedRequestException(final String message, final Throwable cause) {
		return new UnsupportedRequestException(message, message, cause);
	}

}
