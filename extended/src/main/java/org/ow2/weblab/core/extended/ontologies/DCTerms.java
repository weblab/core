/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.ontologies;

/**
 * The DublinCore properties according to <tt>http://dublincore.org/documents/dcmi-terms</tt>, the DCMI Metadata Terms.
 * 
 * @author Cassidian WebLab Team
 */
public final class DCTerms {


	/**
	 * http://purl.org/dc/terms/
	 */
	public static final String NAMESPACE = "http://purl.org/dc/terms/";


	/**
	 * Preferred prefix (dct)
	 */
	public static final String PREFERRED_PREFIX = "dct";


	/**
	 * A summary of the resource.
	 */
	public static final String ABSTRACT = DCTerms.NAMESPACE + "abstract";


	/**
	 * Information about who can access the resource or an indication of its security status.<br>
	 * Access Rights may include information regarding access or restrictions based on privacy, security, or other policies.
	 */
	public static final String ACCESS_RIGHTS = DCTerms.NAMESPACE + "accessRights";


	/**
	 * The method by which items are added to a collection.
	 */
	public static final String ACCRUAL_METHOD = DCTerms.NAMESPACE + "accrualMethod";


	/**
	 * The frequency with which items are added to a collection.
	 */
	public static final String ACCRUAL_PERIODICITY = DCTerms.NAMESPACE + "accrualPeriodicity";


	/**
	 * The policy governing the addition of items to a collection.
	 */
	public static final String ACCRUAL_POLICY = DCTerms.NAMESPACE + "accrualPolicy";


	/**
	 * A resource that acts or has the power to act.<br>
	 * Examples of Agent include person, organization, and software agent.
	 */
	public static final String AGENT = DCTerms.NAMESPACE + "Agent";


	/**
	 * A group of agents.<br>
	 * Examples of Agent Class include groups seen as classes, such as students, women, charities, lecturers.
	 */
	public static final String AGENT_CLASS = DCTerms.NAMESPACE + "AgentClass";


	/**
	 * An alternative name for the resource.<br>
	 * The distinction between titles and alternative titles is application-specific.
	 */
	public static final String ALTERNATIVE = DCTerms.NAMESPACE + "alternative";


	/**
	 * A class of entity for whom the resource is intended or useful.
	 */
	public static final String AUDIENCE = DCTerms.NAMESPACE + "audience";


	/**
	 * Date (often a range) that the resource became or will become available.
	 */
	public static final String AVAILABLE = DCTerms.NAMESPACE + "available";


	/**
	 * A bibliographic reference for the resource.<br>
	 * Recommended practice is to include sufficient bibliographic detail to identify the resource as unambiguously as possible.
	 */
	public static final String BIBLIOGRAPHIC_CITATION = DCTerms.NAMESPACE + "bibliographicCitation";


	/**
	 * A book, article, or other documentary resource.
	 */
	public static final String BIBLIOGRAPHIC_RESOURCE = DCTerms.NAMESPACE + "BibliographicResource";


	/**
	 * The set of regions in space defined by their geographic coordinates according to the DCMI Box Encoding Scheme.
	 */
	public static final String BOX = DCTerms.NAMESPACE + "Box";


	/**
	 * An established standard to which the described resource conforms.
	 */
	public static final String CONFORMS_TO = DCTerms.NAMESPACE + "conformsTo";


	/**
	 * An entity responsible for making contributions to the resource.<br>
	 * Examples of a Contributor include a person, an organization, or a service. Typically, the name of a Contributor should be used to indicate the entity.
	 */
	public static final String CONTRIBUTOR = DCTerms.NAMESPACE + "contributor";


	/**
	 * The spatial or temporal topic of the resource, the spatial applicability of the resource, or the jurisdiction under which the resource is relevant.<br>
	 * Spatial topic and spatial applicability may be a named place or a location specified by its geographic coordinates. Temporal topic may be a named period,
	 * date, or date range. A jurisdiction may be a named administrative entity or a geographic place to which the resource applies. Recommended best practice
	 * is to use a controlled vocabulary such as the Thesaurus of Geographic Names [TGN]. Where appropriate, named places or time periods can be used in
	 * preference to numeric identifiers such as sets of coordinates or date ranges.<br>
	 */
	public static final String COVERAGE = DCTerms.NAMESPACE + "coverage";


	/**
	 * Date of creation of the resource.
	 */
	public static final String CREATED = DCTerms.NAMESPACE + "created";


	/**
	 * An entity primarily responsible for making the resource.<br>
	 * Examples of a Creator include a person, an organization, or a service. Typically, the name of a Creator should be used to indicate the entity.
	 */
	public static final String CREATOR = DCTerms.NAMESPACE + "creator";


	/**
	 * A point or period of time associated with an event in the lifecycle of the resource.<br>
	 * Date may be used to express temporal information at any level of granularity. Recommended best practice is to use an encoding scheme, such as the W3CDTF
	 * profile of ISO 8601 [W3CDTF].
	 */
	public static final String DATE = DCTerms.NAMESPACE + "date";


	/**
	 * Date of acceptance of the resource.<br>
	 * Examples of resources to which a Date Accepted may be relevant are a thesis (accepted by a university department) or an article (accepted by a journal).
	 */
	public static final String DATE_ACCEPTED = DCTerms.NAMESPACE + "dateAccepted";


	/**
	 * Date of copyright.
	 */
	public static final String DATE_COPYRIGHTED = DCTerms.NAMESPACE + "dateCopyrighted";


	/**
	 * Date of submission of the resource.<br>
	 * Examples of resources to which a Date Submitted may be relevant are a thesis (submitted to a university department) or an article (submitted to a
	 * journal).
	 */
	public static final String DATE_SUBMITTED = DCTerms.NAMESPACE + "dateSubmitted";


	/**
	 * The set of classes specified by the DCMI Type Vocabulary, used to categorize the nature or genre of the resource.
	 */
	public static final String DCMI_TYPE = DCTerms.NAMESPACE + "DCMIType";


	/**
	 * The set of conceptual resources specified by the Dewey Decimal Classification.
	 */
	public static final String DDC = DCTerms.NAMESPACE + "DDC";


	/**
	 * An account of the resource.<br>
	 * Description may include but is not limited to: an abstract, a table of contents, a graphical representation, or a free-text account of the resource.
	 */
	public static final String DESCRIPTION = DCTerms.NAMESPACE + "description";


	/**
	 * Audience Education Level<br>
	 * A class of entity, defined in terms of progression through an educational or training context, for which the described resource is intended.
	 */
	public static final String EDUCATION_LEVEL = DCTerms.NAMESPACE + "educationLevel";


	/**
	 * The size or duration of the resource.
	 */
	public static final String EXTENT = DCTerms.NAMESPACE + "extent";


	/**
	 * A digital resource format.<br>
	 * Examples include the formats defined by the list of Internet Media Types.
	 */
	public static final String FILE_FORMAT = DCTerms.NAMESPACE + "FileFormat";


	/**
	 * A set of format encoding schemes.
	 */
	public static final String FORMAT = DCTerms.NAMESPACE + "format";


	/**
	 * A rate at which something recurs.
	 */
	public static final String FREQUENCY = DCTerms.NAMESPACE + "Frequency";


	/**
	 * A related resource that is substantially the same as the pre-existing described resource, but in another format.
	 */
	public static final String HAS_FORMAT = DCTerms.NAMESPACE + "hasFormat";


	/**
	 * A related resource that is included either physically or logically in the described resource.
	 */
	public static final String HAS_PART = DCTerms.NAMESPACE + "hasPart";


	/**
	 * A related resource that is a version, edition, or adaptation of the described resource.
	 */
	public static final String HAS_VERSION = DCTerms.NAMESPACE + "hasVersion";


	/**
	 * An unambiguous reference to the resource within a given context.<br>
	 * Recommended best practice is to identify the resource by means of a string conforming to a formal identification system.
	 */
	public static final String IDENTIFIER = DCTerms.NAMESPACE + "identifier";


	/**
	 * The set of media types specified by the Internet Assigned Numbers Authority.
	 */
	public static final String IMT = DCTerms.NAMESPACE + "IMT";


	/**
	 * A process, used to engender knowledge, attitudes and skills, that the described resource is designed to support.<br>
	 * Instructional Method will typically include ways of presenting instructional materials or conducting instructional activities, patterns of
	 * learner-to-learner and learner-to-instructor interactions, and mechanisms by which group and individual levels of learning are measured. Instructional
	 * methods include all aspects of the instruction and learning processes from planning and implementation through evaluation and feedback.
	 */
	public static final String INSTRUCTIONAL_METHOD = DCTerms.NAMESPACE + "instructionalMethod";


	/**
	 * A related resource that is substantially the same as the described resource, but in another format.
	 */
	public static final String IS_FORMAT_OF = DCTerms.NAMESPACE + "isFormatOf";


	/**
	 * A related resource in which the described resource is physically or logically included.
	 */
	public static final String IS_PART_OF = DCTerms.NAMESPACE + "isPartOf";


	/**
	 * A related resource that references, cites, or otherwise points to the described resource.
	 */
	public static final String IS_REFERENCED_BY = DCTerms.NAMESPACE + "isReferencedBy";


	/**
	 * A related resource that supplants, displaces, or supersedes the described resource.
	 */
	public static final String IS_REPLACED_BY = DCTerms.NAMESPACE + "isReplacedBy";


	/**
	 * A related resource that requires the described resource to support its function, delivery, or coherence.
	 */
	public static final String IS_REQUIRED_BY = DCTerms.NAMESPACE + "isRequiredBy";


	/**
	 * A related resource of which the described resource is a version, edition, or adaptation.<br>
	 * Changes in version imply substantive changes in content rather than differences in format.
	 */
	public static final String IS_VERSION_OF = DCTerms.NAMESPACE + "isVersionOf";


	/**
	 * The set of codes listed in ISO 3166-1 for the representation of names of countries.
	 */
	public static final String ISO3166 = DCTerms.NAMESPACE + "ISO3166";


	/**
	 * The three-letter alphabetic codes listed in ISO639-2 for the representation of names of languages.
	 */
	public static final String ISO639_2 = DCTerms.NAMESPACE + "ISO639-2";


	/**
	 * The set of three-letter codes listed in ISO 639-3 for the representation of names of languages.
	 */
	public static final String ISO639_3 = DCTerms.NAMESPACE + "ISO639-3";


	/**
	 * Date of formal issuance (e.g., publication) of the resource.
	 */
	public static final String ISSUED = DCTerms.NAMESPACE + "issued";


	/**
	 * The extent or range of judicial, law enforcement, or other authority.
	 */
	public static final String JURISDICTION = DCTerms.NAMESPACE + "Jurisdiction";


	/**
	 * A language of the resource.<br>
	 * Recommended best practice is to use a controlled vocabulary such as RFC 4646 [RFC4646].
	 */
	public static final String LANGUAGE = DCTerms.NAMESPACE + "language";


	/**
	 * The set of conceptual resources specified by the Library of Congress Classification.
	 */
	public static final String LCC = DCTerms.NAMESPACE + "LCC";


	/**
	 * The set of labelled concepts specified by the Library of Congress Subject Headings.
	 */
	public static final String LCSH = DCTerms.NAMESPACE + "LCSH";


	/**
	 * A legal document giving official permission to do something with the resource.
	 */
	public static final String LICENSE = DCTerms.NAMESPACE + "license";


	/**
	 * A legal document giving official permission to do something with a Resource.
	 */
	public static final String LICENSE_DOCUMENT = DCTerms.NAMESPACE + "LicenseDocument";


	/**
	 * Examples include written, spoken, sign, and computer languages.<br>
	 * A system of signs, symbols, sounds, gestures, or rules used in communication.
	 */
	public static final String LINGUISTIC_SYSTEM = DCTerms.NAMESPACE + "LinguisticSystem";


	/**
	 * A spatial region or named place.
	 */
	public static final String LOCATION = DCTerms.NAMESPACE + "Location";


	/**
	 * A location, period of time, or jurisdiction.
	 */
	public static final String LOCATION_PERIOD_OR_JURISDICTION = DCTerms.NAMESPACE + "LocationPeriodOrJurisdiction";


	/**
	 * A file format or physical medium.
	 */
	public static final String MEDIA_TYPE = DCTerms.NAMESPACE + "MediaType";


	/**
	 * A media type or extent.
	 */
	public static final String MEDIA_TYPE_OR_EXTENT = DCTerms.NAMESPACE + "MediaTypeOrExtent";


	/**
	 * An entity that mediates access to the resource and for whom the resource is intended or useful.<br>
	 * In an educational context, a mediator might be a parent, teacher, teaching assistant, or care-giver.
	 */
	public static final String MEDIATOR = DCTerms.NAMESPACE + "mediator";


	/**
	 * The material or physical carrier of the resource.
	 */
	public static final String MEDIUM = DCTerms.NAMESPACE + "medium";


	/**
	 * The set of labelled concepts specified by the Medical Subject Headings.
	 */
	public static final String MESH = DCTerms.NAMESPACE + "MESH";


	/**
	 * A method by which resources are added to a collection.
	 */
	public static final String METHOD_OF_ACCRUAL = DCTerms.NAMESPACE + "MethodOfAccrual";


	/**
	 * A process that is used to engender knowledge, attitudes, and skills.
	 */
	public static final String METHOD_OF_INSTRUCTION = DCTerms.NAMESPACE + "MethodOfInstruction";


	/**
	 * Date on which the resource was changed.
	 */
	public static final String MODIFIED = DCTerms.NAMESPACE + "modified";


	/**
	 * The set of conceptual resources specified by the National Library of Medicine Classification.
	 */
	public static final String NLM = DCTerms.NAMESPACE + "NLM";


	/**
	 * The set of time intervals defined by their limits according to the DCMI Period Encoding Scheme.
	 */
	public static final String PERIOD = DCTerms.NAMESPACE + "Period";


	/**
	 * An interval of time that is named or defined by its start and end dates.
	 */
	public static final String PERIOD_OF_TIME = DCTerms.NAMESPACE + "PeriodOfTime";


	/**
	 * A physical material or carrier.<br>
	 * Examples include paper, canvas, or DVD.
	 */
	public static final String PHYSICAL_MEDIUM = DCTerms.NAMESPACE + "PhysicalMedium";


	/**
	 * A material thing.
	 */
	public static final String PHYSICAL_RESOURCE = DCTerms.NAMESPACE + "PhysicalResource";


	/**
	 * The set of points in space defined by their geographic coordinates according to the DCMI Point Encoding Scheme.
	 */
	public static final String POINT = DCTerms.NAMESPACE + "Point";


	/**
	 * A plan or course of action by an authority, intended to influence and determine decisions, actions, and other matters.
	 */
	public static final String POLICY = DCTerms.NAMESPACE + "Policy";


	/**
	 * A statement of any changes in ownership and custody of the resource since its creation that are significant for its authenticity, integrity, and
	 * interpretation.<br>
	 * The statement may include a description of any changes successive custodians made to the resource.
	 */
	public static final String PROVENANCE = DCTerms.NAMESPACE + "provenance";


	/**
	 * A statement of any changes in ownership and custody of a resource since its creation that are significant for its authenticity, integrity, and
	 * interpretation.
	 */
	public static final String PROVENANCE_STATEMENT = DCTerms.NAMESPACE + "ProvenanceStatement";


	/**
	 * An entity responsible for making the resource available.<br>
	 * Examples of a Publisher include a person, an organization, or a service. Typically, the name of a Publisher should be used to indicate the entity.
	 */
	public static final String PUBLISHER = DCTerms.NAMESPACE + "publisher";


	/**
	 * A related resource that is referenced, cited, or otherwise pointed to by the described resource.
	 */
	public static final String REFERENCES = DCTerms.NAMESPACE + "references";


	/**
	 * A related resource.<br>
	 * Recommended best practice is to identify the related resource by means of a string conforming to a formal identification system.
	 */
	public static final String RELATION = DCTerms.NAMESPACE + "relation";


	/**
	 * A related resource that is supplanted, displaced, or superseded by the described resource.
	 */
	public static final String REPLACES = DCTerms.NAMESPACE + "replaces";


	/**
	 * A related resource that is required by the described resource to support its function, delivery, or coherence.
	 */
	public static final String REQUIRES = DCTerms.NAMESPACE + "requires";


	/**
	 * The set of tags, constructed according to RFC 1766, for the identification of languages.
	 */
	public static final String RFC1766 = DCTerms.NAMESPACE + "RFC1766";


	/**
	 * The set of tags constructed according to RFC 3066 for the identification of languages.<br>
	 * RFC 3066 has been obsoleted by RFC 4646.
	 */
	public static final String RFC3066 = DCTerms.NAMESPACE + "RFC3066";


	/**
	 * The set of tags constructed according to RFC 4646 for the identification of languages.
	 */
	public static final String RFC4646 = DCTerms.NAMESPACE + "RFC4646";


	/**
	 * Information about rights held in and over the resource.<br>
	 * Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights.
	 */
	public static final String RIGHTS = DCTerms.NAMESPACE + "rights";


	/**
	 * A person or organization owning or managing rights over the resource.
	 */
	public static final String RIGHTS_HOLDER = DCTerms.NAMESPACE + "rightsHolder";


	/**
	 * A statement about the intellectual property rights (IPR) held in or over a Resource, a legal document giving official permission to do something with a
	 * resource, or a statement about access rights.
	 */
	public static final String RIGHTS_STATEMENTS = DCTerms.NAMESPACE + "RightsStatement";


	/**
	 * A dimension or extent, or a time taken to play or execute.<br>
	 * Examples include a number of pages, a specification of length, width, and breadth, or a period in hours, minutes, and seconds.
	 */
	public static final String SIZE_OR_DURATION = DCTerms.NAMESPACE + "SizeOrDuration";


	/**
	 * A related resource from which the described resource is derived.<br>
	 * The described resource may be derived from the related resource in whole or in part. Recommended best practice is to identify the related resource by
	 * means of a string conforming to a formal identification system.
	 */
	public static final String SOURCE = DCTerms.NAMESPACE + "source";


	/**
	 * Spatial characteristics of the resource.
	 */
	public static final String SPATIAL = DCTerms.NAMESPACE + "spatial";


	/**
	 * A basis for comparison; a reference point against which other things can be evaluated.
	 */
	public static final String STANDARD = DCTerms.NAMESPACE + "Standard";


	/**
	 * The topic of the resource.<br>
	 * Typically, the subject will be represented using keywords, key phrases, or classification codes. Recommended best practice is to use a controlled
	 * vocabulary. To describe the spatial or temporal topic of the resource, use the Coverage element.
	 */
	public static final String SUBJECT = DCTerms.NAMESPACE + "subject";


	/**
	 * A list of subunits of the resource.
	 */
	public static final String TABLE_OF_CONTENTS = DCTerms.NAMESPACE + "tableOfContents";


	/**
	 * Temporal characteristics of the resource.
	 */
	public static final String TEMPORAL = DCTerms.NAMESPACE + "temporal";


	/**
	 * The set of places specified by the Getty Thesaurus of Geographic Names.
	 */
	public static final String TGN = DCTerms.NAMESPACE + "TGN";


	/**
	 * A name given to the resource.
	 */
	public static final String TITLE = DCTerms.NAMESPACE + "title";


	/**
	 * The nature or genre of the resource.<br>
	 * Recommended best practice is to use a controlled vocabulary such as the DCMI Type Vocabulary [DCMITYPE]. To describe the file format, physical medium, or
	 * dimensions of the resource, use the Format element.
	 */
	public static final String TYPE = DCTerms.NAMESPACE + "type";


	/**
	 * The set of conceptual resources specified by the Universal Decimal Classification.
	 */
	public static final String UDC = DCTerms.NAMESPACE + "UDC";


	/**
	 * The set of identifiers constructed according to the generic syntax for Uniform Resource Identifiers as specified by the Internet Engineering Task Force.
	 */
	public static final String URI = DCTerms.NAMESPACE + "URI";


	/**
	 * Date (often a range) of validity of a resource.
	 */
	public static final String VALID = DCTerms.NAMESPACE + "valid";


	/**
	 * The set of dates and times constructed according to the W3C Date and Time Formats Specification.
	 */
	public static final String W3CDTF = DCTerms.NAMESPACE + "W3CDTF";


	/**
	 * Constructors Do not use it.
	 */
	private DCTerms() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}

}
