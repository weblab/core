/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.extended.ontologies;

import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Segment;


/**
 * Contains every properties that are defined in the model.owl file of the WebLab model project.
 * 
 * This version includes annotable object of the model i.e. Resources and their relationships. It also provides some properties to be used in annotations.
 * 
 * @author WebLab Cassidian team
 */
public final class WebLabModel {


	/**
	 * Namespace of the WebLab model ontology.
	 */
	public static final String NAMESPACE = "http://weblab.ow2.org/core/1.2/ontology/model#";


	/**
	 * Preferred prefix to be used to represent the WebLab Model ontology namespace.
	 */
	public static final String PREFIX = "model";


	/**
	 * URI of the Annotation object
	 */
	public static final String ANNOTATION = WebLabModel.NAMESPACE + "Annotation";


	/**
	 * URI of the Audio object
	 */
	public static final String AUDIO = WebLabModel.NAMESPACE + "Audio";


	/**
	 * URI of the ComposedQuery object
	 */
	public static final String COMPOSED_QUERY = WebLabModel.NAMESPACE + "ComposedQuery";


	/**
	 * URI of the ComposedResource object
	 */
	public static final String COMPOSED_RESOURCE = WebLabModel.NAMESPACE + "ComposedResource";


	/**
	 * URI of the Coordinate object
	 */
	public static final String COORDINATE = WebLabModel.NAMESPACE + "Coordinate";


	/**
	 * URI of the Document object
	 */
	public static final String DOCUMENT = WebLabModel.NAMESPACE + "Document";


	/**
	 * URI of the ElementaryQuery object
	 */
	public static final String ELEMENTARY_QUERY = WebLabModel.NAMESPACE + "ElementaryQuery";


	/**
	 * URI of the endAt property. The end value of a LinearSegment (char in UTF-8) or a TemporalSegment (milliseconds).
	 */
	public static final String END_AT = WebLabModel.NAMESPACE + "endAt";


	/**
	 * URI of the hasAbscissa property. Abscissa value in pixels, with 0 at the left border. Other measure references are possible.
	 */
	public static final String HAS_ABSCISSA = WebLabModel.NAMESPACE + "hasAbscissa";


	/**
	 * URI of the hasAnnotation property. Links a Resource to one of its Annotation.
	 */
	public static final String HAS_ANNOTATION = WebLabModel.NAMESPACE + "hasAnnotation";


	/**
	 * URI of the hasContent property. The optional content as a bytes array.
	 */
	public static final String HAS_CONTENT = WebLabModel.NAMESPACE + "hasContent";


	/**
	 * URI of the hasCoordinate property. Links a SpatialSegment or a SpatioTemporalSegment to one of its Coordinate.
	 */
	public static final String HAS_COORDINATE = WebLabModel.NAMESPACE + "hasCoordinate";


	/**
	 * URI of the hasData property. Should be an ARFF content for a LowLevelDescriptor and a RDF/XML content for a PoK.
	 */
	public static final String HAS_DATA = WebLabModel.NAMESPACE + "hasData";


	/**
	 * URI of the hasLowLevelDescriptor property. Links a Resource to one of its LowLevelDescriptor.
	 */
	public static final String HAS_LOW_LEVEL_DESCRIPTOR = WebLabModel.NAMESPACE + "hasLowLevelDescriptor";


	/**
	 * URI of the hasOperator property. This is a simple restriction of String to reduce the values to the three boolean operators AND/OR/NOT. Note that in a
	 * n-ary context, the unary operator NOT is considered as a AND NOT.
	 */
	public static final String HAS_OPERATOR = WebLabModel.NAMESPACE + "hasOperator";


	/**
	 * URI of the hasOrdonate property. Ordinate value in pixels, with 0 at the bottom border. Other measure references are possible.
	 */
	public static final String HAS_ORDINATE = WebLabModel.NAMESPACE + "hasOrdinate";


	/**
	 * URI of the hasPok property. Links a ResultSet to its PieceOfKnowledge.
	 */
	public static final String HAS_POK = WebLabModel.NAMESPACE + "hasPok";


	/**
	 * URI of the hasRequest property. The string query in a free text syntax.
	 */
	public static final String HAS_REQUEST = WebLabModel.NAMESPACE + "hasRequest";


	/**
	 * URI of the hasSegment property. Links a MediaUnit to one of its Segment.
	 */
	public static final String HAS_SEGMENT = WebLabModel.NAMESPACE + "hasSegment";


	/**
	 * URI of the hasSubElement property. Links a Resource or a Segment to one of its content.
	 */
	public static final String HAS_SUB_ELEMENT = WebLabModel.NAMESPACE + "hasSubElement";


	/**
	 * URI of the hasSubElement property. Links a Resource or a Segment to its container.
	 */
	public static final String HAS_SUPER_ELEMENT = WebLabModel.NAMESPACE + "hasSuperElement";


	/**
	 * URI of the hasTextContent property. The String content of the text. Might be empty, in that case an annotation referring to the whole large content
	 * should be present.
	 */
	public static final String HAS_TEXT_CONTENT = WebLabModel.NAMESPACE + "hasTextContent";


	/**
	 * URI of the hasTimeStamp property. The timestamp of a SpatioTemporalSegment
	 */
	public static final String HAS_TIMESTAMP = WebLabModel.NAMESPACE + "hasTimeStamp";


	/**
	 * URI of the hasWeight property. A weight used for Query combination.
	 */
	public static final String HAS_WEIGHT = WebLabModel.NAMESPACE + "hasWeight";


	/**
	 * URI of the Image object
	 */
	public static final String IMAGE = WebLabModel.NAMESPACE + "Image";


	/**
	 * URI of the inComposedQuery property. Links a Query to its parent ComposedQuery.
	 */
	public static final String IN_COMPOSED_QUERY = WebLabModel.NAMESPACE + "inComposedQuery";


	/**
	 * URI of the inComposedResource property. Links a resource to its parent ComposedResource or SimilarityQuery
	 */
	public static final String IN_COMPOSED_RESOURCE = WebLabModel.NAMESPACE + "inComposedResource";


	/**
	 * URI of the inDocument property. Links a MediaUnit to its parent Document.
	 */
	public static final String IN_DOCUMENT = WebLabModel.NAMESPACE + "inDocument";


	/**
	 * URI of the inResultSet property. Links a resource to its parent ResultSet.
	 */
	public static final String IN_RESULT_SET = WebLabModel.NAMESPACE + "inResultSet";


	/**
	 * URI of the isComposedByMediaUnit property. Links a Document to one of its sub-MediaUnit.
	 */
	public static final String IS_COMPOSED_BY_MEDIA_UNIT = WebLabModel.NAMESPACE + "isComposedByMediaUnit";


	/**
	 * URI of the isComposedByQuery property. Links a ComposedQuery to one of its Query.
	 */
	public static final String IS_COMPOSED_BY_QUERY = WebLabModel.NAMESPACE + "isComposedByQuery";


	/**
	 * URI of the isComposedByResource property. Links a ComposedResource or a SimilarityQuery to one of its Resource.
	 */
	public static final String IS_COMPOSED_BY_RESOURCE = WebLabModel.NAMESPACE + "isComposedByResource";


	/**
	 * URI of the isComposedByResourceResultSet property. Links a ResultSet to one of its Resource.
	 */
	public static final String IS_COMPOSED_BY_RESOURCE_RESULT_SET = WebLabModel.NAMESPACE + "isComposedByResourceResultSet";


	/**
	 * URI of the isComposedBySegment property. Links a TrackSegment to one of its Segment.
	 */
	public static final String IS_COMPOSED_BY_SEGMENT = WebLabModel.NAMESPACE + "isComposedBySegment";


	/**
	 * URI of the LinearSegment object
	 */
	public static final String LINEAR_SEGMENT = WebLabModel.NAMESPACE + "LinearSegment";


	/**
	 * URI of the LowLevelDescriptor object
	 */
	public static final String LOW_LEVEL_DESCRIPTOR = WebLabModel.NAMESPACE + "LowLevelDescriptor";


	/**
	 * URI of the MediaUnit object
	 */
	public static final String MEDIA_UNIT = WebLabModel.NAMESPACE + "MediaUnit";


	/**
	 * URI of the PieceOfKnowledge object
	 */
	public static final String PIECE_OF_KNOWLEDGE = WebLabModel.NAMESPACE + "PieceOfKnowledge";


	/**
	 * URI of the Query object
	 */
	public static final String QUERY = WebLabModel.NAMESPACE + "Query";


	/**
	 * URI of the Resource object
	 */
	public static final String RESOURCE = WebLabModel.NAMESPACE + "Resource";


	/**
	 * URI of the ResultSet object
	 */
	public static final String RESULT_SET = WebLabModel.NAMESPACE + "ResultSet";


	/**
	 * URI of the Segment object
	 */
	public static final String SEGMENT = WebLabModel.NAMESPACE + "Segment";


	/**
	 * URI of the SimilarityQuery object
	 */
	public static final String SIMILARITY_QUERY = WebLabModel.NAMESPACE + "SimilarityQuery";


	/**
	 * URI of the SpatialSegment object
	 */
	public static final String SPATIAL_SEGMENT = WebLabModel.NAMESPACE + "SpatialSegment";


	/**
	 * URI of the SpatioTemporalSegment object
	 */
	public static final String SPATIO_TEMPORAL_SEGMENT = WebLabModel.NAMESPACE + "SpatioTemporalSegment";


	/**
	 * URI of the startAt property. The start value of a LinearSegment (char in UTF-8) or a TemporalSegment (milliseconds).
	 */
	public static final String START_AT = WebLabModel.NAMESPACE + "startAt";


	/**
	 * URI of the StringQuery object
	 */
	public static final String STRING_QUERY = WebLabModel.NAMESPACE + "StringQuery";


	/**
	 * URI of the TemporalSegment object
	 */
	public static final String TEMPORAL_SEGMENT = WebLabModel.NAMESPACE + "TemporalSegment";


	/**
	 * URI of the Text object
	 */
	public static final String TEXT = WebLabModel.NAMESPACE + "Text";


	/**
	 * URI of the TrackSegment object
	 */
	public static final String TRACK_SEGMENT = WebLabModel.NAMESPACE + "TrackSegment";


	/**
	 * URI of the Video object
	 */
	public static final String VIDEO = WebLabModel.NAMESPACE + "Video";


	/**
	 * Constructors Do not use it.
	 */
	private WebLabModel() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Inverse method of {@link #getUriFromResource(Class)} and {@link #getUriFromSegment(Class)}
	 * 
	 * @see #getUriFromResource(Class)
	 * @see #getUriFromSegment(Class)
	 * @param uri
	 *            The URI denoting a WebLab class in the ontology
	 * @return The java class
	 * @throws ClassNotFoundException
	 *             If the URI does not represent a class that exists.
	 * @throws ClassCastException
	 *             If the URI does not represent a Resource or a Segment class defined in the model.
	 */
	public static Class<?> getClassFromUri(final String uri) throws ClassNotFoundException {
		if ((uri == null) || (uri.length() <= WebLabModel.NAMESPACE.length()) || !uri.startsWith(WebLabModel.NAMESPACE)) {
			throw new WebLabUncheckedException("Uri: " + uri + " was not a WebLab class. ");
		}
		final String className = '.' + uri.substring(WebLabModel.NAMESPACE.length());

		final Class<?> clazz = ClassLoader.getSystemClassLoader().loadClass(Resource.class.getPackage().getName() + className);
		if (Resource.class.isAssignableFrom(clazz)) {
			return clazz.asSubclass(Resource.class);
		} else if (Segment.class.isAssignableFrom(clazz)) {
			return clazz.asSubclass(Segment.class);
		}
		throw new ClassCastException("The URI '" + uri + "' denotes the class " + clazz.getCanonicalName() + " which is neither a resource nor a segment.");
	}


	/**
	 * Use this to get the WebLab ontology owl:Class URI corresponding to a Resource class.
	 * 
	 * @see #getClassFromUri(String)
	 * @see #getUriFromSegment(Class)
	 * @param resourceClass
	 *            the java Class
	 * @return the ontology URI class
	 */
	public static String getUriFromResource(final Class<? extends Resource> resourceClass) {
		return WebLabModel.NAMESPACE + resourceClass.getSimpleName();
	}


	/**
	 * Use this to get the WebLab ontology owl:Class URI corresponding to a Segment class.
	 * 
	 * @see #getClassFromUri(String)
	 * @see #getUriFromResource(Class)
	 * @param segmentClass
	 *            the java Class
	 * @return the ontology URI class
	 */
	public static String getUriFromSegment(final Class<? extends Segment> segmentClass) {
		return WebLabModel.NAMESPACE + segmentClass.getSimpleName();
	}

}
