/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.factory;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.uri.WebLabRI;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.LowLevelDescriptor;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.model.TrackSegment;

/**
 *
 * This class provides methods to generate URI that do not already exist in a Resource.
 *
 * Resource creation factories mostly use only two methods:
 * <ul>
 * <li>createUniqueURIFrom(resource, isChild, isAnnotation) {</li>
 * <li>createSegmentURIFor(segmentContainer)</li>
 * </ul>
 *
 * @author Cassidian WebLab Team
 * @since 1.2
 */
abstract class AbstractFactory {


	/**
	 * This data structure is not well adapted to manage WebLab Resource
	 */
	protected static volatile Map<String, Set<String>> INNER_MAP = new ConcurrentHashMap<>();


	/**
	 * The modulo used in the uris
	 */
	protected static final int MODULO = 1000;


	/**
	 * Constructors Do not use it.
	 */
	protected AbstractFactory() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Adds an annotation on the resource.
	 * 
	 * @param annotation
	 *            the annotation to add
	 * @param resource
	 *            the resource in which we want to add the annotation
	 * @param root
	 *            the root resource containing the previous resource, or the resource itself
	 */
	protected static synchronized void addAnnotation(final Annotation annotation, final Resource resource, final Resource root) {
		resource.getAnnotation().add(annotation);
		AbstractFactory.listUri(root).add(annotation.getUri());
	}


	/**
	 * Add a sub-MediaUnit to a document
	 * 
	 * @param mediaUnit
	 *            the mediaunit to add to the document
	 * @param parent
	 *            a document.
	 */
	protected static synchronized void addChild(final MediaUnit mediaUnit, final Document parent) {
		final Set<String> uris = AbstractFactory.listUri(parent);
		parent.getMediaUnit().add(mediaUnit);
		uris.add(mediaUnit.getUri());

	}


	/**
	 * Adds a LowLevelDescriptor to a resource.
	 * 
	 * @param descriptor
	 *            LowLevelDescriptor to add
	 * @param parent
	 *            the resource in which we want to add the LowLevelDescriptor
	 * @param root
	 *            the root resource containing the previous resource, or the resource itself
	 */
	protected static synchronized void addDescriptor(final LowLevelDescriptor descriptor, final Resource parent, final Resource root) {
		parent.getDescriptor().add(descriptor);
		AbstractFactory.listUri(root).add(descriptor.getUri());
	}


	/**
	 * Adds a PieceOfKnowledge to a ResultSet
	 * 
	 * @param pok
	 *            a PieceOfKnowledge
	 * @param parentRoot
	 *            a ResultSet
	 */
	protected static synchronized void addPokInResultSet(final PieceOfKnowledge pok, final ResultSet parentRoot) {
		parentRoot.setPok(pok);
		AbstractFactory.listUri(parentRoot).add(pok.getUri());
	}


	/**
	 * Adds a Segment to a MediaUnit
	 * 
	 * @param segment
	 *            the Segment to add to the mediaunit
	 * @param parent
	 *            a mediaunit
	 * @param root
	 *            the root resource containing the previous mediaunit
	 */
	protected static synchronized void addSegment(final Segment segment, final MediaUnit parent, final Resource root) {
		parent.getSegment().add(segment);
		AbstractFactory.listUri(root).add(segment.getUri());
	}


	/**
	 * Cleans the internal mapping INNER_MAP for every occurrence of the URI
	 *
	 * @param uri
	 *            The URI to remove from the INNER_MAP
	 */
	protected static synchronized void cleanMapping(final String uri) {
		final Iterator<Entry<String, Set<String>>> mapIterator = AbstractFactory.INNER_MAP.entrySet().iterator();
		while (mapIterator.hasNext()) {
			final Entry<String, Set<java.lang.String>> entry = mapIterator.next();
			if (entry.getValue().contains(uri)) {
				mapIterator.remove();
			}
		}
	}


	/**
	 * Create an URI for a segment
	 * 
	 * @param segmentContainer
	 *            the Segment object
	 * @return an URI coherent w.r.t. knowledge of this factory
	 */
	protected static synchronized String createSegmentURIFor(final Object segmentContainer) {
		if (segmentContainer instanceof Segment) {
			final Segment segment = (Segment) segmentContainer;
			return AbstractFactory.createUniqueURIin(AbstractFactory.listUri(segment, true), true, false, segment.getUri());
		} else if (segmentContainer instanceof Resource) {
			final Resource resource = (Resource) segmentContainer;
			return AbstractFactory.createUniqueURIin(AbstractFactory.listUri(resource, true), true, false, resource.getUri());
		} else {
			throw new WebLabUncheckedException("Object of type " + segmentContainer.getClass() + " can not contain Segment.");
		}

	}


	/**
	 * Create a URI checking it does not already exist in the resource.
	 *
	 * @param resource
	 *            a top container resource
	 * @param isChild
	 *            if true, the uri will describe a child or subchild of this resource
	 * @param isAnnotation
	 *            if true, the uri will describe a annotation on this resource
	 * @return an URI unique among all URI in the resource and its sub resources
	 */
	protected static synchronized String createUniqueURIFrom(final Resource resource, final boolean isChild, final boolean isAnnotation) {
		return AbstractFactory.createUniqueURIin(AbstractFactory.listUri(resource, true), isChild, isAnnotation, resource.getUri());
	}


	/**
	 * Create an URI
	 * 
	 * @param uris
	 *            list of existing URIs
	 * @param isChild
	 *            if true, this URI will refer to a child of a resource
	 * @param isAnnotation
	 *            if true, this URI will refer to an annotation
	 * @param parentUri
	 *            the parent URI of this URI
	 * @return an URI coherent w.r.t. knowledge of this factory
	 */
	protected static synchronized String createUniqueURIin(final Set<String> uris, final boolean isChild, final boolean isAnnotation, final String parentUri) {
		final String ref = "webLabFactory";
		final String res = "auto-parent";
		String template = "weblab://" + ref + '/' + res;
		if (isAnnotation) {
			if (parentUri.indexOf('#') == -1) {
				template = parentUri + "#a";
			} else {
				template = parentUri + "-a";
			}
		}
		if (isChild) {
			if (parentUri.indexOf('#') == -1) {
				template = parentUri + '#';
			} else {
				template = parentUri + '-';
			}
		}

		int index = uris.size() - 1;

		String uri = null;
		while ((uri == null) || uris.contains(uri)) {
			if ((index + 1) % AbstractFactory.MODULO == 0) {
				template += index + '_';
				index = 0;
			}
			uri = template + (index++);
		}

		uris.add(uri);

		return new WebLabRI(uri).toString();
	}


	/**
	 * Searches for the parent of a MediaUnit inside a Resource.
	 * 
	 * @param mediaUnit
	 *            the MediaUnit to search for its parent.
	 * @param resource
	 *            the "root" resource containing all resources in which we perform the search
	 * @return the parent resource of the MediaUnit or null if this MediaUnitwas not in the root resource.
	 */
	protected static synchronized Resource findParent(final MediaUnit mediaUnit, final Resource resource) {
		if (resource instanceof ComposedResource) {
			final ComposedResource composedResource = (ComposedResource) resource;
			if (composedResource.getResource().contains(mediaUnit)) {
				return resource;
			}
			for (final Resource inner : composedResource.getResource()) {
				final Resource res = AbstractFactory.findParent(mediaUnit, inner);
				if (res != null) {
					return res;
				}
			}
		} else if (resource instanceof Document) {
			final Document doc = (Document) resource;
			if (doc.isSetMediaUnit() && doc.getMediaUnit().contains(mediaUnit)) {
				return resource;
			}
		}
		return null;
	}


	/**
	 * @param res
	 *            The <code>Resource</code> to be used to create <code>WebLabRI</code>
	 * @param isChild
	 *            Whether <code>res</code> is a root or a sub-resource
	 * @param isAnnotation
	 *            Whether <code>res</code> is an <code>Annotation</code> or <code>Resource</code>
	 * @return The <code>String</code> to be used as <code>URI</code> by the <code>MediaUnit</code> created
	 */
	protected static final synchronized String getUniqueWebLabRIFrom(final Resource res, final boolean isChild, final boolean isAnnotation) {
		return AbstractFactory.createUniqueURIFrom(res, isChild, isAnnotation);
	}


	/**
	 * Returns the set of URI linked to this URI.
	 * Two URIs are linked if they they share the same root resource.
	 * 
	 * @param key
	 *            an URI
	 * @return the set of URIs linked to this URI.
	 */
	protected static synchronized Set<String> getUris(final String key) {
		if (!AbstractFactory.INNER_MAP.containsKey(key)) {
			for (final Set<String> test : AbstractFactory.INNER_MAP.values()) {
				if (test.contains(key)) {
					return test;
				}
			}
			return new HashSet<>();
		}
		return AbstractFactory.INNER_MAP.get(key);
	}


	/**
	 * List uris in the resource
	 *
	 * @param resource
	 *            a resource
	 * @return a set of uri in this resource
	 */
	protected static synchronized Set<String> listUri(final Resource resource) {
		return AbstractFactory.listUri(resource, false);
	}


	/**
	 * Lists all URI in a resource and its subresources.
	 *
	 * @warning this function does not support cyclic resources (a resource that contains itself) if force is true
	 *
	 * @param resource
	 *            a resource
	 * @param force
	 *            if false URI in this resource will not be listed if the resource have already been processed, else they will not matter what.
	 * @return a list of all URI in this resource the last time this resource was processed
	 */
	protected static synchronized Set<String> listUri(final Resource resource, final boolean force) {
		if (resource.getUri() != null && !force) {
			final Set<String> bufferedUris = AbstractFactory.listUri(resource.getUri());
			if (!bufferedUris.isEmpty()) {
				return bufferedUris;
			}
		}

		final Set<String> set = Collections.synchronizedSet(new HashSet<String>());
		if (resource.getUri() != null) {
			set.add(resource.getUri());
		}
		if (resource instanceof MediaUnit) {
			final MediaUnit mediaUnit = (MediaUnit) resource;
			if (mediaUnit.isSetSegment()) {
				for (final Segment s : mediaUnit.getSegment()) {
					set.addAll(AbstractFactory.listUri(s, force));
				}
			}
			if (mediaUnit instanceof Document) {
				final Document doc = (Document) mediaUnit;
				if (doc.isSetMediaUnit()) {
					for (final MediaUnit inner : doc.getMediaUnit()) {
						set.addAll(AbstractFactory.listUri(inner, force));
					}
				}
			}
		} else if (resource instanceof ComposedResource) {
			final ComposedResource composedRes = (ComposedResource) resource;
			for (final Resource inner : composedRes.getResource()) {
				set.addAll(AbstractFactory.listUri(inner, force));
			}
		} else if (resource instanceof ResultSet) {
			final ResultSet resultSet = (ResultSet) resource;
			if (resultSet.isSetPok()) {
				set.addAll(AbstractFactory.listUri(resultSet.getPok(), force));
			}

		} else if (resource instanceof ComposedQuery) {
			final ComposedQuery composedQuery = (ComposedQuery) resource;
			for (final Query query : composedQuery.getQuery()) {
				set.addAll(AbstractFactory.listUri(query, force));
			}
		} else if (resource instanceof SimilarityQuery) {
			final SimilarityQuery simQuery = (SimilarityQuery) resource;
			for (final Resource subresource : simQuery.getResource()) {
				set.addAll(AbstractFactory.listUri(subresource, force));
			}
		}

		if (resource.isSetAnnotation()) {
			for (final Annotation a : resource.getAnnotation()) {
				set.addAll(AbstractFactory.listUri(a, force));
			}
		}
		if (resource.isSetDescriptor()) {
			for (final LowLevelDescriptor lld : resource.getDescriptor()) {
				set.addAll(AbstractFactory.listUri(lld, force));
			}
		}
		if (resource.getUri() != null) {
			AbstractFactory.setUris(resource.getUri(), set);
		}
		return set;
	}


	/**
	 * Lists all URI in a segment and its subsegments.
	 *
	 * @param segment
	 *            a segment
	 * @return a list of all URI in this segment the last time this segment was processed
	 */
	protected static synchronized Set<String> listUri(final Segment segment) {
		return listUri(segment, false);
	}



	/**
	 * Lists all URI in a segment and its subsegments.
	 *
	 * @param segment
	 *            a segment
	 * @param force
	 *            whether or not to dig into the segment
	 * @return a list of all URI in this segment the last time this segment was processed
	 */
	protected static synchronized Set<String> listUri(final Segment segment, final boolean force) {
		if (segment.getUri() != null && !force) {
			final Set<String> bufferedUris = AbstractFactory.listUri(segment.getUri());
			if (!bufferedUris.isEmpty()) {
				return bufferedUris;
			}
		}

		final Set<String> set = Collections.synchronizedSet(new HashSet<String>());
		if (segment.getUri() != null) {
			set.add(segment.getUri());
		}

		if (segment instanceof TrackSegment) {
			final TrackSegment trackSeg = (TrackSegment) segment;
			for (final Segment inner : trackSeg.getSegment()) {
				set.addAll(AbstractFactory.listUri(inner, true));
			}
		}

		if (segment.getUri() != null) {
			AbstractFactory.setUris(segment.getUri(), set);
		}

		return set;
	}


	/**
	 * Returns linked URI set with this URI
	 *
	 * @param uri
	 *            an URI
	 * @return a set of linked URI.
	 *
	 * @see #getUris(String) for more information
	 */
	protected static Set<String> listUri(final String uri) {
		return AbstractFactory.getUris(uri);
	}


	/**
	 * Removes a MediaUnit from a Resource
	 * 
	 * @param mediaUnit
	 *            the MediaUnit to remove
	 * @param parent
	 *            the parent of this MediaUnit
	 * @param root
	 *            the root resource containing the MediaUnit and its parent or the parent itself.
	 */
	protected static synchronized void removeChild(final MediaUnit mediaUnit, final Resource parent, final Resource root) {
		final Set<String> uris = AbstractFactory.listUri(root);
		if (parent instanceof ComposedResource) {
			final ComposedResource composedResource = (ComposedResource) parent;
			composedResource.getResource().remove(mediaUnit);
			uris.remove(mediaUnit.getUri());
		}
	}


	/**
	 * Sets the linked set of URIs of a given URI.
	 * 
	 * @param key
	 *            an URI.
	 * @param uris
	 *            the linked set of URIs.
	 */
	protected static synchronized void setUris(final String key, final Set<String> uris) {
		while (AbstractFactory.INNER_MAP.size() > AbstractFactory.MODULO) {
			final String fkey = AbstractFactory.INNER_MAP.keySet().iterator().next();
			AbstractFactory.INNER_MAP.remove(fkey);
		}
		AbstractFactory.INNER_MAP.put(key, uris);
	}
}
