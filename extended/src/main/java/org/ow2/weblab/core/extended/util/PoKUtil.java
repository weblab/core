/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.util;

import javax.xml.transform.TransformerException;

import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.ontologies.RDF;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Uses to manipulate <code>PieceOfKnowledge</code> data field with a <code>String</code>.
 *
 * @author Cassidian WebLab Team
 */
public final class PoKUtil {


	/**
	 * Constructors Do not use it.
	 */
	private PoKUtil() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Returns a <code>String</code> containing the RDF statements of the <code>PieceOfKnowledge</code>.
	 *
	 * @param pok
	 *            <code>PieceOfKnowledge</code> you want to get the <code>String</code> content
	 * @return The <code>String</code> content of the <code>PieceOfKnowledge</code>
	 */
	public static String getPoKData(final PieceOfKnowledge pok) {
		/*
		 * if the pok is empty, return an empty String.
		 */
		if (pok.getData() == null) {
			return "";
		}
		// If the pok is the the default Object instance, then return the empty String
		if (pok.getData().getClass().equals(Object.class)) {
			return "";
		}

		final Element elem = ((Element) pok.getData());
		if (elem.getFirstChild() == null) {
			return "";
		}

		boolean rdfRDFFound = false;
		final StringBuilder strBuilder = new StringBuilder();
		final NodeList nodes = elem.getChildNodes();
		for (int k = 0; k < nodes.getLength(); k++) {
			final Node domElement = nodes.item(k);
			if ((domElement.getNodeType() == Node.TEXT_NODE) && (domElement.getNodeValue().trim().length() == 0)) {
				// Just a bad indentation. No matter. Skipping it.
				continue;
			}
			if (domElement.getNodeType() == Node.TEXT_NODE) {
				// Someone added texts directly in the Node ?
				throw new WebLabUncheckedException("PieceOfKnowledge contains a text node. Is it really valid RDF/XML?");
			}
			if (rdfRDFFound) {
				throw new WebLabUncheckedException("PieceOfKnowledge contains a least a node in addition to a rdf:RDF one. It is not allowed in RDF/XML.");
			}

			final String fullName = domElement.getNamespaceURI() + domElement.getLocalName();
			if (fullName.compareToIgnoreCase(RDF.NAMESPACE + "rdf") == 0) {
				try {
					strBuilder.append(DOMUtil.extractXMLAsString(domElement));
				} catch (final TransformerException te) {
					throw new WebLabUncheckedException("Unable to transform data field into a String. PieceOfKnowledge is corrupted!", te);
				}
				rdfRDFFound = true;
			} else if (fullName.compareToIgnoreCase(RDF.NAMESPACE + "description") == 0) {
				try {
					strBuilder.append(DOMUtil.extractXMLAsString(domElement));
				} catch (final TransformerException te) {
					throw new WebLabUncheckedException("Unable to transform data field into a String. PieceOfKnowledge is corrupted!", te);
				}
			} else {
				throw new WebLabUncheckedException("PieceOfKnowledge contains a least a non rdf node. It is not allowed in RDF/XML.");
			}
		}
		return strBuilder.toString();
	}


	/**
	 * Same as <code>setPoKData(pok, pokStr, true);</code>
	 *
	 * @param pok
	 *            the <code>PieceOfKnowledge</code> you want to modify
	 * @param pokStr
	 *            contains the RDF/XML <code>String</code>
	 * @see #setPoKData(PieceOfKnowledge, String, boolean)
	 */
	public static void setPoKData(final PieceOfKnowledge pok, final String pokStr) {
		PoKUtil.setPoKData(pok, pokStr, true);
	}


	/**
	 * Set the <code>PieceOfKnowledge</code> data using a <code>String</code>, the parameter have to be a valid XML <code>String</code>. If the parameter don't
	 * start with '&lt;data' it add it automatically.
	 *
	 * @param pok
	 *            the <code>PieceOfKnowledge</code> you want to modify
	 * @param pokStr
	 *            contains the RDF/XML <code>String</code>
	 * @param strict
	 *            whether or not to be string and block some bad RDF/XML data
	 */
	public static void setPoKData(final PieceOfKnowledge pok, final String pokStr, final boolean strict) {
		pok.setData(DOMUtil.stringToDataDOMElement(pokStr));
		if (strict) {
			PoKUtil.getPoKData(pok);
		}
	}

}
