/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.properties;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.slf4j.LoggerFactory;


/**
 * Useful class to be used for retrieving configuration files, and their properties.
 *
 * @author Cassidian WebLab Team
 */
public final class PropertiesLoader {


	/**
	 * Constructors Do not use it.
	 */
	private PropertiesLoader() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Same as <code>loadProperties(filename, PropertiesLoader.class);</code>
	 *
	 * @param filename
	 *            The name of the properties file to load.
	 * @return Properties values mapped by keys
	 */
	public static Map<String, String> loadProperties(final String filename) {
		return PropertiesLoader.loadProperties(filename, PropertiesLoader.class);
	}


	/**
	 * Loads properties into a map from a <tt>properties</tt> file defined by filename every properties found.
	 *
	 * @param filename
	 *            The name of the properties file to load.
	 * @param clazz
	 *            The class to be used to get the class loader.
	 * @return The map of properties and their values.
	 */
	public static Map<String, String> loadProperties(final String filename, final Class<?> clazz) {
		final Properties properties = new Properties();

		final Enumeration<URL> urls;
		try {
			urls = clazz.getClassLoader().getResources(filename);
		} catch (final IOException ioe) {
			throw new WebLabUncheckedException("Unable to load properties file: '" + filename + "'.", ioe);
		}

		final URL file;
		if (urls.hasMoreElements()) {
			file = urls.nextElement();
		} else {
			throw new WebLabUncheckedException("Properties file '" + filename + "' not found.");
		}

		if (urls.hasMoreElements()) {
			final StringBuilder warn = new StringBuilder();
			warn.append('-' + file.toString());
			while (urls.hasMoreElements()) {
				warn.append(System.getProperty("line.separator"));
				warn.append('-');
				warn.append(urls.nextElement().toString());
			}
			LoggerFactory.getLogger(PropertiesLoader.class).warn("Too many properties files found:" + System.getProperty("line.separator") + warn.toString());
			LoggerFactory.getLogger(PropertiesLoader.class).warn("Selected: " + file);
		}

		try (final InputStream stream = file.openStream()) {
			properties.load(stream);
		} catch (final IOException ioe) {
			throw new WebLabUncheckedException("Unable to load " + filename + " file.", ioe);
		}

		final Map<String, String> propertiesMap = new HashMap<>(properties.size());
		for (final Entry<Object, Object> entry : properties.entrySet()) {
			propertiesMap.put((String) entry.getKey(), (String) entry.getValue());
		}
		return propertiesMap;
	}

}
