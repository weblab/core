/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.util;

import java.util.List;

import org.ow2.weblab.core.extended.exception.WebLabNotYetImplementedException;
import org.ow2.weblab.core.model.Coordinate;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.SpatioTemporalSegment;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.TrackSegment;


/**
 * A Util class for handling segments. It provide a method to test inclusion of Segments.
 * 
 * *
 * <p>
 * <b>Warning:</b>The comparison of <code>SpatialSegment</code> and <code>SpatioTemporalSegment</code> are not yet implemented.
 * 
 * @author Cassidian WebLab Team
 */
public final class SegmentUtil {


	/**
	 * Constructors
	 * Do not use it.
	 */
	private SegmentUtil() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * *
	 * <p>
	 * <b>Warning:</b>If one of the segments in parameter is from an unknown type
	 * (i.e. neither Linear, nor Spatial, nor Temporal, a <code>RuntimeException</code> may be thrown!!!
	 * <p>
	 * <b>Warning:</b> Today if segments are spatial, a runtime exception will be thrown.
	 * 
	 * @param theSegment
	 *            <code>Segment</code> to test "be included" by <code>inThisSegment</code>.
	 * @param inThisSegment
	 *            <code>Segment</code> to test "includes" <code>theSegment</code>.
	 * @return <code>false</code> if one <code>Segment</code> is <code>null</code>, or if the Segments aren't of the same <code>Segment</code> type, or if
	 *         according to their type <code>theSegment</code> isn't included in <code>inThisSegment</code>; <code>true</code> if they are included.
	 */
	public static boolean isIncluded(final Segment theSegment, final Segment inThisSegment) {
		if (theSegment == inThisSegment) {
			return true;
		}
		if ((theSegment == null) || (inThisSegment == null)) {
			return false;
		}
		if (!theSegment.getClass().equals(inThisSegment.getClass())) {
			// Two segment of different class cannot be included
			return false;
		}

		if (theSegment instanceof LinearSegment) {
			return SegmentUtil.isIncludedLinear((LinearSegment) theSegment, (LinearSegment) inThisSegment);
		}
		if (theSegment instanceof TemporalSegment) {
			return SegmentUtil.isIncludedTemporal((TemporalSegment) theSegment, (TemporalSegment) inThisSegment);
		}
		if (theSegment instanceof SpatioTemporalSegment) {
			return SegmentUtil.isIncludedSpatioTemporalSegment((SpatioTemporalSegment) theSegment, (SpatioTemporalSegment) inThisSegment);
		}
		if (theSegment instanceof SpatialSegment) {
			return SegmentUtil.isIncludedSpatial((SpatialSegment) theSegment, (SpatialSegment) inThisSegment);
		}
		if (theSegment instanceof TrackSegment) {
			// Inclusion of TrackingSegment has no sens.
			return false;
		}
		throw new WebLabNotYetImplementedException("Inclusion test for type " + theSegment.getClass().getName() + " not yet implemented !!!");
	}


	/**
	 * @param coordinate
	 *            The list of coordinate of the Segment to test "be include" by <code>inCoordinates</code>
	 * @param inCoordinates
	 *            The list of coordinate of the Segment to test "includes" by <code>coordinate</code>
	 * @return nothing always throw RuntimeException
	 * @warning always throw RuntimeException
	 * @throws WebLabNotYetImplementedException
	 *             In any case!
	 */
	private static boolean isIncludedCoordinate(final List<Coordinate> coordinate, final List<Coordinate> inCoordinates) {
		// TODO Shape inclusion should be implemented one day.
		throw new WebLabNotYetImplementedException("Inclusion test using coordinates is not yet implemented!");
	}


	/**
	 * @param theSegment
	 *            Segment to test "be include" by <code>inThisSegment</code>
	 * @param inThisSegment
	 *            Segment to test "includes" <code>theSegment</code>
	 * @return <code>false</code> if one <code>Segment</code> is null, or if
	 *         the Segments aren't of the same <code>Segment</code> type, or
	 *         if according to their type <code>theSegment</code> isn't
	 *         included in <code>inThisSegment</code>. <code>true</code> if
	 *         they are included.
	 */
	private static boolean isIncludedLinear(final LinearSegment theSegment, final LinearSegment inThisSegment) {
		return (theSegment.getStart() >= inThisSegment.getStart()) && (theSegment.getEnd() <= inThisSegment.getEnd());
	}


	/**
	 * @param theSegment
	 *            Segment to test "be include" by <code>inThisSegment</code>
	 * @param inThisSegment
	 *            Segment to test "includes" <code>theSegment</code>
	 * @warning {@link #isIncludedCoordinate(List, List)}
	 * @see #isIncludedCoordinate(List, List)
	 * @return <code>false</code> <code>theSegment</code> isn't included in <code>inThisSegment</code>. <code>true</code> if they are included.
	 */
	private static boolean isIncludedSpatial(final SpatialSegment theSegment, final SpatialSegment inThisSegment) {
		return SegmentUtil.isIncludedCoordinate(theSegment.getCoordinate(), inThisSegment.getCoordinate());
	}


	/**
	 * To be included SpatioTemporal segments should be at the same timestamp in addition to ashape comparison.
	 * 
	 * @param theSegment
	 *            Segment to test "be include" by <code>inThisSegment</code>
	 * @param inThisSegment
	 *            Segment to test "includes" <code>theSegment</code>
	 * @return <code>false</code> <code>theSegment</code> isn't included in <code>inThisSegment</code>. <code>true</code> if they are included.
	 */
	private static boolean isIncludedSpatioTemporalSegment(final SpatioTemporalSegment theSegment, final SpatioTemporalSegment inThisSegment) {
		if (theSegment.getTimestamp() == inThisSegment.getTimestamp()) {
			return SegmentUtil.isIncludedCoordinate(theSegment.getCoordinate(), inThisSegment.getCoordinate());
		}
		return false;
	}


	/**
	 * @param theSegment
	 *            Segment to test "be include" by <code>inThisSegment</code>
	 * @param inThisSegment
	 *            Segment to test "includes" <code>theSegment</code>
	 * @return <code>false</code> <code>theSegment</code> isn't included in <code>inThisSegment</code>. <code>true</code> if they are included.
	 */
	private static boolean isIncludedTemporal(final TemporalSegment theSegment, final TemporalSegment inThisSegment) {
		return (theSegment.getStart() >= inThisSegment.getStart()) && (theSegment.getEnd() <= inThisSegment.getEnd());
	}

}
