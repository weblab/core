/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.messages;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.LoggerFactory;


/**
 * This class enable to load messages from the resource bundle in param of each method.
 * It provides two method, one retrieving the message itself and another one using {@link MessageFormat} mechanism for substitution of parameters.
 * 
 * @see MessageFormat
 * @author WebLab Cassidian Team
 */
public final class Messages {


	/**
	 * The exclamation mark used for not found keys.
	 */
	private static final char EXCLAMATION_MARK = '!';


	/**
	 * The error message to be logged in the internationalisation is not working.
	 */
	private static final String KEY_NOT_FOUND = "The key ''{0}'' has not been found in Bundle ''{1}''.";


	/**
	 * The exception message when the constructor is used.
	 */
	private static final String NO_CONSTRUCTOR = "This class should not been instanciated.";



	/**
	 * The default constructor that should not be used.
	 */
	private Messages() {
		// To prevent direct instantiation of this class
		throw new UnsupportedOperationException(Messages.NO_CONSTRUCTOR);
	}


	/**
	 * Lookup in <code>bundle</code> for the <code>key</code> in parameter. If <code>key</code> is not found, returns <code>!key!</code>
	 * 
	 * @param bundle
	 *            The resource bundle that contains the messages.
	 * @param key
	 *            The key of the message in the resource file.
	 * @return The value in the resource file (or <code>!key!</code> if not found).
	 */
	public static String getString(final ResourceBundle bundle, final String key) {
		try {
			return bundle.getString(key);
		} catch (final MissingResourceException mre) {
			LoggerFactory.getLogger(Messages.class).warn(MessageFormat.format(Messages.KEY_NOT_FOUND, key, bundle), mre);
			return Messages.EXCLAMATION_MARK + key + Messages.EXCLAMATION_MARK;
		}
	}


	/**
	 * Lookup in <code>bundle</code> for the <code>key</code> in parameter. The parameters in ellipse are used through the
	 * {@link MessageFormat#format(String, Object...)} method. If <code>key</code> is not found, returns <code>'!'+key'!'+Arrays.toString(params)</code>.
	 * 
	 * @param bundle
	 *            The resource bundle that contains the messages.
	 * @param key
	 *            The key of the message.
	 * @param params
	 *            The array used by the inner message formatter class to replace informations in the message.
	 * @return The value associated to the key in the property file, or <code>'!'+key'!'+Arrays.toString(params)</code> if not found. Using the array in
	 *         parameter to format the message.
	 */
	public static String getString(final ResourceBundle bundle, final String key, final Object... params) {
		final String pattern;
		try {
			pattern = bundle.getString(key);
		} catch (final MissingResourceException mre) {
			LoggerFactory.getLogger(Messages.class).warn(MessageFormat.format(Messages.KEY_NOT_FOUND, key, bundle), mre);
			return Messages.EXCLAMATION_MARK + key + Messages.EXCLAMATION_MARK + Arrays.toString(params);
		}
		return MessageFormat.format(pattern, params);
	}

}
