/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.factory;

import org.ow2.weblab.core.extended.exception.WebLabRISyntaxException;
import org.ow2.weblab.core.extended.exception.WebLabResourceCreationException;
import org.ow2.weblab.core.extended.uri.WebLabRI;
import org.ow2.weblab.core.extended.util.LLDUtil;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.LowLevelDescriptor;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;


/**
 * A factory to be used to create all the <code>Resource</code>s defined in the model. When creating a <code>Resource</code>, this factory will ensure that it
 * contains an <code>URI</code> compliant with <code>WebLabRI</code>. <br>
 *
 * This class contains two main types of methods.
 * <ul>
 * <li>The first one is {@link #createResource(String, String, Class)}. It enables to create any kind of resources. Note that is should only be used when creating standalone resource. See the method
 * comment for more details.</li>
 * <li>Every other method are of the second kind: i.e. <code>createAndLink...</code>. It enables to create resources that need to be added in another one. For instance, a <code>MediaUnit</code> should
 * only be created through the {@link #createAndLinkMediaUnit(Document, Class)} that create a <code>MediaUnit</code> and add it at the end of the <code>MediaUnit</code> list of the
 * <code>Document</code>.</li>
 * </ul>
 *
 * <code>Segment</code> cannot be created through this class. The class {@link SegmentFactory} is dedicated to this task.
 *
 * @see SegmentFactory
 * @author Cassidian WebLab Team
 * @since 1.2.3
 */
public final class WebLabResourceFactory {


	/**
	 * Constructors
	 * Do not use it.
	 */
	private WebLabResourceFactory() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * To create <code>Annotation</code>s we strongly encourage to use this method. It enables the creation of an <code>Annotation</code> having an <code>URI</code> compliant with the
	 * <code>WebLabRI</code> recommendations. <br>
	 * Since an <code>Annotation</code> hasn't any sense without being attached to a <code>Resource</code>, we only provide a method for creating <code>Annotation</code>s that takes a
	 * <code>Resource</code> in parameter. As a result, the created <code>Annotation</code> is added to the <code>Annotation</code> <code>List</code> of the <code>Resource</code>.
	 *
	 * @param resource
	 *            the <code>Resource</code> that will contains the created <code>Annotation</code>
	 * @return a new <code>Annotation</code> with a valid <code>WebLabRI</code> and added to <code>resource</code>
	 */
	public static Annotation createAndLinkAnnotation(final Resource resource) {
		final Annotation annotation = new Annotation();
		try {
			annotation.setUri(AbstractFactory.createUniqueURIFrom(resource, false, true));
			AbstractFactory.addAnnotation(annotation, resource, resource);
		} catch (final WebLabRISyntaxException wlrise) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + "Annotation", wlrise);
		}
		PoKUtil.setPoKData(annotation, "");
		return annotation;
	}



	/**
	 * This method provides an easy and strongly recommended way to create <code>LowLevelDescriptor</code>s. It enables the creation of a <code>LowLevelDescriptor</code> having an <code>URI</code>
	 * compliant with the <code>WebLabRI</code> recommendations. <br>
	 * Since an <code>LowLevelDescriptor</code> hasn't any sense without being attached to a <code>Resource</code>, we only provide a method for creating <code>LowLevelDescriptor</code>s that takes a
	 * <code>Resource</code> in parameter. As a result, the created <code>LowLevelDescriptor</code> is
	 * added to the <code>LowLevelDescriptor</code> <code>List</code> of the <code>Resource</code>.
	 *
	 * @param res
	 *            The <code>Resource</code> to be described by the <code>LowLevelDescriptor</code>.
	 * @return The <code>LowLevelDescriptor</code> describing <code>res</code>.
	 */
	public static LowLevelDescriptor createAndLinkLowLevelDescriptor(final Resource res) {
		final LowLevelDescriptor desc = new LowLevelDescriptor();
		desc.setUri(AbstractFactory.createUniqueURIFrom(res, true, false));
		AbstractFactory.addDescriptor(desc, res, res);
		LLDUtil.setLLDData(desc, "");
		return desc;
	}


	/**
	 * To create <code>MediaUnit</code>s that are not <code>Document</code> we strongly encourage to use this method. It enables the creation of a <code>MediaUnit</code> having an <code>URI</code>
	 * compliant with the <code>WebLabRI</code> recommendations. <br>
	 * Since a non-<code>Document</code> <code>MediaUnit</code> hasn't any sense without being attached to a <code>Document</code>, we only provide a method for
	 * creating <code>MediaUnit</code>s that takes a <code>Document</code> in parameter. As a result, the created <code>MediaUnit</code> (instance of the class
	 * given in parameter) is added to the <code>MediaUnit</code> <code>List</code> of the <code>Document</code>.
	 *
	 * @param <T>
	 *            The parametrised type of <code>MediaUnit</code> to be created and returned
	 * @param doc
	 *            The document in which to add the created <code>MediaUnit</code>
	 * @param muClass
	 *            The parametrised type of <code>MediaUnit</code> to be created and returned
	 * @return The created <code>MediaUnit</code>
	 */
	public static <T extends MediaUnit> T createAndLinkMediaUnit(final Document doc, final Class<T> muClass) {
		final T mediaUnit;
		try {
			mediaUnit = muClass.newInstance();
		} catch (final InstantiationException ie) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + muClass.getSimpleName(), ie);
		} catch (final IllegalAccessException iae) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + muClass.getSimpleName(), iae);
		}

		try {
			mediaUnit.setUri(AbstractFactory.getUniqueWebLabRIFrom(doc, true, false));
			AbstractFactory.addChild(mediaUnit, doc);
		} catch (final WebLabRISyntaxException wlrise) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + muClass.getSimpleName(), wlrise);
		}

		return mediaUnit;
	}


	/**
	 * You may need to create a <code>PieceOfKnowledge</code> in three main cases. <br>
	 *
	 * <ul>
	 * <li>First, you may need to add a PoK inside the PoK field of a <code>ResultSet</code>. In this case, we strongly encourage to use this method.</li>
	 * <li>Secondly, you may need a standalone <code>PieceOfKnowledge</code>. You'd rather to use the method {@link #createResource(String, String, Class)}.</li>
	 * <li>Finally, you may need to create an <code>Annotation</code> on a <code>Resource</code>. In that case, we strongly encourage you to use the method
	 * {@link #createAndLinkAnnotation(Resource)}.</li>
	 * </ul>
	 *
	 * Automatically set a valid <code>WebLabRI</code> to the created <code>PieceOfKnowledge</code> using the <code>ResultSet</code> in parameter to generate an
	 * unique id. Created <code>PoK</code> will be added to <code>resultSet</code>.
	 *
	 * @param resultSet
	 *            the <code>ResultSet</code> that will contains the created <code>PieceOfKnowledge</code>
	 * @return a new <code>PieceOfKnowledge</code> with a valid <code>WebLabRI</code> and added to <code>resultSet</code>
	 */
	public static PieceOfKnowledge createAndLinkPoK(final ResultSet resultSet) {
		final PieceOfKnowledge pok = new PieceOfKnowledge();
		pok.setUri(AbstractFactory.createUniqueURIFrom(resultSet, true, false));
		AbstractFactory.addPokInResultSet(pok, resultSet);
		PoKUtil.setPoKData(pok, "");
		return pok;
	}


	/**
	 * We strongly encourage <b>NOT</b> to use this method in order to create non standalone resources. In fact, use this
	 * class to create <code>Document</code>, <code>ComposedResource</code>, <code>Query</code> and <code>PieceOfKnowledge</code>, the only standalone <code>Resource</code>s. I.e.
	 * <code>Resource</code>s that have a sense alone, and that are not contained by anything
	 * (but a <code>ComposedResource</code>).<br>
	 * For other <code>Resource</code> s, you should use other methods of this class.
	 *
	 * @param idRef
	 *            Resource creator unique reference
	 * @param idRes
	 *            Resource unique reference in the creator
	 * @param resClass
	 *            The implementation class of the resource
	 * @param <T>
	 *            The <code>class</code> of the <code>Resource</code> to be instancied.
	 * @return An instance of resourceClass with a valid WebLabRI
	 */
	public static <T extends Resource> T createResource(final String idRef, final String idRes, final Class<T> resClass) {
		final T resource;
		try {
			resource = resClass.newInstance();
		} catch (final InstantiationException ie) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + resClass.getSimpleName(), ie);
		} catch (final IllegalAccessException iae) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + resClass.getSimpleName(), iae);
		}

		try {
			final WebLabRI wri = new WebLabRI("weblab://" + idRef + "/" + idRes);
			if (wri.getFragment() != null) {
				throw new WebLabRISyntaxException(wri.toString());
			}
			resource.setUri(wri.toString());
			AbstractFactory.cleanMapping(wri.toString());
		} catch (final WebLabRISyntaxException wlrise) {
			throw new WebLabResourceCreationException(WebLabResourceCreationException.EXCEPTION + resClass.getSimpleName(), wlrise);
		}

		if (resource instanceof PieceOfKnowledge) {
			PoKUtil.setPoKData(((PieceOfKnowledge) resource), "");
		} else if (resource instanceof LowLevelDescriptor) {
			LLDUtil.setLLDData(((LowLevelDescriptor) resource), "");
		}
		AbstractFactory.listUri(resource);
		return resource;
	}

}
