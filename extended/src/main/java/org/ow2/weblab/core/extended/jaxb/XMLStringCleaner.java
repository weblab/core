/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.jaxb;

/**
 * Some unicode valid (and UTF-8) valid chars are not valid in XML. Using the information given on W3C website (<a
 * href="http://www.w3.org/TR/REC-xml/#charsets">W3C XML reference</a>), this class enable to clean Strings carefully.
 * 
 * @author Cassidian WebLab Team
 */
public final class XMLStringCleaner {


	/**
	 * Constructors
	 * 
	 * Do not use it.
	 */
	private XMLStringCleaner() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Removes the chars that are not recommended in XML according to the <a href="http://www.w3.org/TR/REC-xml/#charsets">W3C XML reference</a>.
	 * 
	 * @param input
	 *            The <code>String</code> to be cleaned.
	 * @return The same <code>String</code> but cleaned from the not recommended in XML characters. <code>null</code> if <code>input</code> was
	 *         <code>null</code>.
	 * @see #isXMLRecommended(char)
	 * @see #getXMLRecommendedString(String, char)
	 */
	public static String getXMLRecommendedString(final String input) {
		return XMLStringCleaner.getXMLRecommendedString(input, false, ' ');
	}


	/**
	 * Replaces the chars in <code>input</code> that are not recommended in XML according to the <a href="http://www.w3.org/TR/REC-xml/#charsets">W3C XML
	 * reference</a> by the <code>replacement</code> character.
	 * 
	 * @param input
	 *            The <code>String</code> to be cleaned.
	 * @param replacement
	 *            The character to replace any non-recommended character found.
	 * @return The same <code>String</code> but cleaned from the not recommended in XML chars. <code>null</code> if <code>input</code> was <code>null</code>.
	 * @see #isXMLRecommended(char)
	 * @see #getXMLRecommendedString(String)
	 */
	public static String getXMLRecommendedString(final String input, final char replacement) {
		return XMLStringCleaner.getXMLRecommendedString(input, true, replacement);
	}


	/**
	 * Removes the chars that are not valid in XML according to the <a href="http://www.w3.org/TR/REC-xml/#charsets">W3C XML reference</a>.
	 * 
	 * @param input
	 *            The <code>String</code> to be cleaned.
	 * @return The same <code>String</code> but cleaned from the invalid in XML characters. <code>null</code> if <code>input</code> was <code>null</code>.
	 * @see #isXMLValid(char)
	 * @see #getXMLValidString(String, char)
	 */
	public static String getXMLValidString(final String input) {
		return XMLStringCleaner.getXMLValidString(input, false, ' ');
	}


	/**
	 * Removes the chars that are not valid in XML according to the <a href="http://www.w3.org/TR/REC-xml/#charsets">W3C XML reference</a>.
	 * 
	 * @param input
	 *            The <code>String</code> to be cleaned.
	 * @param replacement
	 *            The character to replace any invalid character found.
	 * @return The same <code>String</code> but cleaned from the invalid in XML characters. <code>null</code> if <code>input</code> was <code>null</code>.
	 * @see #isXMLValid(char)
	 * @see #getXMLValidString(String)
	 */
	public static String getXMLValidString(final String input, final char replacement) {
		return XMLStringCleaner.getXMLValidString(input, true, replacement);
	}


	/**
	 * The XML reference said that the use of the following chars is discouraged:<br>
	 * [#x7F-#x84], [#x86-#x9F], [#xFDD0-#xFDDF], [#x1FFFE-#x1FFFF], [#x2FFFE-#x2FFFF], [#x3FFFE-#x3FFFF], [#x4FFFE-#x4FFFF], [#x5FFFE-#x5FFFF],
	 * [#x6FFFE-#x6FFFF], [#x7FFFE-#x7FFFF], [#x8FFFE-#x8FFFF], [#x9FFFE-#x9FFFF], [#xAFFFE-#xAFFFF], [#xBFFFE-#xBFFFF], [#xCFFFE-#xCFFFF], [#xDFFFE-#xDFFFF],
	 * [#xEFFFE-#xEFFFF], [#xFFFFE-#xFFFFF], [#x10FFFE-#x10FFFF].
	 * 
	 * <br>
	 * <br>
	 * Since chars in Java can't be bigger than #xFFFF (not part of the BMP), only the third first blocks are considered.
	 * 
	 * @param character
	 *            The <code>char</code> to test.
	 * @return <code>true</code> if the <code>c</code> is XMLWritable and is not part of:<br>
	 *         [#x7F-#x84], [#x86-#x9F], [#xFDD0-#xFDDF].
	 * @see #isXMLValid(char)
	 */
	public static boolean isXMLRecommended(final char character) {
		final boolean temp = XMLStringCleaner.isXMLValid(character) && !((character >= '\u007F') && (character <= '\u0084'));
		return temp && !((character >= '\u0086') && (character <= '\u009F')) && !((character >= '\uFDD0') && (character <= '\uFDDF'));
	}


	/**
	 * The <a href="http://www.w3.org/TR/REC-xml/#charsets">W3C XML reference</a> said that a valid XML char is one of:<br>
	 * #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
	 * 
	 * <br>
	 * Since chars in Java can't be bigger than #xFFFF (not part of the BMP), the last block is not tested.
	 * 
	 * @param character
	 *            The <code>char</code> to test.
	 * @return <code>true</code> if <code>c</code> is one of:<br>
	 *         #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD].
	 */
	public static boolean isXMLValid(final char character) {
		final boolean temp = (character == '\u0009') || (character == '\n') || (character == '\r') || ((character >= '\u0020') && (character <= '\uD7FF'));
		return temp || ((character >= '\uE000') && (character <= '\uFFFD'));
	}


	/**
	 * Removes the chars in <code>input</code> that are not recommended in XML according to the <a href="http://www.w3.org/TR/REC-xml/#charsets">W3C XML
	 * reference</a> or replace them the <code>replacement</code> character, according to <code>replace</code>.
	 * 
	 * @param input
	 *            The <code>String</code> to be cleaned.
	 * @param replace
	 *            Whether or not to replace not recommended characters by <code>replacement</code>.
	 * @param replacement
	 *            The character to replace any non-recommended character found.
	 * @return The same <code>String</code> but cleaned from the not recommended in XML chars. <code>null</code> if <code>input</code> was <code>null</code>.
	 */
	private static String getXMLRecommendedString(final String input, final boolean replace, final char replacement) {
		if (input == null) {
			return null;
		}
		if ("".equals(input)) {
			return "";
		}

		final StringBuilder strBldr = new StringBuilder();
		for (final char c : input.toCharArray()) {
			if (XMLStringCleaner.isXMLRecommended(c)) {
				strBldr.append(c);
			} else if (replace) {
				strBldr.append(replacement);
			}
		}
		return strBldr.toString();
	}


	/**
	 * Removes the chars in <code>input</code> that are not valid in XML according to the <a href="http://www.w3.org/TR/REC-xml/#charsets">W3C XML reference</a>
	 * or replace them the <code>replacement</code> character, according to <code>replace</code>.
	 * 
	 * @param input
	 *            The <code>String</code> to be cleaned.
	 * @param replace
	 *            Whether or not to replace invalid characters by <code>replacement</code>.
	 * @param replacement
	 *            The character to replace any invalid character found
	 * @return The same <code>String</code> but cleaned from the invalid in XML characters. <code>null</code> if <code>input</code> was <code>null</code>.
	 * @see #isXMLValid(char)
	 */
	private static String getXMLValidString(final String input, final boolean replace, final char replacement) {
		if (input == null) {
			return null;
		}

		if ("".equals(input)) {
			return "";
		}

		final StringBuilder strBldr = new StringBuilder();
		for (final char c : input.toCharArray()) {
			if (XMLStringCleaner.isXMLValid(c)) {
				strBldr.append(c);
			} else if (replace) {
				strBldr.append(replacement);
			}
		}
		return strBldr.toString();
	}

}
