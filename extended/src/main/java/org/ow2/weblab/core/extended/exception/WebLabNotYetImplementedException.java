/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.exception;

/**
 * <code>RuntimeException</code> to be thrown when a method is not implemented, or if a type of argument is not yet handle by the method.
 * 
 * @author Cassidian WebLab Team
 */
public class WebLabNotYetImplementedException extends WebLabUncheckedException {


	private static final String MESSAGE = "Method not yet implemented.";


	private static final long serialVersionUID = 129L;


	/**
	 * Default constructor
	 */
	public WebLabNotYetImplementedException() {
		this(WebLabNotYetImplementedException.MESSAGE);
	}


	/**
	 * @param message
	 *            The message
	 */
	public WebLabNotYetImplementedException(final String message) {
		super(message);
	}


	/**
	 * @param message
	 *            The message
	 * @param cause
	 *            The <code>Throwable</code> that causes the <code>RuntimeException</code>.
	 */
	public WebLabNotYetImplementedException(final String message, final Throwable cause) {
		super(message, cause);
	}


	/**
	 * @param cause
	 *            The <code>Throwable</code> that causes the <code>RuntimeException</code>
	 */
	public WebLabNotYetImplementedException(final Throwable cause) {
		this(WebLabNotYetImplementedException.MESSAGE, cause);
	}
}
