/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.ontologies;

/**
 * Just a useful class containing the <code>String</code> URIs of the WGS 84 Positioning ontology (https://www.w3.org/2003/01/geo/wgs84_pos).
 * 
 * @author Cassidian WebLab Team
 */
public final class WGS84Pos {


	/**
	 * The namespace of all those classes and properties.
	 */
	public static final String NAMESPACE = "http://www.w3.org/2003/01/geo/wgs84_pos#";


	/**
	 * Preferred prefix to be used to represent the WGS 84 positioning geo ontology namespace.
	 */
	public static final String PREFERRED_PREFIX = "wgs84";



	/**
	 * Anything with spatial extent, i.e. size, shape, or position.
	 * e.g. people, places, bowling balls, as well as abstract areas like cubes.
	 */
	public static final String SPATIAL_THING = WGS84Pos.NAMESPACE + "SpatialThing";


	/**
	 * A point, typically described using a coordinate system relative to Earth, such as WGS84.
	 * 
	 * Uniquely identified by lat/long/alt. i.e.
	 * 
	 * spaciallyIntersects(P1, P2) :- lat(P1, LAT), long(P1, LONG), alt(P1, ALT),
	 * lat(P2, LAT), long(P2, LONG), alt(P2, ALT).
	 * 
	 * sameThing(P1, P2) :- type(P1, Point), type(P2, Point), spaciallyIntersects(P1, P2).
	 */
	public static final String POINT = WGS84Pos.NAMESPACE + "Point";


	/**
	 * The WGS84 latitude of a SpatialThing (decimal degrees).
	 */
	public static final String LAT = WGS84Pos.NAMESPACE + "lat";


	/**
	 * The relation between something and the point, or other geometrical thing in space, where it is. For example, the relationship between a radio tower and a Point with a given lat and long. Or a
	 * relationship between a park and its outline as a closed arc of points, or a road and its location as a arc (a sequence of points). Clearly in practice there will be limit to the accuracy of any
	 * such statement, but one would expect an accuracy appropriate for the size of the object and uses such as mapping .
	 */
	public static final String LOCATION = WGS84Pos.NAMESPACE + "location";


	/**
	 * The WGS84 longitude of a SpatialThing (decimal degrees).
	 */
	public static final String LONG = WGS84Pos.NAMESPACE + "long";


	/**
	 * The WGS84 altitude of a SpatialThing (decimal meters above the local reference ellipsoid).
	 */
	public static final String ALT = WGS84Pos.NAMESPACE + "alt";


	/**
	 * A comma-separated representation of a latitude, longitude coordinate.
	 */
	public static final String LAT_LONG = WGS84Pos.NAMESPACE + "lat_long";


	/**
	 * Constructors
	 * Do not use it.
	 */
	private WGS84Pos() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}

}
