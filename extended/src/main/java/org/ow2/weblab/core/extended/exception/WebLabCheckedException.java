/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.exception;

/**
 * WebLab Checked Exception : that could/should be treated by enclosing module based on
 * "Best Practices for Exception Handling".
 * 
 * @author Cassidian WebLab Team
 */
public class WebLabCheckedException extends Exception {


	private static final long serialVersionUID = 129L;


	/**
	 * @param message
	 *            the message
	 */
	public WebLabCheckedException(final String message) {
		super(message);
	}


	/**
	 * @param message
	 *            the message
	 * @param cause
	 *            the <code>Throwable</code> that causes this <code>Exception</code>.
	 */
	public WebLabCheckedException(final String message, final Throwable cause) {
		super(message, cause);
	}


	/**
	 * @param cause
	 *            the <code>Throwable</code> that causes this <code>Exception</code>.
	 */
	public WebLabCheckedException(final Throwable cause) {
		super(cause);
	}

}
