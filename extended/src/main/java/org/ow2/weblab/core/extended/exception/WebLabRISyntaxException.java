/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.exception;

import java.net.URISyntaxException;

/**
 * <code>WebLabUncheckedException</code> to be thrown when having syntax troubles with the <code>WebLabRI</code>.
 * 
 * @author Cassidian WebLab Team
 */
public class WebLabRISyntaxException extends WebLabUncheckedException {


	private static final String RESOURCE_EXCEPTION = "Top Level resources can't contain '#'. idRef or idRes might contain one. Invalid WebLabRI: ";


	private static final long serialVersionUID = 129L;


	/**
	 * @param badUri
	 *            The badUri.
	 */
	public WebLabRISyntaxException(final String badUri) {
		super(WebLabRISyntaxException.RESOURCE_EXCEPTION + badUri);
	}


	/**
	 * @param cause
	 *            The <code>URISyntaxException</code> that causes the <code>RuntimeException</code>.
	 */
	public WebLabRISyntaxException(final URISyntaxException cause) {
		super(cause);
	}

}
