/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.util;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.Text;


/**
 * A very simple util class that enable to handle <code>LinearSegment</code> and <code>Text</code>.
 * 
 * @author Cassidian WebLab Team
 */
public final class TextUtil {


	/**
	 * Constructors
	 * Do not use it.
	 */
	private TextUtil() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Extract the <code>String</code> covered in <code>text</code> by <code>segment</code>. This cannot work in the
	 * case of a <code>Text</code> that do not have content (and uses a <code>TextContent</code> object instead).
	 * 
	 * @param text
	 *            The <code>Text</code> section in which we look for content.
	 * @param segment
	 *            The <code>Segment</code> defining the position of the text
	 *            to retrieve. <code>segment</code> must be contained by the <code>List&gt;Segment&lt;</code> of text.
	 * @throws WebLabCheckedException
	 *             If the segment is not part of the text
	 * @return the <code>String</code> content of the <code>Text</code> at
	 *         the position defined by the <code>Segment</code>
	 */
	public static String getSegmentText(final Text text, final LinearSegment segment) throws WebLabCheckedException {
		if ((text != null) && (segment != null)) {
			if (text.isSetSegment() && text.getSegment().contains(segment)) {
				final String temp = text.getContent();
				if (temp != null) {
					if (temp.length() >= segment.getEnd()) {
						return temp.substring(segment.getStart(), segment.getEnd());
					}
					throw new WebLabCheckedException("Segment (" + segment.getUri() + ") ends after of content of Text: " + text.getUri() + ".");
				}
				throw new WebLabCheckedException("Content is null in Text: " + text.getUri());
			}
			throw new WebLabCheckedException("Text (" + text.getUri() + ") is not the parent of Segment: " + segment.getUri() + ".");
		}
		throw new WebLabCheckedException("Text (" + text + ") or Segment (" + segment + ") were null.");
	}

}
