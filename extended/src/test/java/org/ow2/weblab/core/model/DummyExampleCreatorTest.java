/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.model;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.DCTerms;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.searcher.ObjectFactory;
import org.ow2.weblab.core.services.searcher.SearchArgs;


/**
 * Trying to handle every kind of Object in the model.
 */
public class DummyExampleCreatorTest {


	WebLabMarshaller wlm = new WebLabMarshaller(true);


	/**
	 * Tests the creation of a simple Document
	 * 
	 * @throws WebLabCheckedException
	 *             If the marshaller fails
	 * @throws JAXBException
	 *             If the marshaller fails
	 */
	@Test
	public void testDocument() throws WebLabCheckedException, JAXBException {
		final Document doc = WebLabResourceFactory.createResource("test", "doctest0", Document.class);

		final Text t0 = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		t0.setContent("This is an introduction.");

		final Text t1 = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		t1.setContent("First section.");

		final Text t2 = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		t2.setContent("This is a text.");

		final Text t3 = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		t3.setContent("Conclusion.");

		final Text t4 = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		t4.setContent("This is a very simple example.");

		this.wlm.marshalResource(doc, new File("target/generated-doc.xml"));

		final ProcessArgs args = new ProcessArgs();
		args.setResource(doc);

		final JAXBContext jc = JAXBContext.newInstance(ProcessArgs.class);
		final Marshaller m = jc.createMarshaller();
		final JAXBElement<ProcessArgs> el = new org.ow2.weblab.core.services.analyser.ObjectFactory().createProcessArgs(args);
		m.marshal(el, new File("target/VITALASSample.xml"));
	}


	/**
	 * Tests the creation of a Document and its annotation
	 * 
	 * @throws WebLabCheckedException
	 *             If the marshaller or the PoKUtil fails
	 */
	@Test
	public void testAnnotatedDocument() throws WebLabCheckedException {
		final Document doc = WebLabResourceFactory.createResource("test", "doctest1", Document.class);

		final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(doc);

		String rdf = "<rdf:rdf xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" + " xmlns:dc=\"http://purl.org/dc/elements/1.1/\" >"
				+ "<rdf:Description rdf:about=\"" + doc.getUri() + "\">" + "<dc:title>Another sample document</dc:title>"
				+ "<dc:source>file:///test.txt</dc:source>" + "</rdf:Description>" + "</rdf:rdf>";

		PoKUtil.setPoKData(annot, rdf);

		final Text t = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		t.setContent("The is a particular text.");

		final LinearSegment seg = SegmentFactory.createAndLinkSegment(t, LinearSegment.class);
		seg.setStart(0);
		seg.setEnd(2);

		final Annotation a = WebLabResourceFactory.createAndLinkAnnotation(t);

		rdf = "<rdf:rdf xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" + " xmlns:j.0=\"http://weblab.eads.com/\" >"
				+ "<rdf:Description rdf:about=\"" + seg.getUri() + "\">" + "<j.0:SampleProperty>&quot;This&quot; is a segment on a word</j.0:SampleProperty>"
				+ "</rdf:Description>" + "</rdf:rdf>";

		PoKUtil.setPoKData(a, rdf);

		this.wlm.marshalResource(doc, new File("target/generated-annotatedText.xml"));
	}


	/**
	 * Tests the creation of an image
	 * 
	 * @throws Exception
	 *             If something weird occurs
	 */
	@Test
	public void testImage() throws Exception {
		final Document doc = WebLabResourceFactory.createResource("test", "doctest2", Document.class);

		final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(doc);

		final String rdf = "<rdf:rdf xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" + " xmlns:weblab=\"http://weblab.eads.com/\""
				+ " xmlns:dc=\"http://purl.org/dc/elements/1.1/\" >" + "<rdf:Description rdf:about=\"" + doc.getUri() + "\">" + "<weblab:date>"
				+ new Date().toString() + "</weblab:date> <dc:source>file:///test.jpeg</dc:source>" + "</rdf:Description>" + "</rdf:rdf>";

		PoKUtil.setPoKData(annot, rdf);

		final Image i = WebLabResourceFactory.createAndLinkMediaUnit(doc, Image.class);
		i.setContent(DummyExampleCreatorTest.getImageBytes("src/test/resources/logo_weblab.png"));

		final Text t = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		t.setContent("The new WebLab logo which is a very nice piece of art or at least designer production... Ok this logo does not look that much good but this is the only we have.");

		this.wlm.marshalResource(doc, new File("target/generated-image.xml"));
	}


	private static byte[] getImageBytes(final String filename) throws Exception {
		final BufferedImage img = ImageIO.read(new File(filename));
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(img, "png", baos);
		return baos.toByteArray();
	}


	/**
	 * @throws WebLabCheckedException
	 *             If the marshaller or pokutil fails
	 * @throws JAXBException
	 *             If the marshaller fails
	 */
	@Test
	public void testCrawlNormFeatConcept() throws WebLabCheckedException, JAXBException {
		final Document doc = WebLabResourceFactory.createResource("IRTcrawler", "doc0003", Document.class);
		Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(doc);

		String rdf = "<rdf:rdf xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" + " xmlns:j.0=\"" + DublinCore.NAMESPACE + "\"" + " xmlns:j.1=\""
				+ DCTerms.NAMESPACE + "\"" + " xmlns:j.2=\"" + WebLabProcessing.NAMESPACE + "\" >" + "<rdf:Description rdf:about=\"" + doc.getUri() + "\">"
				+ "<j.1:created>" + new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date()) + "</j.1:created>"
				+ "<j.0:source>file:///test.jpg</j.0:source>" + "<j.2:hasNativeContent>weblab://IRTcrawler/content0003</j.2:hasNativeContent>"
				+ "</rdf:Description>" + "</rdf:rdf>";

		PoKUtil.setPoKData(annot, rdf);

		this.wlm.marshalResource(doc, new File("target/generated-crawled.xml"));

		final Image i = WebLabResourceFactory.createAndLinkMediaUnit(doc, Image.class);
		// Choose whether to add content as byte array or add annotation on normalised content
		// i.setContent(getImageBytes("src/test/resources/logo_weblab.png"));

		annot = WebLabResourceFactory.createAndLinkAnnotation(i);
		rdf = "<rdf:RDF xmlns:j.1=\"http://weblab-project.org/core/model/1.0/property/\" xmlns:j.0=\"http://www.weblab-project.org/external/metadata/iptc/\""
				+ " xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" >" + "<rdf:Description rdf:about=\"" + i.getUri()
				+ "\"><j.1:hasNormalizedContent>weblab://EADSnormalizer/content0003</j.1:hasNormalizedContent>"
				+ "<j.1:hasReducedContent>weblab://IRTnormalizer/reducedcontent0003</j.1:hasReducedContent>"
				+ "<j.0:title>TANZANIA PRINCESS ASTRID ROLL BACK MALARIA SPECIAL</j.0:title>" + "<j.0:creationdate>2007-10-02T00:00:00</j.0:creationdate>"
				+ "<j.0:creator>ERIC LALMAND</j.0:creator>" + "<j.0:keywords>ROYALS PRINCESS ASTRID TANZANIA AFPIMF</j.0:keywords>"
				+ "<j.0:category>SPO</j.0:category>" + "<j.0:supplementalCategory>SOCCER</j.0:supplementalCategory>"
				+ "<j.0:creatorTitle>STR</j.0:creatorTitle>" + "<j.0:city>LIEGE</j.0:city>"
				+ "<j.0:countryPrimaryLocationName>BELGIUM</j.0:countryPrimaryLocationName>"
				+ "<j.0:countryPrimaryLocationCode>BEL</j.0:countryPrimaryLocationCode>" + "<j.0:credit>BELGA</j.0:credit>" + "<j.0:source>BELGA</j.0:source>"
				+ "<j.0:writer>HEC</j.0:writer>" + "<j.0:language>EN</j.0:language>" + "</rdf:Description></rdf:RDF>";
		PoKUtil.setPoKData(annot, rdf);

		final Text t = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		t.setContent("20071002 - DAR ES SALAAM, TANZANIA: Princess Astrid of Belgium visits "
				+ "a hospital in Dar Es Salaam, Tuesday 2 October 2007 in Dar Es Salaam, "
				+ "Tanzania. Princess Astrid of Belgium is in Tanzania for the Roll Back "
				+ "malaria special mission untill Thursday 4 October 2007...BELGA PHOTO ERIC" + " LALMAND");

		this.wlm.marshalResource(doc, new File("target/generated-normalised.xml"));

		annot = WebLabResourceFactory.createAndLinkAnnotation(i);
		rdf = "<rdf:RDF xmlns:j.0=\"http://weblab-project.org/core/model/1.0/property/\"" + " xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" >"
				+ "<rdf:Description rdf:about=\"" + i.getUri() + "\"><j.0:exposedAs>http://rbtker.es/vitalas/content0003.jpg</j.0:exposedAs>"
				+ "<j.0:exposedAsThumbnail>http://rbtker.es/vitalas/reducedcontent0003.jpg</j.0:exposedAsThumbnail>" + "</rdf:Description></rdf:RDF>";
		PoKUtil.setPoKData(annot, rdf);

		this.wlm.marshalResource(doc, new File("target/generated-exposed.xml"));

		final LowLevelDescriptor desc = WebLabResourceFactory.createAndLinkLowLevelDescriptor(i);
		final String values = "0.123, 0.123, 0.123, 0.123, 0.123, 0.123, 0.123, 0.123, 0.123, 0.123, 0.123, 0.123, 0.123, 0.123, 0.123";
		desc.setData(values);

		this.wlm.marshalResource(doc, new File("target/generated-Feat.xml"));

		annot = WebLabResourceFactory.createAndLinkAnnotation(i);
		rdf = "<rdf:RDF " + "xmlns:j.0=\"http://www.weblab-project.org/model/1.0/property/extracted/\" "
				+ "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">" + "<rdf:Description rdf:nodeID=\"A0\">" + "	<rdf:subject" + "		rdf:resource=\""
				+ i.getUri()
				+ "\" />"
				+ "	<rdf:predicate"
				+ "		rdf:resource=\"http://www.weblab-project.org/model/1.0/property/extracted/concept/visual\" />"
				+ "	<rdf:object>driver</rdf:object>"
				+ "	<rdf:type"
				+ "		rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement\" />"
				+ "	<j.0:visualconceptscore>0.236</j.0:visualconceptscore>"
				+ "</rdf:Description>"
				+ "<rdf:Description rdf:nodeID=\"A1\">"
				+ "	<rdf:subject rdf:resource=\""
				+ i.getUri()
				+ "\" />"
				+ "	<rdf:predicate"
				+ "		rdf:resource=\"http://weblab-project.org/core/model/1.0/property/extracted/concept/visual\" />"
				+ "	<rdf:object>Raikkonen</rdf:object>"
				+ "	<rdf:type"
				+ "		rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement\" />"
				+ "	<j.0:visualconceptscore>0.634</j.0:visualconceptscore>"
				+ "</rdf:Description>"
				+ "<rdf:Description rdf:nodeID=\"A2\">"
				+ "	<rdf:subject"
				+ "		rdf:resource=\""
				+ i.getUri()
				+ "\" />"
				+ "	<rdf:predicate"
				+ "		rdf:resource=\"http://weblab-project.org/core/model/1.0/property/extracted/concept/visual\" />"
				+ "	<rdf:object>formula</rdf:object>"
				+ "	<rdf:type"
				+ "		rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement\" />"
				+ "	<j.0:visualconceptscore>0.425</j.0:visualconceptscore>" + "</rdf:Description>" + "</rdf:RDF>";
		PoKUtil.setPoKData(annot, rdf);

		this.wlm.marshalResource(doc, new File("target/generated-visualConcept.xml"));

		annot = WebLabResourceFactory.createAndLinkAnnotation(t);
		rdf = "<rdf:RDF " + "xmlns:j.0=\"http://weblab-project.org/core/model/1.0/property/extracted/\" "
				+ "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">" + "<rdf:Description rdf:nodeID=\"A0\">" + "	<rdf:subject" + "		rdf:resource=\""
				+ t.getUri()
				+ "\" />"
				+ "	<rdf:predicate"
				+ "		rdf:resource=\"http://weblab-project.org/core/model/1.0/property/extracted/concept/text\" />"
				+ "	<rdf:object>driver</rdf:object>"
				+ "	<rdf:type"
				+ "		rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement\" />"
				+ "	<j.0:textconceptscore>0.236</j.0:textconceptscore>"
				+ "</rdf:Description>"
				+ "<rdf:Description rdf:nodeID=\"A1\">"
				+ "	<rdf:subject"
				+ "		rdf:resource=\""
				+ t.getUri()
				+ "\" />"
				+ "	<rdf:predicate"
				+ "		rdf:resource=\"http://weblab-project.org/core/model/1.0/property/extracted/concept/text\" />"
				+ "	<rdf:object>Raikkonen</rdf:object>"
				+ "	<rdf:type"
				+ "		rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement\" />"
				+ "	<j.0:textconceptscore>0.634</j.0:textconceptscore>"
				+ "</rdf:Description>"
				+ "<rdf:Description rdf:nodeID=\"A2\">"
				+ "	<rdf:subject"
				+ "		rdf:resource=\""
				+ t.getUri()
				+ "\" />"
				+ "	<rdf:predicate"
				+ "		rdf:resource=\"http://weblab-project.org/core/model/1.0/property/extracted/concept/text\" />"
				+ "	<rdf:object>formula</rdf:object>"
				+ "	<rdf:type"
				+ "		rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement\" />"
				+ "	<j.0:textconceptscore>0.425</j.0:textconceptscore>" + "</rdf:Description>" + "</rdf:RDF>";
		PoKUtil.setPoKData(annot, rdf);

		this.wlm.marshalResource(doc, new File("target/generated-textConcept.xml"));

		final StringQuery query = WebLabResourceFactory.createResource("RobotikerSearchGUI", "q0653", StringQuery.class);

		query.setRequest("belga");

		final SearchArgs args = new SearchArgs();
		args.setLimit(Integer.valueOf(10));
		args.setOffset(Integer.valueOf(0));
		args.setQuery(query);

		JAXBContext.newInstance(SearchArgs.class).createMarshaller()
				.marshal(new ObjectFactory().createSearchArgs(args), new File("target/generated-searchArgsFulltText.xml"));

		final SimilarityQuery query2 = WebLabResourceFactory.createResource("RobotikerSearchGUI", "q0654", SimilarityQuery.class);

		query2.getResource().add(doc);

		final SearchArgs simargs = new SearchArgs();
		simargs.setLimit(Integer.valueOf(10));
		simargs.setOffset(Integer.valueOf(0));
		simargs.setQuery(query2);

		JAXBContext.newInstance(SearchArgs.class).createMarshaller()
				.marshal(new ObjectFactory().createSearchArgs(simargs), new File("target/generated-searchArgsSimilarity.xml"));

		final StringQuery ftquery = WebLabResourceFactory.createResource("ina.GUI", "q0653", StringQuery.class);

		ftquery.setRequest("text:(belga tennis) uri:(\"weblab://fileRepository/1220602148241/res_124\" " + "\"weblab://fileRepository/1220602148241/res_56\" "
				+ "\"weblab://fileRepository/1220602148241/res_53\" " + "\"weblab://fileRepository/1220602148241/res_3\" "
				+ "\"weblab://fileRepository/1220602148241/res_65\" " + "\"weblab://fileRepository/1220602148241/res_233\" "
				+ "\"weblab://fileRepository/1220602148241/res_512\")");

		this.wlm.marshalResource(ftquery, new File("target/generated-complexFullTextQuery.xml"));

		final SimilarityQuery simQuery = WebLabResourceFactory.createResource("ina.GUI", "q0654", SimilarityQuery.class);

		simQuery.getResource().add(doc);
		this.wlm.marshalResource(simQuery, new File("target/generated-complexSimQuery.xml"));
	}


	/**
	 * @throws WebLabCheckedException
	 *             If the marshaller or pokutil fails
	 */
	@Test
	public void testResultSet() throws WebLabCheckedException {
		final ComposedQuery query = WebLabResourceFactory.createResource("test", "q0653", ComposedQuery.class);

		final StringQuery fq = WebLabResourceFactory.createResource("test", "q0654", StringQuery.class);
		fq.setRequest("foo");
		final StringQuery fq2 = WebLabResourceFactory.createResource("test", "q0655", StringQuery.class);
		fq2.setRequest("bar");

		query.setOperator(Operator.OR);
		query.getQuery().add(fq);
		query.getQuery().add(fq2);

		this.wlm.marshalResource(query, new File("target/generated-composedQuery.xml"));

		final ResultSet res = WebLabResourceFactory.createResource("EADSTextSearch", "res08523", ResultSet.class);
		final PieceOfKnowledge pok = WebLabResourceFactory.createResource("EADSTextSearchPOK", "pok08523", PieceOfKnowledge.class);

		String rdf = "<rdf:rdf xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" + " xmlns:j.0=\"" + WebLabRetrieval.NAMESPACE + "\" >"
				+ "<rdf:Description rdf:about=\"" + res.getUri() + "\">" + "<j.0:isResultOf>" + query.getUri() + "</j.0:isResultOf>"
				+ "<j.0:hasNumberOfResults>" + 345 + "</j.0:hasNumberOfResults>" + "</rdf:Description>" + "</rdf:rdf>";
		// Just to see if it works for POK and ontologies
		PoKUtil.setPoKData(pok, rdf);

		Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(res);
		PoKUtil.setPoKData(annot, PoKUtil.getPoKData(pok));

		for (int k = 1; k < 4; k++) {
			final Document doc = WebLabResourceFactory.createResource("EADSTextSearch", "doc0043" + k, Document.class);

			annot = WebLabResourceFactory.createAndLinkAnnotation(doc);

			rdf = "<rdf:rdf xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" + " xmlns:j.0=\"" + WebLabRetrieval.NAMESPACE + "\" >"
					+ "<rdf:Description rdf:about=\"" + doc.getUri() + "\">" + "<j.0:isResultOf>" + query.getUri() + "</j.0:isResultOf>" + "<j.0:hasHitScore>"
					+ Math.atan(1.0 / k) + "</j.0:hasHitScore>" + "<j.0:isProducedFrom>weblab://IRTcrawler/doc0" + DummyExampleCreatorTest.safeAbs(rdf.hashCode())
					+ "</j.0:isProducedFrom>" + "<j.0:exposedAs>http://RobotikerServer/images/belga/full/pic" + DummyExampleCreatorTest.safeAbs(rdf.hashCode())
					+ ".jpg</j.0:exposedAs>" + "<j.0:exposedAsThumbnail>http://RobotikerServer/images/belga/thumbnails/thb"
					+ DummyExampleCreatorTest.safeAbs(rdf.hashCode()) + ".jpg</j.0:exposedAsThumbnail>" + "</rdf:Description>" + "</rdf:rdf>";

			PoKUtil.setPoKData(annot, rdf);

			res.getResource().add(doc);
		}

		this.wlm.marshalResource(res, new File("target/generated-results.xml"));
	}


	private static int safeAbs(final int hashCode) {
		if (hashCode == Integer.MIN_VALUE) {
			return Integer.MAX_VALUE;
		}
		return Math.abs(hashCode);
	}


	/**
	 * @throws JAXBException
	 *             If the marshaller fails
	 */
	@Test
	public void testProcessArgs() throws JAXBException {
		final Document doc = WebLabResourceFactory.createResource("EADSTextSearch", "doc0043", Document.class);

		final String context = "weblab://EADSTextSearch/context0043";

		final ProcessArgs args = new ProcessArgs();

		args.setResource(doc);
		args.setUsageContext(context);

		final JAXBElement<ProcessArgs> el = new org.ow2.weblab.core.services.analyser.ObjectFactory().createProcessArgs(args);

		final JAXBContext jc;
		final Marshaller marshaler;
		jc = JAXBContext.newInstance(ProcessArgs.class);
		marshaler = jc.createMarshaller();
		marshaler.marshal(el, new File("target/generated-args.xml"));
	}

}
