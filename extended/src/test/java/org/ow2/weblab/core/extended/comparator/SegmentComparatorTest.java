/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.comparator;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Coordinate;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.SpatioTemporalSegment;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.TrackSegment;

/**
 * @author Cassidian WebLab Team
 */
public class SegmentComparatorTest {


	/**
	 * 
	 */
	@Test
	public void testComparator() {
		final Document doc = WebLabResourceFactory.createResource("azert", "yuiop", Document.class);
		final Text theText = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		theText.setContent("Another wonderful text for tests");

		final DummySegment final1Segment = SegmentFactory.createAndLinkSegment(theText, DummySegment.class);
		final DummySegment final2Segment = SegmentFactory.createAndLinkSegment(theText, DummySegment.class);

		final TrackSegment sixteenthSegment = SegmentFactory.createAndLinkSegment(theText, TrackSegment.class);

		final LinearSegment firstSegment = SegmentFactory.createAndLinkSegment(theText, LinearSegment.class);
		firstSegment.setStart(0);
		firstSegment.setEnd(5);

		final TrackSegment seventeenthSegment = SegmentFactory.createAndLinkSegment(theText, TrackSegment.class);
		seventeenthSegment.getSegment().add(new SpatialSegment());
		seventeenthSegment.getSegment().add(firstSegment);

		final TemporalSegment fifthSegment = SegmentFactory.createAndLinkSegment(theText, TemporalSegment.class);
		fifthSegment.setStart(0);
		fifthSegment.setEnd(5);

		final LinearSegment thirdSegment = SegmentFactory.createAndLinkSegment(theText, LinearSegment.class);
		thirdSegment.setStart(1);
		thirdSegment.setEnd(2);

		final SpatialSegment eighthSegment = SegmentFactory.createAndLinkSegment(theText, SpatialSegment.class);
		final Coordinate coordo1 = new Coordinate();
		coordo1.setX(1);
		coordo1.setY(0);
		final Coordinate coordo2 = new Coordinate();
		coordo2.setX(0);
		coordo2.setY(1);
		eighthSegment.getCoordinate().add(coordo1);
		eighthSegment.getCoordinate().add(coordo2);

		final SpatioTemporalSegment thirteenthSegment = SegmentFactory.createAndLinkSegment(theText, SpatioTemporalSegment.class);
		thirteenthSegment.getCoordinate().add(coordo1);
		thirteenthSegment.getCoordinate().add(coordo2);
		thirteenthSegment.setTimestamp(5);

		final TemporalSegment sixthSegment = SegmentFactory.createAndLinkSegment(theText, TemporalSegment.class);
		sixthSegment.setStart(0);
		sixthSegment.setEnd(6);

		final TemporalSegment seventhSegment = SegmentFactory.createAndLinkSegment(theText, TemporalSegment.class);
		seventhSegment.setStart(1);
		seventhSegment.setEnd(2);

		final DummySegment final3Segment = SegmentFactory.createAndLinkSegment(theText, DummySegment.class);

		final SpatioTemporalSegment fourteenthSegment = SegmentFactory.createAndLinkSegment(theText, SpatioTemporalSegment.class);
		fourteenthSegment.getCoordinate().add(coordo1);
		fourteenthSegment.getCoordinate().add(coordo2);
		fourteenthSegment.setTimestamp(5555);

		final LinearSegment secondSegment = SegmentFactory.createAndLinkSegment(theText, LinearSegment.class);
		secondSegment.setStart(0);
		secondSegment.setEnd(6);

		final TrackSegment eighteenthSegment = SegmentFactory.createAndLinkSegment(theText, TrackSegment.class);
		eighteenthSegment.getSegment().add(new SpatialSegment());
		eighteenthSegment.getSegment().add(fourteenthSegment);

		final DummySegment final4Segment = SegmentFactory.createAndLinkSegment(theText, DummySegment2.class);
		final DummySegment final5Segment = SegmentFactory.createAndLinkSegment(theText, DummySegment.class);

		final SpatialSegment tenthSegment = SegmentFactory.createAndLinkSegment(theText, SpatialSegment.class);
		final Coordinate coordo3 = new Coordinate();
		coordo3.setX(2);
		coordo3.setY(0);
		tenthSegment.getCoordinate().add(coordo1);
		tenthSegment.getCoordinate().add(coordo2);
		tenthSegment.getCoordinate().add(coordo3);

		final SpatioTemporalSegment fifteenthSegment = SegmentFactory.createAndLinkSegment(theText, SpatioTemporalSegment.class);
		fifteenthSegment.getCoordinate().add(coordo1);
		fifteenthSegment.getCoordinate().add(coordo2);
		fifteenthSegment.getCoordinate().add(coordo3);
		fifteenthSegment.setTimestamp(5555);

		final DummySegment final6Segment = SegmentFactory.createAndLinkSegment(theText, DummySegment.class);

		final SpatialSegment eleventhSegment = SegmentFactory.createAndLinkSegment(theText, SpatialSegment.class);
		final Coordinate coordo4 = new Coordinate();
		coordo4.setX(0);
		coordo4.setY(3);
		eleventhSegment.getCoordinate().add(coordo1);
		eleventhSegment.getCoordinate().add(coordo4);
		eleventhSegment.getCoordinate().add(coordo3);

		final LinearSegment fourthSegment = SegmentFactory.createAndLinkSegment(theText, LinearSegment.class);
		fourthSegment.setStart(1);
		fourthSegment.setEnd(3);
		final DummySegment final7Segment = SegmentFactory.createAndLinkSegment(theText, DummySegment.class);

		final SpatialSegment ninethSegment = SegmentFactory.createAndLinkSegment(theText, SpatialSegment.class);
		ninethSegment.getCoordinate().add(coordo3);
		ninethSegment.getCoordinate().add(coordo2);

		final SpatialSegment twelfthSegment = SegmentFactory.createAndLinkSegment(theText, SpatialSegment.class);
		twelfthSegment.getCoordinate().add(coordo1);
		twelfthSegment.getCoordinate().add(coordo2);
		twelfthSegment.getCoordinate().add(coordo3);
		twelfthSegment.getCoordinate().add(coordo4);

		final DummySegment final8Segment = SegmentFactory.createAndLinkSegment(theText, DummySegment.class);


		Collections.sort(theText.getSegment(), new SegmentComparator());
		Assert.assertEquals(0, theText.getSegment().indexOf(firstSegment));
		Assert.assertEquals(1, theText.getSegment().indexOf(secondSegment));
		Assert.assertEquals(2, theText.getSegment().indexOf(thirdSegment));
		Assert.assertEquals(3, theText.getSegment().indexOf(fourthSegment));
		Assert.assertEquals(4, theText.getSegment().indexOf(fifthSegment));
		Assert.assertEquals(5, theText.getSegment().indexOf(sixthSegment));
		Assert.assertEquals(6, theText.getSegment().indexOf(seventhSegment));
		Assert.assertEquals(7, theText.getSegment().indexOf(eighthSegment));
		Assert.assertEquals(8, theText.getSegment().indexOf(ninethSegment));
		Assert.assertEquals(9, theText.getSegment().indexOf(tenthSegment));
		Assert.assertEquals(10, theText.getSegment().indexOf(eleventhSegment));
		Assert.assertEquals(11, theText.getSegment().indexOf(twelfthSegment));
		Assert.assertEquals(12, theText.getSegment().indexOf(thirteenthSegment));
		Assert.assertEquals(13, theText.getSegment().indexOf(fourteenthSegment));
		Assert.assertEquals(14, theText.getSegment().indexOf(fifteenthSegment));
		Assert.assertEquals(15, theText.getSegment().indexOf(sixteenthSegment));
		Assert.assertEquals(16, theText.getSegment().indexOf(seventeenthSegment));
		Assert.assertEquals(17, theText.getSegment().indexOf(eighteenthSegment));


		// finals 1, 2, and so on are at the end of the list , we don't know before (hashcode comparison for unknown types).
		Assert.assertTrue(theText.getSegment().indexOf(final1Segment) > 17);
		Assert.assertTrue(theText.getSegment().indexOf(final2Segment) > 17);
		Assert.assertTrue(theText.getSegment().indexOf(final3Segment) > 17);
		Assert.assertTrue(theText.getSegment().indexOf(final4Segment) > 17);
		Assert.assertTrue(theText.getSegment().indexOf(final5Segment) > 17);
		Assert.assertTrue(theText.getSegment().indexOf(final6Segment) > 17);
		Assert.assertTrue(theText.getSegment().indexOf(final7Segment) > 17);
		Assert.assertTrue(theText.getSegment().indexOf(final8Segment) > 17);
	}


	/**
	 * 
	 */
	@Test
	public void testSameSegments() {

		final SegmentComparator comparator = new SegmentComparator();

		final LinearSegment ls1 = new LinearSegment();
		ls1.setUri("weblab:ls1");
		ls1.setStart(12);
		ls1.setEnd(19);

		final LinearSegment ls2 = new LinearSegment();
		ls2.setUri("weblab:ls12");
		ls2.setStart(12);
		ls2.setEnd(19);

		Assert.assertEquals(0, comparator.compare(ls1, ls2));
		Assert.assertEquals(0, comparator.compare(ls1, ls1));

		final TemporalSegment ts1 = new TemporalSegment();
		ts1.setUri("weblab:ts1");
		ts1.setStart(12);
		ts1.setEnd(19);

		final TemporalSegment ts2 = new TemporalSegment();
		ts2.setUri("weblab:ts2");
		ts2.setStart(12);
		ts2.setEnd(19);

		Assert.assertEquals(0, comparator.compare(ts1, ts2));

		final SpatialSegment ss1 = new SpatialSegment();
		ss1.setUri("weblab:ss1");
		final Coordinate c11 = new Coordinate();
		c11.setX(1);
		c11.setY(2);
		final Coordinate c12 = new Coordinate();
		c12.setX(9);
		c12.setY(10);
		final Coordinate c13 = new Coordinate();
		c13.setX(1899);
		c13.setY(1033);
		ss1.getCoordinate().add(c11);
		ss1.getCoordinate().add(c12);
		ss1.getCoordinate().add(c13);

		final SpatialSegment ss2 = new SpatialSegment();
		ss2.setUri("weblab:ss2");
		final Coordinate c21 = new Coordinate();
		c21.setX(1);
		c21.setY(2);
		final Coordinate c22 = new Coordinate();
		c22.setX(9);
		c22.setY(10);
		final Coordinate c23 = new Coordinate();
		c23.setX(1899);
		c23.setY(1033);
		ss2.getCoordinate().add(c21);
		ss2.getCoordinate().add(c22);
		ss2.getCoordinate().add(c23);

		Assert.assertEquals(0, comparator.compare(ss1, ss2));
		Assert.assertEquals(0, comparator.compare(ss1, ss1));

		final SpatioTemporalSegment sts1 = new SpatioTemporalSegment();
		sts1.setUri("weblab:sts1");
		sts1.setTimestamp(50);
		sts1.getCoordinate().addAll(ss1.getCoordinate());

		final SpatioTemporalSegment sts2 = new SpatioTemporalSegment();
		sts2.setUri("weblab:sts2");
		sts2.setTimestamp(50);
		sts2.getCoordinate().addAll(ss2.getCoordinate());

		Assert.assertEquals(0, comparator.compare(sts1, sts2));
		Assert.assertEquals(0, comparator.compare(sts1, sts1));


		final TrackSegment tks1 = new TrackSegment();
		tks1.setUri("weblab:tks1");
		tks1.getSegment().add(ss1);
		tks1.getSegment().add(ts1);
		tks1.getSegment().add(sts1);
		tks1.getSegment().add(ls1);

		final TrackSegment tks2 = new TrackSegment();
		tks2.setUri("weblab:tks2");
		tks2.getSegment().add(ss2);
		tks2.getSegment().add(ts2);
		tks2.getSegment().add(sts2);
		tks2.getSegment().add(ls2);

		Assert.assertEquals(0, comparator.compare(tks1, tks2));
		Assert.assertEquals(0, comparator.compare(tks2, tks1));

		// Compare empty segments
		Assert.assertEquals(0, comparator.compare(new SpatialSegment(), new SpatialSegment()));
		Assert.assertEquals(0, comparator.compare(new TrackSegment(), new TrackSegment()));
	}


	/**
	 *
	 */
	public static class DummySegment2 extends DummySegment {


		private static final long serialVersionUID = 129L;

	}

}
