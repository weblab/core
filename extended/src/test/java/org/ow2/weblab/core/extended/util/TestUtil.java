/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Map;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.comparator.DummySegment;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.LowLevelDescriptor;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.SpatioTemporalSegment;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.TrackSegment;


/**
 *
 */
public class TestUtil {


	/**
	 * @throws Exception
	 *             If something unexpected occurs
	 */
	@Test
	public void testPoKUtil() throws Exception {

		final String myData = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"/>";

		Annotation annotation = new WebLabMarshaller(true).unmarshal(new File("src/test/resources/annotationTest.xml"), Annotation.class);
		String str1 = PoKUtil.getPoKData(annotation);
		Assert.assertTrue(str1.length() > 1);
		PoKUtil.setPoKData(annotation, str1);
		Assert.assertEquals(str1.replaceAll(">\\s+<", "><").trim(), PoKUtil.getPoKData(annotation).replaceAll(">\\s+<", "><").trim());

		annotation = new WebLabMarshaller(true).unmarshal(new File("src/test/resources/annotationTest2.xml"), Annotation.class);
		str1 = PoKUtil.getPoKData(annotation);
		Assert.assertTrue(str1.length() > 1);
		PoKUtil.setPoKData(annotation, str1);
		Assert.assertEquals(str1.replaceAll(">\\s+<", "><").trim(), PoKUtil.getPoKData(annotation).replaceAll(">\\s+<", "><").trim());

		annotation = new WebLabMarshaller(true).unmarshal(new File("src/test/resources/badAnnotationTest.xml"), Annotation.class);
		try {
			PoKUtil.getPoKData(annotation);
			Assert.fail("Exception should have been thrown.");
		} catch (final WebLabUncheckedException wlue) {
			// Bad RDF
			Assert.assertNotNull(wlue);
		}

		final PieceOfKnowledge pok = WebLabResourceFactory.createResource("d", "ddd", PieceOfKnowledge.class);
		Assert.assertEquals("", PoKUtil.getPoKData(pok));
		PoKUtil.setPoKData(pok, "                                                               \n\n\t" + myData);
		Assert.assertEquals(myData.replaceAll(">\\s+<", "><").trim(), PoKUtil.getPoKData(pok).replaceAll(">\\s+<", "><").trim());
		PoKUtil.setPoKData(pok, "\n\n" + myData);
		Assert.assertEquals(myData.replaceAll(">\\s+<", "><").trim(), PoKUtil.getPoKData(pok).replaceAll(">\\s+<", "><").trim());

		try {
			PoKUtil.setPoKData(pok, "<data><rdf></rdf></data>");
			Assert.fail("An exception should have been thrown.");
		} catch (final WebLabUncheckedException wlue) {
			// Bad RDF
			Assert.assertNotNull(wlue);
		}
		try {
			PoKUtil.setPoKData(pok, "<data>\n   <rdf></rdf></data>");
			Assert.fail("An exception should have been thrown.");
		} catch (final WebLabUncheckedException wlue) {
			// Bad RDF
			Assert.assertNotNull(wlue);
		}
		try {
			PoKUtil.setPoKData(pok, "<data>text node</data>");
			Assert.fail("An exception should have been thrown.");
		} catch (final WebLabUncheckedException wlue) {
			// Bad RDF
			Assert.assertNotNull(wlue);
		}
		try {
			PoKUtil.setPoKData(pok, myData + myData);
			Assert.fail("An exception should have been thrown.");
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
			PoKUtil.setPoKData(pok, myData + myData, false);
		}


		// This shall never be done !!!
		pok.setData(myData);
		try {
			PoKUtil.getPoKData(annotation);
			Assert.fail("An exception should have been thrown.");
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
			PoKUtil.setPoKData(pok, myData + myData, false);
		}

		// This neither !
		DOMResult domRes = new DOMResult();
		StringReader strW = new StringReader("<something><validXML/><butNotRDF/></something>");
		TransformerFactory.newInstance().newTransformer().transform(new StreamSource(strW), domRes);
		pok.setData(((org.w3c.dom.Document) domRes.getNode()).getDocumentElement());
		try {
			PoKUtil.getPoKData(annotation);
			Assert.fail("An exception should have been thrown.");
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
			PoKUtil.setPoKData(pok, myData + myData, false);
		}

		domRes = new DOMResult();
		strW = new StringReader("<emptyData />");
		TransformerFactory.newInstance().newTransformer().transform(new StreamSource(strW), domRes);
		pok.setData(((org.w3c.dom.Document) domRes.getNode()).getDocumentElement());
		Assert.assertEquals("", PoKUtil.getPoKData(pok));


		domRes = new DOMResult();
		strW = new StringReader("<data>toto</data>");
		TransformerFactory.newInstance().newTransformer().transform(new StreamSource(strW), domRes);
		pok.setData(((org.w3c.dom.Document) domRes.getNode()).getDocumentElement());
		try {
			PoKUtil.getPoKData(annotation);
			Assert.fail("An exception should have been thrown.");
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
			PoKUtil.setPoKData(pok, myData + myData, false);
		}
		domRes = new DOMResult();
		strW = new StringReader("<data>                                " + "\n" + "" + "" + "\n</data>");
		TransformerFactory.newInstance().newTransformer().transform(new StreamSource(strW), domRes);
		pok.setData(((org.w3c.dom.Document) domRes.getNode()).getDocumentElement());
		Assert.assertEquals("".replaceAll(">\\s+<", "><"), PoKUtil.getPoKData(pok).replaceAll(">\\s+<", "><"));

		final PieceOfKnowledge pok2 = new PieceOfKnowledge();
		Assert.assertEquals("", PoKUtil.getPoKData(pok2));

		pok2.setData(null);
		Assert.assertEquals("", PoKUtil.getPoKData(pok2));
	}


	/**
	 * @throws Exception
	 *             If something unexpected occurs
	 */
	@Test
	public void testLLDUtil() throws Exception {
		final String myData = "<xmlCustomInput><bla blabla=\"test\"/></xmlCustomInput>";

		LowLevelDescriptor lld = new WebLabMarshaller(true).unmarshal(new File("src/test/resources/lldTest.xml"), LowLevelDescriptor.class);
		String str1 = LLDUtil.getLLDData(lld);
		Assert.assertTrue(str1.length() > 1);
		LLDUtil.setLLDData(lld, str1);
		Assert.assertEquals(str1.replaceAll(">\\s+<", "><").trim(), LLDUtil.getLLDData(lld).replaceAll(">\\s+<", "><").trim());

		final LowLevelDescriptor lld2 = WebLabResourceFactory.createResource("d", "ddd", LowLevelDescriptor.class);
		Assert.assertEquals("", LLDUtil.getLLDData(lld2));
		LLDUtil.setLLDData(lld2, "                                                               \n\n\t" + myData);
		Assert.assertEquals(myData.replaceAll(">\\s+<", "><").trim(), LLDUtil.getLLDData(lld2).replaceAll(">\\s+<", "><").trim());
		LLDUtil.setLLDData(lld2, "\n\n" + myData);
		Assert.assertEquals(myData.replaceAll(">\\s+<", "><").trim(), LLDUtil.getLLDData(lld2).replaceAll(">\\s+<", "><").trim());

		try {
			LLDUtil.setLLDData(lld2, myData + " text node making the XML invalid!" + myData);
			Assert.fail("An exception should have been thrown.");
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
			LLDUtil.setLLDData(lld2, myData + " text node making the XML invalid!" + myData, false);
		}

		final LowLevelDescriptor lld3 = new LowLevelDescriptor();
		Assert.assertEquals("", LLDUtil.getLLDData(lld3));

		lld3.setData(null);
		Assert.assertEquals("", LLDUtil.getLLDData(lld3));
	}


	/**
	 * @throws WebLabCheckedException
	 *             If the marshaller fails
	 * @throws IOException
	 *             If an IO occurs
	 *
	 */
	@Test
	public void testResourceUtil() throws WebLabCheckedException, IOException {
		final Document doc = WebLabResourceFactory.createResource("test", "res", Document.class);

		final Text text = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		WebLabResourceFactory.createAndLinkMediaUnit(doc, Image.class);

		WebLabResourceFactory.createAndLinkAnnotation(text);
		WebLabResourceFactory.createAndLinkLowLevelDescriptor(text);

		try (final FileWriter fw = new FileWriter(new File("target/temp.txt"))) {
			ResourceUtil.printTree(doc, fw);
		}

		Assert.assertEquals(0, ResourceUtil.getSelectedSubResources(doc, Document.class).size());
		Assert.assertEquals(2, ResourceUtil.getSelectedSubResources(doc, Text.class).size());
		Assert.assertEquals(5, ResourceUtil.getSubResources(doc).size());

		final Map<Text, Resource> theMap = ResourceUtil.getSelectedSubResourcesMap(doc, Text.class);
		Assert.assertEquals(2, theMap.size()); // mu1, mu2.
		Assert.assertEquals(1, new HashSet<>(theMap.values()).size()); // doc.

		final String xml = ResourceUtil.saveToXMLString(doc);
		final Resource res = ResourceUtil.loadFromXMLString(xml);
		if (res instanceof Document) {
			final Document doc1 = (Document) res;
			Assert.assertEquals(doc.getAnnotation().size(), doc1.getAnnotation().size());
			Assert.assertEquals(doc.getMediaUnit().size(), doc1.getMediaUnit().size());
			Assert.assertEquals(doc.getUri(), doc1.getUri());
		} else {
			Assert.fail("Unable to load from previouly generated xml");
		}

		final ResultSet rs = WebLabResourceFactory.createResource("test", "rs", ResultSet.class);
		final ResultSet emptyRs = WebLabResourceFactory.createResource("test", "emptyRs", ResultSet.class);
		rs.setPok(WebLabResourceFactory.createResource("test", "pok", PieceOfKnowledge.class));
		final ComposedResource cr = WebLabResourceFactory.createResource("test", "cr", ComposedResource.class);
		final ComposedQuery cq = WebLabResourceFactory.createResource("test", "cq", ComposedQuery.class);
		final SimilarityQuery sq = WebLabResourceFactory.createResource("test", "sq", SimilarityQuery.class);
		sq.getResource().add(text);
		cq.getQuery().add(sq);
		cr.getResource().add(cq);
		rs.getResource().add(cr);
		rs.getResource().add(emptyRs);
		rs.getResource().add(WebLabResourceFactory.createResource("test", "emptyDoc", Document.class));

		Assert.assertEquals(9, ResourceUtil.getSubResources(rs).size());
	}


	/**
	 *
	 */
	@Test
	public void testSegmentUtil() {
		final Document doc = WebLabResourceFactory.createResource("azert", "yuiop", Document.class);
		final Text theText = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		theText.setContent("Another wonderful text for tests");

		final LinearSegment ls_0_5 = SegmentFactory.createAndLinkLinearSegment(theText, 0, 5);

		final TemporalSegment ts_0_5 = SegmentFactory.createAndLinkTemporalSegment(theText, 0, 5);

		final LinearSegment ls_1_2 = SegmentFactory.createAndLinkLinearSegment(theText, 1, 2);

		final int[] shape_0_1_3_6 = { 0, 1, 3, 6 };
		final SpatialSegment ss_0_1_3_6 = SegmentFactory.createAndLinkSpatialSegment(theText, shape_0_1_3_6);

		final SpatioTemporalSegment sts_10_0_1_3_6 = SegmentFactory.createAndLinkSpatioTemporalSegment(theText, 10, shape_0_1_3_6);
		final SpatioTemporalSegment sts_12_0_1_3_6 = SegmentFactory.createAndLinkSpatioTemporalSegment(theText, 12, shape_0_1_3_6);

		final TemporalSegment ts_0_6 = SegmentFactory.createAndLinkTemporalSegment(theText, 0, 6);

		final TemporalSegment ts_1_2 = SegmentFactory.createAndLinkTemporalSegment(theText, 1, 2);

		final LinearSegment ls_0_6 = SegmentFactory.createAndLinkLinearSegment(theText, 0, 6);

		final int[] shape_3_6_0_1 = { 3, 6, 0, 1 };
		final SpatialSegment ss_3_6_0_1 = SegmentFactory.createAndLinkSpatialSegment(theText, shape_3_6_0_1);

		final SpatioTemporalSegment sts_10_3_6_0_1 = SegmentFactory.createAndLinkSpatioTemporalSegment(theText, 10, shape_3_6_0_1);

		final TrackSegment tr1 = SegmentFactory.createAndLinkSegment(theText, TrackSegment.class);
		final TrackSegment tr2 = SegmentFactory.createAndLinkSegment(theText, TrackSegment.class);

		final DummySegment dummy1 = SegmentFactory.createAndLinkSegment(theText, DummySegment.class);
		final DummySegment dummy2 = SegmentFactory.createAndLinkSegment(theText, DummySegment.class);


		// Test same object
		Assert.assertTrue(SegmentUtil.isIncluded(ls_0_6, ls_0_6));

		// Test null input
		Assert.assertFalse(SegmentUtil.isIncluded(null, ls_0_6));
		Assert.assertFalse(SegmentUtil.isIncluded(ls_0_6, null));

		// Test isIncluded every type against other
		Assert.assertFalse(SegmentUtil.isIncluded(ls_0_6, ts_0_5));
		Assert.assertFalse(SegmentUtil.isIncluded(ls_0_5, ss_0_1_3_6));
		Assert.assertFalse(SegmentUtil.isIncluded(ls_0_5, sts_10_0_1_3_6));
		Assert.assertFalse(SegmentUtil.isIncluded(ls_0_5, tr1));

		Assert.assertFalse(SegmentUtil.isIncluded(ts_0_6, ls_0_5));
		Assert.assertFalse(SegmentUtil.isIncluded(ts_0_5, ss_0_1_3_6));
		Assert.assertFalse(SegmentUtil.isIncluded(ts_0_5, sts_10_3_6_0_1));
		Assert.assertFalse(SegmentUtil.isIncluded(ts_0_5, tr1));

		Assert.assertFalse(SegmentUtil.isIncluded(ss_0_1_3_6, ls_0_5));
		Assert.assertFalse(SegmentUtil.isIncluded(ss_0_1_3_6, ts_0_5));
		Assert.assertFalse(SegmentUtil.isIncluded(ss_0_1_3_6, sts_10_0_1_3_6));
		Assert.assertFalse(SegmentUtil.isIncluded(ss_0_1_3_6, tr1));

		Assert.assertFalse(SegmentUtil.isIncluded(sts_10_0_1_3_6, ls_0_5));
		Assert.assertFalse(SegmentUtil.isIncluded(sts_10_0_1_3_6, ts_0_5));
		Assert.assertFalse(SegmentUtil.isIncluded(sts_10_0_1_3_6, ss_0_1_3_6));
		Assert.assertFalse(SegmentUtil.isIncluded(sts_10_0_1_3_6, tr1));

		Assert.assertFalse(SegmentUtil.isIncluded(tr1, ls_0_5));
		Assert.assertFalse(SegmentUtil.isIncluded(tr1, ts_0_5));
		Assert.assertFalse(SegmentUtil.isIncluded(tr1, ss_0_1_3_6));
		Assert.assertFalse(SegmentUtil.isIncluded(tr1, sts_10_0_1_3_6));


		/*
		 * Test implemented comparisons
		 */
		Assert.assertFalse(SegmentUtil.isIncluded(ls_0_6, ls_0_5));
		Assert.assertFalse(SegmentUtil.isIncluded(ls_0_5, ls_1_2));
		Assert.assertTrue(SegmentUtil.isIncluded(ls_0_5, ls_0_6));
		Assert.assertFalse(SegmentUtil.isIncluded(ts_0_6, ts_1_2));
		Assert.assertFalse(SegmentUtil.isIncluded(ts_0_6, ts_0_5));
		Assert.assertTrue(SegmentUtil.isIncluded(ts_1_2, ts_0_5));
		Assert.assertFalse(SegmentUtil.isIncluded(tr1, tr2));
		Assert.assertFalse(SegmentUtil.isIncluded(sts_12_0_1_3_6, sts_10_3_6_0_1));

		/*
		 * Test not implemented comparisons
		 */
		try {
			SegmentUtil.isIncluded(ss_0_1_3_6, ss_3_6_0_1);
			Assert.fail("An exception should have been thrown before.");
		} catch (final WebLabUncheckedException wlce) {
			Assert.assertNotNull(wlce);
		}
		try {
			SegmentUtil.isIncluded(sts_10_0_1_3_6, sts_10_3_6_0_1);
			Assert.fail("An exception should have been thrown before.");
		} catch (final WebLabUncheckedException wlce) {
			Assert.assertNotNull(wlce);
		}
		try {
			SegmentUtil.isIncluded(dummy1, dummy2);
			Assert.fail("An exception should have been thrown before.");
		} catch (final WebLabUncheckedException wlce) {
			Assert.assertNotNull(wlce);
		}

	}


	/**
	 * @throws WebLabCheckedException
	 *             If the segment util test fails
	 *
	 */
	@Test
	public void testTextUtil() throws WebLabCheckedException {
		final Document doc = WebLabResourceFactory.createResource("test", "doc12", Document.class);
		final Text text = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final Text text2 = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final Text text3 = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);

		text.setContent("This is my wonderful test Text");
		text2.setContent("This is another wonderful test Text");

		final LinearSegment ls1 = SegmentFactory.createAndLinkSegment(text, LinearSegment.class);
		ls1.setStart(0);
		ls1.setEnd(3);

		final LinearSegment ls2 = SegmentFactory.createAndLinkSegment(text, LinearSegment.class);
		ls2.setStart(4);
		ls2.setEnd(10);

		final LinearSegment ls3 = SegmentFactory.createAndLinkSegment(text, LinearSegment.class);
		ls3.setStart(5);
		ls3.setEnd(222);

		final LinearSegment ls = SegmentFactory.createAndLinkSegment(text3, LinearSegment.class);
		ls.setStart(0);
		ls.setEnd(0);

		Assert.assertEquals(text.getContent().substring(ls1.getStart(), ls1.getEnd()), TextUtil.getSegmentText(text, ls1));
		Assert.assertNotNull(TextUtil.getSegmentText(text, ls2));

		try {
			TextUtil.getSegmentText(text2, ls1);
			Assert.fail("Error should have been thrown");
		} catch (final WebLabCheckedException wlce) {
			Assert.assertNotNull(wlce);
		}
		try {
			TextUtil.getSegmentText(text, ls3);
			Assert.fail("Error should have been thrown");
		} catch (final WebLabCheckedException wlce) {
			Assert.assertNotNull(wlce);
		}
		try {
			TextUtil.getSegmentText(null, ls3);
			Assert.fail("Error should have been thrown");
		} catch (final WebLabCheckedException wlce) {
			Assert.assertNotNull(wlce);
		}
		try {
			TextUtil.getSegmentText(text, null);
			Assert.fail("Error should have been thrown");
		} catch (final WebLabCheckedException wlce) {
			Assert.assertNotNull(wlce);
		}
		try {
			text.setContent(null);
			TextUtil.getSegmentText(text, ls1);
			Assert.fail("Error should have been thrown");
		} catch (final WebLabCheckedException wlce) {
			Assert.assertNotNull(wlce);
		}
		try {
			TextUtil.getSegmentText(text, ls);
			Assert.fail("Error should have been thrown");
		} catch (final WebLabCheckedException wlce) {
			Assert.assertNotNull(wlce);
		}
	}


	@Test(expected = WebLabUncheckedException.class)
	public void testBadAnnotation2() throws Exception {
		final PieceOfKnowledge pok = new WebLabMarshaller(false).unmarshal(new File("src/test/resources/badAnnotationTest2.xml"), PieceOfKnowledge.class);
		PoKUtil.getPoKData(pok);
	}


	/**
	 * @throws Exception
	 *             If someting unexpected appear
	 */
	@Test
	public void testIntro() throws Exception {
		try {
			for (final Constructor<?> c : PoKUtil.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}
		try {
			for (final Constructor<?> c : ResourceUtil.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}
		try {
			for (final Constructor<?> c : SegmentUtil.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}
		try {
			for (final Constructor<?> c : TextUtil.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}
		try {
			for (final Constructor<?> c : LLDUtil.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}
		try {
			for (final Constructor<?> c : DOMUtil.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}
	}

}
