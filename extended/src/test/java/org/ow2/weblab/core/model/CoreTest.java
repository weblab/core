/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.model;

import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.Marshaller;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.LLDUtil;


/**
 * @author Cassidian WebLab Team
 */
public class CoreTest {


	private static Document DOC = WebLabResourceFactory.createResource("testRef", "testDocument", Document.class);


	private final WebLabMarshaller wlm = new WebLabMarshaller(true);


	/**
	 * @throws WebLabCheckedException
	 *             If the marshaller fails
	 */
	@Test
	public void testSegment() throws WebLabCheckedException {
		final Text t = WebLabResourceFactory.createAndLinkMediaUnit(CoreTest.DOC, Text.class);
		t.setContent("Yet another boring text " + "with strange ç 新 闻网 页贴 吧知 道MP3图 片 € characters.");

		final Image i = WebLabResourceFactory.createAndLinkMediaUnit(CoreTest.DOC, Image.class);

		final LinearSegment seg1 = SegmentFactory.createAndLinkSegment(t, LinearSegment.class);
		seg1.setEnd(0);
		seg1.setEnd(2);

		final LinearSegment seg2 = SegmentFactory.createAndLinkSegment(t, LinearSegment.class);
		seg2.setEnd(3);
		seg2.setEnd(7);

		final SpatialSegment spatPos = SegmentFactory.createAndLinkSegment(i, SpatialSegment.class);

		final Coordinate coo1 = new Coordinate();
		coo1.setX(-5);
		coo1.setY(8);
		spatPos.getCoordinate().add(coo1);

		final Coordinate coo2 = new Coordinate();
		coo2.setX(12);
		coo2.setY(-7);
		spatPos.getCoordinate().add(coo2);

		this.wlm.marshalResource(CoreTest.DOC, new StringWriter());
	}


	/**
	 * @throws WebLabCheckedException
	 *             If the marshaller fails
	 */
	@Test
	public void testApp2() throws WebLabCheckedException {
		final Audio audio = WebLabResourceFactory.createAndLinkMediaUnit(CoreTest.DOC, Audio.class);
		// audio.setContent("file:///test.mp3");
		final Video video = WebLabResourceFactory.createAndLinkMediaUnit(CoreTest.DOC, Video.class);
		// video.setContent("file:///test.mpeg");

		StringWriter sw = new StringWriter();

		this.wlm.marshalResource(audio, sw);

		Assert.assertNotNull(sw.toString());
		Assert.assertFalse(sw.toString().equals(""));

		sw = new StringWriter();
		this.wlm.marshalResource(video, sw);
		Assert.assertNotNull(sw.toString());
		Assert.assertFalse(sw.toString().equals(""));

		sw = new StringWriter();

		this.wlm.marshalResource(CoreTest.DOC, sw);

		Assert.assertNotNull(sw.toString());
		Assert.assertFalse(sw.toString().equals(""));
	}


	/**
	 * @throws WebLabCheckedException
	 *             If the marshaller fails
	 */
	@Test
	public void testLowLevelDescriptor() throws WebLabCheckedException {
		this.wlm.setMarshallerProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		final LowLevelDescriptor lowLevelTest = new LowLevelDescriptor();
		lowLevelTest.setUri("weblab://lowleveltest/test");
		LLDUtil.setLLDData(lowLevelTest, "");

		final StringWriter sw = new StringWriter();

		this.wlm.marshalResource(lowLevelTest, sw);

		Assert.assertNotNull(sw.toString());
		Assert.assertFalse(sw.toString().equals(""));

		Assert.assertNotNull(lowLevelTest.getData());

		this.wlm.unmarshal(new File("src/test/resources/resourceTest.xml"), Resource.class).getClass();

		final LowLevelDescriptor lowLevelDescriptor = this.wlm.unmarshal(new File("src/test/resources/resourceTest.xml"), LowLevelDescriptor.class);

		Assert.assertNotNull(lowLevelDescriptor);
	}
}
