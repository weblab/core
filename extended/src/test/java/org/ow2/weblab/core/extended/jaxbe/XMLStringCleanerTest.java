/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.jaxbe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.jaxb.XMLStringCleaner;
import org.ow2.weblab.core.model.Text;

/**
 * @author Cassidian WebLab Team
 */
public class XMLStringCleanerTest {


	/**
	 * @throws IOException
	 *             If a problem occurs during temproary file creation
	 * @throws WebLabCheckedException
	 *             If a problem occurs during marshalling/unmarshalling
	 */
	@Test
	public void testIsValidXML() throws IOException, WebLabCheckedException {
		final WebLabMarshaller wlm = new WebLabMarshaller(true);
		Assert.assertTrue(XMLStringCleaner.isXMLValid('\n'));
		Assert.assertTrue(XMLStringCleaner.isXMLValid('\r'));
		Assert.assertTrue(XMLStringCleaner.isXMLValid('\u0020'));
		Assert.assertTrue(XMLStringCleaner.isXMLRecommended('\r'));
		Assert.assertTrue(XMLStringCleaner.isXMLRecommended('\u0020'));
		// \f == == ASCII char FF and is not allowed in XML
		Assert.assertFalse(XMLStringCleaner.isXMLValid('\u000c'));
		Assert.assertFalse(XMLStringCleaner.isXMLRecommended('\u000c'));
		Assert.assertFalse(XMLStringCleaner.isXMLRecommended('\u007F'));
		Assert.assertTrue(XMLStringCleaner.isXMLValid('\u007F'));
		Assert.assertTrue(XMLStringCleaner.isXMLValid('\u0021'));
		Assert.assertFalse(XMLStringCleaner.isXMLValid('\uD800'));
		Assert.assertFalse(XMLStringCleaner.isXMLRecommended('\uFFFF'));
		Assert.assertFalse(XMLStringCleaner.isXMLRecommended('\uFDDF'));
		Assert.assertTrue(XMLStringCleaner.isXMLRecommended('\uFFFC'));
		Assert.assertFalse(XMLStringCleaner.isXMLRecommended('\u0080'));
		Assert.assertFalse(XMLStringCleaner.isXMLRecommended('\u0087'));
		Assert.assertFalse(XMLStringCleaner.isXMLRecommended('\u009F'));
		Assert.assertTrue(XMLStringCleaner.isXMLRecommended('\u0085'));


		String string = "";
		Assert.assertEquals(string, XMLStringCleaner.getXMLValidString(string));
		string = "This is a simple string with allowed chars \n\r\t";
		Assert.assertEquals(string, XMLStringCleaner.getXMLValidString(string));
		Assert.assertEquals(string, XMLStringCleaner.getXMLValidString(string + "\f"));
		string = string + "\f";
		final Text text1 = WebLabResourceFactory.createResource("text", "1", Text.class);
		text1.setContent(string);
		File temp = null;
		try {
			temp = File.createTempFile("ddd", ".txt");
			wlm.marshalResource(text1, temp);
			wlm.unmarshal(temp, Text.class);
			Assert.fail();
		} catch (final Exception e) {
			Assert.assertNotNull(e);
		}
		if ((temp != null) && !temp.delete()) {
			temp.deleteOnExit();
		}

		temp = File.createTempFile("ddd", ".txt");
		try (final OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(temp))) {
			wlm.marshalResource(text1, osw, true);
			wlm.unmarshal(temp, Text.class);
		} finally {
			if (!temp.delete()) {
				temp.deleteOnExit();
			}
		}

		Assert.assertEquals("", XMLStringCleaner.getXMLRecommendedString(""));
		Assert.assertEquals(null, XMLStringCleaner.getXMLRecommendedString(null));
		Assert.assertEquals(null, XMLStringCleaner.getXMLValidString(null));
		Assert.assertEquals("", XMLStringCleaner.getXMLValidString(""));
		Assert.assertEquals("a", XMLStringCleaner.getXMLRecommendedString("\f", 'a'));
		Assert.assertEquals("aa", XMLStringCleaner.getXMLValidString("a\f", 'a'));
	}


	/**
	 *
	 */
	@Test
	public void testIntro() {
		try {
			for (final Constructor<?> c : XMLStringCleaner.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		} catch (final Exception e) {
			e.printStackTrace();
			Assert.fail("An unexpected exception occurs.");
		}
	}

}
