/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.jaxbe.test;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;
import javax.xml.bind.helpers.AbstractMarshallerImpl;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;


/**
 *
 */
public class DummyMarchaller extends AbstractMarshallerImpl {


	private int action = 3; // 1 close stream


	/**
	 * @param action
	 *            ??
	 */
	public DummyMarchaller(final int action) {
		this.action = action;
	}


	@Override
	public void marshal(final Object jaxbElement, final Result result) throws JAXBException {
		if (this.action == 3) {
			throw new JAXBException("testing ... ");
		}
		if (result instanceof StreamResult) {
			final StreamResult sresult = (StreamResult) result;
			try {
				sresult.getWriter().close();
				@SuppressWarnings("resource")
				final Writer w = sresult.getWriter();
				if (w instanceof OutputStreamWriter) {
					final OutputStreamWriter osws = (OutputStreamWriter) w;
					osws.close();
				}

			} catch (final IOException ioe) {
				throw new JAXBException(ioe);
			}
		}
	}


	@Override
	public void setProperty(final String name, final Object value) throws PropertyException {
		throw new PropertyException("testing purposes.");
	}


	@Override
	public void setSchema(final Schema schema) {
		// Nothing to do
	}


}
