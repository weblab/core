/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 */
public class TestInThread {


	/**
	 * @throws Exception
	 *             If an error happens in on of the threads
	 */
	@Test
	public void testInThread() throws Exception {
		final int nbThread = 1;
		final int maxIteration = 5;

		final List<Callable<Boolean>> callables = new ArrayList<>(nbThread);
		for (int k = 0; k < nbThread; k++) {
			callables.add(new FactoryTestThread(maxIteration));
		}

		final ExecutorService executor = Executors.newFixedThreadPool(nbThread);
		final List<Future<Boolean>> returned = executor.invokeAll(callables, 50, TimeUnit.MINUTES);
		for (final Future<Boolean> future : returned) {
			Assert.assertTrue(future.isDone());
			Assert.assertFalse(future.isCancelled());
			try {
				Assert.assertTrue(future.get().booleanValue());
			} catch (final ExecutionException ee) {
				throw ee;
			}
		}
		executor.shutdown();
	}

}
