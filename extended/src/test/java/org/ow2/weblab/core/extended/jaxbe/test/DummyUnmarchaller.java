/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.jaxbe.test;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;
import javax.xml.bind.UnmarshallerHandler;
import javax.xml.bind.helpers.AbstractUnmarshallerImpl;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.validation.Schema;

import org.ow2.weblab.core.model.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;


/**
 *
 */
public class DummyUnmarchaller extends AbstractUnmarshallerImpl {


	@Override
	protected Object unmarshal(final XMLReader reader, final InputSource source) throws JAXBException {
		return new JAXBElement<>(new QName("my", "string"), String.class, "value");
	}


	@SuppressWarnings("unchecked")
	@Override
	public <T> JAXBElement<T> unmarshal(final Source source, final Class<T> expectedType) throws JAXBException {
		return (JAXBElement<T>) new JAXBElement<>(new QName("my", "string"), Document.class, new Document());
	}


	@Override
	public UnmarshallerHandler getUnmarshallerHandler() {
		return null;
	}


	@Override
	public Object unmarshal(final Node node) throws JAXBException {
		return null;
	}


	@Override
	public void setProperty(final String name, final Object value) throws PropertyException {
		// Normal
	}


	@Override
	public void setSchema(final Schema schema) {
		// Nothing to do
	}

}
