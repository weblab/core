/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.jaxbe;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;


/**
 */
public class XMLNSTest {


	final WebLabMarshaller wlm = new WebLabMarshaller(true);


	/**
	 * @throws WebLabCheckedException
	 *             If a problem occurs during marshalling/unmarshalling
	 * @throws IOException
	 *             If a problem occurs while creating a temporary file
	 */
	@Test
	public void testXMLNSDeclareInside() throws WebLabCheckedException, IOException {
		Document doc = this.wlm.unmarshal(new File("src/test/resources/testXMLNS.xml"), Document.class);
		Assert.assertEquals(1, doc.getAnnotation().size());
		Assert.assertNotNull(doc.getAnnotation().get(0).getData());
		final File temp = File.createTempFile("temp", "test", new File("target"));
		this.wlm.marshalResource(doc, temp);
		doc = this.wlm.unmarshal(temp, Document.class);
		Assert.assertNotNull(doc.getAnnotation().get(0).getData());

	}
}
