/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.jaxbe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Coordinate;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.Video;


/**
 *
 */
public class MarshallerTest {


	final Document doc;


	final Document badDoc;


	final File file = new File("src/test/resources/weblabDocument.xml");


	final WebLabMarshaller wlm;


	final WebLabMarshaller wlm2;


	/**
	 * @throws WebLabCheckedException
	 *             If the marshaller fails
	 */
	public MarshallerTest() throws WebLabCheckedException {
		this.wlm = new WebLabMarshaller();
		this.wlm2 = new WebLabMarshaller(true);
		this.wlm.setMarshallerProperty("", "");
		this.wlm.setUnmarshallerProperty("", "");
		this.doc = this.wlm.unmarshal(this.file, Document.class);
		this.badDoc = new Document();
	}


	/**
	 * @throws IOException
	 *             If the creation of a temporary file fails
	 * @throws WebLabCheckedException
	 *             If the marshaller fails
	 */
	@Test
	public void testMarshalers() throws IOException, WebLabCheckedException {
		final File temp = File.createTempFile("temp", ".xml");
		this.wlm.marshalResource(this.doc, temp);

		Document doc2 = (Document) this.wlm.unmarshal(temp, MediaUnit.class);
		this.compareWithDoc(doc2);

		this.wlm2.marshalResource(this.doc, temp);

		doc2 = (Document) this.wlm2.unmarshal(temp, MediaUnit.class);



		try (final FileOutputStream fos = new FileOutputStream(temp); final OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");) {
			this.wlm.marshalResource(this.doc, osw, true);
		}
		// We also test when using a super class of the final result
		doc2 = (Document) this.wlm.unmarshal(temp, MediaUnit.class);
		this.compareWithDoc(doc2);

		doc2 = (Document) this.wlm2.unmarshal(temp, MediaUnit.class);
		this.compareWithDoc(doc2);

		doc2 = this.wlm.unmarshal(temp, Document.class);
		this.compareWithDoc(doc2);

		doc2 = this.wlm2.unmarshal(temp, Document.class);
		this.compareWithDoc(doc2);

		try (final FileOutputStream fos = new FileOutputStream(temp)) {
			this.wlm.marshalResource(this.doc, fos);
		}
		doc2 = (Document) this.wlm.unmarshal(temp, Resource.class);
		this.compareWithDoc(doc2);
		doc2 = (Document) this.wlm2.unmarshal(temp, Resource.class);
		this.compareWithDoc(doc2);

		StringWriter sw = new StringWriter();
		this.wlm.marshalResource(doc2, sw, true);
		doc2 = this.wlm.unmarshal(new StringReader(sw.toString()), Document.class);
		this.compareWithDoc(doc2);
		doc2 = this.wlm2.unmarshal(new StringReader(sw.toString()), Document.class);
		this.compareWithDoc(doc2);

		sw = new StringWriter();
		this.wlm.marshalResource(doc2, sw, true);
		doc2 = this.wlm.unmarshal(new StringReader(sw.toString()), Document.class);
		this.compareWithDoc(doc2);
		doc2 = this.wlm2.unmarshal(new StringReader(sw.toString()), Document.class);
		this.compareWithDoc(doc2);

		this.wlm.marshalResource(doc2, temp);
		doc2 = this.wlm.unmarshal(temp, Document.class);
		this.compareWithDoc(doc2);
		doc2 = this.wlm2.unmarshal(temp, Document.class);
		this.compareWithDoc(doc2);

		this.wlm.marshalResource(doc2, temp);
		doc2 = this.wlm.unmarshal(temp, Document.class);
		this.compareWithDoc(doc2);
		doc2 = this.wlm2.unmarshal(temp, Document.class);
		this.compareWithDoc(doc2);

		try (final FileOutputStream fos = new FileOutputStream(temp)) {
			this.wlm.marshalResource(doc2, fos);
		}
		doc2 = this.wlm.unmarshal(temp, Document.class);
		this.compareWithDoc(doc2);
		doc2 = this.wlm2.unmarshal(temp, Document.class);
		this.compareWithDoc(doc2);

		try (final FileOutputStream fos = new FileOutputStream(temp)) {
			this.wlm.marshalResource(doc2, fos);
		}
		doc2 = this.wlm.unmarshal(temp, Document.class);
		this.compareWithDoc(doc2);
		doc2 = this.wlm2.unmarshal(temp, Document.class);
		this.compareWithDoc(doc2);

		if (!temp.delete()) {
			temp.deleteOnExit();
		}
		final StringWriter writer = new StringWriter();

		this.wlm.marshalResource(this.doc, writer);
		final String xml = writer.toString();
		Assert.assertNotNull(xml);

		doc2 = this.wlm.unmarshal(new StringReader(xml), Document.class);
		this.compareWithDoc(doc2);
		doc2 = this.wlm2.unmarshal(new StringReader(xml), Document.class);
		this.compareWithDoc(doc2);

		doc2 = (Document) this.wlm.unmarshal(new StringReader(xml), Resource.class);
		this.compareWithDoc(doc2);
		doc2 = (Document) this.wlm2.unmarshal(new StringReader(xml), Resource.class);
		this.compareWithDoc(doc2);

		doc2 = (Document) this.wlm.unmarshal(new StringReader(xml), MediaUnit.class);
		this.compareWithDoc(doc2);
		doc2 = (Document) this.wlm2.unmarshal(new StringReader(xml), MediaUnit.class);
		this.compareWithDoc(doc2);

		this.wlm.marshal(doc2, new StringWriter(), new QName("test"));
		this.wlm.marshal(doc2, new StringWriter(), "test");
	}


	@Test
	public void testMarshallerBadWithoutValid() throws IOException, WebLabCheckedException {
		final File temp = File.createTempFile("temp", "xml");
		this.wlm.marshalResource(this.badDoc, temp);
		final Document badDocReloaded = this.wlm.unmarshal(temp, Document.class);
		Assert.assertNull(badDocReloaded.getUri());
		Assert.assertFalse(badDocReloaded.isSetAnnotation());
		Assert.assertFalse(badDocReloaded.isSetDescriptor());
		Assert.assertFalse(badDocReloaded.isSetMediaUnit());
		Assert.assertFalse(badDocReloaded.isSetSegment());
	}


	@Test(expected = WebLabCheckedException.class)
	public void testMarshallBadWithValid() throws IOException, WebLabCheckedException {
		final File temp = File.createTempFile("temp", "xml");
		this.wlm2.marshalResource(this.badDoc, temp);
	}


	@Test(expected = WebLabCheckedException.class)
	public void testUnmarshallBadWithValid() throws IOException, WebLabCheckedException {
		final File temp = File.createTempFile("temp", "xml");
		try {
			this.wlm.marshalResource(this.badDoc, temp);
		} catch (final Exception e) {
			Assert.fail("Exception should be thrown later..." + e.getMessage());
		}
		this.wlm2.unmarshal(temp, Document.class); // Should throw an exception
	}



	/**
	 * @throws IOException
	 *             Should not appear
	 * @throws WebLabCheckedException
	 *             If the marshaller fails
	 */
	@Test
	public void testMarshalerAndPackages() throws IOException, WebLabCheckedException {
		// The rc should contain (or the resource contained by the rc should contain) a least a class by package for all packages marshalable.
		final ComposedResource cr1 = WebLabResourceFactory.createResource("test", "cr1", ComposedResource.class);
		// doc contains Document, Annotation, LinearSegment, Text
		cr1.getResource().add(this.doc);

		final Document docVid = WebLabResourceFactory.createResource("test", "docVid", Document.class);
		final Video vid = WebLabResourceFactory.createAndLinkMediaUnit(docVid, Video.class);

		final SpatialSegment seg = SegmentFactory.createAndLinkSegment(vid, SpatialSegment.class);

		final Coordinate coordo1 = new Coordinate();
		coordo1.setX(12);
		coordo1.setY(5);
		seg.getCoordinate().add(coordo1);

		final Coordinate coordo2 = new Coordinate();
		coordo2.setX(3);
		coordo2.setY(6);
		seg.getCoordinate().add(coordo2);

		// docVid contains video, spatial segment and coordinate
		cr1.getResource().add(docVid);

		final File temp = File.createTempFile("tempRC", ".xml");
		try (final FileOutputStream fos = new FileOutputStream(temp)) {
			this.wlm.marshalResource(cr1, fos);
		}
		ComposedResource rc2;
		try (final FileInputStream fis = new FileInputStream(temp)) {
			rc2 = this.wlm.unmarshal(fis, ComposedResource.class);
		}
		if (!temp.delete()) {
			temp.deleteOnExit();
		}

		Assert.assertEquals(cr1.getResource().size(), rc2.getResource().size());
		final Iterator<Resource> it1 = cr1.getResource().iterator();
		final Iterator<Resource> it2 = rc2.getResource().iterator();
		while (it1.hasNext()) {
			MarshallerTest.compare(it1.next(), it2.next());
		}
	}



	@Test(expected = WebLabCheckedException.class)
	public void testWriteInFolderAsFile() throws Exception {
		this.wlm.marshalResource(this.doc, new File("target"));
	}


	@Test(expected = WebLabCheckedException.class)
	public void testWriteInDirtyStream() throws Exception {
		try (final FailOutputStream stream = new FailOutputStream()) {
			this.wlm.marshalResource(this.doc, stream);
		}
	}


	@Test(expected = WebLabCheckedException.class)
	public void testWriteInDirtyWriter() throws Exception {
		try (final FailOutputStreamWriter writer = new FailOutputStreamWriter()) {
			this.wlm.marshalResource(this.doc, writer);
		}
	}


	private void compareWithDoc(final Document doc2) {
		if (doc2 != null) {
			Assert.assertEquals(this.doc.getClass(), doc2.getClass());
			Assert.assertEquals(this.doc.getAnnotation().size(), doc2.getAnnotation().size());
			Assert.assertEquals(this.doc.getMediaUnit().size(), doc2.getMediaUnit().size());
			Assert.assertEquals(this.doc.getSegment().size(), doc2.getSegment().size());
			Assert.assertEquals(this.doc.getUri(), doc2.getUri());
			// If weblabDocument changes it might fail !!!
			Assert.assertEquals(this.doc.getMediaUnit().get(0).getSegment().size(), this.doc.getMediaUnit().get(0).getSegment().size());

			Assert.assertFalse(this.doc.getUri().equals(doc2.getMediaUnit().get(0).getUri()));

		} else {
			Assert.fail("Document loading was null");
		}
	}


	private static void compare(final Resource res1, final Resource res2) {
		Assert.assertEquals(res1.getClass(), res2.getClass());
		Assert.assertEquals(res1.getUri(), res2.getUri());
	}


	/**
	 * @throws InterruptedException
	 *             If a problem appears during threaded test
	 */
	@Test
	public void testThreadMarshaller() throws InterruptedException {
		final int nbThread = 10;
		final int nbPerThread = 5;
		final List<ThreadMarhaller> marshallerList = new ArrayList<>(nbThread * 2);
		for (int i = 0; i < nbThread; i++) {
			marshallerList.add(new ThreadMarhaller(this.doc, nbPerThread));
			marshallerList.add(new ThreadMarhaller(this.file, nbPerThread));
		}
		Collections.shuffle(marshallerList);
		for (final ThreadMarhaller threadMarhaller : marshallerList) {
			threadMarhaller.start();
		}

		for (final ThreadMarhaller threadMarhaller : marshallerList) {
			threadMarhaller.join();
			if (threadMarhaller.failed) {
				Assert.fail();
			}
		}
	}


	class ThreadMarhaller extends Thread {


		Resource res;


		final int repeat;


		File fileT;


		final boolean marshall;


		/**
		 *
		 */
		public boolean failed = false;


		/**
		 * @param res
		 *            The resource to marshall
		 * @param repeat
		 *            The number of repetition
		 */
		public ThreadMarhaller(final Resource res, final int repeat) {
			this.res = res;
			this.repeat = repeat;
			this.marshall = true;
		}


		/**
		 * @param file
		 *            The file to unmarshal
		 * @param repeat
		 *            The number of repetition
		 */
		public ThreadMarhaller(final File file, final int repeat) {
			this.fileT = file;
			this.repeat = repeat;
			this.marshall = false;
		}


		@Override
		public void run() {
			for (int i = 0; i < this.repeat; i++) {
				try (NullOutputStream stream = new NullOutputStream();) {
					if (this.marshall) {
						MarshallerTest.this.wlm.marshalResource(this.res, stream);
					} else {
						MarshallerTest.this.wlm.unmarshal(this.fileT, Resource.class);
					}
				} catch (final WebLabCheckedException wlce) {
					wlce.printStackTrace();
					this.failed = true;
				} catch (IOException ioe) {
					ioe.printStackTrace();
					this.failed = true;
				}
			}
		}
	}


	static class NullOutputStream extends OutputStream {


		@Override
		public void write(final byte[] b, final int off, final int len) {
			// to /dev/null
		}


		@Override
		public void write(final int b) {
			// to /dev/null
		}


		@Override
		public void write(final byte[] b) throws IOException {
			// to /dev/null
		}
	}


	static class FailOutputStream extends OutputStream {


		@Override
		public void write(int b) throws IOException {
			throw new IOException();
		}
	}


	static class FailOutputStreamWriter extends OutputStreamWriter {


		@SuppressWarnings("resource")
		public FailOutputStreamWriter() {
			super(new FailOutputStream());
		}


		@Override
		public void write(int c) throws IOException {
			throw new IOException();
		}

	}


}
