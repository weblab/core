/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.properties;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Enumeration;


/**
 *
 */
public class DummyClassLoader extends ClassLoader {


	/**
	 *
	 */
	public DummyClassLoader() {
		super(null);
	}


	protected int dummyURLs = 0; // 0 standard, 1 dummy urls, 2 exception, 3 modified urlhandlers


	private static final int BUFFER_SIZE = 8192;


	@Override
	protected synchronized Class<?> loadClass(final String className, final boolean resolve) throws ClassNotFoundException {

		byte[] classBytes;
		final byte[] buffer = new byte[DummyClassLoader.BUFFER_SIZE];
		final ByteArrayOutputStream out = new ByteArrayOutputStream();

		try (InputStream in = new FileInputStream("src/test/resources/Dummy.class")) {
			int n = -1;
			while ((n = in.read(buffer, 0, DummyClassLoader.BUFFER_SIZE)) != -1) {
				out.write(buffer, 0, n);
			}
		} catch (final IOException ioe) {
			throw new ClassNotFoundException("Error while reading Dummy.class", ioe);
		}

		classBytes = out.toByteArray();


		Class<?> cls = null;
		// 4. turn the byte array into a Class
		try {
			cls = this.defineClass(className, classBytes, 0, classBytes.length);
			if (resolve) {
				this.resolveClass(cls);
			}
		} catch (@SuppressWarnings("unused") final SecurityException e) {
			// loading core java classes such as java.lang.String is prohibited, throws java.lang.SecurityException.
			// delegate to parent if not allowed to load class
			cls = super.loadClass(className, resolve);
		}

		return cls;
	}


	@Override
	public Enumeration<URL> getResources(final String name) throws IOException {
		if ((this.dummyURLs == 1) || (this.dummyURLs == 3)) {
			return new Enumeration<URL>() {


				private int i = 0;


				@Override
				public boolean hasMoreElements() {
					return this.i < 1;
				}


				@Override
				public URL nextElement() {
					this.i++;
					URL u = null;
					try {
						if (DummyClassLoader.this.dummyURLs == 3) {
							final URLStreamHandler handler = new URLStreamHandler() {


								@Override
								protected URLConnection openConnection(final URL url) throws IOException {
									final URLConnection duc = new URLConnection(url) {


										@Override
										public void connect() throws IOException {
											// Normal
										}


										@Override
										public InputStream getInputStream() throws IOException {
											final InputStream is = new InputStream() {


												@Override
												public int read() throws IOException {
													return -1;
												}


												@Override
												public void close() throws IOException {
													throw new IOException();
												}
											};
											return is;
										}
									};
									return duc;
								}
							};

							u = new URL(new URL("http://test"), "myyhost", handler);

						} else {
							u = new URL("http://nothingtoseehere");
						}
					} catch (final MalformedURLException e) {
						e.printStackTrace();
					}
					return u;
				}

			};
		} else if (this.dummyURLs == 2) {
			throw new IOException();
		}
		return super.getResources(name);
	}


	@Override
	public String toString() {
		return "dummy one !";
	}
}
