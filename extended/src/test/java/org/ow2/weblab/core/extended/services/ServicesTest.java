/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.services;


import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.Configurable;
import org.ow2.weblab.core.services.Indexer;
import org.ow2.weblab.core.services.QueueManager;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.Searcher;
import org.ow2.weblab.core.services.Trainable;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.core.services.configurable.ConfigureArgs;
import org.ow2.weblab.core.services.configurable.ConfigureReturn;
import org.ow2.weblab.core.services.configurable.ResetConfigurationArgs;
import org.ow2.weblab.core.services.configurable.ResetConfigurationReturn;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.indexer.IndexReturn;
import org.ow2.weblab.core.services.queuemanager.NextResourceArgs;
import org.ow2.weblab.core.services.queuemanager.NextResourceReturn;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceReturn;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceReturn;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.core.services.searcher.SearchReturn;
import org.ow2.weblab.core.services.trainable.AddTrainResourceArgs;
import org.ow2.weblab.core.services.trainable.AddTrainResourceReturn;
import org.ow2.weblab.core.services.trainable.ResetTrainedModelArgs;
import org.ow2.weblab.core.services.trainable.ResetTrainedModelReturn;
import org.ow2.weblab.core.services.trainable.TrainArgs;
import org.ow2.weblab.core.services.trainable.TrainReturn;


/**
 */
@SuppressWarnings("deprecation")
public class ServicesTest {


	/**
	 * @throws Exception
	 *             Should never appear
	 */
	@Test
	public void testServices() throws Exception {
		final ServicesTestImpl services = new ServicesTestImpl();
		Assert.assertNotNull(services);

		// Objects of the model to be used
		final Resource res = WebLabResourceFactory.createResource("IdRef", "idResMU", Document.class);
		final String uc = "weblab://IdRef/idResUC";

		final Annotation annotation = WebLabResourceFactory.createAndLinkAnnotation(res);

		final Query query = WebLabResourceFactory.createResource("dqsds", "dsdfsd", SimilarityQuery.class);

		// Analyser
		final ProcessArgs processArgs = new ProcessArgs();
		processArgs.setResource(res);
		processArgs.setUsageContext(uc);
		final ProcessReturn processReturn = ((Analyser) services).process(processArgs);
		Assert.assertNotNull(processReturn);
		Assert.assertNotNull(processReturn.getResource());

		// Configurable
		final ConfigureArgs confArgs = new ConfigureArgs();
		confArgs.setConfiguration(annotation);
		confArgs.setUsageContext(uc);
		final ConfigureReturn confRet = ((Configurable) services).configure(confArgs);
		Assert.assertNotNull(confRet);

		final ResetConfigurationArgs resConfArgs = new ResetConfigurationArgs();
		resConfArgs.setUsageContext(uc);
		final ResetConfigurationReturn rcr = ((Configurable) services).resetConfiguration(resConfArgs);
		Assert.assertNotNull(rcr);

		// QueueManager
		final NextResourceArgs nra = new NextResourceArgs();
		nra.setUsageContext(uc);
		final NextResourceReturn nrr = ((QueueManager) services).nextResource(nra);
		Assert.assertNotNull(nrr);

		// Indexer
		final IndexArgs ia = new IndexArgs();
		ia.setResource(res);
		final IndexReturn ir = ((Indexer) services).index(ia);
		Assert.assertNotNull(ir);

		// ResourceContainer
		final LoadResourceArgs lra = new LoadResourceArgs();
		lra.setResourceId("the new id");
		final LoadResourceReturn lrr = ((ResourceContainer) services).loadResource(lra);
		Assert.assertNotNull(lrr);
		Assert.assertNotNull(lrr.getResource());

		final SaveResourceArgs sra = new SaveResourceArgs();
		sra.setResource(res);
		final SaveResourceReturn srr = ((ResourceContainer) services).saveResource(sra);
		Assert.assertNotNull(srr);
		Assert.assertNotNull(srr.getResourceId());

		// Searcher
		final SearchArgs sa = new SearchArgs();
		sa.setLimit(Integer.valueOf(1));
		sa.setOffset(Integer.valueOf(15));
		sa.setQuery(query);
		final SearchReturn sr = ((Searcher) services).search(sa);
		Assert.assertNotNull(sr);
		Assert.assertNotNull(sr.getResultSet());
		Assert.assertNotNull(sr.getResultSet().getPok());
		Assert.assertNotNull(sr.getResultSet().getResource());

		// Trainable
		final AddTrainResourceArgs atra = new AddTrainResourceArgs();
		atra.setResource(res);
		atra.setUsageContext(uc);
		final AddTrainResourceReturn atrr = ((Trainable) services).addTrainResource(atra);
		Assert.assertNotNull(atrr);

		final TrainArgs ta = new TrainArgs();
		ta.setUsageContext(uc);
		final TrainReturn tr = ((Trainable) services).train(ta);
		Assert.assertNotNull(tr);

		final ResetTrainedModelArgs rtma = new ResetTrainedModelArgs();
		rtma.setUsageContext(uc);
		final ResetTrainedModelReturn rtmr = ((Trainable) services).resetTrainedModel(rtma);
		Assert.assertNotNull(rtmr);
	}


	static class ServicesTestImpl implements Analyser, Configurable, QueueManager, Indexer, ResourceContainer, Searcher, Trainable {


		@Override
		public ProcessReturn process(final ProcessArgs args) {
			final ProcessReturn toReturn = new ProcessReturn();
			Assert.assertNotNull(args);
			final Resource res = args.getResource();
			Assert.assertNotNull(args.getUsageContext());
			Assert.assertNotNull(res);
			toReturn.setResource(res);
			return toReturn;
		}


		@Override
		public ConfigureReturn configure(final ConfigureArgs args) {
			final ConfigureReturn toReturn = new ConfigureReturn();
			Assert.assertNotNull(args);
			Assert.assertNotNull(args.getConfiguration());
			Assert.assertNotNull(args.getUsageContext());
			return toReturn;
		}


		@Override
		public ResetConfigurationReturn resetConfiguration(final ResetConfigurationArgs args) {
			final ResetConfigurationReturn toRet = new ResetConfigurationReturn();
			Assert.assertNotNull(args);
			Assert.assertNotNull(args.getUsageContext());
			return toRet;
		}


		@Override
		public NextResourceReturn nextResource(final NextResourceArgs args) {
			final NextResourceReturn toRet = new NextResourceReturn();
			Assert.assertNotNull(args);
			Assert.assertNotNull(args.getUsageContext());
			return toRet;
		}


		@Override
		public IndexReturn index(final IndexArgs args) {
			final IndexReturn toRet = new IndexReturn();
			Assert.assertNotNull(args);
			Assert.assertNotNull(args.getResource());
			return toRet;
		}


		@Override
		public LoadResourceReturn loadResource(final LoadResourceArgs args) {
			final LoadResourceReturn toRet = new LoadResourceReturn();
			Assert.assertNotNull(args);
			Assert.assertNotNull(args.getResourceId());
			toRet.setResource(WebLabResourceFactory.createResource("ddqsd", "dsdsdq", Document.class));
			return toRet;
		}


		@Override
		public SaveResourceReturn saveResource(final SaveResourceArgs args) {
			final SaveResourceReturn toRet = new SaveResourceReturn();
			Assert.assertNotNull(args);
			Assert.assertNotNull(args.getResource());
			toRet.setResourceId(args.getResource().getUri());
			return toRet;
		}


		@Override
		public SearchReturn search(final SearchArgs args) {
			final SearchReturn toRet = new SearchReturn();
			Assert.assertNotNull(args);
			Assert.assertNotNull(args.getLimit());
			Assert.assertNotNull(args.getOffset());
			Assert.assertNotNull(args.getQuery());
			toRet.setResultSet(WebLabResourceFactory.createResource("dsdsd", "dsdsdds", ResultSet.class));
			toRet.getResultSet().getResource().add(WebLabResourceFactory.createResource("dsdsdsdsdds", "dsddsd", Document.class));
			toRet.getResultSet().setPok(WebLabResourceFactory.createResource("dsdsdsdsdds", "dsddsd", PieceOfKnowledge.class));
			return toRet;
		}


		@Override
		public AddTrainResourceReturn addTrainResource(final AddTrainResourceArgs args) {
			final AddTrainResourceReturn toRet = new AddTrainResourceReturn();
			Assert.assertNotNull(args);
			Assert.assertNotNull(args.getResource());
			Assert.assertNotNull(args.getUsageContext());
			return toRet;
		}


		@Override
		public TrainReturn train(final TrainArgs args) {
			final TrainReturn toRet = new TrainReturn();
			Assert.assertNotNull(args);
			Assert.assertNotNull(args.getUsageContext());
			return toRet;
		}


		@Override
		public ResetTrainedModelReturn resetTrainedModel(final ResetTrainedModelArgs arg0) {
			Assert.assertNotNull(arg0);
			Assert.assertNotNull(arg0.getUsageContext());
			final ResetTrainedModelReturn toReturn = new ResetTrainedModelReturn();
			return toReturn;
		}

	}
}
