/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.extended.properties;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class PropertiesLoaderTest {


	@Test
	public void testLoad() throws IOException {
		final Map<String, String> props1 = PropertiesLoader.loadProperties("helper1.properties");
		final Map<String, String> props2 = PropertiesLoader.loadProperties("helper2.properties");
		final Map<String, String> props11 = PropertiesLoader.loadProperties("helper1.properties", this.getClass());
		final Map<String, String> props21 = PropertiesLoader.loadProperties("helper2.properties", this.getClass());

		Assert.assertNotNull(props1);
		Assert.assertNotNull(props2);
		Assert.assertNotNull(props11);
		Assert.assertNotNull(props21);

		Assert.assertEquals(props1.get("aaaa"), props11.get("aaaa"));
		Assert.assertEquals(props2.get("aaaa"), props21.get("aaaa"));
		Assert.assertEquals(props1.get("aaaaa"), props11.get("aaaaa"));
		Assert.assertEquals(props2.get("aaaaa"), props21.get("aaaaa"));

		Assert.assertEquals(props1.get("aaaa"), props1.get("aaaaa"));

		try {
			final Map<String, String> props = PropertiesLoader.loadProperties("notfound.properties", this.getClass());
			Assert.fail(props.toString());
		} catch (final WebLabUncheckedException wlue) {
			// File not found
			Assert.assertNotNull(wlue);
			Assert.assertNull(wlue.getCause());
		}

		FileUtils.copyFile(new File("src/test/resources/helper1.properties"), new File("target/classes/duplicate.properties"));
		FileUtils.copyFile(new File("src/test/resources/helper1.properties"), new File("target/test-classes/duplicate.properties"));
		final Map<String, String> dups = PropertiesLoader.loadProperties("duplicate.properties", this.getClass());

		final Map<String, String> props = PropertiesLoader.loadProperties("helper1.properties");
		Assert.assertNotNull(props);
		Assert.assertEquals(dups, props);
	}


	/**
	 * @throws Exception
	 *             Should never appear
	 */
	@Test
	public void testIntro() throws Exception {
		try {
			for (final Constructor<?> c : PropertiesLoader.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}
	}


	/**
	 *
	 */
	@Test
	public void testIOException() {

		try {
			final URL url = new URL("http://test.on..url.which.is.obviously.a.wrong.url");
			url.openConnection();
			LoggerFactory.getLogger(this.getClass()).warn("WARNING: this test is skipped because you're connected to a network with defaut DNS response.");
		} catch (final MalformedURLException murle) {
			Assert.fail(murle.getMessage());
			// never happen
		} catch (final IOException ioe) {
			Assert.assertNotNull(ioe);
			// if there isn't default DNS answer, then do the test
			final DummyClassLoader cl = new DummyClassLoader();

			Class<?> dummyClass = null;
			try {
				dummyClass = Class.forName("org.ow2.weblab.core.extended.properties.Dummy", true, cl);
			} catch (final ClassNotFoundException exception) {
				exception.printStackTrace();
				Assert.fail();
			}
			cl.dummyURLs = 1;
			try {
				PropertiesLoader.loadProperties("notfound.properties", dummyClass);
				Assert.fail();
			} catch (final WebLabUncheckedException wue) {
				// Normal
				Assert.assertNotNull(wue);
			}
			cl.dummyURLs = 2;
			try {
				PropertiesLoader.loadProperties("notfound.properties", dummyClass);
				Assert.fail();
			} catch (final WebLabUncheckedException wue) {
				// Normal
				Assert.assertNotNull(wue);
			}
			cl.dummyURLs = 3;
			PropertiesLoader.loadProperties("notfound.properties", dummyClass);
		}
	}

}
