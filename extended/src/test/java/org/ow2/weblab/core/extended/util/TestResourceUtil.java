/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.extended.util;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.SpatioTemporalSegment;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.TrackSegment;
import org.ow2.weblab.core.model.Video;


/**
 * @see ResourceUtil
 * @author WebLab Cassidian Team
 */
public class TestResourceUtil {


	/**
	 * Tests that the class not be instanciated.
	 * 
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	@Test
	public void testConstructor() throws Exception {
		try {
			for (final Constructor<?> c : ResourceUtil.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail("Cause of failure was not an UnsupportedOperationException but a " + ite.getCause());
			}
		}
	}


	@Test
	public void testGetSelectedSubResources() {
		final Resource theRes = this.createComplexResource();

		Assert.assertEquals(1, ResourceUtil.getSelectedSubResources(theRes, Document.class).size());
		Assert.assertEquals(1, ResourceUtil.getSelectedSubResources(theRes, Video.class).size());
		Assert.assertEquals(1, ResourceUtil.getSelectedSubResources(theRes, Audio.class).size());
		Assert.assertEquals(1, ResourceUtil.getSelectedSubResources(theRes, Text.class).size());
		Assert.assertEquals(1, ResourceUtil.getSelectedSubResources(theRes, Image.class).size());
		Assert.assertEquals(4, ResourceUtil.getSelectedSubResources(theRes, Annotation.class).size());
		Assert.assertEquals(4, ResourceUtil.getSelectedSubResources(theRes, PieceOfKnowledge.class).size());
		Assert.assertEquals(5, ResourceUtil.getSelectedSubResources(theRes, MediaUnit.class).size());
	}


	@Test
	public void testGetSelectedSubResourcesMap() {
		final Resource theRes = this.createComplexResource();

		Assert.assertEquals(1, ResourceUtil.getSelectedSubResourcesMap(theRes, Document.class).size());
		Assert.assertEquals(1, ResourceUtil.getSelectedSubResourcesMap(theRes, Video.class).size());
		Assert.assertEquals(1, ResourceUtil.getSelectedSubResourcesMap(theRes, Audio.class).size());
		Assert.assertEquals(1, ResourceUtil.getSelectedSubResourcesMap(theRes, Text.class).size());
		Assert.assertEquals(1, ResourceUtil.getSelectedSubResourcesMap(theRes, Image.class).size());
		Assert.assertEquals(4, ResourceUtil.getSelectedSubResourcesMap(theRes, Annotation.class).size());
		Assert.assertEquals(4, ResourceUtil.getSelectedSubResourcesMap(theRes, PieceOfKnowledge.class).size());
		Assert.assertEquals(5, ResourceUtil.getSelectedSubResourcesMap(theRes, MediaUnit.class).size());


		final ComposedResource cr = (ComposedResource) theRes;

		Assert.assertEquals(theRes, ResourceUtil.getSelectedSubResourcesMap(theRes, Document.class).values().iterator().next());
		Assert.assertEquals(cr.getResource().get(0), ResourceUtil.getSelectedSubResourcesMap(theRes, Document.class).keySet().iterator().next());

		Assert.assertEquals(cr.getResource().get(0), ResourceUtil.getSelectedSubResourcesMap(theRes, Video.class).values().iterator().next());
		Assert.assertEquals(cr.getResource().get(0), ResourceUtil.getSelectedSubResourcesMap(theRes, Audio.class).values().iterator().next());
		Assert.assertEquals(cr.getResource().get(0), ResourceUtil.getSelectedSubResourcesMap(theRes, Text.class).values().iterator().next());
		Assert.assertEquals(cr.getResource().get(0), ResourceUtil.getSelectedSubResourcesMap(theRes, Image.class).values().iterator().next());

		for (final Entry<Annotation, Resource> entry : ResourceUtil.getSelectedSubResourcesMap(theRes, Annotation.class).entrySet()) {
			Assert.assertTrue(entry.getValue().getAnnotation().contains(entry.getKey()));
		}
		for (final Entry<PieceOfKnowledge, Resource> entry : ResourceUtil.getSelectedSubResourcesMap(theRes, PieceOfKnowledge.class).entrySet()) {
			Assert.assertTrue(entry.getValue().getAnnotation().contains(entry.getKey()));
		}
		for (final Entry<MediaUnit, Resource> entry : ResourceUtil.getSelectedSubResourcesMap(theRes, MediaUnit.class).entrySet()) {
			if (entry.getValue() instanceof Document) {
				((Document) entry.getValue()).getMediaUnit().contains(entry.getKey());
			} else if (entry.getValue() instanceof ComposedResource) {
				((ComposedResource) entry.getValue()).getResource().contains(entry.getKey());
			} else {
				Assert.fail("Unexpected type of container: " + entry.getValue().getClass().getCanonicalName());
			}
		}
	}


	@Test
	public void testGetResourceUriMap() {
		final Resource theRes = this.createComplexResource();

		final Map<String, Resource> map = ResourceUtil.getResourceUriMap(theRes);

		Assert.assertEquals(1 + 1 + 4 + 4 + 3, map.size()); // 1 cr + 1 doc + 4 mu + 4 annot + 3 lls
		for (final Entry<String, Resource> entry : map.entrySet()) {
			Assert.assertEquals(entry.getKey(), entry.getValue().getUri());
		}
	}


	@Test
	public void testGetSegmentUriMap() {
		final Resource theRes = this.createComplexResource();

		final Map<String, Segment> map = ResourceUtil.getSegmentUriMap(theRes);

		Assert.assertEquals(2 + 2 + 2 + 3, map.size()); // 2 for text + 2 for img + 2 for audio + 3 for vid
		for (final Entry<String, Segment> entry : map.entrySet()) {
			Assert.assertEquals(entry.getKey(), entry.getValue().getUri());
		}
	}


	@Test
	public void testGetSubResources() {
		final Resource theRes = this.createComplexResource();

		final List<Resource> list = ResourceUtil.getSubResources(theRes);

		Assert.assertEquals(1 + 4 + 4 + 3, list.size()); // 1 doc + 4 mu + 4 annot + 3 lld
	}


	@Test
	public void testGetSelectedSegmentsMap() {
		final ComposedResource theRes = this.createComplexResource();

		final Map<Segment, MediaUnit> fullMap = ResourceUtil.getSelectedSegmentsMap(theRes, Segment.class);

		Assert.assertEquals(2 + 2 + 2 + 3, fullMap.size()); // 2 for text + 2 for img + 2 for audio + 3 for vid
		for (final Entry<Segment, MediaUnit> entry : fullMap.entrySet()) {
			Assert.assertTrue(entry.getValue().getSegment().contains(entry.getKey()));
		}

		final Map<LinearSegment, MediaUnit> linearMap = ResourceUtil.getSelectedSegmentsMap(theRes, LinearSegment.class);
		Assert.assertEquals(1, linearMap.size());
		Assert.assertTrue(linearMap.values().iterator().next().getSegment().contains(linearMap.keySet().iterator().next()));
		Assert.assertTrue(linearMap.values().iterator().next() instanceof Text);


		final Map<SpatialSegment, MediaUnit> spatialMap = ResourceUtil.getSelectedSegmentsMap(theRes, SpatialSegment.class);
		Assert.assertEquals(1, spatialMap.size());
		Assert.assertTrue(spatialMap.values().iterator().next().getSegment().contains(spatialMap.keySet().iterator().next()));
		Assert.assertTrue(spatialMap.values().iterator().next() instanceof Image);


		final Map<SpatioTemporalSegment, MediaUnit> spTempMap = ResourceUtil.getSelectedSegmentsMap(theRes, SpatioTemporalSegment.class);
		Assert.assertEquals(1, spTempMap.size());
		Assert.assertTrue(spTempMap.values().iterator().next().getSegment().contains(spTempMap.keySet().iterator().next()));
		Assert.assertTrue(spTempMap.values().iterator().next() instanceof Video);

		final Map<SpatioTemporalSegment, MediaUnit> spTempMap2 = ResourceUtil.getSelectedSegmentsMap(spTempMap.values().iterator().next(), SpatioTemporalSegment.class);
		Assert.assertEquals(1, spTempMap2.size());
		Assert.assertTrue(spTempMap2.values().iterator().next().getSegment().contains(spTempMap2.keySet().iterator().next()));
		Assert.assertTrue(spTempMap2.values().iterator().next() instanceof Video);


		final Map<TemporalSegment, MediaUnit> tempMap = ResourceUtil.getSelectedSegmentsMap(theRes, TemporalSegment.class);
		Assert.assertEquals(2, tempMap.size());
		for (final Entry<TemporalSegment, MediaUnit> entry : tempMap.entrySet()) {
			Assert.assertTrue(entry.getValue().getSegment().contains(entry.getKey()));
			Assert.assertTrue((entry.getValue() instanceof Video) || (entry.getValue() instanceof Audio));
		}

		final Map<TrackSegment, MediaUnit> trackMap = ResourceUtil.getSelectedSegmentsMap(theRes, TrackSegment.class);
		Assert.assertEquals(4, trackMap.size());
		for (final Entry<TrackSegment, MediaUnit> entry : trackMap.entrySet()) {
			Assert.assertTrue(entry.getValue().getSegment().contains(entry.getKey()));
		}

		final Map<TrackSegment, MediaUnit> trackMap2 = ResourceUtil.getSelectedSegmentsMap(theRes.getResource().get(0), TrackSegment.class);
		Assert.assertEquals(4, trackMap2.size());
		for (final Entry<TrackSegment, MediaUnit> entry : trackMap2.entrySet()) {
			Assert.assertTrue(entry.getValue().getSegment().contains(entry.getKey()));
		}
	}


	@Test
	public void testGetSelectedSegments() {
		final Resource theRes = this.createComplexResource();

		for (final MediaUnit mu : ((Document) ((ComposedResource) theRes).getResource().get(0)).getMediaUnit()) {
			final List<Segment> fullList = ResourceUtil.getSelectedSegments(mu, Segment.class);
			final List<LinearSegment> linearList = ResourceUtil.getSelectedSegments(mu, LinearSegment.class);
			final List<TemporalSegment> temporalList = ResourceUtil.getSelectedSegments(mu, TemporalSegment.class);
			final List<SpatialSegment> spatialList = ResourceUtil.getSelectedSegments(mu, SpatialSegment.class);
			final List<SpatioTemporalSegment> spTpList = ResourceUtil.getSelectedSegments(mu, SpatioTemporalSegment.class);
			final List<TrackSegment> trackList = ResourceUtil.getSelectedSegments(mu, TrackSegment.class);

			Assert.assertTrue(mu.getSegment().containsAll(fullList));
			Assert.assertTrue(mu.getSegment().containsAll(linearList));
			Assert.assertTrue(mu.getSegment().containsAll(temporalList));
			Assert.assertTrue(mu.getSegment().containsAll(spatialList));
			Assert.assertTrue(mu.getSegment().containsAll(spTpList));
			Assert.assertTrue(mu.getSegment().containsAll(trackList));

			int nbFull = 0;
			int nbLinear = 0;
			int nbTemporal = 0;
			int nbSpatial = 0;
			int nbSpatioTemporal = 0;
			int nbTrack = 0;
			if (mu instanceof Text) {
				nbLinear = 1;
				nbTrack = 1;
				nbFull = 2;
			} else if (mu instanceof Audio) {
				nbTemporal = 1;
				nbTrack = 1;
				nbFull = 2;
			} else if (mu instanceof Image) {
				nbSpatial = 1;
				nbTrack = 1;
				nbFull = 2;
			} else if (mu instanceof Video) {
				nbTemporal = 1;
				nbTrack = 1;
				nbSpatioTemporal = 1;
				nbFull = 3;
				nbSpatial = 0;
			}

			Assert.assertEquals(nbFull, fullList.size());
			Assert.assertEquals(nbLinear, linearList.size());
			Assert.assertEquals(nbSpatial, spatialList.size());
			Assert.assertEquals(nbSpatioTemporal, spTpList.size());
			Assert.assertEquals(nbTemporal, temporalList.size());
			Assert.assertEquals(nbTrack, trackList.size());
		}

	}



	@Test
	public void testPrintTree() throws IOException {
		final Resource r = this.createComplexResource();
		StringWriter writer = new StringWriter();
		ResourceUtil.printTree(r, writer);
		String str = writer.toString();
		Assert.assertEquals("12 subresources + the main resource", 13, str.split("\n").length);
		writer.close();
		writer = new StringWriter();
		ResourceUtil.printTree(r, writer, false);
		str = writer.toString();
		Assert.assertEquals("9 subresources + the main resource", 13, str.split("\n").length);
		writer.close();
		writer = new StringWriter();
		ResourceUtil.printTree(r, writer, true);
		str = writer.toString();
		Assert.assertEquals("9 subresources + the main resource + 9 segments", 22, str.split("\n").length);
	}




	protected ComposedResource createComplexResource() {

		/*
		 * Create a document with one MediaUnit of each type. Each containing an annotation. And a segment of allowed types.
		 */
		final Document doc = WebLabResourceFactory.createResource("TestResourceUtil", UUID.randomUUID().toString(), Document.class);

		// A Text with an annot, a linear segment and a Track segment with 2 linear
		final Text text = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		WebLabResourceFactory.createAndLinkAnnotation(text);
		SegmentFactory.createAndLinkLinearSegment(text, 0, 10);
		final TrackSegment trackText = SegmentFactory.createAndLinkSegment(text, TrackSegment.class);
		SegmentFactory.createAndLinkLinearSegment(trackText, 0, 10);
		SegmentFactory.createAndLinkLinearSegment(trackText, 10, 20);

		// An Image with an annot, a spatial segment and a Track segment with 2 spatial
		final Image image = WebLabResourceFactory.createAndLinkMediaUnit(doc, Image.class);
		WebLabResourceFactory.createAndLinkAnnotation(image);
		SegmentFactory.createAndLinkSpatialSegment(image, 0, 0, 0, 10, 10, 10, 10, 0);
		final TrackSegment trackImg = SegmentFactory.createAndLinkSegment(image, TrackSegment.class);
		SegmentFactory.createAndLinkSpatialSegment(trackImg, 0, 0, 0, 10, 10, 10, 10, 0);
		SegmentFactory.createAndLinkSpatialSegment(trackImg, 0, 0, 0, 10, 10, 10, 10, 0);

		// An Audio with an annot, a temporal segment and a Track segment with 2 temporal
		final Audio audio = WebLabResourceFactory.createAndLinkMediaUnit(doc, Audio.class);
		WebLabResourceFactory.createAndLinkAnnotation(audio);
		SegmentFactory.createAndLinkTemporalSegment(audio, 0, 10);
		final TrackSegment trackAudio = SegmentFactory.createAndLinkSegment(audio, TrackSegment.class);
		SegmentFactory.createAndLinkTemporalSegment(trackAudio, 0, 10);
		SegmentFactory.createAndLinkTemporalSegment(trackAudio, 10, 20);

		// A Video with an annot, a temporal segment, a spatiotemporal and a Track segment with 2 spatiotemporal (and 3 lld)
		final Video video = WebLabResourceFactory.createAndLinkMediaUnit(doc, Video.class);
		WebLabResourceFactory.createAndLinkAnnotation(video);
		SegmentFactory.createAndLinkTemporalSegment(video, 0, 10);
		SegmentFactory.createAndLinkSpatioTemporalSegment(video, 122, 0, 0, 0, 10, 10, 10, 10, 0);
		final TrackSegment trackVideo = SegmentFactory.createAndLinkSegment(video, TrackSegment.class);
		SegmentFactory.createAndLinkSpatioTemporalSegment(trackVideo, 122, 0, 0, 0, 10, 10, 10, 10, 0);
		SegmentFactory.createAndLinkSpatioTemporalSegment(trackVideo, 150, 0, 0, 0, 10, 10, 10, 10, 0);
		WebLabResourceFactory.createAndLinkLowLevelDescriptor(video);
		WebLabResourceFactory.createAndLinkLowLevelDescriptor(video);
		WebLabResourceFactory.createAndLinkLowLevelDescriptor(video);


		final ComposedResource cr = WebLabResourceFactory.createResource("TestResourceUtil", UUID.randomUUID().toString(), ComposedResource.class);
		cr.getResource().add(doc);
		return cr;
	}

}
