/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.factory;

import java.util.ConcurrentModificationException;
import java.util.concurrent.Callable;

import org.junit.Assert;


/**
 *
 */
public class FactoryTestThread implements Callable<Boolean> {


	private final int nbIteration;


	private static boolean running = true;


	/**
	 * @param nbIteration
	 *            The number to thread to be launched
	 */
	public FactoryTestThread(final int nbIteration) {
		this.nbIteration = nbIteration;
	}



	@Override
	public Boolean call() throws Exception {
		for (int k = 0; (k < this.nbIteration) && FactoryTestThread.running; k++) {
			final TestFactories tstFactrs = new TestFactories();
			try {
				tstFactrs.testBuffer();
				tstFactrs.testFactories();
				tstFactrs.testMediaUnitFactory();
				tstFactrs.testPoKFactory();
				tstFactrs.testSegmentFactory();
				tstFactrs.testUnbearableNumberofElementCreation();
				tstFactrs.testWithConstructorsIntro();
			} catch (final Throwable t) {
				/*
				 * Some of the tests are no consistent with the behavior of the factory.
				 * The may throw assertion errors, that should not prevent the test to continue.
				 * This has to be changed.
				 */
				if (t instanceof ConcurrentModificationException) {
					t.printStackTrace();
					FactoryTestThread.running = false;
					Assert.fail("ConcurrentModificationException encountered !");
				}
			}
			Thread.yield();
		}
		return Boolean.TRUE;
	}
}
