/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.factory;

import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabRISyntaxException;
import org.ow2.weblab.core.extended.exception.WebLabResourceCreationException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Coordinate;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.LowLevelDescriptor;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.SpatioTemporalSegment;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.TrackSegment;

/**
 *
 */
public class TestFactories {


	@Before
	public void clearMap() {
		AbstractFactory.INNER_MAP.clear();
	}


	@Test
	public void testFactories() throws WebLabCheckedException {
		// DocumentFactory tests
		Document doc = null;

		// this tests must failed because of '#'
		try {
			doc = WebLabResourceFactory.createResource("test_1", "666-666#", Document.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException wrce) {
			Assert.assertNotNull(wrce);
		}

		try {
			doc = WebLabResourceFactory.createResource("test_1", "666-666#99", Document.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException wrce) {
			Assert.assertNotNull(wrce);
		}

		try {
			doc = WebLabResourceFactory.createResource("test_1#", "666-666", Document.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException wrce) {
			Assert.assertNotNull(wrce);
		}

		// this one is right
		doc = WebLabResourceFactory.createResource("test_1", "666-666", Document.class);

		Assert.assertEquals("weblab://test_1/666-666", doc.getUri());
		Assert.assertNotSame("weblab://test_2/666", doc.getUri());

		// MediaUnitFactory tests
		final Text text1 = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);

		Assert.assertEquals("weblab://test_1/666-666#0", text1.getUri());

		final Document comp = WebLabResourceFactory.createAndLinkMediaUnit(doc, Document.class);

		Assert.assertEquals("weblab://test_1/666-666#1", comp.getUri());

		final Text text2 = WebLabResourceFactory.createAndLinkMediaUnit(comp, Text.class);

		Assert.assertEquals("weblab://test_1/666-666#1-0", text2.getUri());

		final Document comp2 = WebLabResourceFactory.createAndLinkMediaUnit(comp, Document.class);
		Assert.assertEquals("weblab://test_1/666-666#1-1", comp2.getUri());

		text1.setContent("Text du premier niveau");
		text2.setContent("Text du deuxième niveau");

		// AnnotationFactory tests
		final Annotation annot1 = WebLabResourceFactory.createAndLinkAnnotation(doc);
		Assert.assertEquals("weblab://test_1/666-666#a4", annot1.getUri());
		final Annotation annot2 = WebLabResourceFactory.createAndLinkAnnotation(text1);
		Assert.assertEquals("weblab://test_1/666-666#0-a0", annot2.getUri());
		final Annotation annot3 = WebLabResourceFactory.createAndLinkAnnotation(text1);
		Assert.assertEquals("weblab://test_1/666-666#0-a1", annot3.getUri());
		final Annotation annot4 = WebLabResourceFactory.createAndLinkAnnotation(annot1);
		Assert.assertEquals("weblab://test_1/666-666#a4-a0", annot4.getUri());
		final Annotation annot5 = WebLabResourceFactory.createAndLinkAnnotation(annot3);
		Assert.assertEquals("weblab://test_1/666-666#0-a1-a0", annot5.getUri());

		try {
			final Resource res = WebLabResourceFactory.createResource("idRef", "idRess", Resource.class);
			res.setUri("khfdjkf--[");
			WebLabResourceFactory.createAndLinkAnnotation(res);
			Assert.fail("'[' not allowed in URI of parent.");
		} catch (final WebLabResourceCreationException wlrce) {
			Assert.assertNotNull(wlrce);
		}

		final LowLevelDescriptor lld = WebLabResourceFactory.createAndLinkLowLevelDescriptor(text1);
		final LowLevelDescriptor lld2 = WebLabResourceFactory.createResource("tata", "tutu", LowLevelDescriptor.class);
		Assert.assertNotNull(lld.getData());
		Assert.assertNotNull(lld2.getData());
		Assert.assertNotEquals(Object.class, lld.getData().getClass());
		Assert.assertNotEquals(Object.class, lld2.getData().getClass());

		/*
		 * Simple segment types first
		 */
		final LinearSegment ls1 = SegmentFactory.createAndLinkSegment(text1, LinearSegment.class);
		ls1.setEnd(0);
		ls1.setStart(0);
		final TemporalSegment ts1 = SegmentFactory.createAndLinkSegment(text1, TemporalSegment.class);
		ts1.setEnd(0);
		ts1.setStart(0);
		final SpatialSegment ss1 = SegmentFactory.createAndLinkSegment(text1, SpatialSegment.class);
		ss1.getCoordinate().addAll(TestFactories.createFakeCoordo());
		final SpatioTemporalSegment sts1 = SegmentFactory.createAndLinkSegment(text1, SpatioTemporalSegment.class);
		sts1.getCoordinate().addAll(TestFactories.createFakeCoordo());
		sts1.setTimestamp(0);
		final TrackSegment track1 = SegmentFactory.createAndLinkSegment(text1, TrackSegment.class);
		final LinearSegment ls2 = SegmentFactory.createAndLinkSegment(track1, LinearSegment.class);
		ls2.setEnd(0);
		ls2.setStart(0);
		final TemporalSegment ts2 = SegmentFactory.createAndLinkSegment(track1, TemporalSegment.class);
		ts2.setEnd(0);
		ts2.setStart(0);
		final SpatialSegment ss2 = SegmentFactory.createAndLinkSegment(track1, SpatialSegment.class);
		ss2.getCoordinate().addAll(TestFactories.createFakeCoordo());
		final SpatioTemporalSegment sts2 = SegmentFactory.createAndLinkSegment(track1, SpatioTemporalSegment.class);
		sts2.getCoordinate().addAll(TestFactories.createFakeCoordo());
		sts2.setTimestamp(0);

		/*
		 * Bad input for linear
		 */
		try {
			SegmentFactory.createAndLinkLinearSegment(text1, 5, 2);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkLinearSegment((Text) null, 2, 5);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkLinearSegment(track1, 5, 2);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkLinearSegment((TrackSegment) null, 2, 5);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}

		/*
		 * Bad input for temporal
		 */
		try {
			SegmentFactory.createAndLinkTemporalSegment(text1, 5, 2);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkTemporalSegment((Text) null, 2, 5);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkTemporalSegment(track1, 5, 2);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkTemporalSegment((TrackSegment) null, 2, 5);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}

		/*
		 * Bad input for spatial
		 */
		try {
			SegmentFactory.createAndLinkSpatialSegment((Text) null, new int[4]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatialSegment(text1, null);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatialSegment(text1, new int[2]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatialSegment(text1, new int[11]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatialSegment((TrackSegment) null, new int[4]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatialSegment(track1, null);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatialSegment(track1, new int[2]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatialSegment(track1, new int[11]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}

		/*
		 * Bad input for spatiotemporal
		 */
		try {
			SegmentFactory.createAndLinkSpatioTemporalSegment((Text) null, 12, new int[4]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatioTemporalSegment(text1, 12, null);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatioTemporalSegment(text1, 12, new int[2]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatioTemporalSegment(text1, 12, new int[79]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatioTemporalSegment((TrackSegment) null, 12, new int[4]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatioTemporalSegment(track1, 12, null);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatioTemporalSegment(track1, 12, new int[2]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}
		try {
			SegmentFactory.createAndLinkSpatioTemporalSegment(track1, 12, new int[79]);
			Assert.fail();
		} catch (final WebLabUncheckedException wlue) {
			Assert.assertNotNull(wlue);
		}

		/*
		 * Test various linear segment creation
		 */
		final Segment sl1 = SegmentFactory.createAndLinkLinearSegment(text1, 2, 5);
		final Segment sl1bis = SegmentFactory.createAndLinkLinearSegment(text1, 2, 5);
		final Segment sl1ter = SegmentFactory.createAndLinkLinearSegment(text1, 2, 5, true);

		final Segment sl2 = SegmentFactory.createAndLinkLinearSegment(text1, 2, 6);
		final Segment sl3 = SegmentFactory.createAndLinkLinearSegment(text1, 3, 5);

		Assert.assertEquals(sl1, sl1bis);
		Assert.assertNotSame(sl1, sl1ter);
		Assert.assertNotSame(sl1bis, sl1ter);
		Assert.assertNotSame(sl1, sl3);
		Assert.assertNotSame(sl1bis, sl2);
		Assert.assertNotSame(sl2, sl3);

		SegmentFactory.createAndLinkLinearSegment(track1, 3, 5);

		/*
		 * Test various temporal segment creation
		 */
		final Segment st1 = SegmentFactory.createAndLinkTemporalSegment(text1, 2, 5);
		final Segment st1bis = SegmentFactory.createAndLinkTemporalSegment(text1, 2, 5);
		final Segment st1ter = SegmentFactory.createAndLinkTemporalSegment(text1, 2, 5, true);

		final Segment st2 = SegmentFactory.createAndLinkTemporalSegment(text1, 2, 6);
		final Segment st3 = SegmentFactory.createAndLinkTemporalSegment(text1, 3, 5);

		Assert.assertEquals(st1, st1bis);
		Assert.assertNotSame(st1, st1ter);
		Assert.assertNotSame(st1bis, st1ter);
		Assert.assertNotSame(st1, st3);
		Assert.assertNotSame(st1bis, st2);
		Assert.assertNotSame(st2, st3);

		SegmentFactory.createAndLinkTemporalSegment(track1, 2, 5);

		/*
		 * Test good spatial segment creation
		 */
		SegmentFactory.createAndLinkSpatialSegment(text1, new int[] { 5, 4, 5, 3, 6, 5 });
		SegmentFactory.createAndLinkSpatialSegment(track1, new int[] { 5, 4, 5, 3, 6, 5 });


		/*
		 * Test good spatiotemporal segment creation
		 */
		SegmentFactory.createAndLinkSpatioTemporalSegment(text1, 54, new int[] { 5, 4, 5, 3, 6, 5 });
		SegmentFactory.createAndLinkSpatioTemporalSegment(track1, 54, new int[] { 5, 4, 5, 3, 6, 5 });


		Assert.assertNull(AbstractFactory.findParent(comp2, WebLabResourceFactory.createResource("tata", "tutu", ComposedResource.class)));
		Assert.assertEquals(comp, AbstractFactory.findParent(comp2, comp));
		Assert.assertNull(AbstractFactory.findParent(comp, comp2));


		WebLabResourceFactory.createAndLinkAnnotation(text1);
		new WebLabMarshaller(true).marshalResource(text1, new StringWriter());




		WebLabResourceFactory.createResource("tata", "tutu", ComposedQuery.class);

	}


	public static List<Coordinate> createFakeCoordo() {
		final List<Coordinate> cordos = new ArrayList<>();
		final Coordinate c1 = new Coordinate();
		c1.setX(0);
		c1.setY(0);
		cordos.add(c1);
		final Coordinate c2 = new Coordinate();
		c2.setX(0);
		c2.setY(0);
		cordos.add(c2);
		final Coordinate c3 = new Coordinate();
		c3.setX(0);
		c3.setY(0);
		cordos.add(c3);
		return cordos;
	}


	/**
	 *
	 */
	@Test
	public void testUnbearableNumberofElementCreation() {

		final Document document = WebLabResourceFactory.createResource("test", "overDocument", Document.class);
		int sum = 1;

		int size = 5;
		sum += size;
		// Creates Texts
		final List<Text> res = new LinkedList<>();
		for (int k = 0; k < size; k++) {
			res.add(WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class));
		}

		final List<Segment> resS = new LinkedList<>();
		// Creates Segments
		size = 1500;
		sum += size;
		for (int k = 0; k < size; k++) {
			final Text t = res.get((int) Math.floor(Math.random() * 5));
			final int s = k;// (int)Math.floor(Math.random()*100000);
			final int end = k + 1;// s+1+(int)Math.floor(Math.random()*100000);
			resS.add(SegmentFactory.createAndLinkLinearSegment(t, s, end));
		}

		// Creates Annotations
		size = 5000;
		sum += size;
		for (int k = 0; k < size; k++) {
			final Text t = res.get((int) Math.floor(Math.random() * 5));
			WebLabResourceFactory.createAndLinkAnnotation(t);
		}

		Assert.assertEquals(sum, AbstractFactory.listUri(document, false).size());
		Assert.assertTrue(AbstractFactory.INNER_MAP.size() <= 1001);
	}


	/**
	 *
	 */
	@Test
	public void testBuffer() {
		final Document doct = new Document();
		doct.setUri("doct");
		final LowLevelDescriptor lld = new LowLevelDescriptor();
		lld.setUri("lld");
		doct.getDescriptor().add(lld);
		final Annotation a = new Annotation();
		a.setUri("ann");
		doct.getAnnotation().add(a);
		final Segment s = new LinearSegment();
		s.setUri("seg");
		doct.getSegment().add(s);
		final Text t = new Text();
		t.setUri("t");
		t.getSegment().add(s);
		doct.getMediaUnit().add(t);
		Assert.assertEquals(5, AbstractFactory.listUri(doct, true).size());

		final ComposedResource cr = new ComposedResource();
		cr.setUri("cr");
		cr.getResource().add(doct);
		Assert.assertEquals(6, AbstractFactory.listUri(cr, true).size());

		AbstractFactory.removeChild(doct, cr, cr);
		Assert.assertEquals(1, AbstractFactory.listUri(cr, true).size());
		AbstractFactory.removeChild(doct, null, cr);
		Assert.assertEquals(1, AbstractFactory.listUri(cr, true).size());

		cr.getResource().add(doct);
		Assert.assertEquals(6, AbstractFactory.listUri(cr, true).size());

		Assert.assertEquals(cr, AbstractFactory.findParent(doct, cr));
		Assert.assertNull(AbstractFactory.findParent(doct, null));
		Assert.assertNull(AbstractFactory.findParent(doct, new Document()));
		Assert.assertEquals(doct, AbstractFactory.findParent(t, cr));

		final Document doc = new Document();
		doc.setUri("http://my test");
		for (int i = 0; i < 1002; i++) {
			doc.setUri("http://my test" + i);
			AbstractFactory.listUri(doc, true);
		}
		Assert.assertTrue(AbstractFactory.INNER_MAP.size() <= 1001);
	}


	@Test
	public void testWithConstructorsIntro() throws Exception {
		TestFactories.testWithIntro(WebLabResourceFactory.class);
		TestFactories.testWithIntro(SegmentFactory.class);
		TestFactories.testWithIntro(ExceptionFactory.class);
	}


	private static void testWithIntro(final Class<?> clazz) throws Exception {
		try {
			for (final Constructor<?> c : clazz.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}
	}


	/**
	 *
	 */
	@Test
	public void testPoKFactory() {
		final ResultSet res = WebLabResourceFactory.createResource("testref", "testres", ResultSet.class);
		Assert.assertNotNull(res);
		final PieceOfKnowledge pok = WebLabResourceFactory.createAndLinkPoK(res);
		Assert.assertNotNull(pok);
		res.setUri("blablablalb:bla !");

		try {
			WebLabResourceFactory.createAndLinkPoK(res);
			Assert.fail();
		} catch (final WebLabRISyntaxException w) {
			// Normal
			Assert.assertNotNull(w);
		}

	}


	/**
	 *
	 */
	@Test
	public void testMediaUnitFactory() {
		final Document doc = new Document();
		try {
			WebLabResourceFactory.createAndLinkMediaUnit(doc, MediaUnit.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException w) {
			// Normal
			Assert.assertNotNull(w);
		}
		try {
			doc.setUri("not an uri !");
			WebLabResourceFactory.createAndLinkMediaUnit(doc, Image.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			WebLabResourceFactory.createAndLinkMediaUnit(doc, DummyResource2.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException w) {
			// Normal
			Assert.assertNotNull(w);
		}

	}


	/**
	 *
	 */
	@Test
	public void testResourceFactory() {
		try {
			WebLabResourceFactory.createResource("none", "none", DummyResource.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			WebLabResourceFactory.createResource("none", "none", DummyResource2.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException w) {
			// Normal
			Assert.assertNotNull(w);
		}

	}


	/**
	 *
	 */
	@Test
	public void testSegmentFactory() {
		try {
			SegmentFactory.createAndLinkSegment(new Document(), Segment.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			SegmentFactory.createAndLinkSegment(new Document(), DummySegment.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		final Document d = WebLabResourceFactory.createResource("doc", "test", Document.class);
		final TrackSegment ts = SegmentFactory.createAndLinkSegment(d, TrackSegment.class);
		final TrackSegment ts2 = SegmentFactory.createAndLinkSegment(d, TrackSegment.class);
		ts.getSegment().add(ts2);

		Assert.assertEquals("weblab://doc/test#2", AbstractFactory.createSegmentURIFor(d));
		Assert.assertEquals("weblab://doc/test#0-1", AbstractFactory.createSegmentURIFor(ts));

		final Text text = WebLabResourceFactory.createResource("text", "test", Text.class);

		SegmentFactory.createAndLinkLinearSegment(text, 0, 12);
		SegmentFactory.createAndLinkLinearSegment(text, 0, 12, true);
		SegmentFactory.createAndLinkLinearSegment(text, 0, 12, false);

		// SegmentFactory.createAndLinkSpatialSegment(d, new int[0]);
		SegmentFactory.createAndLinkSpatialSegment(d, new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE });

		SegmentFactory.createAndLinkSpatioTemporalSegment(d, 0, new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE });

		SegmentFactory.createAndLinkTemporalSegment(d, 0, 1);
		SegmentFactory.createAndLinkTemporalSegment(d, 0, 1, true);
		SegmentFactory.createAndLinkTemporalSegment(d, 0, 1, false);


		try {
			SegmentFactory.createAndLinkLinearSegment((Text) null, 0, 12);
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}
		try {
			SegmentFactory.createAndLinkLinearSegment(new Text(), 12, 0);
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			SegmentFactory.createAndLinkSpatialSegment((Text) null, new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE });
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			SegmentFactory.createAndLinkSpatialSegment(new Document(), null);
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			SegmentFactory.createAndLinkSpatialSegment(new Document(), new int[] {});
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			SegmentFactory.createAndLinkSpatialSegment(new Document(), new int[] { 0 });
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			SegmentFactory.createAndLinkSpatialSegment(new Document(), new int[] { 0, 1 });
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			SegmentFactory.createAndLinkSpatialSegment(new Document(), new int[] { 0, 1, -2 });
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			SegmentFactory.createAndLinkTemporalSegment((Text) null, 0, 1);
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			SegmentFactory.createAndLinkTemporalSegment(new Document(), 1, 0);
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}
	}


	@Test
	public void testAbstractFactory() {
		Assert.assertEquals(0, AbstractFactory.INNER_MAP.size());

		final Document res_1, res_2;
		final TrackSegment ts;
		final SimilarityQuery sq;
		final ComposedQuery cq;

		res_1 = WebLabResourceFactory.createResource("doc_test_1", "123456", Document.class);
		res_2 = WebLabResourceFactory.createResource("doc_test_2", "123456", Document.class);
		ts = SegmentFactory.createAndLinkSegment(res_1, TrackSegment.class);
		SegmentFactory.createAndLinkLinearSegment(ts, 0, 1);
		sq = WebLabResourceFactory.createResource("squery", "123456", SimilarityQuery.class);
		cq = WebLabResourceFactory.createResource("cquery", "123456", ComposedQuery.class);

		sq.getResource().add(res_1);
		sq.getResource().add(res_2);

		cq.getQuery().add(sq);

		Assert.assertEquals(2, AbstractFactory.listUri(ts).size());
		Assert.assertEquals(2, AbstractFactory.listUri(ts, true).size());
		Assert.assertNotEquals(5, AbstractFactory.listUri(sq).size());
		Assert.assertEquals(5, AbstractFactory.listUri(sq, true).size());

		Assert.assertTrue(AbstractFactory.INNER_MAP.containsKey(res_1.getUri()));
		Assert.assertTrue(AbstractFactory.INNER_MAP.containsKey(res_2.getUri()));
		Assert.assertTrue(AbstractFactory.INNER_MAP.containsKey(ts.getUri()));
		Assert.assertTrue(AbstractFactory.INNER_MAP.containsKey(sq.getUri()));
		Assert.assertTrue(AbstractFactory.INNER_MAP.containsKey(cq.getUri()));
		AbstractFactory.cleanMapping(cq.getUri());
		Assert.assertFalse(AbstractFactory.INNER_MAP.containsKey(cq.getUri()));
		AbstractFactory.cleanMapping("not an uri");

		try {
			AbstractFactory.createSegmentURIFor(new Object());
			Assert.fail("Should not create Segment on an Object.");
		} catch (final WebLabUncheckedException e) {
			Assert.assertNotNull(e);
		}

		Assert.assertEquals(6, AbstractFactory.listUri(cq, false).size());
		cq.setUri(null);
		Assert.assertEquals(5, AbstractFactory.listUri(cq, false).size());
		Assert.assertEquals(5, AbstractFactory.listUri(cq, true).size());
	}


	@Test
	public void testIllegalSegmentCreation() throws IllegalArgumentException, InstantiationException, IllegalAccessException {
		final Constructor<?>[] constructors = SegmentFactory.class.getDeclaredConstructors();
		try {
			constructors[0].setAccessible(true);
			constructors[0].newInstance(new Object[] {});
			Assert.fail("Should not be instantiated.");
		} catch (final InvocationTargetException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		try {
			SegmentFactory.createAndLinkSegment(new TrackSegment(), Segment.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException w) {
			// Normal
			Assert.assertNotNull(w);
		}

		try {
			SegmentFactory.createAndLinkSegment(new TrackSegment(), DummySegment.class);
			Assert.fail();
		} catch (final WebLabResourceCreationException w) {
			// Normal
			Assert.assertNotNull(w);
		}
	}


	@Test(expected = InstantiationException.class)
	public void testIntro() throws Throwable {
		// Unable to instanciate, it's an abstract class!
		final Constructor<?>[] constructors = AbstractFactory.class.getDeclaredConstructors();
		constructors[0].setAccessible(true);
		constructors[0].newInstance();
	}


	@Test(expected = UnsupportedOperationException.class)
	public void testWithAbstarctImpl() throws Throwable {
		final AbstractFactory facto = new DummyFactory();
		Assert.fail(facto.toString() + " has been created but shouldn't!");
	}

}
