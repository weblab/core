/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.ontologies;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.LinearSegment;


/**
 *
 */
public class TestOntology {


	protected static <T> void constructorCall(final Class<T> clazz) throws InvocationTargetException, InstantiationException, IllegalAccessException, IllegalArgumentException {
		final Constructor<?> constructor = clazz.getDeclaredConstructors()[0];
		constructor.setAccessible(true);
		constructor.newInstance();
	}


	@Test
	public void testOntologyCreation() throws InstantiationException, IllegalAccessException, IllegalArgumentException {
		final Class<?>[] ontoToTest = new Class<?>[] { DCTerms.class, DublinCore.class, RDF.class, RDFS.class, WebLabRetrieval.class, WebLabProcessing.class, WebLabModel.class, MediaOntology.class,
				WGS84Pos.class, Geonames.class };
		for (final Class<?> clazz : ontoToTest) {
			try {
				TestOntology.constructorCall(clazz);
				Assert.fail(clazz + "should throw an InvocationTargetException");
			} catch (final InvocationTargetException ite) {
				// Normal
				Assert.assertNotNull(ite);
			}
		}
	}


	/**
	 * @throws ClassNotFoundException
	 *             If Document or LinearSegment are not found.
	 */
	@Test
	public void testWebLabModelOntology() throws ClassNotFoundException {
		final String uriDoc = WebLabModel.getUriFromResource(Document.class);
		Assert.assertEquals("http://weblab.ow2.org/core/1.2/ontology/model#Document", uriDoc);
		final Class<?> classDoc = WebLabModel.getClassFromUri(uriDoc);
		Assert.assertNotNull(classDoc);
		Assert.assertEquals("org.ow2.weblab.core.model.Document", classDoc.getName());

		final String uriLinSeg = WebLabModel.getUriFromSegment(LinearSegment.class);
		Assert.assertEquals("http://weblab.ow2.org/core/1.2/ontology/model#LinearSegment", uriLinSeg);
		final Class<?> classLinSeg = WebLabModel.getClassFromUri(uriLinSeg);
		Assert.assertNotNull(classLinSeg);
		Assert.assertEquals("org.ow2.weblab.core.model.LinearSegment", classLinSeg.getName());

		try {
			WebLabModel.getClassFromUri(null);
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}
		try {
			WebLabModel.getClassFromUri("");
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}
		try {
			WebLabModel.getClassFromUri("weblab://blablablablabllab");
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}
		try {
			WebLabModel.getClassFromUri("http://weblab.ow2.org/1.2/model/class#Toto");
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}
		try {
			WebLabModel.getClassFromUri("http://weblab.ow2.org/1.2/model/class/veryLongNameThatDoNotStartWithTjeGoodNamespace#Toto");
			Assert.fail();
		} catch (final WebLabUncheckedException w) {
			// Normal
			Assert.assertNotNull(w);
		}
		try {
			WebLabModel.getClassFromUri("http://weblab.ow2.org/core/1.2/ontology/model#Coordinate");
			Assert.fail();
		} catch (final ClassCastException cce) {
			// Normal
			Assert.assertNotNull(cce);
		}
	}


}
