/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.exception;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Cassidian WebLab Team
 */
public class ExceptionTest {


	/**
	 * 
	 */
	@Test
	public void testWLCE() {
		try {
			throw new WebLabCheckedException("The message");
		} catch (final WebLabCheckedException wlce) {
			Assert.assertNotNull(wlce);
			Assert.assertEquals("The message", wlce.getMessage());
		}
	}


	/**
	 * 
	 */
	@Test
	public void testWLUE() {
		try {
			throw new WebLabUncheckedException("The message");
		} catch (final RuntimeException re) {
			Assert.assertNotNull(re);
			Assert.assertEquals("The message", re.getMessage());
		}
	}


	/**
	 * 
	 */
	@Test
	public void testInclusion() {
		try {
			throw new WebLabCheckedException("The message", new WebLabUncheckedException("The Unchecked message", new WebLabResourceCreationException(
					"XXXXXXXXXXXXX", new WebLabUncheckedException(new WebLabUncheckedException(new WebLabResourceCreationException(new WebLabRISyntaxException(
							"fdfsd")))))));
		} catch (final Throwable t) {
			Assert.assertNotNull(t);
			Assert.assertNotNull(t.getCause());
			Assert.assertNotNull(t.getCause().getCause());
			Assert.assertEquals("XXXXXXXXXXXXX", t.getCause().getCause().getMessage());
		}
	}


	/**
	 * 
	 */
	@Test
	public void testWLNYIE() {
		try {
			try {
				try {
					try {
						throw new WebLabNotYetImplementedException();
					} catch (final WebLabNotYetImplementedException wlnyie) {
						Assert.assertNotNull(wlnyie);
						Assert.assertNotNull(wlnyie.getMessage());
						Assert.assertFalse(wlnyie.getMessage().isEmpty());
						throw new WebLabNotYetImplementedException("");
					}
				} catch (final WebLabNotYetImplementedException wlnyie) {
					Assert.assertNotNull(wlnyie);
					Assert.assertNotNull(wlnyie.getMessage());
					Assert.assertTrue(wlnyie.getMessage().isEmpty());
					throw new WebLabNotYetImplementedException(wlnyie);
				}
			} catch (final WebLabNotYetImplementedException wlnyie) {
				Assert.assertNotNull(wlnyie);
				Assert.assertNotNull(wlnyie.getMessage());
				Assert.assertFalse(wlnyie.getMessage().isEmpty());
				throw new WebLabNotYetImplementedException("", wlnyie);
			}
		} catch (final WebLabNotYetImplementedException wlnyie) {
			Assert.assertNotNull(wlnyie);
		}
	}


	/**
	 * 
	 */
	@Test
	public void testWebLabResourceCreationException() {
		try {
			throw new WebLabResourceCreationException("The message");
		} catch (final WebLabResourceCreationException wlrce) {
			Assert.assertNotNull(wlrce);
			Assert.assertEquals("The message", wlrce.getMessage());
		}
	}

}
