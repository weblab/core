/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.factory;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.EmptyQueueException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;

/**
 * Testing the ExceptionFactory class.
 * 
 * @author ymombrun
 */
public final class ExceptionFactoryTest {


	private final String message = "Message";


	/**
	 * Test method for {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createAccessDeniedException(String)} and
	 * {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createAccessDeniedException(String, Throwable)}
	 */
	@Test
	public final void testCreateAccessDeniedException() {
		final AccessDeniedException ade1 = ExceptionFactory.createAccessDeniedException(this.message);
		final AccessDeniedException ade2 = ExceptionFactory.createAccessDeniedException(this.message, ade1);

		Assert.assertNotNull(ade1);
		Assert.assertNotNull(ade1.getFaultInfo());
		Assert.assertNotNull(ade1.getMessage());
		Assert.assertNull(ade1.getCause());
		Assert.assertNotNull(ade2);
		Assert.assertNotNull(ade2.getFaultInfo());
		Assert.assertNotNull(ade2.getMessage());
		Assert.assertNotNull(ade2.getCause());
		Assert.assertSame(ade1, ade2.getCause());
		Assert.assertSame(ade1.getClass(), ade2.getClass());
	}



	/**
	 * Test method for {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createContentNotAvailableException(String)}, and
	 * {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createContentNotAvailableException(String, Throwable)}.
	 */
	@Test
	public final void testCreateContentNotAvailableException() {
		final ContentNotAvailableException cnae1 = ExceptionFactory.createContentNotAvailableException(this.message);
		final ContentNotAvailableException cnae2 = ExceptionFactory.createContentNotAvailableException(this.message, cnae1);

		Assert.assertNotNull(cnae1);
		Assert.assertNotNull(cnae1.getFaultInfo());
		Assert.assertNotNull(cnae1.getMessage());
		Assert.assertNull(cnae1.getCause());
		Assert.assertNotNull(cnae2);
		Assert.assertNotNull(cnae2.getFaultInfo());
		Assert.assertNotNull(cnae2.getMessage());
		Assert.assertNotNull(cnae2.getCause());
		Assert.assertSame(cnae1, cnae2.getCause());
		Assert.assertSame(cnae1.getClass(), cnae2.getClass());
	}


	/**
	 * Test method for {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createEmptyQueueException(String)} and
	 * {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createEmptyQueueException(String, Throwable)}.
	 */
	@Test
	public final void testCreateEmptyQueueException() {
		final EmptyQueueException eqe1 = ExceptionFactory.createEmptyQueueException(this.message);
		final EmptyQueueException eqe2 = ExceptionFactory.createEmptyQueueException(this.message, eqe1);

		Assert.assertNotNull(eqe1);
		Assert.assertNotNull(eqe1.getFaultInfo());
		Assert.assertNotNull(eqe1.getMessage());
		Assert.assertNull(eqe1.getCause());
		Assert.assertNotNull(eqe2);
		Assert.assertNotNull(eqe2.getFaultInfo());
		Assert.assertNotNull(eqe2.getMessage());
		Assert.assertNotNull(eqe2.getCause());
		Assert.assertSame(eqe1, eqe2.getCause());
		Assert.assertSame(eqe1.getClass(), eqe2.getClass());
	}


	/**
	 * Test method for {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createInsufficientResourcesException(String)} and
	 * {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createInsufficientResourcesException(String, Throwable)}.
	 */
	@Test
	public final void testCreateInsufficientResourcesException() {
		final InsufficientResourcesException ire1 = ExceptionFactory.createInsufficientResourcesException(this.message);
		final InsufficientResourcesException ire2 = ExceptionFactory.createInsufficientResourcesException(this.message, ire1);

		Assert.assertNotNull(ire1);
		Assert.assertNotNull(ire1.getFaultInfo());
		Assert.assertNotNull(ire1.getMessage());
		Assert.assertNull(ire1.getCause());
		Assert.assertNotNull(ire2);
		Assert.assertNotNull(ire2.getFaultInfo());
		Assert.assertNotNull(ire2.getMessage());
		Assert.assertNotNull(ire2.getCause());
		Assert.assertSame(ire1, ire2.getCause());
		Assert.assertSame(ire1.getClass(), ire2.getClass());
	}


	/**
	 * Test method for {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createInvalidParameterException(String)} and
	 * {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createInvalidParameterException(String, Throwable)}.
	 */
	@Test
	public final void testCreateInvalidParameterException() {
		final InvalidParameterException ipe1 = ExceptionFactory.createInvalidParameterException(this.message);
		final InvalidParameterException ipe2 = ExceptionFactory.createInvalidParameterException(this.message, ipe1);

		Assert.assertNotNull(ipe1);
		Assert.assertNotNull(ipe1.getFaultInfo());
		Assert.assertNotNull(ipe1.getMessage());
		Assert.assertNull(ipe1.getCause());
		Assert.assertNotNull(ipe2);
		Assert.assertNotNull(ipe2.getFaultInfo());
		Assert.assertNotNull(ipe2.getMessage());
		Assert.assertNotNull(ipe2.getCause());
		Assert.assertSame(ipe1, ipe2.getCause());
		Assert.assertSame(ipe1.getClass(), ipe2.getClass());
	}



	/**
	 * Test method for {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createServiceNotConfiguredException(String)} and
	 * {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createServiceNotConfiguredException(String, Throwable)}.
	 */
	@Test
	public final void testCreateServiceNotConfiguredException() {
		final ServiceNotConfiguredException snce1 = ExceptionFactory.createServiceNotConfiguredException(this.message);
		final ServiceNotConfiguredException snce2 = ExceptionFactory.createServiceNotConfiguredException(this.message, snce1);

		Assert.assertNotNull(snce1);
		Assert.assertNotNull(snce1.getFaultInfo());
		Assert.assertNotNull(snce1.getMessage());
		Assert.assertNull(snce1.getCause());
		Assert.assertNotNull(snce2);
		Assert.assertNotNull(snce2.getFaultInfo());
		Assert.assertNotNull(snce2.getMessage());
		Assert.assertNotNull(snce2.getCause());
		Assert.assertSame(snce1, snce2.getCause());
		Assert.assertSame(snce1.getClass(), snce2.getClass());
	}



	/**
	 * Test method for {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createUnexpectedException(String)} and
	 * {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createUnexpectedException(String, Throwable)}.
	 */
	@Test
	public final void testCreateUnexpectedException() {
		final UnexpectedException ue1 = ExceptionFactory.createUnexpectedException(this.message);
		final UnexpectedException ue2 = ExceptionFactory.createUnexpectedException(this.message, ue1);

		Assert.assertNotNull(ue1);
		Assert.assertNotNull(ue1.getFaultInfo());
		Assert.assertNotNull(ue1.getMessage());
		Assert.assertNull(ue1.getCause());
		Assert.assertNotNull(ue2);
		Assert.assertNotNull(ue2.getFaultInfo());
		Assert.assertNotNull(ue2.getMessage());
		Assert.assertNotNull(ue2.getCause());
		Assert.assertSame(ue1, ue2.getCause());
		Assert.assertSame(ue1.getClass(), ue2.getClass());
	}



	/**
	 * Test method for {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createUnsupportedRequestException(String)} and
	 * {@link org.ow2.weblab.core.extended.factory.ExceptionFactory#createUnsupportedRequestException(String, Throwable)}.
	 */
	@Test
	public final void testCreateUnsupportedRequestException() {
		final UnsupportedRequestException ure1 = ExceptionFactory.createUnsupportedRequestException(this.message);
		final UnsupportedRequestException ure2 = ExceptionFactory.createUnsupportedRequestException(this.message, ure1);

		Assert.assertNotNull(ure1);
		Assert.assertNotNull(ure1.getFaultInfo());
		Assert.assertNotNull(ure1.getMessage());
		Assert.assertNull(ure1.getCause());
		Assert.assertNotNull(ure2);
		Assert.assertNotNull(ure2.getFaultInfo());
		Assert.assertNotNull(ure2.getMessage());
		Assert.assertNotNull(ure2.getCause());
		Assert.assertSame(ure1, ure2.getCause());
		Assert.assertSame(ure1.getClass(), ure2.getClass());
	}


}
