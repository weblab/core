/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.uri;


import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabRISyntaxException;


/**
 *
 */
public class WLRITest {


	/**
	 *
	 */
	@Test
	public void testWLRI() {
		final WebLabRI wlri1 = new WebLabRI("uri");
		wlri1.addFragment("t");
		wlri1.addFragment("t");

		try {
			wlri1.addFragment("d#");
			Assert.fail();
		} catch (final WebLabRISyntaxException wlrise) {
			Assert.assertNotNull(wlrise);
		}

		try {
			final WebLabRI wlri = new WebLabRI("uri++***[");
			Assert.fail(wlri.toString());
		} catch (final WebLabRISyntaxException wlrise) {
			Assert.assertNotNull(wlrise);
		}



		try {
			final WebLabRI wlri = new WebLabRI("uri");
			wlri.addFragment("d#");
			Assert.fail();
		} catch (final WebLabRISyntaxException wlrise) {
			Assert.assertNotNull(wlrise);
		}
	}
}
