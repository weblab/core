/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.jaxbe;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.jaxbe.test.DummyResource;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.Resource;


/**
 *
 */
public class ExceptionsTest {


	@Test
	public void testExceptions() throws Exception {
		DummyResource.setDummyResourceType(1);
		WebLabMarshaller wm = new WebLabMarshaller(true);
		try (FileReader fr = new FileReader(new File("src/test/resources/nonWebLab.xml"))) {
			wm.unmarshal(fr, DummyResource.class);
			Assert.fail();
		} catch (final WebLabUncheckedException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		wm = new WebLabMarshaller(true);
		try {
			DummyResource.setDummyResourceType(2);
			wm.marshal(new DummyResource(), new StringWriter(), "");
			Assert.fail();
		} catch (final WebLabUncheckedException e) {
			// success
			Assert.assertNotNull(e);
		}

		wm = new WebLabMarshaller(true);
		try {
			DummyResource.setDummyResourceType(3);
			wm.marshal(new DummyResource(), new StringWriter(), "");
			Assert.fail();
		} catch (final WebLabCheckedException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		wm = new WebLabMarshaller(true);
		try {
			DummyResource.setDummyResourceType(3);
			wm.marshalResource(new DummyResource(), new StringWriter(), true);
			Assert.fail();
		} catch (final WebLabCheckedException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		wm = new WebLabMarshaller(true);
		try (Writer writer = new Writer() {


			@Override
			public void write(final char[] cbuf, final int off, final int len) throws IOException {
				throw new IOException("testing");

			}


			@Override
			public void flush() throws IOException {
				throw new IOException("testing");

			}


			@Override
			public void close() throws IOException {
				// Normal
			}
		}) {
			DummyResource.setDummyResourceType(3);

			wm.marshalResource(new DummyResource(), writer, true);
			Assert.fail();
		} catch (final WebLabCheckedException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		wm = new WebLabMarshaller(true);
		try {
			DummyResource.setDummyResourceType(0);
			wm.unmarshal(new File(""), Resource.class);
			Assert.fail();
		} catch (final WebLabCheckedException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		wm = new WebLabMarshaller(true);
		try {
			DummyResource.setDummyResourceType(4);
			wm.unmarshal(new File("src/test/resources/resourceTest.xml"), Image.class);
			Assert.fail();
		} catch (final WebLabCheckedException e) {
			// Normal
			Assert.assertNotNull(e);
		}
		Assert.assertTrue(wm.setUnmarshallerProperty("test", Boolean.TRUE));

		wm = new WebLabMarshaller(true);
		try {
			DummyResource.setDummyResourceType(2);
			wm.unmarshal(new File("src/test/resources/resourceTest.xml"), Resource.class);
			Assert.fail();
		} catch (final WebLabUncheckedException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		wm = new WebLabMarshaller(true);
		try {
			DummyResource.setDummyResourceType(0);
			wm.marshalResource(new Resource(), new File(""));
			Assert.fail();
		} catch (final WebLabCheckedException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		wm = new WebLabMarshaller(true);
		try {
			DummyResource.setDummyResourceType(3);
			wm.marshalResource(new Resource(), new StringWriter(), true);
			Assert.fail();
		} catch (final WebLabCheckedException e) {
			// Normal
			Assert.assertNotNull(e);
		}

		wm = new WebLabMarshaller(true);
		try (Writer writer = new Writer() {


			@Override
			public void close() throws IOException {
				throw new IOException();
			}


			@Override
			public void flush() throws IOException {
				throw new IOException();

			}


			@Override
			public void write(final char[] cbuf, final int off, final int len) throws IOException {
				throw new IOException();

			}

		};) {
			DummyResource.setDummyResourceType(0);

			wm.marshalResource(new Resource(), writer, true);
			Assert.fail();
		} catch (final WebLabCheckedException e) {
			// Normal
			Assert.assertNotNull(e);
		}

	}


	static void setFinalStatic(final Field field, final Object newValue) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		field.setAccessible(true);

		final Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

		field.set(null, newValue);
	}

}
