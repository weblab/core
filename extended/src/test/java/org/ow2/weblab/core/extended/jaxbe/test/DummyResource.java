/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.jaxbe.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.Validator;

import org.ow2.weblab.core.model.Resource;


/**
 *
 */
@SuppressWarnings("deprecation")
public class DummyResource extends Resource {


	private static final long serialVersionUID = 129L;


	/**
	 * 
	 */
	private static int result = 0;


	/**
	 * @return The result
	 */
	public static int getResult() {
		return DummyResource.result;
	}


	/**
	 * @param i
	 *            The type of the dummy resource
	 */
	public static void setDummyResourceType(final int i) {
		DummyResource.result = i;
	}


	/**
	 * @param classes
	 *            The classes of the context
	 * @param properties
	 *            The properties of the context
	 * @return The context
	 * @throws IllegalAccessException
	 *             ??
	 * @throws ClassNotFoundException
	 *             ??
	 * @throws SecurityException
	 *             ??
	 * @throws NoSuchMethodException
	 *             ??
	 * @throws IllegalArgumentException
	 *             ??
	 * @throws InvocationTargetException
	 *             ??
	 * 
	 */
	public static Object createContext(final Class<?>[] classes, final Map<?, ?> properties) throws IllegalAccessException, ClassNotFoundException,
			SecurityException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
		final Object o;
		switch (DummyResource.result) {
			case -1:
				o = null;
				break;
			case 0:
				final Class<?> clazz = ClassLoader.getSystemClassLoader().loadClass("com.sun.xml.internal.bind.v2.ContextFactory");
				final Method m = clazz.getMethod("createContext", Class[].class, Map.class);
				o = m.invoke(null, classes, properties);
				break;
			case 1:
				throw new IllegalAccessException("for testing purposes");
			case 2:
				o = new JAXBContext() {


					@Override
					@Deprecated
					public Validator createValidator() throws JAXBException {
						throw new JAXBException("testing ...");
					}


					@Override
					public Unmarshaller createUnmarshaller() throws JAXBException {
						throw new JAXBException("testing ...");
					}


					@Override
					public Marshaller createMarshaller() throws JAXBException {
						throw new JAXBException("testing ...");
					}
				};
				break;
			case 3:
				o = new JAXBContext() {


					@Override
					@Deprecated
					public Validator createValidator() throws JAXBException {
						throw new JAXBException("testing ...");
					}


					@Override
					public Unmarshaller createUnmarshaller() throws JAXBException {
						throw new JAXBException("testing ...");
					}


					@Override
					public Marshaller createMarshaller() throws JAXBException {
						return new DummyMarchaller(DummyResource.getResult());
					}
				};
				break;
			case 4:
				o = new JAXBContext() {


					@Override
					@Deprecated
					public Validator createValidator() throws JAXBException {
						throw new JAXBException("testing ...");
					}


					@Override
					public Unmarshaller createUnmarshaller() throws JAXBException {
						return new DummyUnmarchaller();
					}


					@Override
					public Marshaller createMarshaller() throws JAXBException {
						throw new JAXBException("testing ...");
					}
				};
				break;
			default:
				o = null;
		}
		return o;
	}

}
