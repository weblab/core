/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.extended.messages;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Test that every messages exists.
 *
 * @author WebLab Cassidian Team
 */
public class MessagesTest {


	protected final Logger logger = LoggerFactory.getLogger(MessagesTest.class);


	@Test
	public void checkEveryKeysForWebLabMarshaller() throws IllegalArgumentException, IllegalAccessException {
		// Loads the keys of the properties from the class
		final List<String> keys = new ArrayList<>();
		for (final Field field : WebLabMarshaller.class.getDeclaredFields()) {
			if (!field.getType().equals(String.class) || !field.getName().startsWith("KEY_")) {
				this.logger.debug("Skipping field: " + field.getName());
				continue;
			}
			field.setAccessible(true); // They are private...
			final String value = (String) field.get(null); // null since fields are static
			Assert.assertNotNull(value);
			keys.add(value);
		}
		Collections.sort(keys);

		// For each Locale (since we might have various properties file)
		for (final Locale locale : Locale.getAvailableLocales()) {
			final ResourceBundle wlmBundle = ResourceBundle.getBundle(WebLabMarshaller.class.getCanonicalName(), locale);
			// Checks that a message can be loaded
			for (final String key : keys) {
				final String message = Messages.getString(wlmBundle, key);
				Assert.assertFalse("Key " + key + " not found.", message.startsWith("!") && message.endsWith("!"));
			}
			// Checks that the keys are the same than the ones from the bundle
			final List<String> bundleKeys = Collections.list(wlmBundle.getKeys());
			Collections.sort(bundleKeys);
			this.logger.debug("BundleKeys" + bundleKeys.toString());
			this.logger.debug("ClassKeys" + keys.toString());
			Assert.assertEquals("Not the same number of keys.", keys.size(), bundleKeys.size());
			Assert.assertEquals(keys, bundleKeys);
		}
	}


	/**
	 * Tests that the class not be instanciated.
	 * 
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	@Test
	public void testConstructor() throws Exception {
		try {
			for (final Constructor<?> c : Messages.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail("Cause of failure was not an UnsupportedOperationException but a " + ite.getCause());
			}
		}
	}


	@Test
	public void testNotExistingKey() {
		for (final Locale locale : Locale.getAvailableLocales()) {
			final ResourceBundle bundle = ResourceBundle.getBundle(WebLabMarshaller.class.getCanonicalName(), locale);
			final String key = "NotFound";
			final String excl = "!";
			Assert.assertEquals("The message should state that the key has not been found.", excl + key + excl, Messages.getString(bundle, key));
			Assert.assertEquals("The message should state that the key has not been found.", excl + key + excl + Arrays.toString(new String[] { key, key }), Messages.getString(bundle, key, key, key));
		}
	}

}
