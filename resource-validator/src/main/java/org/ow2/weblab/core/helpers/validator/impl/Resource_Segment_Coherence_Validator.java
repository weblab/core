/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.validator.impl;

import java.util.List;

import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helpers.validator.Validator;
import org.ow2.weblab.core.helpers.validator.util.Keys;
import org.ow2.weblab.core.helpers.validator.util.Messages;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.SpatioTemporalSegment;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.TrackSegment;
import org.ow2.weblab.core.model.Video;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Check that a MediaUnit does not contain any incoherent type of Segment.
 * For example, check that a Text resource does not contain any TemporalSegment.
 *
 * More over it does some check on model constraints (TrackSegment contains 2 or more Segments, SpatialSegment have two or more Coordinates, Temporal and Linear
 * Segment starts before they ends).
 */
public final class Resource_Segment_Coherence_Validator implements Validator {


	private static final String TRACK_SEGMENT_IN_ERROR = "TrackSegment in error: ";


	private static final String SEGMENT_URI = "segment uri: ";


	/**
	 * The logger used inside.
	 */
	private final Logger log = LoggerFactory.getLogger(this.getClass());


	@Override
	public boolean validate(final Resource resource) {
		// Get all audios in the resource and check them
		final List<Audio> audioList = ResourceUtil.getSelectedSubResources(resource, Audio.class);
		if (resource instanceof Audio) {
			audioList.add((Audio) resource);
		}
		for (final Audio mediaUnit : audioList) {
			if (mediaUnit.isSetSegment() && !this.checkSegments_Audio(mediaUnit.getSegment())) {
				return false;
			}
		}
		audioList.clear();


		// Get all images in resource and check them
		final List<Image> imageList = ResourceUtil.getSelectedSubResources(resource, Image.class);
		if (resource instanceof Image) {
			imageList.add((Image) resource);
		}
		for (final Image mediaUnit : imageList) {
			if (mediaUnit.isSetSegment() && !this.checkSegments_Image(mediaUnit.getSegment())) {
				return false;
			}
		}
		imageList.clear();


		// Get all texts in resource and check them
		final List<Text> textList = ResourceUtil.getSelectedSubResources(resource, Text.class);
		if (resource instanceof Text) {
			textList.add((Text) resource);
		}
		for (final Text mediaUnit : textList) {
			if (mediaUnit.isSetSegment() && !this.checkSegments_Text(mediaUnit.getSegment())) {
				return false;
			}
		}
		textList.clear();


		// Get all videos in resource and check them
		final List<Video> videoList = ResourceUtil.getSelectedSubResources(resource, Video.class);
		if (resource instanceof Video) {
			videoList.add((Video) resource);
		}
		for (final Video mediaUnit : videoList) {
			if (mediaUnit.isSetSegment() && !this.checkSegments_Video(mediaUnit.getSegment())) {
				return false;
			}
		}
		videoList.clear();


		this.log.info(Messages.getMessage(Keys.VALIDATOR_VALID_FOR, this.getClass().getSimpleName()));
		this.log.info(Messages.getMessage(Keys.VALIDATOR_SEGMENT_COHERENCE_OK));
		return true;
	}


	/**
	 * @param segments
	 *            The list of segment to check validity
	 * @return <code>true</code> if and only if segments are valid (start before end, contains two or more coordinates or segment). (See class comment).
	 */
	private boolean checkSegments(final List<Segment> segments) {
		for (final Segment segment : segments) {
			if (segment instanceof LinearSegment) {
				final LinearSegment linearSeg = (LinearSegment) segment;
				if (linearSeg.getStart() > linearSeg.getEnd()) {
					this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
					this.log.warn(Messages.getMessage(Keys.VALIDATOR_SEGMENT_END_BEFORE_START, segment.getUri()));
					return false;
				}
			} else if (segment instanceof TemporalSegment) {
				final TemporalSegment tempSeg = (TemporalSegment) segment;
				if (tempSeg.getStart() > tempSeg.getEnd()) {
					this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
					this.log.warn(Messages.getMessage(Keys.VALIDATOR_SEGMENT_END_BEFORE_START, segment.getUri()));
					return false;
				}
			} else if (segment instanceof SpatialSegment) {
				// SpatioTemporalSegment goes here too
				final SpatialSegment spatialSeg = (SpatialSegment) segment;
				if (spatialSeg.getCoordinate().size() < 2) {
					this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
					this.log.warn(Messages.getMessage(Keys.VALIDATOR_SEGMENT_TO_SMALL_SPAT, segment.getUri()));
					return false;
				}
			} else if (segment instanceof TrackSegment) {
				final TrackSegment trackSeg = (TrackSegment) segment;
				if (!trackSeg.isSetSegment()) {
					this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
					this.log.warn(Messages.getMessage(Keys.VALIDATOR_SEGMENT_TO_SMALL_TRACK, segment.getUri()));
					return false;
				}
			} else if (segment instanceof SpatioTemporalSegment) {
				final SpatioTemporalSegment spatioTempSeg = (SpatioTemporalSegment) segment;
				if (spatioTempSeg.getCoordinate().size() < 2) {
					this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
					this.log.warn(Messages.getMessage(Keys.VALIDATOR_SEGMENT_TO_SMALL_SPAT, segment.getUri()));
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * @param segments
	 *            The list of segment to be checked
	 * @return True if and only if segments are either linear segment or track segment of linear segment
	 */
	private boolean checkSegments_Text(final List<Segment> segments) {
		if (!this.checkSegments(segments)) {
			return false;
		}

		for (final Segment segment : segments) {
			if (!(segment instanceof LinearSegment) && !(segment instanceof TrackSegment)) {
				this.log.debug(Resource_Segment_Coherence_Validator.SEGMENT_URI + segment.getUri());
				this.log.info(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
				this.log.info(Messages.getMessage(Keys.VALIDATOR_SEGMENT_INCOHERENCE, Text.class.getSimpleName(), segment.getClass().getSimpleName()));
				return false;
			}
			if (segment instanceof TrackSegment) {
				final TrackSegment trackSegments = (TrackSegment) segment;
				if (!this.checkSegments_Text(trackSegments.getSegment())) {
					this.log.debug(Resource_Segment_Coherence_Validator.TRACK_SEGMENT_IN_ERROR + segment.getUri());
					return false;
				}
			}
		}

		return true;
	}


	/**
	 * @param segments
	 *            The list of segment to be checked
	 * @return True if and only if segments are either spatial segment or track segment of spatial segment
	 */
	private boolean checkSegments_Image(final List<Segment> segments) {

		if (!this.checkSegments(segments)) {
			return false;
		}

		for (final Segment segment : segments) {
			if (!(segment instanceof SpatialSegment) && !(segment instanceof TrackSegment)) {
				this.log.debug(Resource_Segment_Coherence_Validator.SEGMENT_URI + segment.getUri());
				this.log.info(Messages.getMessage("resource.invalidfor", this.getClass().getSimpleName()));
				this.log.info(Messages.getMessage("mediaunit.segment.incoherence", Image.class.getSimpleName(), segment.getClass().getSimpleName()));
				return false;
			}
			if (segment instanceof TrackSegment) {
				final TrackSegment trackSegments = (TrackSegment) segment;
				if (!this.checkSegments_Image(trackSegments.getSegment())) {
					this.log.debug(Resource_Segment_Coherence_Validator.TRACK_SEGMENT_IN_ERROR + segment.getUri());
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * @param segments
	 *            The list of segment to be checked
	 * @return True if and only if segments are either temporal segment or track segment of temporal segment
	 */
	private boolean checkSegments_Audio(final List<Segment> segments) {
		if (!this.checkSegments(segments)) {
			return false;
		}

		for (final Segment segment : segments) {
			if (!(segment instanceof TemporalSegment) && !(segment instanceof TrackSegment)) {
				this.log.debug(Resource_Segment_Coherence_Validator.SEGMENT_URI + segment.getUri());
				this.log.info(Messages.getMessage("resource.invalidfor", this.getClass().getSimpleName()));
				this.log.info(Messages.getMessage("mediaunit.segment.incoherence", Audio.class.getSimpleName(), segment.getClass().getSimpleName()));
				return false;
			}
			if (segment instanceof TrackSegment) {
				final TrackSegment trackSegments = (TrackSegment) segment;
				if (!this.checkSegments_Audio(trackSegments.getSegment())) {
					this.log.debug(Resource_Segment_Coherence_Validator.TRACK_SEGMENT_IN_ERROR + segment.getUri());
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * @param segments
	 *            The list of segment to be checked
	 * @return True if and only if segments are either temporal segment, spatio temporal segment or track segment of temporal and spatio temporal segment
	 */
	private boolean checkSegments_Video(final List<Segment> segments) {
		if (!this.checkSegments(segments)) {
			return false;
		}

		for (final Segment segment : segments) {
			if (!(segment instanceof TemporalSegment) && !(segment instanceof SpatioTemporalSegment) && !(segment instanceof TrackSegment)) {
				this.log.debug(Resource_Segment_Coherence_Validator.SEGMENT_URI + segment.getUri());
				this.log.info(Messages.getMessage("resource.invalidfor", this.getClass().getSimpleName()));
				this.log.info(Messages.getMessage("mediaunit.segment.incoherence", Video.class.getSimpleName(), segment.getClass().getSimpleName()));
				return false;
			}
			if (segment instanceof TrackSegment) {
				final TrackSegment trackSegments = (TrackSegment) segment;
				if (!this.checkSegments_Video(trackSegments.getSegment())) {
					this.log.debug(Resource_Segment_Coherence_Validator.TRACK_SEGMENT_IN_ERROR + segment.getUri());
					return false;
				}
			}
		}

		return true;
	}

}
