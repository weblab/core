/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.validator.util;


/**
 * This class only contains static final String constants of messages to be created through the <code>ResourceBundle</code> in {@link Messages}.
 *
 * @author Airbus WebLab Team
 */
public final class Keys {


	public static final String IN_MEMORY_FAILS_ON = "InMemory.FailsOn";


	public static final String VALIDATOR_FILE_CREATE_FAIL = "file.create.fail";


	public static final String VALIDATOR_INVALID = "resource.invalid";


	public static final String VALIDATOR_INVALID_FOR = "resource.invalidfor";


	public static final String VALIDATOR_JENA_FAILS = "resources.jena.models.fails";


	public static final String VALIDATOR_JENA_MODEL_DIF = "resources.jena.models.different";


	public static final String VALIDATOR_MARSHAL_FAILS = "resource.marshall.fail";


	public static final String VALIDATOR_MARSHAL_OK = "resource.marshall.ok";


	public static final String VALIDATOR_MARSHAL_UNMARSHAL_DIF = "resource.marshall.unmarshall.different";


	public static final String VALIDATOR_MARSHAL_UNMARSHAL_OK = "resource.marshall.unmarshall.equals";


	public static final String VALIDATOR_SEGMENT_COHERENCE_OK = "mediaunit.segment.coherence";


	public static final String VALIDATOR_SEGMENT_END_BEFORE_START = "mediaunit.segment.endsbeforestarting";


	public static final String VALIDATOR_SEGMENT_INCOHERENCE = "mediaunit.segment.incoherence";


	public static final String VALIDATOR_SEGMENT_TO_SMALL_SPAT = "mediaunit.segment.toosmallspatial";


	public static final String VALIDATOR_SEGMENT_TO_SMALL_TRACK = "mediaunit.segment.toosmalltrack";


	public static final String VALIDATOR_UNMARSHAL_FAILS = "resource.unmarshall.fail";


	public static final String VALIDATOR_URI_NULL = "resource.uri.null";


	public static final String VALIDATOR_URI_TWICE = "uri.twice";


	public static final String VALIDATOR_URI_UNIQUE = "uri.uniques";


	public static final String VALIDATOR_URI_WELLFORMED = "resource.uri.wellformed";


	public static final String VALIDATOR_URIS_DIFFERS = "resources.uris.different";


	public static final String VALIDATOR_URIS_MALFORMED = "resource.uri.malformed";


	public static final String VALIDATOR_VALID_FOR = "resource.validfor";


	/**
	 * Private unused constructor.
	 */
	private Keys() {
		throw new UnsupportedOperationException("This class only contains constants; no need to instantiate it.");
	}

}
