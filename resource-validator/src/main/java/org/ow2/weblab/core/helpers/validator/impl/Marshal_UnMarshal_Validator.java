/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.validator.impl;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.impl.JenaResourceStructureHelper;
import org.ow2.weblab.core.helpers.validator.Validator;
import org.ow2.weblab.core.helpers.validator.util.Keys;
import org.ow2.weblab.core.helpers.validator.util.Messages;
import org.ow2.weblab.core.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Check that unmarshaling a marshaled resource resolves the and exactly identical resource
 */
public class Marshal_UnMarshal_Validator implements Validator {


	private static final String TEMP_FILE_BASE_NAME = "testMarshallingValidator";


	private static final String TEMP_FILE_PREFIX = ".xml";


	/**
	 * The logger used inside.
	 */
	private final Logger log = LoggerFactory.getLogger(this.getClass());


	@Override
	public boolean validate(final Resource resource) {

		final WebLabMarshaller marshaller = new WebLabMarshaller();

		// Create a temporary file
		final File tempFile;
		try {
			tempFile = File.createTempFile(Marshal_UnMarshal_Validator.TEMP_FILE_BASE_NAME, Marshal_UnMarshal_Validator.TEMP_FILE_PREFIX);
		} catch (final IOException ioe) {
			this.log.error(Messages.getMessage(Keys.VALIDATOR_FILE_CREATE_FAIL), ioe);
			throw new WebLabUncheckedException(Messages.getMessage(Keys.VALIDATOR_FILE_CREATE_FAIL), ioe);
		}

		// Marshal the resource into it
		try {
			marshaller.marshalResource(resource, tempFile);
		} catch (final WebLabCheckedException wlce) {
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID) + " " + Messages.getMessage(Keys.VALIDATOR_MARSHAL_FAILS), wlce);
			return false;
		}

		// Unmarshal another resource from the temporary file
		final Resource marshalledResource;
		try {
			marshalledResource = marshaller.unmarshal(tempFile, Resource.class);
		} catch (final WebLabCheckedException wlce) {
			this.log.info(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
			this.log.info(Messages.getMessage(Keys.VALIDATOR_INVALID) + " " + Messages.getMessage(Keys.VALIDATOR_UNMARSHAL_FAILS), wlce);
			return false;
		}

		// Test equality of resources
		final boolean equals = this.areEquals(marshalledResource, resource);
		if (equals) {
			this.log.info(Messages.getMessage(Keys.VALIDATOR_VALID_FOR, this.getClass().getSimpleName()));
			this.log.info(Messages.getMessage(Keys.VALIDATOR_MARSHAL_UNMARSHAL_OK));

			// Remove the temp file if it worked
			if (!tempFile.delete()) {
				tempFile.deleteOnExit();
			}

		} else {
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID) + " " + Messages.getMessage(Keys.VALIDATOR_MARSHAL_UNMARSHAL_DIF));
		}

		return equals;
	}


	/**
	 * First checks that both resources contains the same resource URIs in the same order and that they Jena model of their structures (using the
	 * rdf-helper-jena-structure) are isomorphic.
	 *
	 * @param resource1
	 *            The resource to be compared to resource2
	 * @param resource2
	 *            The resource to be compared to resources1
	 * @return <code>true</code> only and only if resoures are equals
	 */
	private boolean areEquals(final Resource resource1, final Resource resource2) {

		final List<String> uris1 = new ArrayList<>();
		for (final Resource res : ResourceUtil.getSelectedSubResources(resource1, Resource.class)) {
			uris1.add(res.getUri());
		}

		final List<String> uris2 = new ArrayList<>();
		for (final Resource res : ResourceUtil.getSelectedSubResources(resource2, Resource.class)) {
			uris2.add(res.getUri());
		}

		if (!uris1.equals(uris2)) {
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_URIS_DIFFERS));
			return false;
		}
		final JenaResourceStructureHelper jenaHelperStruct1;
		final JenaResourceStructureHelper jenaHelperStruct2;
		try {
			// check same Jena Model
			jenaHelperStruct1 = new JenaResourceStructureHelper(resource1);
			jenaHelperStruct2 = new JenaResourceStructureHelper(resource2);


		} catch (final RuntimeException re) {
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_JENA_FAILS), re);
			return false;
		}

		if (!jenaHelperStruct1.getInnerJenaModel().isIsomorphicWith(jenaHelperStruct2.getInnerJenaModel())) {
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_JENA_MODEL_DIF));
			return false;
		}

		return true;
	}



}
