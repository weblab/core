/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.validator.impl;


import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.helpers.validator.Validator;
import org.ow2.weblab.core.helpers.validator.util.Keys;
import org.ow2.weblab.core.helpers.validator.util.Messages;
import org.ow2.weblab.core.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Check that the resource can be marshaled in an internal byte array output stream.
 */
public final class CanBeMarshalled_Validator implements Validator {


	/**
	 * The logger used inside.
	 */
	private final Logger log = LoggerFactory.getLogger(this.getClass());


	@Override
	public boolean validate(final Resource resource) {
		final WebLabMarshaller marshaller = new WebLabMarshaller();

		try (final ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
			marshaller.marshalResource(resource, baos);
		} catch (final WebLabCheckedException wlce) {
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID) + " " + Messages.getMessage(Keys.VALIDATOR_MARSHAL_FAILS), wlce);
			return false;
		} catch (final IOException ioe) {
			this.log.debug("Error when closing a stream. Just ignoring it.", ioe);
		}

		this.log.info(Messages.getMessage(Keys.VALIDATOR_VALID_FOR, this.getClass().getSimpleName()));
		this.log.info(Messages.getMessage(Keys.VALIDATOR_MARSHAL_OK));

		return true;
	}

}
