/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.validator.util;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.LoggerFactory;

/**
 * Utility class that provide access to a resource bundle having the canonical name of this class as name.
 */
public final class Messages {


	/**
	 * Private unused constructor.
	 */
	private Messages() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(Messages.class.getCanonicalName());


	/**
	 * @param key
	 *            The key of the message
	 * @return The value of the message in {@link Messages} resource bundle.
	 */
	public static String getMessage(final String key) {
		try {
			return Messages.RESOURCE_BUNDLE.getString(key);
		} catch (final MissingResourceException mre) {
			LoggerFactory.getLogger(Messages.class).warn("No message for key: " + key, mre);
			return "!!!" + key + "!!!";
		}
	}


	/**
	 * @param key
	 *            The key of the message.
	 * @param params
	 *            An array of args to be used for String substitution with the message retrieved.
	 * @return The value of the message in {@link Messages} resource bundle with parameter substitution.
	 */
	public static String getMessage(final String key, final Object... params) {
		try {
			return MessageFormat.format(Messages.RESOURCE_BUNDLE.getString(key), params);
		} catch (final MissingResourceException mre) {
			LoggerFactory.getLogger(Messages.class).warn("No message for key: " + key + ". Other args were: " + Arrays.deepToString(params), mre);
			return "!!!" + key + "!!!";
		}
	}


}
