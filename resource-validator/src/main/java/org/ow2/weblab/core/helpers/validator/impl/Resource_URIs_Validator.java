/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.validator.impl;


import java.net.URI;
import java.net.URISyntaxException;

import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helpers.validator.Validator;
import org.ow2.weblab.core.helpers.validator.util.Keys;
import org.ow2.weblab.core.helpers.validator.util.Messages;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Segment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Check that the URIs contained in the resource are wellFormed.
 */
public final class Resource_URIs_Validator implements Validator {


	/**
	 * The logger used inside.
	 */
	private final Logger log = LoggerFactory.getLogger(this.getClass());


	@Override
	public boolean validate(final Resource resource) {
		if (!this.wellFormedUri(resource.getUri())) {
			return false;
		}

		// Get every sub resources
		for (final Resource res : ResourceUtil.getSelectedSubResources(resource, Resource.class)) {
			if (!this.wellFormedUri(res.getUri())) {
				return false;
			}
		}

		// Get every MediaUnit and their segments
		for (final MediaUnit mediaUnit : ResourceUtil.getSelectedSubResources(resource, MediaUnit.class)) {
			if (mediaUnit.isSetSegment()) {
				for (final Segment seg : mediaUnit.getSegment()) {
					if (!this.wellFormedUri(seg.getUri())) {
						return false;
					}
				}
			}
		}

		// If the resource is it self a media unit, retrieve its segments
		if (resource instanceof MediaUnit) {
			final MediaUnit mediaUnit = (MediaUnit) resource;
			if (mediaUnit.isSetSegment()) {
				for (final Segment seg : mediaUnit.getSegment()) {
					if (!this.wellFormedUri(seg.getUri())) {
						return false;
					}
				}
			}
		}


		this.log.info(Messages.getMessage(Keys.VALIDATOR_VALID_FOR, this.getClass().getSimpleName()));
		this.log.info(Messages.getMessage(Keys.VALIDATOR_URI_WELLFORMED));

		return true;
	}


	/**
	 * @param uriString
	 *            The string that represents an URI
	 * @return <code>true</code> if and only if the uri can be created using the URI default constructor
	 */
	private boolean wellFormedUri(final String uriString) {
		if (uriString == null) {
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID) + " " + Messages.getMessage(Keys.VALIDATOR_URI_NULL));
			return false;
		}

		final URI uri;
		try {
			uri = new URI(uriString);
		} catch (final URISyntaxException urise) {
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
			this.log.warn(Messages.getMessage(Keys.VALIDATOR_INVALID) + " " + Messages.getMessage(Keys.VALIDATOR_URIS_MALFORMED, uriString), urise);
			return false;
		}
		return uri.isAbsolute();
	}



}
