/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.validator;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Resource;

/**
 * @author lkhelif
 */
public final class ResourceValidator {



	private ResourceValidator() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	/**
	 * Usage: java -jar ResourceValidator [FileToValidate]
	 *
	 * @param args
	 *            The received arguments
	 */
	public static void main(final String[] args) {
		if (args.length == 0) {
			System.out.println("Usage: java -jar ResourceValidator [FileToValidate]");
		} else {
			final File file = new File(args[0]);
			if (file.exists()) {
				System.out.println(ResourceValidator.validateFile(file) ? "Resource is valid!!" : "Resource is NOT valid!!");
			} else {
				System.out.println("No such file: " + args[0]);
			}
		}
	}


	/**
	 * @param xml
	 *            The XML string to validate
	 * @return Whether or not the xml was a valid WebLab resource
	 */
	public static boolean validateXml(final String xml) {
		final InMemoryExecutor validator = new InMemoryExecutor();
		final InputStream stream = new ByteArrayInputStream(xml.getBytes());
		Resource resource;

		try {
			resource = new WebLabMarshaller().unmarshal(stream, Resource.class);
		} catch (final WebLabCheckedException e) {
			e.printStackTrace();
			return false;
		}

		return validator.validate(resource);
	}


	/**
	 * @param xml
	 *            The xml file to validate
	 * @return Whether or not the xml was a valid WebLab resource
	 */
	public static boolean validateFile(final File xml) {
		final InMemoryExecutor validator = new InMemoryExecutor();
		try {
			return validator.validate(new WebLabMarshaller().unmarshal(xml, Resource.class));
		} catch (final WebLabCheckedException e) {
			e.printStackTrace();
			return false;
		}
	}

}
