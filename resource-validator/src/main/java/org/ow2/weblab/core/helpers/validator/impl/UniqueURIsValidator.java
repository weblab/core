/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.validator.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helpers.validator.Validator;
import org.ow2.weblab.core.helpers.validator.util.Keys;
import org.ow2.weblab.core.helpers.validator.util.Messages;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Segment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Checks that URIs of each resource and segment inside this resource are unique within the scope of the main resource.
 */
public final class UniqueURIsValidator implements Validator {


	/**
	 * The logger used inside.
	 */
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	@Override
	public boolean validate(final Resource resource) {
		final Set<String> uris = new HashSet<String>();

		// Add the main resource
		uris.add(resource.getUri());

		// Check for every resource in resource
		final List<Resource> resources = ResourceUtil.getSelectedSubResources(resource, Resource.class);
		for (final Resource res : resources) {
			if (!uris.add(res.getUri())) {
				this.logger.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
				this.logger.warn(Messages.getMessage(Keys.VALIDATOR_INVALID) + " " + Messages.getMessage(Keys.VALIDATOR_URI_TWICE, res.getUri()));
				return false;
			}
		}
		resources.clear();

		// Check for every segment in every mediaUnit
		final List<MediaUnit> mediaUnits = ResourceUtil.getSelectedSubResources(resource, MediaUnit.class);
		for (final MediaUnit mu : mediaUnits) {
			if (mu.isSetSegment()) {
				for (final Segment seg : mu.getSegment()) {
					if (!uris.add(seg.getUri())) {
						this.logger.warn(Messages.getMessage(Keys.VALIDATOR_INVALID_FOR, this.getClass().getSimpleName()));
						this.logger.warn(Messages.getMessage(Keys.VALIDATOR_INVALID) + " " + Messages.getMessage(Keys.VALIDATOR_URI_TWICE, seg.getUri()));
						return false;
					}
				}
			}
		}
		mediaUnits.clear();
		uris.clear();

		this.logger.info(Messages.getMessage(Keys.VALIDATOR_VALID_FOR, this.getClass().getSimpleName()));
		this.logger.info(Messages.getMessage(Keys.VALIDATOR_URI_UNIQUE));

		return true;
	}

}
