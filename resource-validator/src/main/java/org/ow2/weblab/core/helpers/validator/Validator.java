/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.validator;

import org.ow2.weblab.core.model.Resource;

/**
 * Interface to be implemented for the validation of WebLab resources.
 *
 * The normal behavior is NOT to throw an exception when a resource is invalid, but to log the error and return false.
 */
public interface Validator {


	/**
	 * Checks that the resource in param is valid against a given pattern.
	 * The normal behavior is NOT to throw an exception when a resource is invalid, but to log the error and return false.
	 *
	 * @param resource
	 *            The resource to be validated
	 * @return <code>true</code> if the resource is valid, <code>false</code> otherwise
	 */
	public boolean validate(final Resource resource);

}
