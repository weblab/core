/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ServiceLoader;

import org.ow2.weblab.core.helpers.validator.util.Keys;
import org.ow2.weblab.core.helpers.validator.util.Messages;
import org.ow2.weblab.core.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This specific validator implementation can be initialised with a list of validator to execute.
 * The validator are executed in the list order and stops at the first that fails.
 */
public final class InMemoryExecutor implements Validator {


	/**
	 * The list of validator to be executed
	 */
	private final List<Validator> validatorsList;

/**
	 * The logger used inside.
	 */
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	/**
	 * Default constructor that load every validator in the defined in the <tt>META-INF/services/org.ow2.weblab.core.helpers.validator.Validator</tt> file.
	 */
	public InMemoryExecutor() {
		this(InMemoryExecutor.getAllImplementingClass());
	}


	/**
	 * Create an InMemoreExecutor with this list of validators.
	 *
	 * @param validators
	 *            Validators to be executed
	 */
	public InMemoryExecutor(final Validator... validators) {
		this(new ArrayList<>(Arrays.asList(validators)));
	}


	/**
	 * Create an InMemoreExecutor with this list of validators.
	 *
	 * @param validators
	 *            Validators to be executed
	 */
	public InMemoryExecutor(final List<Validator> validators) {
		this.validatorsList = new ArrayList<>(validators);
		this.logger.debug("InMemoryExecutor created with the following list of validator: " + this.validatorsList);
	}


	@Override
	public boolean validate(final Resource resource) {
		for (final Validator validator : this.validatorsList) {
			if (!validator.validate(resource)) {
				this.logger.warn(Messages.getMessage(Keys.IN_MEMORY_FAILS_ON, resource.getUri(), validator.getClass().getCanonicalName()));
				return false;
			}
		}
		return true;
	}


	/**
	 * @return Every Validator in the classloader
	 */
	private static List<Validator> getAllImplementingClass() {
		final ServiceLoader<Validator> loader = ServiceLoader.load(Validator.class);
		final List<Validator> validators = new LinkedList<>();
		for (final Validator validator : loader) {
			validators.add(validator);
		}
		return validators;
	}


	/**
	 * add a validator to the InMemoreExecutor list of validators.
	 *
	 * @param validator
	 *            Validator to be add
	 */
	public void addValidator(final Validator validator) {
		this.validatorsList.add(validator);
	}

}
