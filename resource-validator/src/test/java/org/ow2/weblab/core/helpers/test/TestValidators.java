/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.test;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helpers.validator.InMemoryExecutor;
import org.ow2.weblab.core.helpers.validator.ResourceValidator;
import org.ow2.weblab.core.helpers.validator.Validator;
import org.ow2.weblab.core.helpers.validator.impl.CanBeMarshalled_Validator;
import org.ow2.weblab.core.helpers.validator.impl.Marshal_UnMarshal_Validator;
import org.ow2.weblab.core.helpers.validator.impl.Resource_Segment_Coherence_Validator;
import org.ow2.weblab.core.helpers.validator.impl.Resource_URIs_Validator;
import org.ow2.weblab.core.helpers.validator.impl.UniqueURIsValidator;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Coordinate;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.SpatioTemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.TrackSegment;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.purl.dc.elements.DublinCoreAnnotator;


public class TestValidators {


	private final File validResource = new File("src/test/resources/ValidResource.xml");


	private final File invalidResource = new File("src/test/resources/InvalidResource.xml");


	@Test
	public void testCanBeMarshalledValidator() throws WebLabCheckedException {
		final Validator validator = new CanBeMarshalled_Validator();

		// Test with working resources
		Assert.assertTrue("Resource should be valid.", validator.validate(new WebLabMarshaller().unmarshal(this.validResource, Resource.class)));
		Assert.assertTrue("Resource should be valid.", validator.validate(this.createComplexMultiDoc()));


		// Create a definitely bad resource with an infinite loop
		final PieceOfKnowledge pok = WebLabResourceFactory.createResource("pok", "withUnmarshableContent", PieceOfKnowledge.class);
		pok.setData(pok);

		// Test with a false resource
		Assert.assertFalse("Resource should be invalid.", validator.validate(pok));
	}


	@Test
	public void testMarshal_Unmarshal_Validator() throws WebLabCheckedException {
		final Validator validator = new Marshal_UnMarshal_Validator();

		// Test with working resources
		Assert.assertTrue("Resource should be valid.", validator.validate(new WebLabMarshaller().unmarshal(this.validResource, Resource.class)));
		Assert.assertTrue("Resource should be valid.", validator.validate(this.createComplexMultiDoc()));

		// Create bad resources with an infinite loop
		final PieceOfKnowledge pok = WebLabResourceFactory.createResource("pok", "withInfiniteLoop", PieceOfKnowledge.class);
		pok.setData(pok);

		final PieceOfKnowledge pok1 = WebLabResourceFactory.createResource("pok", "withStringData", Annotation.class);
		pok1.setData("You should never do this. The content here should be a valid RDF/XML graph, not a String");

		final Text text = WebLabResourceFactory.createResource("text", "WithUnknownSegmentType", Text.class);
		text.getSegment().add(new FakeSegment());

		// Test with a false resource
		Assert.assertFalse("Resource should be invalid (loop).", validator.validate(pok));
		Assert.assertFalse("Resource should be invalid (String in pok data).", validator.validate(pok1));
		Assert.assertFalse("Resource should be invalid (Unknown segment type).", validator.validate(text));
	}


	@Test
	public void testResource_Segment_Coherence_Validator() throws WebLabCheckedException {
		final Validator validator = new Resource_Segment_Coherence_Validator();

		// Start with simple tests with working resources

		Assert.assertTrue("Resource should be valid.", validator.validate(new WebLabMarshaller().unmarshal(this.validResource, Resource.class)));
		Assert.assertTrue("Resource should be valid.", validator.validate(this.createComplexMultiDoc()));

		final Document emptyDocument = WebLabResourceFactory.createResource("doc", "EmptyDocument", Document.class);
		WebLabResourceFactory.createAndLinkMediaUnit(emptyDocument, Image.class);
		WebLabResourceFactory.createAndLinkMediaUnit(emptyDocument, Video.class);
		WebLabResourceFactory.createAndLinkMediaUnit(emptyDocument, Text.class);
		WebLabResourceFactory.createAndLinkMediaUnit(emptyDocument, Audio.class);
		Assert.assertTrue("Resource should be valid.", validator.validate(emptyDocument));

		Assert.assertFalse("Resource should be invalid.", validator.validate(new WebLabMarshaller().unmarshal(this.invalidResource, Resource.class)));


		// Then work with Text documents
		final Text text1 = WebLabResourceFactory.createResource("text", "WithUnknownSegmentType", Text.class);
		SegmentFactory.createAndLinkLinearSegment(text1, 0, 10);
		text1.getSegment().add(new FakeSegment());
		Assert.assertFalse("Resource should be invalid (Unknown segment type).", validator.validate(text1));

		final Text text2 = WebLabResourceFactory.createResource("text", "WithSpatialSegment", Text.class);
		final TrackSegment textTs2 = SegmentFactory.createAndLinkSegment(text2, TrackSegment.class);
		SegmentFactory.createAndLinkLinearSegment(textTs2, 0, 5);
		SegmentFactory.createAndLinkLinearSegment(textTs2, 2, 6);
		SegmentFactory.createAndLinkSpatialSegment(text2, 0, 0, 0, 0, 0, 0, 0, 0);
		Assert.assertFalse("Resource should be invalid (spatial segment in text).", validator.validate(text2));

		final Text text3 = WebLabResourceFactory.createResource("text", "WithInvertedLinear", Text.class);
		SegmentFactory.createAndLinkLinearSegment(text3, 0, 0).setStart(10);
		Assert.assertFalse("Resource should be invalid (inverted linear segment).", validator.validate(text3));

		final Text text4 = WebLabResourceFactory.createResource("text", "WithEmptyTrack", Text.class);
		SegmentFactory.createAndLinkSegment(text4, TrackSegment.class);
		Assert.assertFalse("Resource should be invalid (empty track).", validator.validate(text4));

		final Text text5 = WebLabResourceFactory.createResource("text", "WithSpatialSegmentInTrack", Text.class);
		final TrackSegment textTs5 = SegmentFactory.createAndLinkSegment(text5, TrackSegment.class);
		SegmentFactory.createAndLinkSpatialSegment(textTs5, 0, 0, 0, 0, 0, 0, 0, 0);
		Assert.assertFalse("Resource should be invalid (spatial segment in track in text).", validator.validate(text5));

		// Then work with Audio documents
		final Audio audio1 = WebLabResourceFactory.createResource("audio", "WithLinearSegment", Audio.class);
		final TrackSegment audioTs1 = SegmentFactory.createAndLinkSegment(audio1, TrackSegment.class);
		SegmentFactory.createAndLinkTemporalSegment(audioTs1, 0, 5);
		SegmentFactory.createAndLinkTemporalSegment(audioTs1, 2, 6);
		SegmentFactory.createAndLinkTemporalSegment(audio1, 0, 50);
		SegmentFactory.createAndLinkSegment(audio1, LinearSegment.class);
		Assert.assertFalse("Resource should be invalid (temporal segment in audio).", validator.validate(audio1));

		final Audio audio2 = WebLabResourceFactory.createResource("audio", "WithInvertedTemporal", Audio.class);
		SegmentFactory.createAndLinkTemporalSegment(audio2, 0, 0).setStart(10);
		Assert.assertFalse("Resource should be invalid (inverted temporal).", validator.validate(audio2));

		final Audio audio3 = WebLabResourceFactory.createResource("audio", "WithLinearSegmentInTrack", Audio.class);
		final TrackSegment audioTs3 = SegmentFactory.createAndLinkSegment(audio3, TrackSegment.class);
		SegmentFactory.createAndLinkSegment(audioTs3, LinearSegment.class);
		Assert.assertFalse("Resource should be invalid (temporal segment in audio in track).", validator.validate(audio3));

		// Then work with Image documents
		final Image image1 = WebLabResourceFactory.createResource("image", "WithEmptySpatialInTrack", Image.class);
		final TrackSegment imageTs1 = SegmentFactory.createAndLinkSegment(image1, TrackSegment.class);
		SegmentFactory.createAndLinkSpatialSegment(imageTs1, 0, 0, 0, 0, 0, 0, 0, 0);
		final TrackSegment imageTs2 = SegmentFactory.createAndLinkSegment(image1, TrackSegment.class);
		SegmentFactory.createAndLinkSegment(imageTs2, SpatialSegment.class).getCoordinate().add(new Coordinate());
		Assert.assertFalse("Resource should be invalid (spatial segment has a single coordinate).", validator.validate(image1));

		final Image image2 = WebLabResourceFactory.createResource("image", "WithLinear", Image.class);
		SegmentFactory.createAndLinkSegment(image2, LinearSegment.class);
		Assert.assertFalse("Resource should be invalid (linear segment in image).", validator.validate(image2));

		// Then work with Video documents
		final Video video1 = WebLabResourceFactory.createResource("video", "WithEmptySpatial", Video.class);
		final TrackSegment videoTs1 = SegmentFactory.createAndLinkSegment(video1, TrackSegment.class);
		SegmentFactory.createAndLinkSegment(videoTs1, SpatioTemporalSegment.class).getCoordinate().add(new Coordinate());
		Assert.assertFalse("Resource should be invalid (spatial temporal segment has a single coordinate).", validator.validate(video1));
	}


	@Test
	public void testResource_URIs_Validator() throws WebLabCheckedException {
		final Validator validator = new Resource_URIs_Validator();

		// Test with working resources
		Assert.assertTrue("Resource should be valid.", validator.validate(new WebLabMarshaller().unmarshal(this.validResource, Resource.class)));
		Assert.assertTrue("Resource should be valid.", validator.validate(this.createComplexMultiDoc()));

		final Text text = WebLabResourceFactory.createResource("text", "WithASegmentWithoutUri", Text.class);
		text.getSegment().add(new FakeSegment());

		final Resource res = new Resource();
		res.setUri(":BAD:---");

		Assert.assertFalse("Resource should be invalid (Unknown segment type).", validator.validate(text));
		Assert.assertFalse("Resource should be invalid (BAD uri).", validator.validate(res));
		Assert.assertFalse("Resource should be invalid.", validator.validate(new WebLabMarshaller().unmarshal(this.invalidResource, Resource.class)));
	}


	@Test
	public void testUniqueURIsValidator() throws WebLabCheckedException {
		final Validator validator = new UniqueURIsValidator();

		// Test with loaded resources
		Assert.assertTrue("Resource should be valid.", validator.validate(new WebLabMarshaller().unmarshal(this.validResource, Resource.class)));
		Assert.assertTrue("Resource should be valid.", validator.validate(this.createComplexMultiDoc()));
		Assert.assertFalse("Resource should be invalid.", validator.validate(new WebLabMarshaller().unmarshal(this.invalidResource, Resource.class)));


		final Document doc1 = WebLabResourceFactory.createResource("doc", "WithAnAnnotationAndASegmentWithTheSameUri", Document.class);
		SegmentFactory.createAndLinkLinearSegment(WebLabResourceFactory.createAndLinkMediaUnit(doc1, Text.class), 0, 10).setUri(WebLabResourceFactory.createAndLinkAnnotation(doc1).getUri());
		Assert.assertFalse("Resource should be invalid (Twice same Uri with segments).", validator.validate(doc1));

		final Document doc2 = WebLabResourceFactory.createResource("doc", "WithAnAnnotationAndASegmentWithTheSameUri", Document.class);
		WebLabResourceFactory.createAndLinkMediaUnit(doc2, Text.class).setUri("duplicate");
		WebLabResourceFactory.createAndLinkMediaUnit(doc2, Text.class).setUri("duplicate");
		Assert.assertFalse("Resource should be invalid (Twice same Uri with documents).", validator.validate(doc2));
	}



	@Test
	public void test_InMemoryExtractor() throws WebLabCheckedException {
		final InMemoryExecutor validator = new InMemoryExecutor();
		Assert.assertTrue(validator.validate(new WebLabMarshaller().unmarshal(this.validResource, Resource.class)));
		Assert.assertTrue(validator.validate(this.createComplexMultiDoc()));

		Assert.assertFalse(validator.validate(new WebLabMarshaller().unmarshal(this.invalidResource, Resource.class)));


		final InMemoryExecutor validator2 = new InMemoryExecutor(new Resource_URIs_Validator());
		Assert.assertTrue(validator2.validate(this.createComplexMultiDoc()));
		validator2.addValidator(new UniqueURIsValidator());
		Assert.assertTrue(validator2.validate(this.createComplexMultiDoc()));
	}


	@Test
	public void testResourceValidator() throws Exception {
		try {
			for (final Constructor<?> c : ResourceValidator.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}

		// Test from file
		Assert.assertTrue(ResourceValidator.validateFile(this.validResource));
		Assert.assertFalse(ResourceValidator.validateFile(this.invalidResource));
		Assert.assertFalse(ResourceValidator.validateFile(new File("target"))); // Fail on not file

		// Test from XML
		Assert.assertTrue(ResourceValidator.validateXml(ResourceUtil.saveToXMLString(this.createComplexMultiDoc())));
		Assert.assertFalse(ResourceValidator.validateXml("Not XML!"));

		// Test using main (how to assess results?)
		ResourceValidator.main(new String[] {});
		ResourceValidator.main(new String[] { "missingFile" });
		ResourceValidator.main(new String[] { this.validResource.getAbsolutePath() });
		ResourceValidator.main(new String[] { this.invalidResource.getAbsolutePath() });
	}



	public Document createComplexMultiDoc() throws WebLabCheckedException {

		/*
		 * Document creation.
		 */
		final Document document = WebLabResourceFactory.createResource("video", "res_584697", Document.class);

		/*
		 * Create an annotator dedicated to the annotation of document using properties defined by dublin core ontology.
		 */
		final DublinCoreAnnotator dcAnnot = new DublinCoreAnnotator(document);

		/*
		 * Add the source of the video
		 */
		dcAnnot.writeSource("http://aljazeera.net/portal");

		/*
		 * Add the mime type of the video file
		 */
		dcAnnot.writeFormat("video/mp4");

		/*
		 * Create another annotator. This one dedicated to the annotation of document using properties defined by WebLab
		 * Processing ontology.
		 */
		WProcessingAnnotator wpAnnot = new WProcessingAnnotator(document);


		/*
		 * Create a Java Date
		 */
		final Calendar cal = Calendar.getInstance();
		cal.set(2007, 3, 9, 17, 37, 53);
		final Date gatheringDate = cal.getTime();

		/*
		 * Add the gathering date (yes it was a long time ago)
		 */
		wpAnnot.writeGatheringDate(gatheringDate);

		wpAnnot.writeExposedAs("ftp://ftp.myHost/video/res_584697.mp4");

		/*
		 * Create a marshaler, that eases the use of JAXB marshalling process.
		 */
		final WebLabMarshaller wlm = new WebLabMarshaller();


		/*
		 * Create a video unit inside the document
		 */
		final Video video = WebLabResourceFactory.createAndLinkMediaUnit(document, Video.class);

		wpAnnot = new WProcessingAnnotator(video);
		wpAnnot.writeNormalisedContent(URI.create("rtp://servX1/" + video.getUri().hashCode()));



		/*
		 * Create 4 images inside the document. They are extracted from the video and have a normalised content URI
		 */
		final Image image1 = WebLabResourceFactory.createAndLinkMediaUnit(document, Image.class);
		wpAnnot = new WProcessingAnnotator(image1);
		wpAnnot.writeNormalisedContent(URI.create("http://10.134.0.1/" + image1.getUri().hashCode()));

		final Image image2 = WebLabResourceFactory.createAndLinkMediaUnit(document, Image.class);
		wpAnnot = new WProcessingAnnotator(image2);
		wpAnnot.writeNormalisedContent(URI.create("http://10.134.0.1/" + image2.getUri().hashCode()));

		final Image image3 = WebLabResourceFactory.createAndLinkMediaUnit(document, Image.class);
		wpAnnot = new WProcessingAnnotator(image3);
		wpAnnot.writeNormalisedContent(URI.create("http://10.134.0.1/" + image3.getUri().hashCode()));

		final Image image4 = WebLabResourceFactory.createAndLinkMediaUnit(document, Image.class);
		wpAnnot = new WProcessingAnnotator(image4);
		wpAnnot.writeNormalisedContent(URI.create("http://10.134.0.1/" + image4.getUri().hashCode()));


		/*
		 * Create an audio unit. It is extracted from the video and has a normalised content URI
		 */
		final Audio audio = WebLabResourceFactory.createAndLinkMediaUnit(document, Audio.class);
		wpAnnot = new WProcessingAnnotator(audio);
		wpAnnot.writeNormalisedContent(URI.create("rtsp://servideo/" + audio.getUri().hashCode()));






		/*
		 * Create the text content of the audio transcription (speech-to-text).
		 */
		final Text transcription = WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class);
		wpAnnot = new WProcessingAnnotator(transcription);

		transcription.setContent(
				"Ù‚ØªÙ„ 25 Ø´Ø®ØµØ§ ÙˆØ£ØµÙŠØ¨ 35 Ø¢Ø®Ø±ÙˆÙ† Ù�ÙŠ Ø§Ù†Ù�Ø¬Ø§Ø± Ù‚ÙˆÙŠ Ù�ÙŠ Ø´Ù…Ø§Ù„ ØºØ±Ø¨ÙŠ Ø¨Ø§ÙƒØ³ØªØ§Ù†ØŒ ÙƒÙ…Ø§ Ù‚ØªÙ„ Ø£Ø±Ø¨Ø¹Ø© Ø£Ø´Ø®Ø§Øµ ÙˆØ£ØµÙŠØ¨ Ø§Ø«Ù†Ø§Ù† Ø¢Ø®Ø±Ø§Ù† Ù�ÙŠ Ù‚ØµÙ� ØµØ§Ø±ÙˆØ®ÙŠ Ø§Ø³ØªÙ‡Ø¯Ù� Ø³ÙŠØ§Ø±Ø© Ù�ÙŠ Ø¥Ù‚Ù„ÙŠÙ… Ø´Ù…Ø§Ù„ ÙˆØ²ÙŠØ±Ø³ØªØ§Ù†ØŒ Ù…Ù…Ø§ Ø¯Ù�Ø¹ Ø§Ù„Ø¹Ø´Ø±Ø§Øª Ù…Ù† Ø§Ù„Ù…ØªØ¶Ø±Ø±ÙŠÙ† Ù…Ù† Ø£Ù…Ø«Ø§Ù„ Ù‡Ø°Ø§ Ø§Ù„Ù‚ØµÙ� Ø¥Ù„Ù‰ Ø§Ù„ØªØ¸Ø§Ù‡Ø± Ø£Ù…Ø§Ù… Ù…Ù‚Ø± Ø§Ù„Ø¨Ø±Ù„Ù…Ø§Ù† Ø§Ù„Ø¨Ø§ÙƒØ³ØªØ§Ù†ÙŠ.");




		/*
		 * Create a text section extracted from a segment in an image.
		 */
		final Text recognisedText = WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class);
		recognisedText.setContent("A Message from Shaykh Usama bin Ladin to the America People");
		wpAnnot = new WProcessingAnnotator(recognisedText);

		/*
		 * Create the spatial segment in image 2 and set its position
		 */
		final SpatialSegment segment = SegmentFactory.createAndLinkSegment(image2, SpatialSegment.class);
		final Coordinate c1 = new Coordinate();
		c1.setX(85);
		c1.setY(282);
		segment.getCoordinate().add(c1);
		final Coordinate c2 = new Coordinate();
		c2.setX(453);
		c2.setY(282);
		segment.getCoordinate().add(c2);
		final Coordinate c3 = new Coordinate();
		c3.setX(453);
		c3.setY(311);
		segment.getCoordinate().add(c3);
		final Coordinate c4 = new Coordinate();
		c4.setX(85);
		c4.setY(311);
		segment.getCoordinate().add(c4);

		/*
		 * Link the recognised text with the spatial segment
		 */





		/*
		 * Extract named entities and create linearsegments in the english text
		 */
		final LinearSegment benLadenSegment = SegmentFactory.createAndLinkLinearSegment(recognisedText, 15, 37);
		wpAnnot.startInnerAnnotatorOn(URI.create(benLadenSegment.getUri()));
		wpAnnot.writeRefersTo(URI.create("http://knowledgeBase/Person/BenLaden#0"));
		wpAnnot.endInnerAnnotator();

		final LinearSegment americaSegment = SegmentFactory.createAndLinkLinearSegment(recognisedText, 45, 52);
		wpAnnot.startInnerAnnotatorOn(URI.create(americaSegment.getUri()));
		wpAnnot.writeRefersTo(URI.create("http://knowledgeBase/Location/America#5"));
		wpAnnot.endInnerAnnotator();


		wlm.marshalResource(document, new File("target/createdDocument.xml"));

		return document;
	}



	class FakeSegment extends Segment {


		private static final long serialVersionUID = -1L;



		FakeSegment() {
			// Nothing to do here
		}
	}




}
