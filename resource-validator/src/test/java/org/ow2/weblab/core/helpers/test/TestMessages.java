/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.helpers.validator.util.Keys;
import org.ow2.weblab.core.helpers.validator.util.Messages;



/**
 * Checks that every key in the Keys class is described in messages.properties.
 */
public class TestMessages {


	@Test
	public void checkEveryKeys() throws IllegalArgumentException, IllegalAccessException {
		for (final Field field : Keys.class.getFields()) {
			if (!field.getType().equals(String.class)) {
				Assert.fail("Field " + field.getName() + " is not String.");
			}
			final String value = (String) field.get(null); // null since fields are static
			final String message = Messages.getMessage(value);
			Assert.assertFalse("Key " + value + " not found.", message.startsWith("!") && message.endsWith("!"));
		}
	}



	@Test
	public void testConstructor() throws InstantiationException, IllegalAccessException {
		try {
			for (final Constructor<?> c : Keys.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}
		try {
			for (final Constructor<?> c : Messages.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail();
			}
		}
	}


	@Test
	public void testMissingMessage() throws Exception {
		Assert.assertEquals("!!!toto!!!", Messages.getMessage("toto"));
		Assert.assertEquals("!!!toto!!!", Messages.getMessage("toto", "any param"));
	}


}
