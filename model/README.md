# WebLab Model

This project is in charge of generating the Java core objects of the WebLab model based on the XSD and WSDL provided in this project.

The generation makes use of CXF Codegen Tool associated with a few plugins (to generate default values in the Java code and to generate some classes marked as deprecated).

## Content of this project folders (inside schemas)
		
### model
It contains the XSD file of the XML WebLab exchange model.	This is used to generate the java classes of the model such as Resource or MediaUnit.

### interfaces
It contains the WSDL files (and their types XSD) defining the interfaces of the components and services.

### services
It contains a single WSDL file implementing all the WebLab interfaces. It is only used to generate the java classes of the interfaces (wsimport need a service in the WSDL that should not appear in interfaces). It is also provided inside the model.jar file resulting of the build.

### bindings
It contains jaxws/jaxb bindings files. It used to customise the XML to Java binding from XSDs/WSDLs. 

### ontologies
It contains the OWL files with standardised semantic properties and objects.	

### uml
It contains the UML class diagram of the WebLab Model.

![WebLab Model 1.2.9](https://gitlab.ow2.org/weblab/core/raw/master/model/schemas/uml/Model.png "WebLab Model 1.2.9")