<?xml version="1.0" encoding="UTF-8"?>
<!--
	WEBLAB: Service oriented integration platform for media mining and intelligence applications

	Copyright (C) 2004 - 2017 Airbus Defence and Space
-->
<definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" 
	xmlns:tns="http://weblab.ow2.org/core/1.2/services/auditable"
	xmlns:types="http://weblab.ow2.org/core/1.2/services/auditable" 
	xmlns:services="http://weblab.ow2.org/core/1.2/services/"
	xmlns:ex="http://weblab.ow2.org/core/1.2/services/exception"
	xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" 
	xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
	xmlns="http://schemas.xmlsoap.org/wsdl/"
	targetNamespace="http://weblab.ow2.org/core/1.2/services/auditable"
	name="Auditable">

	<documentation>
	WEBLAB: Service oriented integration platform for media mining and intelligence applications

	Copyright (C) 2004 - 2017 Airbus Defence and Space

	This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
	Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your
	option) any later version.


	This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
	even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
	the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
</documentation>

	<types>
		<xsd:schema>
			<xsd:import namespace="http://weblab.ow2.org/core/1.2/services/auditable" schemaLocation="AuditableTypes.xsd" />
			<xsd:import namespace="http://weblab.ow2.org/core/1.2/services/exception" schemaLocation="exception.xsd" />
		</xsd:schema>
	</types>

	<import namespace="http://weblab.ow2.org/core/1.2/services/" location="ExceptionMessages.wsdl" />

	<message name="ReportRequest">
		<part element="types:reportArgs" name="args" />
	</message>
	<message name="ReportResponse">
		<part element="types:reportReturn" name="return" />
	</message>

	<portType name="Auditable">
		<documentation>The Auditable interface defines the operation report that allows the service consumer to get a summary information about the service execution. The generated report is represented by
			a PoK within a set of RDF properties.
		</documentation>
		<operation name="report">
			<documentation>Report operation allows the service consumer to get a summary information about the service execution.</documentation>
			<input name="reportInput" message="tns:ReportRequest" />
			<output name="reportOutput" message="tns:ReportResponse" />
			<fault name="invalidParameterException" message="services:invalidParameterException">
				<documentation>To be thrown when the parameter are not valid.</documentation>
			</fault>
			<fault name="insufficientResourcesException" message="services:insufficientResourcesException">
				<documentation>To be thrown when an error occurs due to the lack of resources.</documentation>
			</fault>
			<fault name="contentNotAvailableException" message="services:contentNotAvailableException">
				<documentation>To be thrown when the content of a resource is needed and not available.</documentation>
			</fault>
			<fault name="accessDeniedException" message="services:accessDeniedException">
				<documentation>To be thrown when an error due to some security restriction.</documentation>
			</fault>
			<fault name="unsupportedRequestException" message="services:unsupportedRequestException">
				<documentation>To be thrown when the parameter are good but some specific process are not supported (for instance, due to a language not handled or a given video format...).</documentation>
			</fault>
			<fault name="serviceNotConfiguredException" message="services:serviceNotConfiguredException">
				<documentation>To be thrown when the service needs to be configured and either your usageContext is null or has not been configured.</documentation>
			</fault>
			<fault name="unexpectedException" message="services:unexpectedException">
				<documentation>To be thrown when an error occurs in the method.</documentation>
			</fault>
		</operation>
	</portType>

	<binding name="AuditableSOAPBinding" type="tns:Auditable">
		<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http" />
		<operation name="report">
			<soap:operation soapAction="report" />
			<input name="reportInput">
				<soap:body use="literal" />
			</input>
			<output name="reportOutput">
				<soap:body use="literal" />
			</output>
			<fault name="invalidParameterException">
				<soap:fault name="invalidParameterException" use="literal" />
			</fault>
			<fault name="insufficientResourcesException">
				<soap:fault name="insufficientResourcesException" use="literal" />
			</fault>
			<fault name="contentNotAvailableException">
				<soap:fault name="contentNotAvailableException" use="literal" />
			</fault>
			<fault name="accessDeniedException">
				<soap:fault name="accessDeniedException" use="literal" />
			</fault>
			<fault name="unsupportedRequestException">
				<soap:fault name="unsupportedRequestException" use="literal" />
			</fault>
			<fault name="serviceNotConfiguredException">
				<soap:fault name="serviceNotConfiguredException" use="literal" />
			</fault>
			<fault name="unexpectedException">
				<soap:fault name="unexpectedException" use="literal" />
			</fault>
		</operation>
	</binding>

</definitions>
