<?xml version="1.0"?>

<!DOCTYPE rdf:RDF [
    <!ENTITY owl "http://www.w3.org/2002/07/owl#" >
    <!ENTITY xsd "http://www.w3.org/2001/XMLSchema#" >
    <!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#" >
    <!ENTITY rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#" >
    <!ENTITY model "http://weblab.ow2.org/core/1.2/ontology/model#" >
]>

<rdf:RDF xmlns="http://weblab.ow2.org/core/1.2/ontology/retrieval#" xml:base="http://weblab.ow2.org/core/1.2/ontology/retrieval" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:model="http://weblab.ow2.org/core/1.2/ontology/model#"
	xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
	<owl:Ontology rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval">
		<rdfs:label xml:lang="en">WebLab Exchange model for Resource Retrieval v0.8</rdfs:label>
		<owl:versionInfo rdf:datatype="&xsd;string">0.8</owl:versionInfo>
		<rdfs:comment xml:lang="en">WebLab Exchange model used for resources retrieval v0.8

			This version includes relationships used for WebLab Resources retrieval.

			WEBLAB: Service oriented
			integration platform for media mining and intelligence applications

			Copyright (C) 2004 - 2017 Airbus Defence and Space
		</rdfs:comment>
		<owl:imports rdf:resource="http://weblab.ow2.org/core/1.2/ontology/model" />
	</owl:Ontology>



	<!-- Object properties -->


	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasHit -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasHit">
		<rdfs:label xml:lang="en">has hit</rdfs:label>
		<rdfs:comment xml:lang="en">Gives a hit for ResultSet.</rdfs:comment>
		<rdfs:domain rdf:resource="&model;ResultSet" />
		<rdfs:range rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#Hit" />
	</owl:ObjectProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasFacetSuggestion -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasFacetSuggestion">
		<rdfs:label xml:lang="en">has facet suggestion</rdfs:label>
		<rdfs:comment xml:lang="en">Links a query to a facet suggestion.</rdfs:comment>
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#Query" />
		<rdfs:range rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#FacetSuggestion" />
	</owl:ObjectProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasOrderedQuery -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasOrderedQuery">
		<rdfs:label xml:lang="en">has ordered query</rdfs:label>
		<rdfs:comment xml:lang="en">Gives a link to the requested query for this order.</rdfs:comment>
		<rdfs:range rdf:resource="&model;Query" />
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#SearchOrder" />
	</owl:ObjectProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasRelevantMediaUnit -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasRelevantMediaUnit">
		<rdfs:label xml:lang="en">has relevant MediaUnit</rdfs:label>
		<rdfs:comment xml:lang="en">Can be used to point out a specific MediaUnit inside the Resource that is linked to a Hit. This is an optional property of a Hit.</rdfs:comment>
		<rdfs:range rdf:resource="&model;MediaUnit" />
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#Hit" />
	</owl:ObjectProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasRelevantSegment -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasRelevantSegment">
		<rdfs:label xml:lang="en">has relevant Segment</rdfs:label>
		<rdfs:comment xml:lang="en">Can be used to point out a specific Segment inside the Resource that is linked to a Hit. This is an optional property of a Hit. But if present it MUST be combined
			with #hasRelevantMediaUnit (to locate the MediaUnit that contains the Segment).
		</rdfs:comment>
		<rdfs:range rdf:resource="&model;Segment" />
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#Hit" />
	</owl:ObjectProperty>


	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasScope -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasScope">
		<rdfs:label xml:lang="en">has scope</rdfs:label>
		<rdfs:comment xml:lang="en">Defines on which property values the query applies. For instance, having dc:title has value, means that the query aims at searching a Resource whose title matches
			the query.</rdfs:comment>
		<rdfs:domain rdf:resource="&model;Query" />
		<rdfs:range rdf:resource="&rdf;Property" />
	</owl:ObjectProperty>


	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#inResultSetHit -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#inResultSetHit">
		<rdfs:label xml:lang="en">in ResultSet hit</rdfs:label>
		<rdfs:comment xml:lang="en">Links a Hit to its ResultSet.</rdfs:comment>
		<rdfs:range rdf:resource="&model;ResultSet" />
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#Hit" />
		<owl:inverseOf rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasHit" />
	</owl:ObjectProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#isLinkedTo -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#isLinkedTo">
		<rdfs:label xml:lang="en">is linked to</rdfs:label>
		<rdfs:comment xml:lang="en">Gives a link to resource for hit.</rdfs:comment>
		<rdfs:range rdf:resource="&model;Resource" />
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#Hit" />
	</owl:ObjectProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#isRankedBy -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#isRankedBy">
		<rdfs:label xml:lang="en">is ranked by</rdfs:label>
		<rdfs:comment xml:lang="en">Defines what property is used to rank the results when the traditional relevance is not the default ranking to use.</rdfs:comment>
		<rdfs:domain rdf:resource="&model;ResultSet" />
		<rdfs:range rdf:resource="&rdf;Property" />
	</owl:ObjectProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#isResultOf -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#isResultOf">
		<rdfs:label xml:lang="en">is result of</rdfs:label>
		<rdfs:comment xml:lang="en">Links a ResultSet to the Query that has been used to retrieve the set of results.</rdfs:comment>
		<rdfs:range rdf:resource="&model;Query" />
		<rdfs:domain rdf:resource="&model;ResultSet" />
	</owl:ObjectProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#toBeRankedBy -->

	<owl:ObjectProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#toBeRankedBy">
		<rdfs:label xml:lang="en">to be ranked by</rdfs:label>
		<rdfs:comment xml:lang="en">Define what is the expected property to be used to rank the results of a request when the traditional relevance is not the default ranking to use.</rdfs:comment>
		<rdfs:domain rdf:resource="&model;Query" />
		<rdfs:range rdf:resource="&rdf;Property" />
	</owl:ObjectProperty>



	<!-- Data properties -->



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasDescription -->

	<owl:DatatypeProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasDescription">
		<rdfs:label xml:lang="en">has description</rdfs:label>
		<rdfs:comment xml:lang="en">Gives a description for a hit.</rdfs:comment>
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#Hit" />
		<rdfs:range rdf:resource="&xsd;string" />
	</owl:DatatypeProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasExpectedLimit -->

	<owl:DatatypeProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasExpectedLimit">
		<rdfs:label xml:lang="en">has expected limit</rdfs:label>
		<rdfs:comment xml:lang="en">Gives the limit for the requested query for this order.</rdfs:comment>
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#SearchOrder" />
		<rdfs:range rdf:resource="&xsd;int" />
	</owl:DatatypeProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasExpectedOffset -->

	<owl:DatatypeProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasExpectedOffset">
		<rdfs:label xml:lang="en">has expected offset</rdfs:label>
		<rdfs:comment xml:lang="en">Gives the expected offset for the requested query for this order.</rdfs:comment>
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#SearchOrder" />
		<rdfs:range rdf:resource="&xsd;int" />
	</owl:DatatypeProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasNumberOfResults -->

	<owl:DatatypeProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasNumberOfResults">
		<rdfs:label xml:lang="en">has number of results</rdfs:label>
		<rdfs:comment xml:lang="en">Gives the total number of results for this result set.</rdfs:comment>
		<rdfs:domain rdf:resource="&model;ResultSet" />
		<rdfs:range rdf:resource="&xsd;int" />
	</owl:DatatypeProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasQueryOffset -->

	<owl:DatatypeProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasQueryOffset">
		<rdfs:label xml:lang="en">has query offset</rdfs:label>
		<rdfs:comment xml:lang="en">Provides the offset about the query in the case of a result set returns by a search service.</rdfs:comment>
		<rdfs:domain rdf:resource="&model;ResultSet" />
		<rdfs:range rdf:resource="&xsd;int" />
	</owl:DatatypeProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasRank -->

	<owl:DatatypeProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasRank">
		<rdfs:label xml:lang="en">has rank</rdfs:label>
		<rdfs:comment xml:lang="en">Gives a rank for hit.</rdfs:comment>
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#Hit" />
		<rdfs:range rdf:resource="&xsd;int" />
	</owl:DatatypeProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#hasScore -->

	<owl:DatatypeProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#hasScore">
		<rdfs:label xml:lang="en">has score</rdfs:label>
		<rdfs:comment xml:lang="en">Gives a score for hit.</rdfs:comment>
		<rdfs:domain rdf:resource="http://weblab.ow2.org/core/1.2/ontology/retrieval#Hit" />
		<rdfs:range rdf:resource="&xsd;double" />
	</owl:DatatypeProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#isExpressedWith -->

	<owl:DatatypeProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#isExpressedWith">
		<rdfs:label xml:lang="en">is expressed with</rdfs:label>
		<rdfs:comment xml:lang="en">Gives the language of the query .</rdfs:comment>
		<rdfs:domain rdf:resource="&model;Query" />
		<rdfs:range rdf:resource="&xsd;string" />
	</owl:DatatypeProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#isRankedAscending -->

	<owl:DatatypeProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#isRankedAscending">
		<rdfs:label xml:lang="en">is ranked ascending</rdfs:label>
		<rdfs:comment xml:lang="en">Default ranking mode is descending, this property allow to state that the result set has been ranked ascendingly.</rdfs:comment>
		<rdfs:domain rdf:resource="&model;ResultSet" />
		<rdfs:range rdf:resource="&xsd;boolean" />
	</owl:DatatypeProperty>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#toBeRankedAscending -->

	<owl:DatatypeProperty rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#toBeRankedAscending">
		<rdfs:label xml:lang="en">to be ranked ascending</rdfs:label>
		<rdfs:comment xml:lang="en">Default ranking mode is descendant, this property allow to ask for results ranked ascendingly.</rdfs:comment>
		<rdfs:domain rdf:resource="&model;Query" />
		<rdfs:range rdf:resource="&xsd;boolean" />
	</owl:DatatypeProperty>



	<!-- Classes -->


	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#FacetSuggestion -->

	<owl:Class rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#FacetSuggestion">
		<rdfs:label xml:lang="en">Facet Suggestion</rdfs:label>
		<rdfs:comment xml:lang="en">A Facet Suggestion is an holder for various queries proposed has facet of another query.</rdfs:comment>
	</owl:Class>


	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#Hit -->

	<owl:Class rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#Hit">
		<rdfs:label xml:lang="en">Hit</rdfs:label>
		<rdfs:comment xml:lang="en">A hit describes media document properties corresponding to a request.</rdfs:comment>
	</owl:Class>



	<!-- http://weblab.ow2.org/core/1.2/ontology/retrieval#SearchOrder -->

	<owl:Class rdf:about="http://weblab.ow2.org/core/1.2/ontology/retrieval#SearchOrder">
		<rdfs:label xml:lang="en">Search order</rdfs:label>
		<rdfs:comment xml:lang="en">A SearchOrder describe a search order requested by a user or a service. It is typically used in inter portlet communication to ask for more results on a same
			query.
		</rdfs:comment>
	</owl:Class>


</rdf:RDF>



<!-- Generated by the OWL API (version 3.2.3.1824) http://owlapi.sourceforge.net -->

