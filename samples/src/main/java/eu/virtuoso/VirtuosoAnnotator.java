/**
* WEBLAB: Service oriented integration platform for media mining and intelligence applications
*
* Copyright (C) 2004 - 2017 Airbus Defence and Space
*
* This library is free software; you can redistribute it and/or modify it under the terms of
* the GNU Lesser General Public License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License along with this
* library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
* Floor, Boston, MA 02110-1301 USA
*/
package eu.virtuoso;


import java.net.URI;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * Simplified Virtuoso Ontology
 *
 * This version is provided as an example to generate Virtuoso annotator
 */
public class VirtuosoAnnotator extends BaseAnnotator {


	protected static final String PREFIX = "virtuoso";


	protected static final URI VIRTUOSO_URI = URI.create("http://www.virtuoso.eu/ontology#");


	protected static final String EXTRACTEDFROM = "isExtractedFrom";


	protected static final String TRANSCRIPTIONOF = "isTranscriptionOf";


	protected static final String RECOGNISEDFROM = "isRecognisedFrom";


	public VirtuosoAnnotator(URI subject, PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	public VirtuosoAnnotator(Resource resource) {
		super(resource);
	}


	/**
	 * Read the resource extracted from a resource.
	 *
	 * @return a URI
	 */
	public Value<URI> readExtractedFrom() {
		return applyOperator(Operator.READ, null, VIRTUOSO_URI, EXTRACTEDFROM, URI.class, null);
	}


	/**
	 * Write the resource extracted from a resource.
	 * 
	 * @param value
	 *            a URI
	 */
	public void writeExtractedFrom(URI value) {
		applyOperator(Operator.WRITE, PREFIX, VIRTUOSO_URI, EXTRACTEDFROM, URI.class, value);
	}


	/**
	 * Read the transcription of a resource.
	 *
	 * @return an URI
	 */
	public Value<URI> readTranscriptionOf() {
		return applyOperator(Operator.READ, null, VIRTUOSO_URI, TRANSCRIPTIONOF, URI.class, null);
	}


	/**
	 * Write the transcription of a resource.
	 * 
	 * @param value
	 *            an URI
	 */
	public void writeTranscriptionOf(URI value) {
		applyOperator(Operator.WRITE, PREFIX, VIRTUOSO_URI, TRANSCRIPTIONOF, URI.class, value);
	}


	/**
	 * Read the resource recognised from an other resource.
	 *
	 * @return a URI
	 */
	public Value<URI> readRecognisedFrom() {
		return applyOperator(Operator.READ, null, VIRTUOSO_URI, RECOGNISEDFROM, URI.class, null);
	}


	/**
	 * Write the resource recognised from an other resource.
	 * 
	 * @param value
	 *            a URI
	 */
	public void writeRecognisedFrom(URI value) {
		applyOperator(Operator.WRITE, PREFIX, VIRTUOSO_URI, RECOGNISEDFROM, URI.class, value);
	}
}

