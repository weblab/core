/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helpers.samples;

import java.io.File;
import java.net.URI;
import java.util.Calendar;
import java.util.Date;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Coordinate;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.utils.BackEndJenaHelper;
import org.purl.dc.elements.DublinCoreAnnotator;

import eu.virtuoso.VirtuosoAnnotator;

/**
 * Create document in java and serialize them in target folder.
 * It is used as a tutorial for the creation of Documents using "extented", "annotator" and "helper".
 */
public class DocumentCreator {


	/**
	 * Creation of a simple Document that only contains three Text MediaUnit.
	 *
	 * @throws WebLabCheckedException
	 *             If marshaling fails
	 */
	public void createSimpleTextDocument() throws WebLabCheckedException {

		/*
		 * Document creation. Use of ResourceFactory in extended.jar Arguments
		 * are the id the resource creator, id of the resource given the
		 * creator's knowledge of the world, class (extending resource). The
		 * first two parameters are used to generate a unique URI (according to
		 * the creator's knowledge of the world...)
		 */
		final Document doc = WebLabResourceFactory.createResource("test", "d-0", Document.class);

		/*
		 * Creation of a Text MediaUnit inside the Document. With a unique based
		 * on the one of the Document.
		 */
		final Text introduction = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);

		/*
		 * Add the text content inside the mediaUnit.
		 */
		introduction.setContent("This is the introduction");

		/*
		 * Another text
		 */
		final Text section = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		section.setContent("First section\nThis is a text...");

		/*
		 * Another text
		 */
		final Text conclusion = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		conclusion.setContent("Conclusion\nThis is a versy simple example.");

		/*
		 * Use a marshaler to serialise the XML file. (the same method exists
		 * for streams, writers or files).
		 */
		final WebLabMarshaller marshaler = new WebLabMarshaller();
		marshaler.marshalResource(doc, new File("target/doc_simple.xml"));
		// marshaler.marshalResource(doc, System.out);
		// marshaler.marshalResource(doc, new StringWriter());
	}


	/**
	 * Creation of the same document but including annotations. As defined in the model, Annotations have a field name data, containing RDF/XML. When using Java directly this field is a DOM Document.
	 * More over, every resources may contain from 0 to n annotations. WebLab provides various way to set this field without handling this complex object. Each one are presented in the following code.
	 *
	 * @throws WebLabCheckedException
	 *             If marshaling fails
	 */
	public void createDocumentWithSimpleAnnotation() throws WebLabCheckedException {
		final Document doc = DocumentCreator.createDocumentWithAnnotation();

		new WebLabMarshaller().marshalResource(doc, new File("target/doc_annot.xml"));

		/*
		 * You can also unmarshal a resource from an XML file.
		 */
		final Document doc2 = new WebLabMarshaller().unmarshal(new File("target/doc_annot.xml"), Document.class);
		new WebLabMarshaller().marshalResource(doc2, new File("target/doc_annot2.xml"));

	}


	/**
	 * @return The created document
	 */
	public static Document createDocumentWithAnnotation() {
		final Document doc = WebLabResourceFactory.createResource("test", "d-1", Document.class);

		final Text introduction = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		introduction.setContent("This is the introduction");
		final Text section = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		section.setContent("First section\nThis is a text...");
		final Text conclusion = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		conclusion.setContent("Conclusion\nThis is a versy simple example.");

		/*
		 * The simplest one, using the annotator, a library introduced in WebLab 1.2 Annotator are dedicated to a single ontology. By default you have DublinCore, WebLabProcessing, WebLabRetrieval,
		 * WebLabModel and DublinCoreTerms. An annotator take as parameter the Resource, in which it will be added. Every statement has, by default, the resource as subject. For instance,
		 */
		final DublinCoreAnnotator annotator = new DublinCoreAnnotator(doc);
		annotator.writeCreator("WebLab resource sample tests");
		annotator.writeSource("http://weblab-project.org/");

		final Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2010, 5, 27, 11, 48);
		annotator.writeDate(cal.getTime());

		/*
		 * You may also want to use more complex process. In this case, you can use the rdf-helper or any library of your choice.
		 * But prior to do that, you need to create an annotation (using extended).
		 */
		final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(doc);

		/*
		 * You can directly use Jena for instance or rely on a our provided abstraction.
		 */
		final BackEndJenaHelper helper = new BackEndJenaHelper();

		helper.createLitStat(doc.getUri(), DublinCore.TITLE_PROPERTY_NAME, "The title.");
		helper.createResStat(annot.getUri(), WebLabProcessing.IS_PRODUCED_BY,
				"http://websvn.ow2.org/filedetails.php?repname=weblab&path=/trunk/WebLabHelpers/samples/src/test/java/org/ow2/weblab/core/DocumentCreation.java");
		helper.createLitStat(annot.getUri(), WebLabProcessing.IS_EXPOSED_AS, "http://weblab-project.org/index.php?title=WebLab&oldid=237");


		/*
		 * If you use you own libraries to handle RDF/XML, and that you API does not allow the export of a DOM Document you still have, the last method. By directly adding the RDF/XML String.
		 */
		PoKUtil.setPoKData(annot, helper.getRdfXml());

		return doc;
	}



	/**
	 * This is just a simple test using many text units in various languages.
	 *
	 * @throws WebLabCheckedException
	 *             If marshaling fails
	 */
	public void createMultilingualDoc() throws WebLabCheckedException {
		final Document doc = WebLabResourceFactory.createResource("language.detection.test", "simple-doc", Document.class);

		final Text enUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String en = "I want to crash the system by adding a little English.";
		enUnit.setContent(en);

		final Text frUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String fr = "Je voudrais faire planter le système en ajoutant un peu de français.";
		frUnit.setContent(fr);

		final Text deUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String de = "Ich möchte das System zum Absturz, indem Sie ein wenig Deutsch.";
		deUnit.setContent(de);

		final Text ruUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String ru = "Я хочу к краху системы, добавив немного русский.";
		ruUnit.setContent(ru);

		final Text ptUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String pt = "Eu quero derrubar o sistema, adicionando um pouco de Português.";
		ptUnit.setContent(pt);

		final Text roUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String ro = "Vreau să prăbuşirii sistemului prin adăugarea de un român putin.";
		roUnit.setContent(ro);

		final Text esUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String es = "Quiero bloquear el sistema, añadiendo un poco de español.";
		esUnit.setContent(es);

		final Text bgUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String bg = "Искам да катастрофа на системата чрез добавяне на малко български.";
		bgUnit.setContent(bg);

		final Text daUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String da = "Jeg ønsker at gå ned i systemet ved at tilføje en smule dansk.";
		daUnit.setContent(da);

		final Text elUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String el = "Θα συντριβή του συστήματος, προσθέτοντας λίγο ελληνικά.";
		elUnit.setContent(el);

		final Text isUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		final String is = "Ég vil að hrun í kerfinu með því að bæta smá íslensku.";
		isUnit.setContent(is);

		new WebLabMarshaller().marshalResource(doc, new File("target/doc_multi_lingual.xml"));
	}


	/**
	 * This is the code used to generate the complex in WebLab presentations.
	 *
	 * @throws WebLabCheckedException
	 *             If marshaling fails
	 */
	public void createComplexMultiDoc() throws WebLabCheckedException {

		/*
		 * Document creation.
		 */
		final Document document = WebLabResourceFactory.createResource("video", "res_584697", Document.class);

		/*
		 * Create an annotator dedicated to the annotation of document using
		 * properties defined by dublin core ontology.
		 */
		final DublinCoreAnnotator dcAnnot = new DublinCoreAnnotator(document);

		/*
		 * Add the source of the video
		 */
		dcAnnot.writeSource("http://aljazeera.net/portal");

		/*
		 * Add the mime type of the video file
		 */
		dcAnnot.writeFormat("video/mp4");

		/*
		 * Create another annotator. This one dedicated to the annotation of
		 * document using properties defined by WebLab Processing ontology.
		 */
		WProcessingAnnotator wpAnnot = new WProcessingAnnotator(document);

		/*
		 * Create a Java Date
		 */
		final Calendar cal = Calendar.getInstance();
		cal.set(2007, 3, 9, 17, 37, 53);
		final Date gatheringDate = cal.getTime();

		/*
		 * Add the gathering date (yes it was a long time ago)
		 */
		wpAnnot.writeGatheringDate(gatheringDate);

		wpAnnot.writeExposedAs("ftp://ftp.myHost/video/res_584697.mp4");

		/*
		 * Create a marshaler, that eases the use of JAXB marshalling process.
		 */
		final WebLabMarshaller wlm = new WebLabMarshaller();

		wlm.marshalResource(document, new File("target/res_584697-v1.xml"));

		/*
		 * Create a video unit inside the document
		 */
		final Video video = WebLabResourceFactory.createAndLinkMediaUnit(document, Video.class);

		wpAnnot = new WProcessingAnnotator(video);
		wpAnnot.writeNormalisedContent(URI.create("rtp://servX1/" + video.getUri().hashCode()));

		wlm.marshalResource(document, new File("target/res_584697-v2.xml"));

		/*
		 * Create 4 images inside the document. They are extracted from the
		 * video and have a normalised content URI
		 */
		final Image image1 = WebLabResourceFactory.createAndLinkMediaUnit(document, Image.class);
		wpAnnot = new WProcessingAnnotator(image1);
		VirtuosoAnnotator vAnnot = new VirtuosoAnnotator(image1);
		vAnnot.writeExtractedFrom(URI.create(video.getUri()));
		wpAnnot.writeNormalisedContent(URI.create("http://10.134.0.1/" + image1.getUri().hashCode()));

		final Image image2 = WebLabResourceFactory.createAndLinkMediaUnit(document, Image.class);
		wpAnnot = new WProcessingAnnotator(image2);
		vAnnot = new VirtuosoAnnotator(image2);
		vAnnot.writeExtractedFrom(URI.create(video.getUri()));
		wpAnnot.writeNormalisedContent(URI.create("http://10.134.0.1/" + image2.getUri().hashCode()));

		final Image image3 = WebLabResourceFactory.createAndLinkMediaUnit(document, Image.class);
		wpAnnot = new WProcessingAnnotator(image3);
		vAnnot = new VirtuosoAnnotator(image3);
		vAnnot.writeExtractedFrom(URI.create(video.getUri()));
		wpAnnot.writeNormalisedContent(URI.create("http://10.134.0.1/" + image3.getUri().hashCode()));

		final Image image4 = WebLabResourceFactory.createAndLinkMediaUnit(document, Image.class);
		wpAnnot = new WProcessingAnnotator(image4);
		vAnnot = new VirtuosoAnnotator(image4);
		vAnnot.writeExtractedFrom(URI.create(video.getUri()));
		wpAnnot.writeNormalisedContent(URI.create("http://10.134.0.1/" + image4.getUri().hashCode()));

		wlm.marshalResource(document, new File("target/res_584697-v3.xml"));

		/*
		 * Create an audio unit. It is extracted from the video and has a
		 * normalised content URI
		 */
		final Audio audio = WebLabResourceFactory.createAndLinkMediaUnit(document, Audio.class);
		wpAnnot = new WProcessingAnnotator(audio);
		vAnnot = new VirtuosoAnnotator(audio);
		vAnnot.writeExtractedFrom(URI.create(video.getUri()));
		wpAnnot.writeNormalisedContent(URI.create("rtsp://servideo/" + audio.getUri().hashCode()));

		wlm.marshalResource(document, new File("target/res_584697-v4.xml"));

		/*
		 * Create the text content of the audio transcription (speech-to-text).
		 */
		final Text transcription = WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class);
		wpAnnot = new WProcessingAnnotator(transcription);
		vAnnot = new VirtuosoAnnotator(transcription);
		vAnnot.writeTranscriptionOf(URI.create(audio.getUri()));

		transcription.setContent(
				"قتل 25 شخصا وأصيب 35 آخرون في انفجار قوي في شمال غربي باكستان، كما قتل أربعة أشخاص وأصيب اثنان آخران في قصف صاروخي استهدف سيارة في إقليم شمال وزيرستان، مما دفع العشرات من المتضررين من أمثال هذا القصف إلى التظاهر أمام مقر البرلمان الباكستاني.");

		wlm.marshalResource(document, new File("target/res_584697-v5.xml"));

		/*
		 * Create a text section extracted from a segment in an image.
		 */
		final Text recognisedText = WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class);
		recognisedText.setContent("A Message from Shaykh Usama bin Ladin to the America People");
		wpAnnot = new WProcessingAnnotator(recognisedText);
		vAnnot = new VirtuosoAnnotator(recognisedText);

		/*
		 * Create the spatial segment in image 2 and set its position
		 */
		final SpatialSegment segment = SegmentFactory.createAndLinkSegment(image2, SpatialSegment.class);
		final Coordinate c1 = new Coordinate();
		c1.setX(85);
		c1.setY(282);
		segment.getCoordinate().add(c1);
		final Coordinate c2 = new Coordinate();
		c2.setX(453);
		c2.setY(282);
		segment.getCoordinate().add(c2);
		final Coordinate c3 = new Coordinate();
		c3.setX(453);
		c3.setY(311);
		segment.getCoordinate().add(c3);
		final Coordinate c4 = new Coordinate();
		c4.setX(85);
		c4.setY(311);
		segment.getCoordinate().add(c4);

		/*
		 * Link the recognised text with the spatial segment
		 */
		vAnnot.writeRecognisedFrom(URI.create(image2.getUri()));

		wlm.marshalResource(document, new File("target/res_584697-v6.xml"));

		/*
		 * Extract named entities and create linearsegments in the english text
		 */
		final LinearSegment benLadenSegment = SegmentFactory.createAndLinkLinearSegment(recognisedText, 15, 37);
		wpAnnot.startInnerAnnotatorOn(URI.create(benLadenSegment.getUri()));
		wpAnnot.writeRefersTo(URI.create("http://knowledgeBase/Person/BenLaden#0"));
		wpAnnot.endInnerAnnotator();

		final LinearSegment americaSegment = SegmentFactory.createAndLinkLinearSegment(recognisedText, 45, 52);
		wpAnnot.startInnerAnnotatorOn(URI.create(americaSegment.getUri()));
		wpAnnot.writeRefersTo(URI.create("http://knowledgeBase/Location/America#5"));
		wpAnnot.endInnerAnnotator();

		wlm.marshalResource(document, new File("target/res_584697-v7.xml"));

		/*
		 * Going further...
		 *
		 * We provide Javadocs and sources for every libraries. When using maven
		 * add -DdownloadJavadocs and -DdownloadSources or have a look at the
		 * Maven OW2 repository
		 * (http://maven.ow2.org/maven2/org/ow2/weblab/core/)
		 *
		 * Check out the sources of WebLab services or portlets similar to your
		 * needs (svn://svn.forge.objectweb.org/svnroot/weblab/WebLabServices).
		 * - Information Extraction in text --> gate-extraction and
		 * simple-gazetteer - Index/Search in text --> solr-engine - Document
		 * storage --> file-repository or simple-file-repository - ... and many
		 * others Have a look at the Wiki:
		 * http://weblab-project.org/index.php?title=WebLab And we encourage you
		 * to use the forum in case of any question regarding the model,
		 * extended, annotator, helper... or whatever is related to the WebLab
		 * platform.
		 */
	}


	/**
	 * @throws WebLabCheckedException
	 *             If marshaling fails
	 */
	public void createCrawledDocument() throws WebLabCheckedException {
		final String[] docNames = { "10.1.1.133.4884.pdf", "10.1.1.167.3624.pdf", "Homo_sapiens-fr.html", "Human-en.html", "webcontent-www2009-stamped.pdf", "Άνθρωπος-el.html" };
		final String[] docSources = { "http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.133.4884&rep=rep1&type=pdf",
				"http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.167.3624&rep=rep1&type=pdf", "http://fr.wikipedia.org/wiki/Homo_sapiens", "http://en.wikipedia.org/wiki/Human",
				"http://forge.ow2.org/docman/view.php/357/207/webcontent-www2009-stamped.pdf", "http://el.wikipedia.org/wiki/%CE%86%CE%BD%CE%B8%CF%81%CF%89%CF%80%CE%BF%CF%82" };

		final String xmlFolderToSaveResults = "./target/xmls";
		final File output = new File(xmlFolderToSaveResults);
		if (!output.exists()) {
			output.mkdirs();
		}
		final WebLabMarshaller marshmallower = new WebLabMarshaller();

		for (int i = 0; i < docNames.length; i++) {
			final File contentDoc = new File("src/test/resources/docs/" + docNames[i]);
			final Document weblabDoc = WebLabResourceFactory.createResource("testForASystem", "doc" + i, Document.class);
			final DublinCoreAnnotator dca = new DublinCoreAnnotator(weblabDoc);
			dca.writeSource(docSources[i]);

			final WProcessingAnnotator wpa = new WProcessingAnnotator(weblabDoc);
			wpa.writeGatheringDate(new Date());
			wpa.writeNativeContent(contentDoc.toURI());

			marshmallower.marshalResource(weblabDoc, new File(output.getAbsoluteFile() + "/doc" + i + ".xml"));
		}
	}


	/**
	 * @return The created document
	 */
	public static Document createSimpleDocumentWithSmallEnTexts() {
		/*
		 * Document creation. Use of ResourceFactory in extended.jar Arguments
		 * are the id the resource creator, id of the resource given the
		 * creator's knowledge of the world, class (extending resource). The
		 * first two parameters are used to generate a unique URI (according to
		 * the creator's knowledge of the world...)
		 */
		final Document doc = WebLabResourceFactory.createResource("test", "d-0", Document.class);

		/*
		 * Creation of a Text MediaUnit inside the Document. With a unique based
		 * on the one of the Document.
		 */
		final Text introduction = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);

		// Add the text content inside the mediaUnit.
		introduction.setContent("This is the introduction");

		// Adding more texts
		final Text section = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		section.setContent("First section\nThis is a text...");
		final Text conclusion = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		conclusion.setContent("Conclusion\nThis is a versy simple example.");

		return doc;
	}


	/**
	 * @return The created document
	 */
	public static Document createSimpleDocumentWithBasicMeta() {
		final Document doc = WebLabResourceFactory.createResource("test", "d" + System.currentTimeMillis(), Document.class);

		// Adding basic metadata on the document
		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		wpa.writeGatheringDate(new Date(System.currentTimeMillis() - (1000 * 3600))); // fake
		wpa.writeProducedBy(URI.create("weblab://helpers/samples/DocumentCreator"));
		return doc;
	}


	/**
	 * @param text
	 *            The created document
	 * @param twoLetterLangCode
	 *            The language code to be written
	 * @return The created document
	 */
	public static Document createDocumentWithText(final String text, final String twoLetterLangCode) {
		final Document doc = DocumentCreator.createSimpleDocumentWithBasicMeta();

		final Text t = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		t.setContent(text);

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(t);
		dca.writeLanguage(twoLetterLangCode);

		return doc;
	}


	/**
	 * @return The created document
	 */
	public static Document createDocWithDeutsch() {
		return DocumentCreator.createDocumentWithText("es gibt' \ndas ist'ein klein haus", "de");
	}


	/**
	 * @return The created document
	 */
	public static Document createDocWithSpanish() {
		final Document doc = DocumentCreator.createDocumentWithText(
				"Un dirigible es un aerostato autopropulsado y con capacidad de maniobra para ser manejado como una aeronave. La sustentación aerostática se logra mediante depósitos llenos de un gas de menor densidad a la atmósfera circundante. Difiere de la sustentación aerodinámica, obtenida mediante el movimiento rápido de un perfil alar, como en el ala de un aeroplano o las aspas de un helicóptero.\n"
						+ "Fue el primer artefacto volador capaz de ser controlado en un vuelo largo. Su uso principal ocurrió aproximadamente entre 1900 y la década de 1930: para disminuir paulatinamente cuando sus capacidades fueron superadas por la de los aeroplanos, y además, después de sufrir varios accidentes de relevancia, el más notable de los cuales fue sin duda el incendio del Hindenburg. Actualmente se los utiliza en una serie de aplicaciones secundarias, especialmente publicidad.",
				"es");
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeSource("http://es.wikipedia.org/wiki/Dirigible");
		dca.writeTitle("Dirigible");
		return doc;
	}


	/**
	 * @return The created document
	 */
	public static Document createDocWithGreek() {
		final Document doc = DocumentCreator.createDocumentWithText(
				"Η λέξη κρυπτογραφία προέρχεται από τα συνθετικά \"κρυπτός\" + \"γράφω\" και είναι ένας επιστημονικός κλάδος που ασχολείται με την μελέτη, την ανάπτυξη και την χρήση τεχνικών κρυπτογράφησης και αποκρυπτογράφησης με σκοπό την απόκρυψη του περιεχομένου των μηνυμάτων.\n\n "
						+ "Η κρυπτογραφία είναι ένας κλάδος της επιστήμης της κρυπτολογίας, η οποία ασχολείται με την μελέτη της ασφαλούς επικοινωνίας. Ο κύριος στόχος της είναι να παρέχει μηχανισμούς για 2 ή περισσότερα μέλη να επικοινωνήσουν χωρίς κάποιος άλλος να είναι ικανός να διαβάζει την πληροφορία εκτός από τα μέλη. Η λέξη κρυπτολογία αποτελείται από την ελληνική λέξη \"κρυπτός\" και την λέξη \"λόγος\" και χωρίζεται σε δύο κλάδους: Την Κρυπτογραφία και την Κρυπτανάλυση με παρεμφερή κλάδο την Στεγανογραφία και αντίστοιχα την Στεγανοανάλυση.",
				"gr");
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeSource("http://el.wikipedia.org/wiki/%CE%9A%CF%81%CF%85%CF%80%CF%84%CE%BF%CE%B3%CF%81%CE%B1%CF%86%CE%AF%CE%B1");
		dca.writeTitle("Κρυπτογραφία");
		return doc;
	}


	/**
	 * @return The created document
	 */
	public static Document createDocumentFromGuttenbergEbooks() {
		final Document document = DocumentCreator.createSimpleDocumentWithSmallEnTexts();
		final WProcessingAnnotator wpa = new WProcessingAnnotator(document);
		wpa.writeGatheringDate(new Date(System.currentTimeMillis()));

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(document);
		final int random = (int) (Math.random() * 30000);
		dca.writeSource("http://www.gutenberg.org/files/" + random + "/" + random + "-0.txt");
		return document;
	}

}
