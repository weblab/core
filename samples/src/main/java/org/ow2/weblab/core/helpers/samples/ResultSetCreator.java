/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.helpers.samples;

import java.io.File;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.searcher.SearchReturn;

public class ResultSetCreator {


	/**
	 * This method create a ResultSet with mandatory properties and no result
	 * 
	 * @return a ResultSet with no result
	 */
	public static ResultSet createEmptyResultSet() {
		ResultSet rset = WebLabResourceFactory.createResource("sample", "minimalRSet", ResultSet.class);
		WebLabResourceFactory.createResource("sample", "minimalRSet", ComposedResource.class);

		final StringQuery q = WebLabResourceFactory.createResource("org.ow2.weblab", "StringQuery1", StringQuery.class);
		q.setRequest("just some keywords in your syntax");

		// adding the query that generated the result set in the result set
		// resources list
		rset.getResource().add(q);

		WRetrievalAnnotator wra = new WRetrievalAnnotator(rset);
		// add annotation to link result set and query
		wra.writeResultOf(URI.create(q.getUri()));
		// add offset/limit expected
		wra.writeExpectedOffset(Integer.valueOf(0));
		wra.writeExpectedLimit(Integer.valueOf(10));
		// add number of results
		wra.writeNumberOfResults(Integer.valueOf(0));

		return rset;
	}


	/**
	 * This method create a ResultSet with mandatory properties and 10 Hits
	 * 
	 * @return a ResultSet with mandatory properties and 10 Hits
	 */
	public static ResultSet createResultSet() {
		ResultSet rset = WebLabResourceFactory.createResource("sample", "minimalRSet", ResultSet.class);
		WebLabResourceFactory.createResource("sample", "minimalRSet", ComposedResource.class);

		final StringQuery q = WebLabResourceFactory.createResource("org.ow2.weblab", "StringQuery2", StringQuery.class);
		q.setRequest("just some keywords in your syntax");

		// adding the query that generated the result set in the result set
		// resources list
		rset.getResource().add(q);

		WRetrievalAnnotator wra = new WRetrievalAnnotator(rset);
		// add annotation to link result set and query
		wra.writeResultOf(URI.create(q.getUri()));
		// add offset/limit expected
		wra.writeExpectedOffset(Integer.valueOf(0));
		wra.writeExpectedLimit(Integer.valueOf(10));
		// add number of results
		wra.writeNumberOfResults(Integer.valueOf(10));

		for (int i = 0; i < 10; i++) {
			URI hitURI = URI.create("weblab://sample/hit" + i);
			wra.writeHit(hitURI);

			wra.startInnerAnnotatorOn(hitURI);
			wra.writeRank(Integer.valueOf(i + 1));
			wra.writeDescription("ablabalbalbla l balb al lbalb labl balb alb lab lbalba lb laal balb lba lba ... qskldqlksdl");
			wra.endInnerAnnotator();
		}

		return rset;
	}


	/**
	 * This method present a "standard" result set for a simple search result.
	 * It contains the minimum to be returned by a Searcher
	 *
	 * @throws WebLabCheckedException
	 *             If marshaling fails
	 */
	public void createResultSetForSearch() throws WebLabCheckedException {
		final WebLabMarshaller m = new WebLabMarshaller();

		// this an example of a simple StringQuery (we don't really need it here
		// but this the object as it should be received in the search arguments)
		final StringQuery q = WebLabResourceFactory.createResource("sample.virtuoso.eu", "StringQuery" + System.currentTimeMillis(), StringQuery.class);
		q.setRequest("just some keywords in your syntax");

		// here we create the actual ResultSet
		final ResultSet results = WebLabResourceFactory.createResource("sample.virtuoso.eu", "ResultSet" + System.currentTimeMillis(), ResultSet.class);
		// create the PoK inside that will describe the results in RDF/XML
		final PieceOfKnowledge pok = WebLabResourceFactory.createResource("sample.virtuoso.eu", "PieceOfKnowledge" + System.currentTimeMillis(), PieceOfKnowledge.class);
		results.setPok(pok);

		// now we suppose that you have the results in a form of a list of URI
		// and a list of scores
		final List<String> resURIs = ResultSetCreator.getResURI(10);
		final List<Double> resScores = ResultSetCreator.getResScore(10);
		//
		// // now for each result we create the minimum statements to describe it
		final long time = System.currentTimeMillis();


		// create the annotator that will ease to write the RDF
		final WRetrievalAnnotator wmAnnotator = new WRetrievalAnnotator(pok);
		// create the first RDF statement to link the ResultSet to the Query
		wmAnnotator.writeResultOf(URI.create(q.getUri()));
		// now for each result we create the minimum statements to describe it

		for (int k = 0; k < resURIs.size(); k++) {
			final String resUri = resURIs.get(k);
			final double resScore = resScores.get(k).doubleValue();
			// create the Hit itself
			wmAnnotator.startInnerAnnotatorOn(URI.create("weblab://sample.virtuoso.eu/" + time + "/hit-" + k));
			wmAnnotator.writeType(URI.create(WebLabRetrieval.HIT));
			// adding hasRank annotation
			wmAnnotator.writeRank(Integer.valueOf(k));
			// adding score annotation
			wmAnnotator.writeScore(Double.valueOf(resScore));
			// link the Hit to a real Document URI
			wmAnnotator.writeLinkedTo(URI.create(resUri));
			// link the Hit to the ResultSet for traceability
			wmAnnotator.writeInResultSetHit(URI.create(resUri));
			wmAnnotator.endInnerAnnotator();
		}

		// prepare response
		final SearchReturn returnedResults = new SearchReturn();
		returnedResults.setResultSet(results);

		// serialize to see the XML result
		m.marshalResource(results, new File("target/resultSet.simple.xml"));

	}


	private static List<Double> getResScore(final int i) {
		final List<Double> scores = new LinkedList<>();
		final Random r = new Random();
		double last = 1.0;
		double current = 0.0;
		for (int k = 0; k < i; k++) {
			current = r.nextDouble();
			scores.add(Double.valueOf(last * current));
			last = current;
		}
		return scores;
	}


	private static List<String> getResURI(final int i) {
		final List<String> uris = new LinkedList<>();
		for (int k = 0; k < i; k++) {
			uris.add("weblab://sample.virtuoso.eu/resource" + System.currentTimeMillis());
		}
		return uris;
	}

}
