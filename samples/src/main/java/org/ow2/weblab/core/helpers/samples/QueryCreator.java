/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helpers.samples;

import java.io.File;
import java.net.URI;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;


public class QueryCreator {


	/**
	 * This code enable the creation of a simple StringQuery
	 *
	 * @throws WebLabCheckedException
	 *             If marshaling fails
	 */
	public void createSimpleStringQuery() throws WebLabCheckedException {
		final StringQuery q = WebLabResourceFactory.createResource("sample", "q-test", StringQuery.class);
		q.setRequest("Virtuoso");
		new WebLabMarshaller().marshalResource(q, new File("target/stringQuery_simple.xml"));
	}


	/**
	 * This code creates complex composed queries
	 *
	 * @throws WebLabCheckedException
	 *             If marshaling fails
	 */
	public void createComposedQuery() throws WebLabCheckedException {
		final ComposedQuery q0 = WebLabResourceFactory.createResource("sample", "q-test0", ComposedQuery.class);

		final StringQuery q1 = WebLabResourceFactory.createResource("sample", "q-test1", StringQuery.class);
		q1.setRequest("Virtuoso");

		final StringQuery q2 = WebLabResourceFactory.createResource("sample", "q-test2", StringQuery.class);
		q2.setRequest("weblab");

		q0.setOperator(Operator.AND);
		q0.getQuery().add(q1);
		q0.getQuery().add(q2);

		new WebLabMarshaller().marshalResource(q0, new File("target/composedQuery.xml"));

		final StringQuery q3 = WebLabResourceFactory.createResource("sample", "q-test3", StringQuery.class);
		q3.setRequest("fr");
		final WRetrievalAnnotator wra = new WRetrievalAnnotator(q3);
		wra.writeScope(URI.create(DublinCore.LANGUAGE_PROPERTY_NAME));
		q0.getQuery().add(q3);

		new WebLabMarshaller().marshalResource(q0, new File("target/complexQuery.xml"));
	}


	/**
	 * This code enable the creation of a SPARQL StringQuery. It has been annotated with the isExpressedWith statement.
	 *
	 * @throws WebLabCheckedException
	 *             If marshaling fails
	 */
	public void createSparqlStringQuery() throws WebLabCheckedException {
		final String sparql = "PREFIX foaf:   <http://xmlns.com/foaf/0.1/>\n" + "PREFIX org:    <http://example.com/ns#>\n" + "CONSTRUCT { ?x foaf:name ?name }\n"
				+ "WHERE  { ?x org:employeeName ?name }\n";

		final StringQuery q = WebLabResourceFactory.createResource("sample", "q-sparql", StringQuery.class);
		q.setRequest(sparql);

		final WRetrievalAnnotator wra = new WRetrievalAnnotator(q);
		wra.writeExpressedWith("SPARQL");

		new WebLabMarshaller().marshalResource(q, new File("target/stringQuery_sparql.xml"));
	}


	/**
	 * This code enable the creation of a SimilarityQuery
	 *
	 * @throws WebLabCheckedException
	 *             If marshaling fails
	 */
	public void createSimilarityQuery() throws WebLabCheckedException {
		final SimilarityQuery q = WebLabResourceFactory.createResource("sample", "q-similarity", SimilarityQuery.class);

		/*
		 * Create positive samples
		 */
		final Image image1 = WebLabResourceFactory.createResource("sample", "i-similarity", Image.class);
		final Image image2 = WebLabResourceFactory.createResource("sample", "i-similarity", Image.class);

		q.getResource().add(image1);
		q.getResource().add(image2);

		new WebLabMarshaller().marshalResource(q, new File("target/similarityQuery.xml"));
	}

}
