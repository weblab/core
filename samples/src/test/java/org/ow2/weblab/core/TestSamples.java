/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.helpers.samples.DocumentCreator;
import org.ow2.weblab.core.helpers.samples.QueryCreator;
import org.ow2.weblab.core.helpers.samples.ResultSetCreator;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.services.UnexpectedException;

import eu.virtuoso.VirtuosoAnnotator;

/**
 * Unit test from samples
 *
 * @author asaval
 */
public class TestSamples {

	@Test
	public void testDocumentCreation() throws WebLabCheckedException, URISyntaxException{
		DocumentCreator documentCreator = new DocumentCreator();

		documentCreator.createComplexMultiDoc();
		documentCreator.createCrawledDocument();
		Assert.assertNotNull(DocumentCreator.createDocumentWithAnnotation());
		documentCreator.createDocumentWithSimpleAnnotation();
		Assert.assertNotNull(DocumentCreator.createDocWithDeutsch());
		Assert.assertNotNull(DocumentCreator.createDocWithGreek());
		Assert.assertNotNull(DocumentCreator.createDocWithSpanish());
		documentCreator.createMultilingualDoc();
		Assert.assertNotNull(DocumentCreator.createSimpleDocumentWithBasicMeta());
		Assert.assertNotNull(DocumentCreator.createSimpleDocumentWithSmallEnTexts());
		Assert.assertNotNull(DocumentCreator.createDocumentFromGuttenbergEbooks());
		documentCreator.createSimpleTextDocument();
	}

	@Test
	public void testQueryCreation() throws WebLabCheckedException, URISyntaxException {
		QueryCreator queryCreator = new QueryCreator();

		queryCreator.createSimilarityQuery();
		queryCreator.createSimpleStringQuery();
		queryCreator.createSparqlStringQuery();
		queryCreator.createComposedQuery();

	}


	@Test
	public void testResultSetCreation() throws WebLabCheckedException, URISyntaxException, UnexpectedException {
		ResultSetCreator resultSetCreator = new ResultSetCreator();

		Assert.assertNotNull(ResultSetCreator.createEmptyResultSet());
		Assert.assertNotNull(ResultSetCreator.createResultSet());
		resultSetCreator.createResultSetForSearch();

	}

	@Test
	public void testVirtuosoAnnotator(){
		Document document = WebLabResourceFactory.createResource("virtuosoTestDocument", "testFactory", Document.class);
		Image image = WebLabResourceFactory.createAndLinkMediaUnit(document, Image.class);
		Audio audio = WebLabResourceFactory.createAndLinkMediaUnit(document, Audio.class);
		Text text = WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class);

		URI audioURI = URI.create(audio.getUri());
		URI textURI = URI.create(text.getUri());
		URI imageURI = URI.create(image.getUri());
		URI weblab = URI.create("http://weblab.ow2.org");

		VirtuosoAnnotator va = new VirtuosoAnnotator(document);
		va.writeExtractedFrom(weblab);

		va.startInnerAnnotatorOn(audioURI);
		va.writeRecognisedFrom(imageURI);

		va.startInnerAnnotatorOn(textURI);
		va.writeTranscriptionOf(audioURI);

		Assert.assertEquals(audioURI, va.readTranscriptionOf().firstTypedValue());

		va = new VirtuosoAnnotator(URI.create(document.getUri()), document.getAnnotation().get(0));

		va.startInnerAnnotatorOn(audioURI);

		Assert.assertEquals(imageURI, va.readRecognisedFrom().firstTypedValue());

		va.endInnerAnnotator();

		Assert.assertEquals(weblab, va.readExtractedFrom().firstTypedValue());


	}

}
