# WebLab Core

The WebLab Core is a Maven multi-module project containing the model and some other utility projects that are helpful when handling the Java version of the model.

# Build status (testing purposes)
[![build status](https://gitlab.ow2.org/weblab/core/badges/master/build.svg)](https://gitlab.ow2.org/weblab/core/commits/master)
