/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.components;

import java.util.LinkedList;
import java.util.List;

import org.ow2.weblab.core.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * WebLab library allowing to merge several resources from a common ancestor,
 * especially useful for parallel executions.
 *
 * @author Clément Caron
 */
public class ResourcesMerger {


	private static ResourcesMerger instance = null;


	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	private ResourcesMerger() {
	}


	public static ResourcesMerger getInstance() {
		if (ResourcesMerger.instance == null) {
			ResourcesMerger.instance = new ResourcesMerger();
		}
		return ResourcesMerger.instance;
	}


	/**
	 * Merge <code>resourcesToMerge</code> from <code>initialResource</code>
	 *
	 * @param initialResource
	 *            The original resource, used as basis before processing
	 * @param resourcesToMerge
	 *            The result of original resource processing by various services. To be merged together.
	 * @return mergedResource
	 */
	public Resource merge(final Resource initialResource, final Resource... resourcesToMerge) {

		/*
		 * check args
		 */
		if ((resourcesToMerge.length == 0) || (initialResource == null)) {
			throw new IllegalArgumentException("No initial Resource or no Resource to merge");
		}
		if (resourcesToMerge.length == 1) {
			if (resourcesToMerge[0] == null) {
				throw new IllegalArgumentException("Resource to merge is null");
			}
			return resourcesToMerge[0];
		}

		final List<Resource> resources = new LinkedList<>();
		for (final Resource resource : resourcesToMerge) {
			if (resource != null) {
				resources.add(resource);
			}
		}

		if (resources.size() == 0) {
			throw new IllegalArgumentException("Resource to merge is null");
		} else if (resources.size() == 1) {
			return resources.get(0);
		}

		/*
		 * call merger
		 */
		final WebLabMerger merger = new WebLabMerger(this.logger);
		final Resource result = merger.merge(initialResource, resourcesToMerge);

		return result;
	}


	/**
	 * Merge <code>resourcesToMerge</code> from <code>initialResource</code>
	 *
	 * @param initialResource
	 *            The original resource, used as basis before processing
	 * @param resourcesToMerge
	 *            The result of original resource processing by various services. To be merged together.
	 * @return mergedResource
	 */
	public Resource merge(final Resource initialResource, final List<Resource> resourcesToMerge) {

		/*
		 * check args
		 */
		if (resourcesToMerge.isEmpty() || (initialResource == null)) {
			throw new IllegalArgumentException("No initial Resource or no Resource to merge");
		}
		if (resourcesToMerge.size() == 1) {
			if (resourcesToMerge.get(0) == null) {
				throw new IllegalArgumentException("Resource to merge is null");
			}
			return resourcesToMerge.get(0);
		}

		final List<Resource> resources = new LinkedList<>();
		for (final Resource resource : resourcesToMerge) {
			if (resource != null) {
				resources.add(resource);
			}
		}

		if (resources.size() == 0) {
			throw new IllegalArgumentException("Resource to merge is null");
		} else if (resources.size() == 1) {
			return resources.get(0);
		}

		/*
		 * call merger
		 */
		final WebLabMerger merger = new WebLabMerger(this.logger);
		final Resource result = merger.merge(initialResource, resourcesToMerge);

		return result;
	}
}
