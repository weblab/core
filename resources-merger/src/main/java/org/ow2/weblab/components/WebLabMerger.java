/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.components;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.jena.rdf.model.Model;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LowLevelDescriptor;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.utils.BackEndJenaHelper;
import org.slf4j.Logger;


/**
 * Merger class using WebLab Tools
 *
 * @author Clément Caron
 * @date Aug 2, 2012
 */
class WebLabMerger {


	private final Logger logger;


	public WebLabMerger(final Logger logger) {
		this.logger = logger;
	}


	/**
	 * merge <code>resourcesToMerge</code> from <code>initialResource</code>
	 *
	 * @param initialResource
	 * @param resourcesToMerge
	 * @return mergedResource
	 */
	Resource merge(final Resource initialResource, final Resource[] resourcesToMerge) {
		final List<String> initialURIs = this.listURIs(initialResource);
		final List<Resource> resources = new LinkedList<>();

		/*
		 * check every resources to merge for conflicting URIs
		 */
		for (final Resource resource : resourcesToMerge) {
			final List<String> URIs = this.listURIs(resource);
			final List<String> toChangedURIs = new LinkedList<>();

			/*
			 * check URIs
			 */
			for (final String string : URIs) {
				if (!initialURIs.contains(string)) {
					toChangedURIs.add(string);
				}
			}

			/*
			 * change URIs if needed
			 */
			if (toChangedURIs.isEmpty()) {
				resources.add(resource);
			} else {
				resources.add(this.changeURIs(resource, toChangedURIs));
			}
		}

		/*
		 * merge
		 */
		final Map<String, String> uriMap = this.mergeResource(initialResource, resources);
		final Resource finalResource = this.treatURI(initialResource, uriMap);
		return finalResource;
	}


	/**
	 * merge <code>resourcesToMerge</code> from <code>initialResource</code>
	 *
	 * @param initialResource
	 * @param resourcesToMerge
	 * @return mergedResource
	 */
	Resource merge(final Resource initialResource, final List<Resource> resourcesToMerge) {
		final List<String> initialURIs = this.listURIs(initialResource);

		final List<Resource> resources = new LinkedList<>();

		/*
		 * check every resources to merge for conflicting URIs
		 */
		for (final Resource resource : resourcesToMerge) {
			final List<String> URIs = this.listURIs(resource);
			final List<String> toChangedURIs = new LinkedList<>();

			/*
			 * check URIs
			 */
			for (final String string : URIs) {
				if (!initialURIs.contains(string)) {
					toChangedURIs.add(string);
				}
			}

			/*
			 * change URIs if needed
			 */
			if (toChangedURIs.isEmpty()) {
				resources.add(resource);
			} else {
				resources.add(this.changeURIs(resource, toChangedURIs));
			}
		}

		/*
		 * merge
		 */
		final Map<String, String> uriMap = this.mergeResource(initialResource, resources);
		final Resource finalResource = this.treatURI(initialResource, uriMap);
		return finalResource;
	}


	private Resource treatURI(final Resource resource, final Map<String, String> uriMap) {
		try {
			final StringWriter sw = new StringWriter();
			final WebLabMarshaller wm = new WebLabMarshaller();
			wm.marshalResource(resource, sw);

			String resourceAsString = sw.toString();

			for (final String uriKey : uriMap.keySet()) {
				resourceAsString = resourceAsString.replace(uriKey, uriMap.get(uriKey));
			}

			final Resource result = wm.unmarshal(new StringReader(resourceAsString), Document.class);
			return result;
		} catch (final WebLabCheckedException e) {
			this.logger.error("Error while changing URIs", e);
		}

		return null;
	}


	/**
	 * Change URIs (in case of conflict) with random URIs
	 *
	 * @param resource
	 * @param toChangedURIs
	 * @return resource
	 */
	private Resource changeURIs(final Resource resource, final List<String> toChangedURIs) {
		try {
			final StringWriter sw = new StringWriter();
			final WebLabMarshaller wm = new WebLabMarshaller();
			wm.marshalResource(resource, sw);

			String resourceAsString = sw.toString();

			for (final String string : toChangedURIs) {
				final String oldUri = "\"" + string + "\"";
				final String newUri = "\"" + this.getNewURI() + "\"";
				resourceAsString = resourceAsString.replace(oldUri, newUri);
			}

			final Resource result = wm.unmarshal(new StringReader(resourceAsString), Document.class);
			return result;
		} catch (final WebLabCheckedException e) {
			this.logger.error("Error while changing URIs", e);
		}

		return null;
	}


	/**
	 * Generate a random URI
	 *
	 * @return uriAsString
	 */
	private String getNewURI() {
		return "weblab://" + UUID.randomUUID().toString();
	}


	private String getNewURIForAnnotation(final Resource annotatedResource) {
		return MergerFactory.getUniqueURIFrom(annotatedResource, false, true);
	}


	private String getNewURIForChild(final Resource parentResource) {
		return MergerFactory.getUniqueURIFrom(parentResource, true, false);
	}


	/**
	 * List URIs of a Resource
	 *
	 * @param resource
	 * @return listOfURIs
	 */
	private List<String> listURIs(final Resource resource) {
		try {
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			final WebLabMarshaller wm = new WebLabMarshaller();
			final SAXBuilder saxBuilder = new SAXBuilder();

			wm.marshalResource(resource, out);
			final org.jdom.Document dom = saxBuilder.build(new ByteArrayInputStream(out.toByteArray()));
			return this.listURIs(dom);
		} catch (final WebLabCheckedException e) {
			this.logger.error("Error while listing URIs", e);
		} catch (final JDOMException e) {
			this.logger.error("Error while listing URIs", e);
		} catch (final IOException e) {
			this.logger.error("Error while listing URIs", e);
		}

		return null;
	}


	/**
	 * List URIs of a <code>org.jdom.Document</code>
	 *
	 * @param dom
	 * @return listOfURIs
	 */
	private List<String> listURIs(final org.jdom.Document dom) {
		final List<String> result = new LinkedList<>();

		try {
			final List<?> list = XPath.selectNodes(dom, "//*[@uri]");
			for (final Object object : list) {
				if (object instanceof Element) {
					final Element e = (Element) object;
					result.add(e.getAttributeValue("uri"));
				}
			}
		} catch (final JDOMException e) {
			this.logger.error("Error while listing URIs", e);
		}
		return result;
	}


	/**
	 * merge <code>resourcesToMerge</code> from <code>initialResource</code>
	 *
	 * @param initialResource
	 * @param resourcesToMerge
	 * @return mergedResource
	 */
	private Map<String, String> mergeResource(final Resource initialResource, final List<Resource> resourcesToMerge) {
		final Map<String, String> uriMap = new HashMap<>();
		final List<String> doneURIs = new LinkedList<>();

		/*
		 * Merge Annotations
		 */
		for (final Annotation initialAnnotation : initialResource.getAnnotation()) {
			if (initialAnnotation.getUri() == null) {
				this.logger.error("Annotation have no URI, and will not be treated");
			} else {
				final List<Resource> annotationsToMerge = new LinkedList<>();
				for (final Resource resource : resourcesToMerge) {
					boolean notFound = true;
					for (int i = 0; (i < resource.getAnnotation().size()) && notFound; i++) {
						if (resource.getAnnotation().get(i).getUri() == null) {
							this.logger.error("Annotation have no URI, and will not be treated");
						} else {
							if (resource.getAnnotation().get(i).getUri().equals(initialAnnotation.getUri())) {
								notFound = false;
								annotationsToMerge.add(resource.getAnnotation().get(i));
							}
						}
					}
					if (notFound) {
						this.logger.warn("Annotation remove not handle");
					}
				}
				uriMap.putAll(this.mergeResource(initialAnnotation, annotationsToMerge));
				doneURIs.add(initialAnnotation.getUri());
			}

		}
		for (final Resource resource : resourcesToMerge) {
			for (final Annotation annotation : resource.getAnnotation()) {
				if (!doneURIs.contains(annotation.getUri())) {
					this.logger.info("Add Annotation to Resource");
					final String newURI = this.getNewURIForAnnotation(initialResource);
					uriMap.put(annotation.getUri(), newURI);
					annotation.setUri(newURI);
					initialResource.getAnnotation().add(annotation);
					uriMap.putAll(this.fixURIOfResource(annotation));
				}
			}
		}

		/*
		 * Merge LowLevelDescriptors
		 */
		for (final LowLevelDescriptor initialLowLevelDescriptor : initialResource.getDescriptor()) {
			if (initialLowLevelDescriptor.getUri() == null) {
				this.logger.error("LowLevelDescriptor have no URI, and will not be treated");
			} else {
				final List<Resource> lowLevelDescriptorsToMerge = new LinkedList<>();
				for (final Resource resource : resourcesToMerge) {
					boolean notFound = true;
					for (int i = 0; (i < resource.getDescriptor().size()) && notFound; i++) {
						if (resource.getDescriptor().get(i).getUri() == null) {
							this.logger.error("LowLevelDescriptor have no URI, and will not be treated");
						} else {
							if (resource.getDescriptor().get(i).getUri().equals(initialLowLevelDescriptor.getUri())) {
								notFound = false;
								lowLevelDescriptorsToMerge.add(resource.getDescriptor().get(i));
							}
						}
					}
					if (notFound) {
						this.logger.warn("LowLevelDescriptor remove not handle");
					}
				}
				uriMap.putAll(this.mergeResource(initialLowLevelDescriptor, lowLevelDescriptorsToMerge));
				doneURIs.add(initialLowLevelDescriptor.getUri());
			}
		}
		for (final Resource resource : resourcesToMerge) {
			for (final LowLevelDescriptor lowLevelDescriptor : resource.getDescriptor()) {
				if (!doneURIs.contains(lowLevelDescriptor.getUri())) {
					this.logger.info("Add LowLevelDescriptor to Resource");
					final String newURI = this.getNewURIForChild(initialResource);
					uriMap.put(lowLevelDescriptor.getUri(), newURI);
					lowLevelDescriptor.setUri(newURI);
					initialResource.getDescriptor().add(lowLevelDescriptor);
					uriMap.putAll(this.fixURIOfResource(lowLevelDescriptor));
				}
			}
		}

		/*
		 * Redirect to resource type function
		 */
		if (initialResource instanceof MediaUnit) {
			final MediaUnit initialMediaUnit = (MediaUnit) initialResource;
			final List<MediaUnit> mediaUnitsToMerge = new LinkedList<>();
			for (final Resource resource : resourcesToMerge) {
				if (resource instanceof MediaUnit) {
					final MediaUnit mediaUnit = (MediaUnit) resource;
					mediaUnitsToMerge.add(mediaUnit);
				} else {
					this.logger.error("Resource is " + resource.getClass() + " and a " + initialResource.getClass() + "was needed");
				}
			}
			uriMap.putAll(this.mergeMediaUnit(initialMediaUnit, mediaUnitsToMerge));
		} else if (initialResource instanceof ComposedResource) {
			final ComposedResource initialComposedResource = (ComposedResource) initialResource;
			final List<ComposedResource> composedResourcesToMerge = new LinkedList<>();
			for (final Resource resource : resourcesToMerge) {
				if (resource instanceof ComposedResource) {
					final ComposedResource composedResource = (ComposedResource) resource;
					composedResourcesToMerge.add(composedResource);
				} else {
					this.logger.error("Resource is " + resource.getClass() + " and a " + initialResource.getClass() + "was needed");
				}
			}
			uriMap.putAll(this.mergeComposedResource(initialComposedResource, composedResourcesToMerge));
		} else if (initialResource instanceof LowLevelDescriptor) {
			final LowLevelDescriptor initialLowLevelDescriptor = (LowLevelDescriptor) initialResource;
			final List<LowLevelDescriptor> lowLevelDescriptorsToMerge = new LinkedList<>();
			for (final Resource resource : resourcesToMerge) {
				if (resource instanceof LowLevelDescriptor) {
					final LowLevelDescriptor annotation = (LowLevelDescriptor) resource;
					lowLevelDescriptorsToMerge.add(annotation);
				} else {
					this.logger.error("Resource is " + resource.getClass() + " and a " + initialResource.getClass() + "was needed");
				}
			}
			uriMap.putAll(this.mergeLowLevelDescriptor(initialLowLevelDescriptor, lowLevelDescriptorsToMerge));
		} else if (initialResource instanceof PieceOfKnowledge) {
			final PieceOfKnowledge initialPOK = (PieceOfKnowledge) initialResource;
			final List<PieceOfKnowledge> poksToMerge = new LinkedList<>();
			for (final Resource resource : resourcesToMerge) {
				if (resource instanceof PieceOfKnowledge) {
					final PieceOfKnowledge pok = (PieceOfKnowledge) resource;
					poksToMerge.add(pok);
				} else {
					this.logger.error("Resource is " + resource.getClass() + " and a " + initialResource.getClass() + "was needed");
				}
			}
			uriMap.putAll(this.mergePieceOfKnowledge(initialPOK, poksToMerge));
		} else if (initialResource instanceof Query) {
			final Query initialQuery = (Query) initialResource;
			final List<Query> querysToMerge = new LinkedList<>();
			for (final Resource resource : resourcesToMerge) {
				if (resource instanceof Query) {
					final Query query = (Query) resource;
					querysToMerge.add(query);
				} else {
					this.logger.error("Resource is " + resource.getClass() + " and a " + initialResource.getClass() + "was needed");
				}
			}
			uriMap.putAll(this.mergeQuery(initialQuery, querysToMerge));
		} else if (initialResource instanceof ResultSet) {
			final ResultSet initialResultSet = (ResultSet) initialResource;
			final List<ResultSet> resultSetsToMerge = new LinkedList<>();
			for (final Resource resource : resourcesToMerge) {
				if (resource instanceof ResultSet) {
					final ResultSet resultSet = (ResultSet) resource;
					resultSetsToMerge.add(resultSet);
				} else {
					this.logger.error("Resource is " + resource.getClass() + " and a " + initialResource.getClass() + "was needed");
				}
			}
			uriMap.putAll(this.mergeResultSet(initialResultSet, resultSetsToMerge));
		} else {
			this.logger.warn("Resource type : " + initialResource.getClass() + "not completely handle");
		}

		return uriMap;
	}


	/**
	 * Merge ResultSets (not handle yet)
	 *
	 * @param initialResultSet
	 * @param resultSetsToMerge
	 */
	private Map<String, String> mergeResultSet(final ResultSet initialResultSet, final List<ResultSet> resultSetsToMerge) {
		this.logger.warn("ResultSet not handle yet (has it to be?...)");
		return new HashMap<>();
	}


	/**
	 * Merge Queries (not handle yet)
	 *
	 * @param initialQuery
	 * @param querysToMerge
	 */
	private Map<String, String> mergeQuery(final Query initialQuery, final List<Query> querysToMerge) {
		this.logger.warn("Query not handle yet (has it to be?...)");
		return new HashMap<>();
	}


	/**
	 * Merge Pieces Of Knowledge
	 *
	 * @param initialPOK
	 * @param poksToMerge
	 */
	private Map<String, String> mergePieceOfKnowledge(final PieceOfKnowledge initialPOK, final List<PieceOfKnowledge> poksToMerge) {
		final Map<String, String> uriMap = new HashMap<>();
		this.mergeData(initialPOK, poksToMerge);

		if (initialPOK instanceof Annotation) {
			final Annotation initialAnnotation = (Annotation) initialPOK;
			final List<Annotation> annotationsToMerge = new LinkedList<>();
			for (final PieceOfKnowledge pok : poksToMerge) {
				if (pok instanceof Annotation) {
					final Annotation annotation = (Annotation) pok;
					annotationsToMerge.add(annotation);
				} else {
					this.logger.error("PieceOfKnowledge is " + pok.getClass() + " and a " + initialPOK.getClass() + "was needed");
				}
			}
			uriMap.putAll(this.mergeAnnotation(initialAnnotation, annotationsToMerge));
		}

		return uriMap;
	}


	/**
	 * Merge Annotations (not handle yet)
	 *
	 * @param initialAnnotation
	 * @param annotationsToMerge
	 */
	private Map<String, String> mergeAnnotation(final Annotation initialAnnotation, final List<Annotation> annotationsToMerge) {
		this.logger.warn("Annotation merge not handle for now, nothing to do here");
		return new HashMap<>();
	}


	/**
	 * Merge ComposedResources
	 *
	 * @param initialComposedResource
	 * @param composedResourcesToMerge
	 */
	private Map<String, String> mergeComposedResource(final ComposedResource initialComposedResource, final List<ComposedResource> composedResourcesToMerge) {

		final Map<String, String> uriMap = new HashMap<>();
		final List<String> doneURIs = new LinkedList<>();

		/*
		 * Merge Resources
		 */
		for (final Resource initialResource : initialComposedResource.getResource()) {
			if (initialResource.getUri() == null) {
				this.logger.error("Resource have no URI, and will not be treated");
			} else {
				final List<Resource> resourcesToMerge = new LinkedList<>();
				for (final ComposedResource composedResource : composedResourcesToMerge) {
					boolean notFound = true;
					for (int i = 0; (i < composedResource.getResource().size()) && notFound; i++) {
						if (composedResource.getResource().get(i).getUri() == null) {
							this.logger.error("Resource have no URI, and will not be treated");
						} else {
							if (composedResource.getResource().get(i).getUri().equals(initialResource.getUri())) {
								notFound = false;
								resourcesToMerge.add(composedResource.getResource().get(i));
							}
						}
					}
					if (notFound) {
						this.logger.warn("Resource remove not handle");
					}
				}
				uriMap.putAll(this.mergeResource(initialResource, resourcesToMerge));
				doneURIs.add(initialResource.getUri());
			}
		}
		for (final ComposedResource composedResource : composedResourcesToMerge) {
			for (final Resource resource : composedResource.getResource()) {
				if (!doneURIs.contains(resource.getUri())) {
					this.logger.info("Add Resource to ComposedResource");
					final String newURI = this.getNewURIForChild(initialComposedResource);
					uriMap.put(resource.getUri(), newURI);
					resource.setUri(newURI);
					initialComposedResource.getResource().add(resource);
					uriMap.putAll(this.fixURIOfResource(resource));
				}
			}
		}

		return uriMap;
	}


	/**
	 * Merge MediaUnits
	 *
	 * @param initialMediaUnit
	 * @param mediaUnitsToMerge
	 */
	private Map<String, String> mergeMediaUnit(final MediaUnit initialMediaUnit, final List<MediaUnit> mediaUnitsToMerge) {
		final Map<String, String> uriMap = new HashMap<>();
		final List<String> doneURIs = new LinkedList<>();

		/*
		 * Merge Segments
		 */
		for (final Segment initialSegment : initialMediaUnit.getSegment()) {
			if (initialSegment.getUri() == null) {
				this.logger.error("Segment have no URI, and will not be treated");
			} else {
				final List<Segment> segmentsToMerge = new LinkedList<>();
				for (final MediaUnit mediaUnit : mediaUnitsToMerge) {
					boolean notFound = true;
					for (int i = 0; (i < mediaUnit.getSegment().size()) && notFound; i++) {
						if (mediaUnit.getSegment().get(i).getUri() == null) {
							this.logger.error("Segment have no URI, and will not be treated");
						} else {
							if (mediaUnit.getSegment().get(i).getUri().equals(initialSegment.getUri())) {
								notFound = false;
								segmentsToMerge.add(mediaUnit.getSegment().get(i));
							}
						}
					}
					if (notFound) {
						this.logger.warn("Segment remove not handle");
					}
				}
				uriMap.putAll(this.mergeSegment(initialSegment, segmentsToMerge));
				doneURIs.add(initialSegment.getUri());
			}
		}
		for (final MediaUnit mediaUnit : mediaUnitsToMerge) {
			for (final Segment segment : mediaUnit.getSegment()) {
				if (!doneURIs.contains(segment.getUri())) {
					this.logger.info("Add Segment to MediaUnit");
					final String newURI = this.getNewURIForChild(initialMediaUnit);
					uriMap.put(segment.getUri(), newURI);
					segment.setUri(newURI);
					initialMediaUnit.getSegment().add(segment);
				}
			}
		}

		/*
		 * Redirect to MediaUnit type function
		 */
		if (initialMediaUnit instanceof Document) {
			final Document initialDocument = (Document) initialMediaUnit;
			final List<Document> documentsToMerge = new LinkedList<>();
			for (final MediaUnit mediaUnit : mediaUnitsToMerge) {
				if (mediaUnit instanceof Document) {
					documentsToMerge.add((Document) mediaUnit);
				} else {
					this.logger.error("MediaUnit is " + mediaUnit.getClass() + " and a " + initialMediaUnit.getClass() + "was needed");
				}
			}

			if (!documentsToMerge.isEmpty()) {
				uriMap.putAll(this.mergeDocument(initialDocument, documentsToMerge));
			}
		} else if (initialMediaUnit instanceof Audio) {
			final Audio initialAudio = (Audio) initialMediaUnit;
			final List<Audio> audiosToMerge = new LinkedList<>();
			for (final MediaUnit mediaUnit : mediaUnitsToMerge) {
				if (mediaUnit instanceof Audio) {
					audiosToMerge.add((Audio) mediaUnit);
				} else {
					this.logger.error("MediaUnit is " + mediaUnit.getClass() + " and a " + initialMediaUnit.getClass() + "was needed");
				}
			}

			if (!audiosToMerge.isEmpty()) {
				uriMap.putAll(this.mergeAudio(initialAudio, audiosToMerge));
			}
		} else if (initialMediaUnit instanceof Image) {
			final Image initialImage = (Image) initialMediaUnit;
			final List<Image> imagesToMerge = new LinkedList<>();
			for (final MediaUnit mediaUnit : mediaUnitsToMerge) {
				if (mediaUnit instanceof Image) {
					imagesToMerge.add((Image) mediaUnit);
				} else {
					this.logger.error("MediaUnit is " + mediaUnit.getClass() + " and a " + initialMediaUnit.getClass() + "was needed");
				}
			}

			if (!imagesToMerge.isEmpty()) {
				uriMap.putAll(this.mergeImage(initialImage, imagesToMerge));
			}
		} else if (initialMediaUnit instanceof Text) {
			final Text initialText = (Text) initialMediaUnit;
			final List<Text> textsToMerge = new LinkedList<>();
			for (final MediaUnit mediaUnit : mediaUnitsToMerge) {
				if (mediaUnit instanceof Text) {
					textsToMerge.add((Text) mediaUnit);
				} else {
					this.logger.error("MediaUnit is " + mediaUnit.getClass() + " and a " + initialMediaUnit.getClass() + "was needed");
				}
			}

			if (!textsToMerge.isEmpty()) {
				uriMap.putAll(this.mergeText(initialText, textsToMerge));
			}
		} else if (initialMediaUnit instanceof Video) {
			final Video initialVideo = (Video) initialMediaUnit;
			final List<Video> videosToMerge = new LinkedList<>();
			for (final MediaUnit mediaUnit : mediaUnitsToMerge) {
				if (mediaUnit instanceof Video) {
					videosToMerge.add((Video) mediaUnit);
				} else {
					this.logger.error("MediaUnit is " + mediaUnit.getClass() + " and a " + initialMediaUnit.getClass() + "was needed");
				}
			}

			if (!videosToMerge.isEmpty()) {
				uriMap.putAll(this.mergeVideo(initialVideo, videosToMerge));
			}
		} else {
			this.logger.warn("MediaUnit type : " + initialMediaUnit.getClass() + "not completely handle");
		}

		return uriMap;
	}


	/**
	 * Merge Videos (not handle yet)
	 *
	 * @param initialVideo
	 * @param videosToMerge
	 */
	private Map<String, String> mergeVideo(final Video initialVideo, final List<Video> videosToMerge) {
		this.logger.warn("Video content not handle yet (has it to be?...)");
		return new HashMap<>();
	}


	/**
	 * Merge Texts (not handle yet)
	 *
	 * @param initialText
	 * @param textsToMerge
	 */
	private Map<String, String> mergeText(final Text initialText, final List<Text> textsToMerge) {
		this.logger.warn("Text content not handle yet (has it to be?...)");
		return new HashMap<>();
	}


	/**
	 * Merge Images (not handle yet)
	 *
	 * @param initialImage
	 * @param imagesToMerge
	 */
	private Map<String, String> mergeImage(final Image initialImage, final List<Image> imagesToMerge) {
		this.logger.warn("Image content not handle yet (has it to be?...)");
		return new HashMap<>();
	}


	/**
	 * Merge Audios (not handle yet)
	 *
	 * @param initialAudio
	 * @param audiosToMerge
	 */
	private Map<String, String> mergeAudio(final Audio initialAudio, final List<Audio> audiosToMerge) {
		this.logger.warn("Audio content not handle yet (has it to be?...)");
		return new HashMap<>();
	}


	/**
	 * Merge Documents
	 *
	 * @param initialDocument
	 * @param documentsToMerge
	 */
	private Map<String, String> mergeDocument(final Document initialDocument, final List<Document> documentsToMerge) {

		final Map<String, String> uriMap = new HashMap<>();
		final List<String> doneURIs = new LinkedList<>();

		/*
		 * Merge MediaUnits
		 */
		for (final MediaUnit initialMediaUnit : initialDocument.getMediaUnit()) {
			if (initialMediaUnit.getUri() == null) {
				this.logger.error("MediaUnit have no URI, and will not be treated");
			} else {
				final List<Resource> mediaUnitsToMerge = new LinkedList<>();
				for (final Document document : documentsToMerge) {
					boolean notFound = true;
					for (int i = 0; (i < document.getMediaUnit().size()) && notFound; i++) {
						if (document.getMediaUnit().get(i).getUri() == null) {
							this.logger.error("MediaUnit have no URI, and will not be treated");
						} else {
							if (document.getMediaUnit().get(i).getUri().equals(initialMediaUnit.getUri())) {
								notFound = false;
								mediaUnitsToMerge.add(document.getMediaUnit().get(i));
							}
						}
					}
					if (notFound) {
						this.logger.warn("MediaUnit remove not handle");
					}
				}
				uriMap.putAll(this.mergeResource(initialMediaUnit, mediaUnitsToMerge));
				doneURIs.add(initialMediaUnit.getUri());
			}
		}
		for (final Document document : documentsToMerge) {
			for (final MediaUnit mediaUnit : document.getMediaUnit()) {
				if (!doneURIs.contains(mediaUnit.getUri())) {
					this.logger.info("Add MediaUnit to Document");
					final String newURI = this.getNewURIForChild(initialDocument);
					uriMap.put(mediaUnit.getUri(), newURI);
					mediaUnit.setUri(newURI);
					initialDocument.getMediaUnit().add(mediaUnit);
					uriMap.putAll(this.fixURIOfResource(mediaUnit));
				}
			}
		}

		return uriMap;
	}


	/**
	 * Merge LowLevelDescriptors (not handle yet)
	 *
	 * @param initialLowLevelDescriptor
	 * @param lowLevelDescriptorsToMerge
	 */
	private Map<String, String> mergeLowLevelDescriptor(final LowLevelDescriptor initialLowLevelDescriptor, final List<LowLevelDescriptor> lowLevelDescriptorsToMerge) {
		this.logger.warn("LowLevelDescriptor merge not handle for now");
		return new HashMap<>();
	}


	/**
	 * Merge Segments (not handle yet)
	 *
	 * @param initialSegment
	 * @param segmentsToMerge
	 */
	private Map<String, String> mergeSegment(final Segment initialSegment, final List<Segment> segmentsToMerge) {
		this.logger.warn("Segment merge not handle for now");
		return new HashMap<>();
	}


	/**
	 * Merge Data
	 *
	 * @param initialPOK
	 * @param poksToMerge
	 */
	private void mergeData(final PieceOfKnowledge initialPOK, final List<PieceOfKnowledge> poksToMerge) {
		final BackEndJenaHelper initialModel = new BackEndJenaHelper(PoKUtil.getPoKData(initialPOK));

		for (final PieceOfKnowledge newPoK : poksToMerge) {
			final Model newModel = new BackEndJenaHelper(PoKUtil.getPoKData(newPoK)).getModel();
			newModel.remove(initialModel.getModel());
			if (!newModel.isEmpty()) {
				initialModel.addToModel(newModel);
			}
		}

		PoKUtil.setPoKData(initialPOK, initialModel.getRdfXml());
	}


	private Map<String, String> fixURIOfResource(final Resource resource) {
		final Map<String, String> uriMap = new HashMap<>();

		for (final Annotation annotation : resource.getAnnotation()) {
			final String newURI = this.getNewURIForAnnotation(resource);
			uriMap.put(annotation.getUri(), newURI);
			annotation.setUri(newURI);

			uriMap.putAll(this.fixURIOfResource(annotation));
		}

		for (final LowLevelDescriptor lld : resource.getDescriptor()) {
			final String newURI = this.getNewURIForChild(resource);
			uriMap.put(lld.getUri(), newURI);
			lld.setUri(newURI);

			uriMap.putAll(this.fixURIOfResource(lld));
		}

		if (resource instanceof MediaUnit) {
			uriMap.putAll(this.fixURIOfMediaUnit((MediaUnit) resource));
		} else if (resource instanceof ComposedResource) {
			uriMap.putAll(this.fixURIOfComposedResource((ComposedResource) resource));
		} else {
			this.logger.warn("Resource type : " + resource.getClass() + "not completely handle");
		}

		return uriMap;
	}


	private Map<String, String> fixURIOfMediaUnit(final MediaUnit resource) {
		final Map<String, String> uriMap = new HashMap<>();

		for (final Segment segment : resource.getSegment()) {
			final String newURI = this.getNewURIForAnnotation(resource);
			uriMap.put(segment.getUri(), newURI);
			segment.setUri(newURI);
		}

		if (resource instanceof Document) {
			uriMap.putAll(this.fixURIOfDocument((Document) resource));
		}

		return uriMap;
	}


	private Map<String, String> fixURIOfDocument(final Document resource) {
		final Map<String, String> uriMap = new HashMap<>();

		for (final MediaUnit mediaUnit : resource.getMediaUnit()) {
			final String newURI = this.getNewURIForAnnotation(resource);
			uriMap.put(mediaUnit.getUri(), newURI);
			mediaUnit.setUri(newURI);
		}

		return uriMap;
	}


	private Map<String, String> fixURIOfComposedResource(final ComposedResource resource) {
		final Map<String, String> uriMap = new HashMap<>();

		for (final Resource res : resource.getResource()) {
			final String newURI = this.getNewURIForAnnotation(resource);
			uriMap.put(res.getUri(), newURI);
			res.setUri(newURI);

			uriMap.putAll(this.fixURIOfResource(res));
		}

		return uriMap;
	}

}
