/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.components;

import java.io.File;
import java.security.CodeSource;

import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourcesMergerTest {


	private static final String FACTORY = "javax.xml.parsers.DocumentBuilderFactory";


	private final ResourcesMerger resourceMerger = ResourcesMerger.getInstance();


	private Logger log = LoggerFactory.getLogger(this.getClass());


	/**
	 * Test with no resources, expect an exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void noArgumentTest() {
		this.resourceMerger.merge(null);
	}


	/**
	 * Test with not enough resources, expect an exception
	 *
	 * @throws WebLabCheckedException
	 */
	@Test(expected = IllegalArgumentException.class)
	public void nullArgumentTest() throws WebLabCheckedException {
		WebLabMarshaller wm = new WebLabMarshaller();
		Resource res0 = wm.unmarshal(new File("src/test/resources/resource.xml"), Document.class);
		this.resourceMerger.merge(res0, null, null);
	}


	/**
	 * Merge Test of 3 resources using various document builderfactories
	 *
	 * @param factory
	 *            The document builder factory to be used for the test
	 *
	 * @throws Exception
	 */
	public void mergeTest(final String factory) throws Exception {
		final String oldFactory = System.getProperty(FACTORY);
		try {
			if (factory == null) {
				System.clearProperty(FACTORY);
			} else {
				System.setProperty(FACTORY, factory);
			}

			this.log.info(this.getDetails());

			final WebLabMarshaller wm = new WebLabMarshaller();
			final Resource res0 = wm.unmarshal(new File("src/test/resources/resource.xml"), Document.class);
			final Resource res1 = wm.unmarshal(new File("src/test/resources/resource2.xml"), Document.class);
			final Resource res2 = wm.unmarshal(new File("src/test/resources/resource3.xml"), Document.class);
			final Resource res3 = wm.unmarshal(new File("src/test/resources/resource4.xml"), Document.class);

			final Resource result = this.resourceMerger.merge(res0, res1, res2, res3);

			wm.marshalResource(result, new File("target", "merge" + System.currentTimeMillis() + ".xml"));
		} catch (Exception e) {
			this.log.error(this.getDetails(), e);
			throw e;
		} finally {
			if (oldFactory == null) {
				System.clearProperty(FACTORY);
			} else {
				System.setProperty(FACTORY, oldFactory);
			}
		}


	}


	/**
	 * Test of 3 resources
	 *
	 * @throws Exception
	 */
	@Test
	public void defaultTest() throws Exception {
		mergeTest(null);
	}


	/**
	 * Xerces Test of 3 resources
	 *
	 * @throws Exception
	 */
	@Test
	public void xercesTest() throws Exception {
		mergeTest("org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
	}


	/**
	 * Xerces Test of 3 resources
	 *
	 * @throws Exception
	 */
	@Test
	public void xercesInternalTest() throws Exception {
		mergeTest("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
	}


	private String getDetails() {
		final StringBuilder sb = new StringBuilder();

		sb.append("\nFactory defined as property is: '");
		sb.append(System.getProperty(FACTORY));
		sb.append("'\n");

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Class<? extends DocumentBuilderFactory> clazz = factory.getClass();

		sb.append("Loaded factory is: '");
		sb.append(clazz.getName());
		sb.append("'\n");

		CodeSource source = clazz.getProtectionDomain().getCodeSource();
		sb.append("Class found in: '");
		sb.append(source == null ? "Java Runtime" : source.getLocation());
		sb.append("'\n");

		return sb.toString();
	}

}
