/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helper.impl;

import java.io.ByteArrayOutputStream;
import java.net.URI;

import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.ontologies.WebLabModel;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.ElementaryQuery;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.LowLevelDescriptor;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.TrackSegment;
import org.ow2.weblab.core.model.Video;

public class SemanticResourceTest {


	@Test
	public void testLoadResource() {
		// Empty objects

		final Resource resource = new Resource();
		resource.setUri("weblab://test/entity");
		final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(resource);
		PoKUtil.setPoKData(annot, "");

		final ComposedResource rc = new ComposedResource();
		rc.setUri("weblab://test/entity/rc");
		final LowLevelDescriptor lld = WebLabResourceFactory.createAndLinkLowLevelDescriptor(resource);

		final Annotation annot2 = WebLabResourceFactory.createAndLinkAnnotation(resource);
		new BaseAnnotator(URI.create("all://your#bases"), annot2).applyOperator(Operator.WRITE, "are", URI.create("are://belong#to"), String.class, "me");

		PieceOfKnowledge onto = new PieceOfKnowledge();
		onto.setData(annot2.getData());

		final SpatialSegment ss = new SpatialSegment();
		ss.setUri("uri://ss");
		final TemporalSegment ts = new TemporalSegment();
		ts.setUri("uri://ts");
		final TrackSegment trs = new TrackSegment();
		trs.setUri("uri://trs");
		final LinearSegment ls = new LinearSegment();
		ls.setUri("uri://ls");

		final Image image = new Image();
		image.setUri("uri://image");
		final Video video = new Video();
		video.setUri("uri://video");
		video.getSegment().add(ss);
		video.getSegment().add(ts);
		video.getSegment().add(trs);
		video.getSegment().add(ls);

		final Document doc = new Document();
		doc.setUri("uri://doc");
		doc.getMediaUnit().add(image);
		doc.getMediaUnit().add(video);

		final ResultSet rs = new ResultSet();
		rs.setUri("uri://resultSet");
		rs.setPok(onto);
		rs.getResource().add(doc);

		final StringQuery ftq = new StringQuery();
		final SimilarityQuery siq = new SimilarityQuery();
		siq.setUri("uri://siq");
		final ComposedQuery cq = new ComposedQuery();
		cq.setUri("uri://cq");
		final Query q = new Query() {


			private static final long serialVersionUID = 1L;
		};
		q.setUri("uri://query");
		final ElementaryQuery eq = new ElementaryQuery() {


			private static final long serialVersionUID = 1L;
		};
		eq.setUri("uri://equery");
		cq.getQuery().add(q);
		cq.getQuery().add(eq);

		final Text text = new Text();
		text.setUri("uri://text");
		final Audio au = new Audio();
		au.setUri("uri://audio");

		siq.getResource().add(text);

		rc.getResource().add(lld);
		rc.getResource().add(annot);
		rc.getResource().add(resource);
		rc.getResource().add(onto);
		rc.getResource().add(ftq);
		rc.getResource().add(siq);
		rc.getResource().add(cq);
		rc.getResource().add(q);
		rc.getResource().add(au);
		rc.getResource().add(text);
		rc.getResource().add(eq);
		rc.getResource().add(rs);

		int i = 0;
		for (final Resource r : rc.getResource()) {
			if (r.getUri() == null) {
				r.setUri("weblab://test/entity/r" + i++);
			}
			SemanticResource sr = new SemanticResource(r);
			Assert.assertNotNull(sr);
		}

		// constructors
		SemanticResource sr = new SemanticResource(rc);

		SemanticResource sr1 = new SemanticResource(rc, ModelFactory.createDefaultModel());
		SemanticResource sr12 = new SemanticResource(rc, ModelFactory.createOntologyModel());


		String url = ClassLoader.getSystemClassLoader().getResource("ontologies/model.owl").toString();

		SemanticResource sr2 = new SemanticResource(rc, OntModelSpec.OWL_DL_MEM, url);
		String[] tb = null;
		sr2.addPrefixForSPARQLQueries((String) null);
		sr2.addPrefixForSPARQLQueries(null, null);
		sr2.addPrefixForSPARQLQueries(tb);
		sr2.addPrefixForSPARQLQueries("");

		sr1.loadAnInferWithOntologies(OntModelSpec.OWL_DL_MEM, url);

		// ask

		Assert.assertTrue(sr.ask("ASK {<uri://image> rdf:type <" + WebLabModel.getUriFromResource(Image.class) + ">}"));

		sr.addPrefixForSPARQLQueries("PREFIX wl:<http://weblab.ow2.org>");

		// describes

		final String describe = "DESCRIBE ?image WHERE { ?image <rdf:type> <" + WebLabModel.getUriFromResource(Image.class) + ">}";

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		sr.describeAsRDF(describe, baos);

		Assert.assertNotNull(baos.toString());

		Model jenaModel = sr.describeAsJenaModel(describe);

		Assert.assertNotNull(jenaModel);

		SemanticResource asr = sr.describeAsSemanticResource(describe);

		Assert.assertNotNull(asr);

		baos.reset();

		// selects

		final String select = "SELECT ?image WHERE { ?image rdf:type <" + WebLabModel.getUriFromResource(Image.class) + "> }";

		sr.selectAsCSV(select, baos);

		Assert.assertEquals("image uri://image ", baos.toString().replaceAll("\\s+", " "));

		baos.reset();

		sr.selectAsJSON(select, baos);

		Assert.assertEquals("{ 'head': { 'vars': [ 'image' ] } , 'results': { 'bindings': [ { 'image': { 'type': 'uri' , 'value': 'uri://image' } } ] } } ",
				baos.toString().replaceAll("\\s+", " ").replaceAll("\"", "'"));

		baos.reset();

		Assert.assertNotNull(sr.selectAsWeblabResources(select));

		baos.reset();

		Assert.assertNotNull(sr.selectAsJenaResultSet(select));

		baos.reset();

		sr.selectAsXML(select, baos);

		Assert.assertNotNull(baos.toString());


		// constructs

		final String construct = "CONSTRUCT { ?s ?p ?o } WHERE { ?s <rdf:type> <" + WebLabModel.getUriFromResource(Image.class) + "> }";

		baos.reset();

		sr.constructAsRDF(construct, baos);

		Assert.assertNotNull(baos.toString());

		Assert.assertNotNull(sr.constructAsJenaModel(construct));

		Assert.assertNotNull(sr.constructAsSemanticResource(construct));

		// test failures now
		sr.jenaModel.close();

		Assert.assertNull(sr.selectAsJenaResultSet(select));

		baos.reset();

		sr.selectAsJSON(select, baos);

		Assert.assertEquals("", baos.toString());

		baos.reset();

		sr.selectAsXML(select, baos);

		Assert.assertEquals("", baos.toString());

		baos.reset();

		sr.selectAsCSV(select, baos);

		Assert.assertEquals("", baos.toString());

		Assert.assertNull(sr.selectAsWeblabResources(select));
		Assert.assertNotNull(sr12);
	}
}
