/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helper.impl;

import java.io.StringWriter;
import java.net.URI;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.ElementaryQuery;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.LowLevelDescriptor;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.TrackSegment;
import org.ow2.weblab.core.model.Video;


/**
 */
public class JenaResourceStructureHelperTest {


	@Test
	public void testAddResourcesTest() throws Exception {
		// Empty objects

		final Resource resource = new Resource();
		resource.setUri("weblab://test/entity");
		final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(resource);

		final ComposedResource rc = new ComposedResource();
		rc.setUri("weblab://test/entity/rc");
		final LowLevelDescriptor lld = WebLabResourceFactory.createAndLinkLowLevelDescriptor(resource);

		final Annotation annot2 = WebLabResourceFactory.createAndLinkAnnotation(resource);
		new BaseAnnotator(URI.create("all://your#bases"), annot2).applyOperator(Operator.WRITE, "are", URI.create("are://belong#to"), String.class, "me");

		final PieceOfKnowledge onto = new PieceOfKnowledge();
		onto.setData(annot2.getData());

		final SegmentM sm = new SegmentM();
		sm.setUri("uri://sm");
		final SpatialSegment ss = new SpatialSegment();
		ss.setUri("uri://ss");
		final TemporalSegment ts = new TemporalSegment();
		ts.setUri("uri://ts");
		final TrackSegment trs = new TrackSegment();
		trs.setUri("uri://trs");
		final LinearSegment ls = new LinearSegment();
		ls.setUri("uri://ls");

		final Image image = new Image();
		image.setUri("uri://image");
		final Video video = new Video();
		video.setUri("uri://video");
		video.getSegment().add(ss);
		video.getSegment().add(ts);
		video.getSegment().add(trs);
		video.getSegment().add(ls);
		video.getSegment().add(sm);

		final Document doc = new Document();
		doc.setUri("uri://doc");
		doc.getMediaUnit().add(image);
		doc.getMediaUnit().add(video);

		final ResultSet rs = new ResultSet();
		rs.setUri("uri://resultSet");
		rs.setPok(onto);
		rs.getResource().add(doc);

		final PieceOfKnowledge ontovide = WebLabResourceFactory.createResource("toto", "tuto", PieceOfKnowledge.class);

		final StringQuery ftq = new StringQuery();
		//ftq.setRequest("toto");
		//ftq.setWeight(0);
		final SimilarityQuery siq = new SimilarityQuery();
		siq.setUri("uri://siq");
		final ComposedQuery cq = new ComposedQuery();
		cq.setUri("uri://cq");
		final Query q = new Query() {
			private static final long serialVersionUID = 1L;
		};
		q.setUri("uri://query");
		final ElementaryQuery eq = new ElementaryQuery() {
			private static final long serialVersionUID = 1L;
		};
		eq.setUri("uri://equery");
		cq.getQuery().add(q);
		cq.getQuery().add(eq);

		final Text text = new Text();
		final Audio au = new Audio();

		siq.getResource().add(ontovide);
		siq.getResource().add(text);
		final MediaUnitM mum = new MediaUnitM();

		rc.getResource().add(lld);
		rc.getResource().add(annot);
		rc.getResource().add(resource);
		rc.getResource().add(onto);
		rc.getResource().add(ontovide);
		rc.getResource().add(ftq);
		rc.getResource().add(siq);
		rc.getResource().add(cq);
		rc.getResource().add(q);
		rc.getResource().add(mum);
		rc.getResource().add(au);
		rc.getResource().add(text);
		rc.getResource().add(new ResourceR());
		rc.getResource().add(eq);
		rc.getResource().add(rs);
		rc.getResource().add(new ResultSet());

		int i = 0;
		for (final Resource r : rc.getResource()) {
			if (r.getUri() == null) {
				r.setUri("weblab://test/entity/r" + i++);
			}
		}

		final WebLabMarshaller wm = new WebLabMarshaller();
		final StringWriter sw = new StringWriter();
		wm.marshalResource(rc, sw);

		final JenaResourceStructureHelper jrshe = new JenaResourceStructureHelper(rc);
		final Annotation struct = WebLabResourceFactory.createAndLinkAnnotation(rc);
		PoKUtil.setPoKData(struct, jrshe.getRdfXml());

		final WebLabMarshaller wm1 = new WebLabMarshaller();
		final StringWriter sw1 = new StringWriter();
		wm1.marshalResource(rc, sw1);

		Assert.assertNotNull(sw);
		Assert.assertNotNull(sw1);
		Assert.assertFalse(sw.toString().equals(sw1.toString()));
		
		jrshe.setResource(struct);
		Assert.assertNotNull(jrshe.getRdfXml());
	}


	class MediaUnitM extends MediaUnit {
		private static final long serialVersionUID = 1L;
	}


	class SegmentM extends Segment {
		private static final long serialVersionUID = 1L;
	}


	class ResourceR extends Resource {
		private static final long serialVersionUID = 1L;
	}

}
