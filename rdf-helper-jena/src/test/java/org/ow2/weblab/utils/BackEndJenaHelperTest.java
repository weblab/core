/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.utils.BackEndJenaHelper.ResourceTypes;

/**
 */
public class BackEndJenaHelperTest {


	private static final String rdfXml = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" " + "       xmlns:vcard='http://www.w3.org/2001/vcard-rdf/3.0#'"
			+ "       xmlns:cat=\"http://nowhere/else#\"" + "	      xmlns:nsA=\"http://somewhere/else#\"" + "       xmlns:test=\"weblab://reifyingProperty#\">"
			+ "			<rdf:Description rdf:about=\"weblab://myStatementUri#1\">" + "				<rdf:subject rdf:resource=\"weblab://mySubjectUri#1\" />"
			+ "				<rdf:predicate rdf:resource=\"http://myPredicateUri.org/aa#a1\" />" + "				<rdf:object rdf:resource=\"weblab://myObjectUri#1\" />"
			+ "				<rdf:type rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement\" />" + "				<test:test rdf:resource=\"weblab://testValue1\" />"
			+ "				<test:test rdf:resource=\"weblab://testValue11\" />" + "				<test:test1>Another dirty object 1</test:test1>" + "			</rdf:Description>"
			+ "			<rdf:Description rdf:about=\"weblab://myStatementUri#2\">" + "				<rdf:subject rdf:resource=\"weblab://mySubjectUri#2\" />"
			+ "				<rdf:predicate rdf:resource=\"http://myPredicateUri.org/aa#a2\" />" + "				<rdf:object>My object number 2</rdf:object>"
			+ "				<rdf:type rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement\" />" + "			</rdf:Description>" + "			<rdf:Description rdf:nodeID=\"Node3\">"
			+ "				<rdf:subject rdf:resource=\"weblab://mySubjectUri#3\" />" + "				<rdf:predicate rdf:resource=\"http://myPredicateUri.org/aa#a3\" />"
			+ "				<rdf:object rdf:resource=\"weblab://myObjectUri#3\" />" + "				<rdf:type rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement\" />"
			+ "			</rdf:Description>" + "			<rdf:Description rdf:nodeID=\"Node4\">" + "				<rdf:subject rdf:resource=\"weblab://mySubjectUri#4\" />"
			+ "				<rdf:predicate rdf:resource=\"http://myPredicateUri.org/aa#a4\" />" + "				<rdf:object>My object number 4</rdf:object>"
			+ "				<rdf:type rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement\" />" + "				<test:test rdf:resource=\"weblab://testValue4\" />"
			+ "			</rdf:Description>" + "	         <rdf:Description rdf:about=\"http://somewhere/else#root\">" + "		         <nsA:P rdf:resource=\"http://somewhere/else#x\"/>"
			+ "		         <nsA:P rdf:resource=\"http://somewhere/else#y\"/>" + "		     </rdf:Description>" + "		     <rdf:Description rdf:about=\"http://somewhere/else#y\">"
			+ "		         <cat:Q rdf:resource=\"http://somewhere/else#z\"/>" + "		     </rdf:Description>" + "<rdf:Description rdf:nodeID=\"A0\">" + "    <vcard:Family>Smith</vcard:Family>"
			+ "    <vcard:Given>John</vcard:Given>" + "  </rdf:Description>" + "  <rdf:Description rdf:about='http://somewhere/JohnSmith/'>" + "    <vcard:FN>John Smith</vcard:FN>"
			+ "    <vcard:FN>Johnny Smith</vcard:FN>" + "    <vcard:N rdf:nodeID=\"A0\"/>" + "	</rdf:Description>" + "	<rdf:Description rdf:about='http://somewhere/SarahJones/'>"
			+ "	<vcard:FN>Sarah Jones</vcard:FN>" + "    <vcard:N rdf:nodeID=\"A1\"/>" + "  </rdf:Description>" + "  <rdf:Description rdf:about='http://somewhere/MattJones/'>"
			+ "    <vcard:FN>Matt Jones</vcard:FN>" + "    <vcard:N rdf:nodeID=\"A2\"/>" + "  </rdf:Description>" + "  <rdf:Description rdf:nodeID=\"A3\">" + "    <vcard:Family>Smith</vcard:Family>"
			+ "    <vcard:Given>Rebecca</vcard:Given>" + "  </rdf:Description>" + "  <rdf:Description rdf:nodeID=\"A1\">" + "    <vcard:Family>Jones</vcard:Family>"
			+ "    <vcard:Given>Sarah</vcard:Given>" + "  </rdf:Description>" + "  <rdf:Description rdf:nodeID=\"A2\">" + "    <vcard:Family>Jones</vcard:Family>"
			+ "    <vcard:Given>Matthew</vcard:Given>" + "  </rdf:Description>  <rdf:Description rdf:about='http://somewhere/RebeccaSmith/'>" + "    <vcard:FN>Becky Smith</vcard:FN>"
			+ "    <vcard:N rdf:nodeID=\"A3\"/>" + "  </rdf:Description>" + "	     </rdf:RDF>";


	private final String vcard = "http://www.w3.org/2001/vcard-rdf/3.0#";


	private final String subj = "http://somewhere/else#root";


	private final String predicateNSAP = "http://somewhere/else#P";


	private final String johnSmith = "http://somewhere/JohnSmith/";


	final String vcardFName = this.vcard + "FN";


	private final String vcardName = this.vcard + "N";


	private BackEndJenaHelper backeJHelper;


	private Model m;


	@Before
	public void setUp() {
		this.backeJHelper = new BackEndJenaHelper(BackEndJenaHelperTest.rdfXml);
		this.m = this.backeJHelper.getModel();
	}


	/**
	 *
	 */
	@Test
	public void testConstructor() {
		// default constructor
		BackEndJenaHelper bjh = new BackEndJenaHelper();
		Assert.assertNotNull(bjh);
		Assert.assertNotNull(bjh.getModel());

		// construct model from string
		bjh = null;
		bjh = new BackEndJenaHelper(BackEndJenaHelperTest.rdfXml);
		Model model = bjh.getModel();
		Assert.assertEquals(40, model.listStatements().toList().size());
		Assert.assertEquals(4, model.listReifiedStatements().toList().size());

		// construct from existing model
		model = ModelFactory.createDefaultModel();
		model.add(bjh.getModel());
		bjh = null;
		bjh = new BackEndJenaHelper(model);
		model = bjh.getModel();
		Assert.assertEquals(40, model.listStatements().toList().size());
		Assert.assertEquals(4, model.listReifiedStatements().toList().size());
	}


	/**
	 * test reified statement listing
	 */
	@Test
	public void testReifiedStatementListing() {
		Assert.assertEquals(this.m.listReifiedStatements().toList().size(),
				this.backeJHelper.listReifiedStatements(ResourceTypes.NAMED).size() + this.backeJHelper.listReifiedStatements(ResourceTypes.ANON).size());
	}


	/**
	 * test prefix map loading
	 */
	@Test
	public void testPrefixMapLoading() {
		Assert.assertEquals(this.m.getNsPrefixMap(), this.backeJHelper.getNSPrefixes());
	}


	/**
	 * test statement listing
	 */
	@Test
	public void testGetPredsOnSubj() {

		final StmtIterator it = this.m.listStatements(new SimpleSelector(null, null, (RDFNode) null) {


			@Override
			public boolean selects(final Statement s) {
				return ((s.getSubject() != null) && (s.getSubject().getURI() != null) && (s.getSubject().getURI().compareTo(BackEndJenaHelperTest.this.subj) == 0));
			}
		});
		Set<String> preds = this.backeJHelper.getPredsOnSubj(this.subj);

		while (it.hasNext()) {
			Assert.assertTrue(preds.contains(it.next().getPredicate().getURI()));
		}

		preds = this.backeJHelper.getPredsOnSubj(this.subj + "toto");
		while (it.hasNext()) {
			Assert.assertFalse(preds.contains(it.next().getPredicate().getURI()));
		}

		try {
			this.backeJHelper.getPredsOnSubj(null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testGetLitsOnPredSubj() {
		final List<String> values = this.backeJHelper.getLitsOnPredSubj(this.johnSmith, this.vcardFName);

		final StmtIterator it = this.m.listStatements(new SimpleSelector(null, null, (RDFNode) null) {


			@Override
			public boolean selects(final Statement s) {
				return ((s.getSubject() != null) && (s.getSubject().getURI() != null) && (s.getSubject().getURI().compareTo(BackEndJenaHelperTest.this.johnSmith) == 0))
						&& ((s.getPredicate() != null) && (s.getPredicate().getURI().compareTo(BackEndJenaHelperTest.this.vcardFName) == 0));
			}
		});

		Assert.assertEquals(it.toList().size(), values.size());

		while (it.hasNext()) {
			boolean ok = false;
			for (final String value : values) {
				if (value.compareTo(it.nextStatement().getString()) == 0) {
					ok = true;
				}
			}
			if (!ok) {
				Assert.fail();
			}
		}

		try {
			this.backeJHelper.getLitsOnPredSubj(null, this.vcardFName);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.getLitsOnPredSubj(this.johnSmith, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.getLitsOnPredSubj(null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testGetRessOnPredSubj() {
		final List<String> values = this.backeJHelper.getRessOnPredSubj(this.subj, this.predicateNSAP);

		final StmtIterator it = this.m.listStatements(new SimpleSelector(null, null, (RDFNode) null) {


			@Override
			public boolean selects(final Statement s) {
				return ((s.getSubject() != null) && (s.getSubject().getURI() != null) && (s.getSubject().getURI().compareTo(BackEndJenaHelperTest.this.subj) == 0))
						&& ((s.getPredicate() != null) && (s.getPredicate().getURI().compareTo(BackEndJenaHelperTest.this.predicateNSAP) == 0));
			}
		});

		Assert.assertEquals(it.toList().size(), values.size());

		while (it.hasNext()) {
			boolean ok = false;
			for (final String value : values) {
				if (value.compareTo(((Resource) it.nextStatement().getObject()).getURI()) == 0) {
					ok = true;
				}
			}
			if (!ok) {
				Assert.fail();
			}
		}

		try {
			this.backeJHelper.getRessOnPredSubj(null, this.predicateNSAP);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.getRessOnPredSubj(this.subj, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.getRessOnPredSubj(null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testHasLitStat() {
		Assert.assertTrue(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "John Smith"));
		Assert.assertTrue(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "Johnny Smith"));
		Assert.assertFalse(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "Johnny Hooker from the bayou"));
		try {
			this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasLitStat(this.johnSmith, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasLitStat(this.johnSmith, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasLitStat(null, this.vcardFName, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasLitStat(null, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasLitStat(null, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasLitStat(null, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testHasResStat() {
		Assert.assertTrue(this.backeJHelper.hasResStat(this.subj, this.predicateNSAP, "http://somewhere/else#x"));
		Assert.assertTrue(this.backeJHelper.hasResStat(this.subj, this.predicateNSAP, "http://somewhere/else#y"));
		Assert.assertFalse(this.backeJHelper.hasResStat(this.subj, this.predicateNSAP, "http://somewhere/else#z"));
		try {
			this.backeJHelper.hasResStat(this.johnSmith, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasResStat(this.johnSmith, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasResStat(this.johnSmith, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasResStat(null, this.vcardFName, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasResStat(null, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasResStat(null, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.hasResStat(null, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 */
	@Test
	public void testCreateLitStat() {
		this.backeJHelper.createLitStat(this.johnSmith, this.vcardFName, "JohnJohn Smith");
		Assert.assertTrue(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "JohnJohn Smith"));
		try {
			this.backeJHelper.createLitStat(this.johnSmith, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStat(this.johnSmith, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStat(this.johnSmith, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStat(null, this.vcardFName, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStat(null, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStat(null, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStat(null, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testCreateResStat() {
		this.backeJHelper.createResStat(this.subj, this.predicateNSAP, "http://somewhere/else#z");
		Assert.assertTrue(this.backeJHelper.hasResStat(this.subj, this.predicateNSAP, "http://somewhere/else#z"));
		try {
			this.backeJHelper.createResStat(this.johnSmith, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStat(this.johnSmith, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStat(this.johnSmith, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStat(null, this.vcardFName, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStat(null, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStat(null, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStat(null, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testRemoveLitStat() {
		this.backeJHelper.createLitStat(this.johnSmith, this.vcardFName, "JohnJohn Smith");
		Assert.assertTrue(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "JohnJohn Smith"));
		this.backeJHelper.removeLitStat(this.johnSmith, this.vcardFName, "JohnJohn Smith");
		Assert.assertFalse(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "JohnJohn Smith"));
		try {
			this.backeJHelper.removeLitStat(this.johnSmith, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeLitStat(this.johnSmith, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeLitStat(this.johnSmith, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeLitStat(null, this.vcardFName, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeLitStat(null, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeLitStat(null, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeLitStat(null, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testRemoveResStat() {
		this.backeJHelper.createResStat(this.subj, this.predicateNSAP, "http://somewhere/else#z");
		Assert.assertTrue(this.backeJHelper.hasResStat(this.subj, this.predicateNSAP, "http://somewhere/else#z"));
		this.backeJHelper.removeResStat(this.subj, this.predicateNSAP, "http://somewhere/else#z");
		Assert.assertFalse(this.backeJHelper.hasResStat(this.subj, this.predicateNSAP, "http://somewhere/else#z"));
		try {
			this.backeJHelper.removeResStat(this.johnSmith, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeResStat(this.johnSmith, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeResStat(this.johnSmith, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeResStat(null, this.vcardFName, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeResStat(null, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeResStat(null, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.removeResStat(null, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testRemoveStatsOnSubj() {
		this.backeJHelper.createLitStat(this.johnSmith, this.vcardFName, "JohnJohn Smith");
		Assert.assertTrue(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "JohnJohn Smith"));
		Assert.assertTrue(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "John Smith"));
		Assert.assertTrue(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "Johnny Smith"));
		this.backeJHelper.removeStatsOnSubj(this.johnSmith);
		Assert.assertFalse(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "JohnJohn Smith"));
		Assert.assertFalse(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "John Smith"));
		Assert.assertFalse(this.backeJHelper.hasLitStat(this.johnSmith, this.vcardFName, "Johnny Smith"));
		try {
			this.backeJHelper.removeStatsOnSubj(null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testRemoveStatsWithPred() {
		Assert.assertEquals(4, this.backeJHelper.getSubjsOnPred(this.vcardFName).size());
		this.backeJHelper.removeStatsWithPred(this.vcardFName);
		Assert.assertEquals(0, this.backeJHelper.getSubjsOnPred(this.vcardFName).size());
		try {
			this.backeJHelper.removeStatsWithPred(null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testRemoveStatsWithLit() {
		Assert.assertEquals(1, this.backeJHelper.getSubjsOnPredLit(this.vcardFName, "John Smith").size());
		this.backeJHelper.removeStatsWithLit("John Smith");
		Assert.assertEquals(0, this.backeJHelper.getSubjsOnPredLit(this.vcardFName, "John Smith").size());
		try {
			this.backeJHelper.removeStatsWithLit(null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testRemoveStatsWithRes() {
		Assert.assertEquals(1, this.backeJHelper.getSubjsOnPredRes(this.predicateNSAP, "http://somewhere/else#x").size());
		this.backeJHelper.removeStatsWithRes("http://somewhere/else#x");
		Assert.assertEquals(0, this.backeJHelper.getSubjsOnPredRes(this.predicateNSAP, "http://somewhere/else#x").size());
		try {
			this.backeJHelper.removeStatsWithRes(null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testGetSubjsOnPred() {
		Assert.assertEquals(1, this.backeJHelper.getSubjsOnPred(this.predicateNSAP).size());
		try {
			this.backeJHelper.getSubjsOnPred(null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testGetSubjsOnPredLit() {
		Assert.assertEquals(1, this.backeJHelper.getSubjsOnPredLit(this.vcardFName, "John Smith").size());
		try {
			this.backeJHelper.getSubjsOnPredLit(null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.getSubjsOnPredLit(this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.getSubjsOnPredLit(null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testGetSubjsOnPredRes() {
		Assert.assertEquals(1, this.backeJHelper.getSubjsOnPredRes(this.predicateNSAP, "http://somewhere/else#x").size());
		try {
			this.backeJHelper.getSubjsOnPredRes(null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.getSubjsOnPredRes(this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.getSubjsOnPredRes(null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testIsDefinedresource() {
		Assert.assertTrue(this.backeJHelper.isDefinedResource(this.subj));
		Assert.assertFalse(this.backeJHelper.isDefinedResource(this.subj + "22"));
		try {
			this.backeJHelper.isDefinedResource(null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testCreateAnonResStat() {
		this.backeJHelper.createAnonResStat(this.johnSmith, this.vcardName, "A1");
		Assert.assertTrue(this.backeJHelper.hasAnonResStat(this.johnSmith, this.vcardName, "A1"));
		try {
			this.backeJHelper.createAnonResStat(this.johnSmith, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStat(this.johnSmith, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStat(this.johnSmith, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStat(null, this.vcardFName, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStat(null, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStat(null, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStat(null, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testCreateAnonResStatOnAnonSubj() {
		this.backeJHelper.createAnonResStatOnAnonSubj("A1", this.vcardName, "A0");
		Assert.assertTrue(this.backeJHelper.hasAnonResStatOnAnonSubj("A1", this.vcardName, "A0"));
		try {
			this.backeJHelper.createAnonResStatOnAnonSubj(this.johnSmith, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStatOnAnonSubj(this.johnSmith, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStatOnAnonSubj(this.johnSmith, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStatOnAnonSubj(null, this.vcardFName, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStatOnAnonSubj(null, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStatOnAnonSubj(null, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createAnonResStatOnAnonSubj(null, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}



	/**
	 *
	 */
	@Test
	public void testCreateLitStatOnAnonSubj() {
		this.backeJHelper.createLitStatOnAnonSubj("A1", this.vcardFName, "aleluyaaaa");
		Assert.assertTrue(this.backeJHelper.hasLitStatOnAnonSubj("A1", this.vcardFName, "aleluyaaaa"));
		try {
			this.backeJHelper.createLitStatOnAnonSubj(this.johnSmith, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStatOnAnonSubj(this.johnSmith, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStatOnAnonSubj(this.johnSmith, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStatOnAnonSubj(null, this.vcardFName, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStatOnAnonSubj(null, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStatOnAnonSubj(null, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createLitStatOnAnonSubj(null, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 *
	 */
	@Test
	public void testCreateResStatOnAnonSubj() {
		this.backeJHelper.createResStatOnAnonSubj("A1", this.predicateNSAP, this.subj);
		Assert.assertTrue(this.backeJHelper.hasResStatOnAnonSubj("A1", this.predicateNSAP, this.subj));
		try {
			this.backeJHelper.createResStatOnAnonSubj(this.johnSmith, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStatOnAnonSubj(this.johnSmith, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStatOnAnonSubj(this.johnSmith, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStatOnAnonSubj(null, this.vcardFName, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStatOnAnonSubj(null, this.vcardFName, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStatOnAnonSubj(null, null, "John Smith");
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
		try {
			this.backeJHelper.createResStatOnAnonSubj(null, null, null);
			Assert.fail();
		} catch (final RDFHelperException e) {
			Assert.assertNotNull(e);
		}
	}


	/**
	 */
	@Test
	public void testGetRDFXMLString() {
		this.backeJHelper.createLitStat(this.johnSmith, this.vcardFName, "JohnJohn Smith");
		this.backeJHelper.createResStat("http://newDirty.org/uri#nb1", "http://newDirty.org/uri#nb2", "http://newDirty.org/uri#nb3");
		Assert.assertFalse(this.backeJHelper.getRdfXml().contains("\n\n"));
		Assert.assertFalse(this.backeJHelper.getRdfXml().contains("\r"));
	}


	@Test
	public void testRTest() {

		final ReflexiveGenerator<BackEndJenaHelper> testGenerator = new ReflexiveGenerator<>();

		final BackEndJenaHelper b = new BackEndJenaHelper();

		// result map for some methods
		// by default it is [ok, fail, fail, fail, ... ]
		final Map<String, boolean[]> customResult = new HashMap<>();
		customResult.put("getLitsOnPredSubj3", new boolean[] { true, false, false, true });
		customResult.put("createLitStat4", new boolean[] { true, false, false, false, true });
		customResult.put("getRdfXml0", new boolean[] { true });
		customResult.put("addToModel1", new boolean[] { false, false });
		customResult.put("removeAnonReifStat1", new boolean[] { true, true });
		customResult.put("removeReifStat1", new boolean[] { true, true });
		customResult.put("setNSPrefix2", new boolean[] { true, true, true });
		customResult.put("createTempLit2", new boolean[] { true, false, true });
		customResult.put("createAnonResStatReif6", new boolean[] { true, false, false, false, true, true, true });
		customResult.put("createAnonResStatReif7", new boolean[] { true, false, false, false, true, true, true, true });
		customResult.put("createAnonResStatReifOnAnonSubj6", new boolean[] { true, false, false, false, true, true, true });
		customResult.put("createAnonResStatReifOnAnonSubj7", new boolean[] { true, false, false, false, true, true, true, true });
		customResult.put("createLitStatReif6", new boolean[] { true, false, false, false, true, true, true });
		customResult.put("createLitStatReif7", new boolean[] { true, false, false, false, true, true, true, true });
		customResult.put("createLitStatReifOnAnonSubj6", new boolean[] { true, false, false, false, true, true, true });
		customResult.put("createLitStatReifOnAnonSubj7", new boolean[] { true, false, false, false, true, true, true, true });
		customResult.put("createResStatReif6", new boolean[] { true, false, false, false, true, true, true });
		customResult.put("createResStatReif7", new boolean[] { true, false, false, false, true, true, true, true });
		customResult.put("createResStatReifOnAnonSubj6", new boolean[] { true, false, false, false, true, true, true });
		customResult.put("createResStatReifOnAnonSubj7", new boolean[] { true, false, false, false, true, true, true, true });

		testGenerator.apply(b, customResult);

	}




}