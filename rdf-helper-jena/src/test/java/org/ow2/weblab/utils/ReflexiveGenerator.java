/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

/**
 * This class allow to automatically generate and check test on supported methods on any object.
 *
 * @author asaval
 * @param <T>
 *            The object on which to apply the reflexive method calls
 */
public class ReflexiveGenerator<T> {


	private String sdefault;


	private Map<?, ?> mapdefault;


	private int counter = 0;


	public ReflexiveGenerator() {
		this("test", new HashMap<>());
	}


	public ReflexiveGenerator(final String sdefault, final Map<?, ?> mapdefault) {
		this.sdefault = sdefault;
		this.mapdefault = mapdefault;
	}


	private List<Object[]> generateParameters(final Class<?>[] classes) {
		final int number = classes.length;
		final List<Object[]> objects = new LinkedList<>();
		objects.add(this.createParameters(classes, -1));
		int escape = 0;
		for (int i = 0; i < number; i++) {
			objects.add(this.createParameters(classes, escape));
			escape++;
		}
		return objects;
	}


	private static boolean[] createParams(final boolean value, final int number, final int escape) {
		final boolean[] o = new boolean[number];
		for (int j = 0; j < number; j++) {
			o[j] = escape == j ? false : value;
		}
		return o;
	}



	private void methodApply(final T object, final Method method, final boolean[] cs) {
		final Class<?>[] classes = method.getParameterTypes();
		final int number = classes.length;
		final List<Object[]> testlist = this.generateParameters(classes);
		// the first element is always ok, all other should fail.

		// check if we use custom results
		boolean[] shouldSucceed;
		if ((cs == null) || (cs.length != testlist.size())) {
			shouldSucceed = ReflexiveGenerator.createParams(false, number + 1, -1);
			shouldSucceed[0] = true;
		} else {
			shouldSucceed = cs;
		}

		int i = 0;
		for (final Object[] params : testlist) {
			try {
				method.invoke(object, params);
				if (!shouldSucceed[i]) {
					Assert.fail(this.print(params) + " should have failed " + method + " on test " + i + " shouldSucceed[" + shouldSucceed[i] + "]");
				}
			} catch (final IllegalArgumentException e) {
				Assert.assertNotNull(e);
			} catch (final IllegalAccessException e) {
				Assert.assertNotNull(e);
			} catch (final InvocationTargetException e) {
				if (shouldSucceed[i]) {
					e.printStackTrace();
					Assert.fail(this.print(params) + " should not have failed " + method + " on test " + i + " shouldSucceed[" + shouldSucceed[i] + "]");
				}
			}
			i++;
		}

	}


	public String print(final Object[] objects) {
		return "[" + StringUtils.join(objects, ",") + "]";
	}


	public void apply(final T object, final Map<String, boolean[]> customResult) {
		this.apply(object, customResult, false);
	}


	public void apply(final T object, final Map<String, boolean[]> customResult, final boolean debug) {

		final Method[] methods = object.getClass().getDeclaredMethods();

		for (final Method method : methods) {
			final Class<?>[] parameters = method.getParameterTypes();
			if (ReflexiveGenerator.isSupportedParameters(parameters)) {
				// test method
				if (debug) {
					System.out.println(method);
				}
				final String name = method.getName();
				if (!name.equals("close") && !name.equals("finalize") && !name.equals("getRdfXml")) {
					this.methodApply(object, method, customResult.get(method.getName() + parameters.length));
				}
			} else {
				if (debug) {
					System.out.println(method + " i s not reflexive supported, " + "add default type support to isSupportedParameters(Class<?>[] classes) and "
							+ "change createParameters(final Class<?>[] classes, String str, Map<?,?> map, final int escape)");
				}
			}
		}
	}


	private static boolean isSupportedParameters(final Class<?>[] classes) {
		for (final Class<?> c : classes) {
			if (!c.isInstance("") && !c.getCanonicalName().equals(Map.class.getCanonicalName())) {
				return false;
			}
		}
		return true;
	}


	private Object[] createParameters(final Class<?>[] classes, final int escape) {
		final String str = this.sdefault;
		final Map<?, ?> map = this.mapdefault;

		final Object[] res = new Object[classes.length];
		for (int i = 0; i < classes.length; i++) {
			if (i != escape) {
				if (classes[i].isInstance(str)) {
					res[i] = str + this.counter++;
				} else if (classes[i].isAssignableFrom(Map.class)) {
					res[i] = map;
				}
			}
		}
		return res;
	}
}
