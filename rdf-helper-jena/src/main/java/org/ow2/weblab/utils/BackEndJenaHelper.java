/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.LogFactory;

import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.RSIterator;
import org.apache.jena.rdf.model.ReifiedStatement;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;

/**
 * This class is used behind the various implementation provided in this package.
 * It eases the handling of RDF statements based on String only.
 *
 * @author WebLab IPCC Team
 */
public final class BackEndJenaHelper {


	public static enum RDFNodeTypes {
		ANON_R, LITERAL, NAMED_R
	}


	public static enum ResourceTypes {
		ANON, NAMED
	}


	private static final String BASE = null;


	private static final String ID_OBJ = " idObj:";


	private static final String ID_SUBJ = " idSubj:";


	private static final String LIT_OBJ = " litObj:";


	private static final String PARAMS_NULL = "Some parameters were null but shouldn't.";


	private static final String STAT_ID = " statId: ";


	private static final String STAT_URI = " statUri: ";


	private static final String SYNTAX = "RDF/XML";


	private static final String URI_OBJ = " uriObj:";


	private static final String URI_PRED = " uriPred:";


	private static final String URI_REIF = " uriReif:";


	private static final String URI_SUBJ = " uriSubj:";



	private Model model;


	private Model tempModel;


	/**
	 *
	 */
	public BackEndJenaHelper() {
		this.model = ModelFactory.createDefaultModel();
		this.tempModel = ModelFactory.createDefaultModel();
	}


	/**
	 * @param model
	 *            The model to add
	 */
	public BackEndJenaHelper(final Model model) {
		this.addToModel(model);
	}


	/**
	 * @param rdfXml
	 *            An RDF/XML String to be added to the model
	 */
	public BackEndJenaHelper(final String rdfXml) {
		this.addToModel(rdfXml);
	}


	/**
	 * Loads a file into a Jena Model.
	 *
	 * @param file
	 *            The file to load as a Jena Model.
	 * @param baseUri
	 *            the base URI to be used when converting relative URI's to absolute URI's. The base URI may be <code>null</code> if there are no relative URIs
	 *            to convert. A base URI of <code>""</code> may permit relative URIs to be used in the model. When handling many models and trying to add them
	 *            into a single model, it's recommended not to use <code>""</code> to prevent from relative URIs.
	 * @param lang
	 *            the language of the serialisation. <code>null</code> selects the default in the source of the input serialisation (RDF/XML). Predefined values
	 *            for <code>lang</code> are "RDF/XML", "N-TRIPLE", "TURTLE" (or "TTL") and "N3". <code>null</code> represents the default language, "RDF/XML".
	 *            "RDF/XML-ABBREV" is a synonym for "RDF/XML".
	 * @return The Jena model of the file.
	 * @throws IOException
	 *             If the file cannot be accessed
	 */
	public static Model modelFromFile(final File file, final String baseUri, final String lang) throws IOException {
		final Model aModel = ModelFactory.createDefaultModel();
		try (FileInputStream fis = FileUtils.openInputStream(file)) {
			aModel.read(fis, baseUri, lang);
		}
		return aModel;
	}


	/**
	 * To prevent from unchecked cast warning, method that convert from a non generic <code>Map</code> to a <code>String, String</code> one.
	 *
	 * @param prefixMap
	 *            The <code>Map</code> of prefixes/uris used in <tt>Jena</tt>.
	 * @return The same <code>Map</code> but parameterized.
	 */
	private static Map<String, String> convertNSMap(final Map<?, ?> prefixMap) {
		final Map<String, String> ret = new HashMap<>(prefixMap.size());
		for (final Entry<?, ?> entry : prefixMap.entrySet()) {
			if ((entry.getKey() instanceof String) && (entry.getValue() instanceof String)) {
				ret.put((String) entry.getKey(), (String) entry.getValue());
			}
		}
		return ret;
	}


	/**
	 * @param modelToAdd
	 *            The <tt>Jena</tt> <code>Model</code> to add to the <code>model</code>
	 */
	public void addToModel(final Model modelToAdd) {
		if (this.model == null) {
			this.model = ModelFactory.createDefaultModel();
		}
		if (this.tempModel == null) {
			this.tempModel = ModelFactory.createDefaultModel();
		}
		this.model.add(modelToAdd);
	}


	/**
	 * @param rdfXml
	 *            An RDF/XML String to be added to the model
	 * @throws RDFHelperException
	 *             If <code>model</code> is <code>null</code> or empty.
	 */
	public void addToModel(final String rdfXml) {
		if (this.model == null) {
			this.model = ModelFactory.createDefaultModel();
		}
		if (this.tempModel == null) {
			this.tempModel = ModelFactory.createDefaultModel();
		}
		if ((rdfXml == null) || rdfXml.isEmpty()) {
			throw new RDFHelperException("Args was null or empty.");
		}
		try (final StringReader reader = new StringReader(rdfXml)) {
			this.model.read(reader, BackEndJenaHelper.BASE, BackEndJenaHelper.SYNTAX);
		}
	}


	/**
	 * Closes the model and the temporary model. Recreates a temporary model just in case.
	 */
	public void close() {
		this.model.close();
		this.tempModel.close();
		this.tempModel = ModelFactory.createDefaultModel();
	}


	public void createAnonResStat(final String uriSubj, final String uriPred, final String idObj) {
		if ((uriSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subject = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		this.createStatement(subject, pred, obj);
	}


	public void createAnonResStatOnAnonSubj(final String idSubj, final String uriPred, final String idObj) {
		if ((idSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		this.createStatement(subj, pred, obj);
	}


	public String createAnonResStatReif(final String uriSubj, final String uriPred, final String idObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId) {
		return this.createAnonResStatReif(uriSubj, uriPred, idObj, mapPredLit, mapPredUri, mapPredId, null);
	}


	public String createAnonResStatReif(final String uriSubj, final String uriPred, final String idObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId, final String statUri) {
		if ((uriSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		final Map<Property, RDFNode> map = new HashMap<>();
		map.putAll(this.convertReifMap(mapPredLit, RDFNodeTypes.LITERAL));
		map.putAll(this.convertReifMap(mapPredUri, RDFNodeTypes.NAMED_R));
		map.putAll(this.convertReifMap(mapPredId, RDFNodeTypes.ANON_R));
		return this.createReifiedStatement(subj, pred, obj, map, statUri);
	}


	public String createAnonResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String idObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId) {
		return this.createAnonResStatReifOnAnonSubj(idSubj, uriPred, idObj, mapPredLit, mapPredUri, mapPredId, null);
	}


	public String createAnonResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String idObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId, final String statUri) {

		if ((idSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		final Map<Property, RDFNode> map = new HashMap<>();
		map.putAll(this.convertReifMap(mapPredLit, RDFNodeTypes.LITERAL));
		map.putAll(this.convertReifMap(mapPredUri, RDFNodeTypes.NAMED_R));
		map.putAll(this.convertReifMap(mapPredId, RDFNodeTypes.ANON_R));
		return this.createReifiedStatement(subj, pred, obj, map, statUri);
	}


	public void createLitStat(final String uriSubj, final String uriPred, final String litObj) {
		this.createLitStat(uriSubj, uriPred, litObj, null);
	}


	public void createLitStat(final String uriSubj, final String uriPred, final String litObj, final String language) {
		if ((uriSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Literal lit = this.createTempLit(litObj, language);
		this.createStatement(subj, pred, lit);
	}


	public void createLitStatOnAnonSubj(final String idSubj, final String uriPred, final String litObj) {
		if ((idSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Literal lit = this.createTempLit(litObj);
		this.createStatement(subj, pred, lit);
	}


	public String createLitStatReif(final String uriSubj, final String uriPred, final String litObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId) {
		return this.createLitStatReif(uriSubj, uriPred, litObj, mapPredLit, mapPredUri, mapPredId, null);
	}


	public String createLitStatReif(final String uriSubj, final String uriPred, final String litObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId, final String statUri) {
		if ((uriSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Literal lit = this.createTempLit(litObj);
		final Map<Property, RDFNode> map = new HashMap<>();
		map.putAll(this.convertReifMap(mapPredLit, RDFNodeTypes.LITERAL));
		map.putAll(this.convertReifMap(mapPredUri, RDFNodeTypes.NAMED_R));
		map.putAll(this.convertReifMap(mapPredId, RDFNodeTypes.ANON_R));
		return this.createReifiedStatement(subj, pred, lit, map, statUri);
	}


	public String createLitStatReifOnAnonSubj(final String idSubj, final String uriPred, final String litObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId) {
		return this.createLitStatReifOnAnonSubj(idSubj, uriPred, litObj, mapPredLit, mapPredUri, mapPredId, null);
	}


	public String createLitStatReifOnAnonSubj(final String idSubj, final String uriPred, final String litObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId, final String statUri) {
		if ((idSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Literal lit = this.createTempLit(litObj);
		final Map<Property, RDFNode> map = new HashMap<>();
		map.putAll(this.convertReifMap(mapPredLit, RDFNodeTypes.LITERAL));
		map.putAll(this.convertReifMap(mapPredUri, RDFNodeTypes.NAMED_R));
		map.putAll(this.convertReifMap(mapPredId, RDFNodeTypes.ANON_R));
		return this.createReifiedStatement(subj, pred, lit, map, statUri);
	}


	public void createResStat(final String uriSubj, final String uriPred, final String uriObj) {
		if ((uriSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		this.createStatement(subj, pred, obj);
	}


	public void createResStatOnAnonSubj(final String idSubj, final String uriPred, final String uriObj) {
		if ((idSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		this.createStatement(subj, pred, obj);
	}


	public String createResStatReif(final String uriSubj, final String uriPred, final String uriObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId) {
		return this.createResStatReif(uriSubj, uriPred, uriObj, mapPredLit, mapPredUri, mapPredId, null);
	}


	public String createResStatReif(final String uriSubj, final String uriPred, final String uriObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId, final String statUri) {
		if ((uriSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		final Map<Property, RDFNode> map = new HashMap<>();
		map.putAll(this.convertReifMap(mapPredLit, RDFNodeTypes.LITERAL));
		map.putAll(this.convertReifMap(mapPredUri, RDFNodeTypes.NAMED_R));
		map.putAll(this.convertReifMap(mapPredId, RDFNodeTypes.ANON_R));
		return this.createReifiedStatement(subj, pred, obj, map, statUri);
	}


	public String createResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String uriObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId) {
		return this.createResStatReifOnAnonSubj(idSubj, uriPred, uriObj, mapPredLit, mapPredUri, mapPredId, null);
	}


	public String createResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String uriObj, final Map<String, String> mapPredLit, final Map<String, String> mapPredUri,
			final Map<String, String> mapPredId, final String statUri) {
		if ((idSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		final Map<Property, RDFNode> map = new HashMap<>();
		map.putAll(this.convertReifMap(mapPredLit, RDFNodeTypes.LITERAL));
		map.putAll(this.convertReifMap(mapPredUri, RDFNodeTypes.NAMED_R));
		map.putAll(this.convertReifMap(mapPredId, RDFNodeTypes.ANON_R));
		return this.createReifiedStatement(subj, pred, obj, map, statUri);
	}


	public List<String> getAnonReifStats() {
		return this.listReifiedStatements(ResourceTypes.ANON);
	}


	public String getAnonResOfAnonReifStat(final String statId) {
		if (statId == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_ID + statId);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementObject(reifs, RDFNodeTypes.ANON_R);
		}
		return null;
	}


	public List<String> getAnonResOnPredAnonResStatReif(final String uriSubj, final String uriPred, final String idObj, final String uriReif) {
		if ((uriSubj == null) || (uriPred == null) || (idObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + idObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.ANON_R));
		}
		return objects;
	}


	public List<String> getAnonResOnPredAnonResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String idObj, final String uriReif) {
		if ((idSubj == null) || (uriPred == null) || (idObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.ANON_R));
		}
		return objects;
	}


	public List<String> getAnonResOnPredAnonStatReif(final String statId, final String uriPred) {
		if ((statId == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_ID + statId + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Property pred = this.createTempProp(uriPred);
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs == null) {
			return new ArrayList<>();
		}
		return this.getObjects(reifs, pred, RDFNodeTypes.ANON_R);
	}


	public List<String> getAnonResOnPredAnonSubj(final String idSubj, final String uriPred) {
		if ((idSubj == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		return this.getObjects(subj, pred, RDFNodeTypes.ANON_R);
	}


	public List<String> getAnonResOnPredLitStatReif(final String uriSubj, final String uriPred, final String litObj, final String uriReif) {
		if ((uriSubj == null) || (uriPred == null) || (litObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_OBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Literal obj = this.createTempLit(litObj);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.ANON_R));
		}
		return objects;
	}


	public List<String> getAnonResOnPredLitStatReifOnAnonSubj(final String idSubj, final String uriPred, final String litObj, final String uriReif) {
		if ((idSubj == null) || (uriPred == null) || (litObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Literal obj = this.createTempLit(litObj);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.ANON_R));
		}
		return objects;
	}


	public List<String> getAnonResOnPredResStatReif(final String uriSubj, final String uriPred, final String uriObj, final String uriReif) {
		if ((uriSubj == null) || (uriPred == null) || (uriObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.ANON_R));
		}
		return objects;
	}


	public List<String> getAnonResOnPredResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String uriObj, final String uriReif) {
		if ((idSubj == null) || (uriPred == null) || (uriObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.ANON_R));
		}
		return objects;
	}


	public List<String> getAnonResOnPredStatReif(final String statUri, final String uriPred) {
		if ((statUri == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_URI + statUri + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Property pred = this.createTempProp(uriPred);
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs == null) {
			return new ArrayList<>();
		}
		return this.getObjects(reifs, pred, RDFNodeTypes.ANON_R);
	}


	public List<String> getAnonResOnPredSubj(final String uriSubj, final String uriPred) {
		if ((uriSubj == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Resource subject = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		return this.getObjects(subject, pred, RDFNodeTypes.ANON_R);
	}


	public String getAnonResOnReifStat(final String statUri) {
		if (statUri == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_URI + statUri);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementObject(reifs, RDFNodeTypes.ANON_R);
		}
		return null;
	}


	public Set<String> getAnonSubj() {
		return this.getSubjects(ResourceTypes.ANON);
	}


	public String getAnonSubjOfAnonReifStat(final String statId) {
		if (statId == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_ID + statId);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementSubject(reifs, ResourceTypes.ANON);
		}
		return null;
	}


	public String getAnonSubjOfReifStat(final String statUri) {
		if (statUri == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_URI + statUri);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementSubject(reifs, ResourceTypes.ANON);
		}
		return null;
	}


	public Set<String> getAnonSubjOnPred(final String uriPred) {
		if (uriPred == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Property pred = this.createTempProp(uriPred);
		return this.getSubjects(pred, null, ResourceTypes.ANON);
	}


	public Set<String> getAnonSubjOnPredAnonRes(final String uriPred, final String idObj) {
		if ((uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		return this.getSubjects(pred, obj, ResourceTypes.ANON);
	}


	public Set<String> getAnonSubjOnPredLit(final String uriPred, final String litObj) {
		if ((uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Property pred = this.createTempProp(uriPred);
		final Literal obj = this.createTempLit(litObj);
		return this.getSubjects(pred, obj, ResourceTypes.ANON);
	}


	public Set<String> getAnonSubjOnPredRes(final String uriPred, final String uriObj) {
		if ((uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		return this.getSubjects(pred, obj, ResourceTypes.ANON);
	}


	public String getLitOfAnonReifStat(final String statId) {
		if (statId == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_ID + statId);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementObject(reifs, RDFNodeTypes.LITERAL);
		}
		return null;
	}


	public String getLitOfReifStat(final String statUri) {
		if (statUri == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_URI + statUri);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementObject(reifs, RDFNodeTypes.LITERAL);
		}
		return null;
	}


	public List<String> getLitOnPredAnonResStatReif(final String uriSubj, final String uriPred, final String idObj, final String uriReif) {
		if ((uriSubj == null) || (uriPred == null) || (idObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement rs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(rs, prop, RDFNodeTypes.LITERAL));
		}
		return objects;
	}


	public List<String> getLitOnPredAnonResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String idObj, final String uriReif) {
		if ((idSubj == null) || (uriPred == null) || (idObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement rs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(rs, prop, RDFNodeTypes.LITERAL));
		}
		return objects;
	}


	public List<String> getLitOnPredAnonStatReif(final String statId, final String uriPred) {
		if ((statId == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_ID + statId + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Property pred = this.createTempProp(uriPred);
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs == null) {
			return new ArrayList<>();
		}
		return this.getObjects(reifs, pred, RDFNodeTypes.LITERAL);
	}


	public List<String> getLitOnPredAnonSubj(final String idSubj, final String uriPred) {
		if ((idSubj == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		return this.getObjects(subj, pred, RDFNodeTypes.LITERAL);
	}


	public List<String> getLitOnPredLitStatReif(final String uriSubj, final String uriPred, final String litObj, final String uriReif) {
		if ((uriSubj == null) || (uriPred == null) || (litObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Literal obj = this.createTempLit(litObj);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.LITERAL));
		}
		return objects;
	}


	public List<String> getLitOnPredLitStatReifOnAnonSubj(final String idSubj, final String uriPred, final String litObj, final String uriReif) {
		if ((idSubj == null) || (uriPred == null) || (litObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Literal obj = this.createTempLit(litObj);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.LITERAL));
		}
		return objects;
	}


	public List<String> getLitOnPredResStatReif(final String uriSubj, final String uriPred, final String uriObj, final String uriReif) {
		if ((uriSubj == null) || (uriPred == null) || (uriObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.LITERAL));
		}
		return objects;
	}


	public List<String> getLitsOnPredResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String uriObj, final String uriReif) {
		if ((idSubj == null) || (uriPred == null) || (uriObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.LITERAL));
		}
		return objects;
	}


	public List<String> getLitsOnPredStatReif(final String statUri, final String uriPred) {
		if ((statUri == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_URI + statUri + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Property pred = this.createTempProp(uriPred);
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs == null) {
			return new ArrayList<>();
		}
		return this.getObjects(reifs, pred, RDFNodeTypes.LITERAL);
	}


	public List<String> getLitsOnPredSubj(final String uriSubj, final String uriPred) {
		return this.getLitsOnPredSubj(uriSubj, uriPred, null);
	}


	public List<String> getLitsOnPredSubj(final String uriSubj, final String uriPred, final String language) {
		if ((uriSubj == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		return this.getObjects(subj, pred, RDFNodeTypes.LITERAL, language);
	}


	/**
	 * @return the model
	 */
	public Model getModel() {
		return this.model;
	}


	/**
	 * @return The full map of prefixes and uris to be used by the model.
	 */
	public Map<String, String> getNSPrefixes() {
		if (this.model != null) {
			return BackEndJenaHelper.convertNSMap(this.model.getNsPrefixMap());
		}
		return new HashMap<>(0);
	}


	public String getPredOfAnonReifStat(final String statId) {
		if (statId == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_ID + statId);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementPredicate(reifs);
		}
		return null;
	}


	public String getPredOfReifStat(final String statUri) {
		if (statUri == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_URI + statUri);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementPredicate(reifs);
		}
		return null;
	}


	public Set<String> getPredOnAnonResStatReif(final String uriSubj, final String uriPred, final String idObj) {
		if ((uriSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		final Set<String> preds = new HashSet<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			preds.addAll(this.getPredicates(reifs));
		}
		return preds;
	}


	public Set<String> getPredOnAnonResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String idObj) {
		if ((idSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		final Set<String> preds = new HashSet<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			preds.addAll(this.getPredicates(reifs));
		}
		return preds;
	}


	public Set<String> getPredOnAnonStatReif(final String statId) {
		if (statId == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_ID + statId);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs == null) {
			return new HashSet<>();
		}
		return this.getPredicates(reifs);
	}


	public Set<String> getPredOnAnonSubj(final String idSubj) {
		if (idSubj == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		return this.getPredicates(subj);
	}


	public Set<String> getPredOnLitStatReif(final String uriSubj, final String uriPred, final String litObj) {
		if ((uriSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Literal obj = this.createTempLit(litObj);
		final Set<String> preds = new HashSet<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			preds.addAll(this.getPredicates(reifs));
		}
		return preds;
	}


	public Set<String> getPredOnLitStatReifOnAnonSubj(final String idSubj, final String uriPred, final String litObj) {
		if ((idSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Literal obj = this.createTempLit(litObj);
		final Set<String> preds = new HashSet<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			preds.addAll(this.getPredicates(reifs));
		}
		return preds;
	}


	public Set<String> getPredOnResStatReif(final String uriSubj, final String uriPred, final String uriObj) {
		if ((uriSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		final Set<String> preds = new HashSet<>();
		for (final ReifiedStatement rs : this.getReifStats(subj, pred, obj)) {
			preds.addAll(this.getPredicates(rs));
		}
		return preds;
	}


	public Set<String> getPredOnResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String uriObj) {
		if ((idSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		final Set<String> preds = new HashSet<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			preds.addAll(this.getPredicates(reifs));
		}
		return preds;
	}


	public Set<String> getPredOnStatReif(final String statUri) {
		if (statUri == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_URI + statUri);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs == null) {
			return new HashSet<>();
		}
		return this.getPredicates(reifs);
	}


	/**
	 * @return A <code>Set</code> containing URIs of properties that are predicate of any statement in the <code>Annotation</code>.
	 */
	public Set<String> getPreds() {
		final Set<String> preds = new HashSet<>();
		for (final Statement stat : this.listStatements()) {
			preds.add(stat.getPredicate().getURI());
		}
		return preds;
	}


	public Set<String> getPredsOnSubj(final String uriSubj) {
		if (uriSubj == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		return this.getPredicates(subj);
	}


	/**
	 * @return The RDF/XML String view of the model.
	 */
	public String getRdfXml() {
		final List<String> prefixToRemove = new LinkedList<>();
		for (final Object prefix : this.model.getNsPrefixMap().keySet()) {
			if (prefix.toString().startsWith("j.")) {
				prefixToRemove.add(prefix.toString());
			}
		}
		for (final String prefix : prefixToRemove) {
			this.model.removeNsPrefix(prefix);
		}

		final StringWriter writer = new StringWriter();
		try {
			this.model.write(writer, BackEndJenaHelper.SYNTAX);
		} catch (final Exception exception) {
			throw new RDFHelperException("Serialisation of Jena model failed. " + BackEndJenaHelper.SYNTAX + " string appears to be invalid: " + writer.toString(), exception);
		}
		return writer.toString().replace("\r", "");
	}


	public List<String> getReifStats() {
		return this.listReifiedStatements(ResourceTypes.NAMED);
	}


	public String getResOfAnonReifStat(final String statId) {
		if (statId == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_ID + statId);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementObject(reifs, RDFNodeTypes.NAMED_R);
		}
		return null;
	}


	public String getResOfReifStat(final String statUri) {
		if (statUri == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_URI + statUri);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementObject(reifs, RDFNodeTypes.NAMED_R);
		}
		return null;
	}


	public List<String> getResOnPredAnonResStatReif(final String uriSubj, final String uriPred, final String idObj, final String uriReif) {
		if ((uriSubj == null) || (uriPred == null) || (idObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.NAMED_R));
		}
		return objects;
	}


	public List<String> getResOnPredAnonResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String idObj, final String uriReif) {
		if ((idSubj == null) || (uriPred == null) || (idObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.NAMED_R));
		}
		return objects;
	}


	public List<String> getResOnPredAnonStatReif(final String statId, final String uriPred) {
		if ((statId == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_ID + statId + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Property pred = this.createTempProp(uriPred);
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs == null) {
			return new ArrayList<>();
		}
		return this.getObjects(reifs, pred, RDFNodeTypes.NAMED_R);
	}


	public List<String> getResOnPredAnonSubj(final String idSubj, final String uriPred) {
		if ((idSubj == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		return this.getObjects(subj, pred, RDFNodeTypes.NAMED_R);
	}


	public List<String> getResOnPredLitStatReif(final String uriSubj, final String uriPred, final String litObj, final String uriReif) {
		if ((uriSubj == null) || (uriPred == null) || (litObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Literal obj = this.createTempLit(litObj);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.NAMED_R));
		}
		return objects;
	}


	public List<String> getResOnPredLitStatReifOnAnonSubj(final String idSubj, final String uriPred, final String litObj, final String uriReif) {
		if ((idSubj == null) || (uriPred == null) || (litObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Literal obj = this.createTempLit(litObj);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.NAMED_R));
		}
		return objects;
	}


	public List<String> getResOnPredResStatReif(final String uriSubj, final String uriPred, final String uriObj, final String uriReif) {
		if ((uriSubj == null) || (uriPred == null) || (uriObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.NAMED_R));
		}
		return objects;
	}


	public List<String> getResOnPredResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String uriObj, final String uriReif) {
		if ((idSubj == null) || (uriPred == null) || (uriObj == null) || (uriReif == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj
					+ BackEndJenaHelper.URI_REIF + uriReif);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Property prop = this.createTempProp(uriReif);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		final List<String> objects = new ArrayList<>();
		for (final ReifiedStatement reifs : this.getReifStats(subj, pred, obj)) {
			objects.addAll(this.getObjects(reifs, prop, RDFNodeTypes.NAMED_R));
		}
		return objects;
	}


	public List<String> getResOnPredStatReif(final String statUri, final String uriPred) {
		if ((statUri == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_URI + statUri + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Property pred = this.createTempProp(uriPred);
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs == null) {
			return new ArrayList<>();
		}
		return this.getObjects(reifs, pred, RDFNodeTypes.NAMED_R);
	}


	public List<String> getRessOnPredSubj(final String uriSubj, final String uriPred) {
		if ((uriSubj == null) || (uriPred == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		return this.getObjects(subj, pred, RDFNodeTypes.NAMED_R);
	}


	public String getSubjOfAnonReifStat(final String statId) {
		if (statId == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_ID + statId);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementSubject(reifs, ResourceTypes.NAMED);
		}
		return null;
	}


	public String getSubjOfReifStat(final String statUri) {
		if (statUri == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.STAT_URI + statUri);
		}
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs != null) {
			return BackEndJenaHelper.getReifiedStatementSubject(reifs, ResourceTypes.NAMED);
		}
		return null;
	}


	public Set<String> getSubjOnPredAnonRes(final String uriPred, final String idObj) {
		if ((uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + " uriPred:" + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		return this.getSubjects(pred, obj, ResourceTypes.NAMED);
	}


	public Set<String> getSubjs() {
		return this.getSubjects(ResourceTypes.NAMED);
	}


	public Set<String> getSubjsOnPred(final String uriPred) {
		if (uriPred == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Property pred = this.createTempProp(uriPred);
		return this.getSubjects(pred, null, ResourceTypes.NAMED);
	}


	public Set<String> getSubjsOnPredLit(final String uriPred, final String litObj) {
		if ((uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Property pred = this.createTempProp(uriPred);
		final Literal obj = this.createTempLit(litObj);
		return this.getSubjects(pred, obj, ResourceTypes.NAMED);
	}


	public Set<String> getSubjsOnPredRes(final String uriPred, final String uriObj) {
		if ((uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		return this.getSubjects(pred, obj, ResourceTypes.NAMED);
	}


	public boolean hasAnonResStat(final String uriSubj, final String uriPred, final String idObj) {
		if ((uriSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subject = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		return this.containsStatement(subject, pred, obj);
	}


	public boolean hasAnonResStatOnAnonSubj(final String idSubj, final String uriPred, final String idObj) {
		if ((idSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		return this.containsStatement(subj, pred, obj);
	}


	public boolean hasLitStat(final String uriSubj, final String uriPred, final String litObj) {
		if ((uriSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Literal lit = this.createTempLit(litObj);
		return this.containsStatement(subj, pred, lit);
	}


	public boolean hasLitStatOnAnonSubj(final String idSubj, final String uriPred, final String litObj) {
		if ((idSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Literal lit = this.createTempLit(litObj);
		return this.containsStatement(subj, pred, lit);
	}


	public boolean hasResStat(final String uriSubj, final String uriPred, final String uriObj) {
		if ((uriSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		return this.containsStatement(subj, pred, obj);
	}


	public boolean hasResStatOnAnonSubj(final String idSubj, final String uriPred, final String uriObj) {
		if ((idSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		return this.containsStatement(subj, pred, obj);
	}


	public boolean isAnonResStatReif(final String uriSubj, final String uriPred, final String idObj) {
		if ((uriSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		return this.isReified(subj, pred, obj);
	}


	public boolean isAnonResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String idObj) {
		if ((idSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		return this.isReified(subj, pred, obj);
	}


	public boolean isDefinedAnonRes(final String identifier) {
		final Resource res = this.createTempRes(identifier, ResourceTypes.ANON);
		return this.containsResource(res);
	}


	public boolean isDefinedResource(final String uri) {
		if (uri == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + " uri: " + uri);
		}
		final Resource res = this.createTempRes(uri, ResourceTypes.NAMED);
		return this.containsResource(res);
	}


	public boolean isLitStatReif(final String uriSubj, final String uriPred, final String litObj) {
		if ((uriSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Literal obj = this.createTempLit(litObj);
		return this.isReified(subj, pred, obj);
	}


	public boolean isLitStatReifOnAnonSubj(final String idSubj, final String uriPred, final String litObj) {
		if ((idSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Literal obj = this.createTempLit(litObj);
		return this.isReified(subj, pred, obj);
	}


	public boolean isResStatReif(final String uriSubj, final String uriPred, final String uriObj) {
		if ((uriSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		return this.isReified(subj, pred, obj);
	}


	public boolean isResStatReifOnAnonSubj(final String idSubj, final String uriPred, final String uriObj) {
		if ((idSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		return this.isReified(subj, pred, obj);
	}


	/**
	 * @param type
	 *            Whether statements to retrieve are anonymous or named.
	 * @return A list of internal ID or URI (depending on <code>type</code>) representing every reified statements in the model
	 */
	public List<String> listReifiedStatements(final ResourceTypes type) {
		final List<String> idsOrUris = new ArrayList<>();
		if (type.equals(ResourceTypes.ANON)) {
			for (final ReifiedStatement reifs : this.listReifStats(type)) {
				idsOrUris.add(reifs.getId().getLabelString());
			}
		} else if (type.equals(ResourceTypes.NAMED)) {
			for (final ReifiedStatement reifs : this.listReifStats(type)) {
				idsOrUris.add(reifs.getURI());
			}
		} else {
			LogFactory.getLog(this.getClass()).warn("Error in listReifiedStatement ResourceType was neither an anon nor a named resource...");
		}
		return idsOrUris;
	}


	public void removeAnonReifStat(final String statId) {
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statId, ResourceTypes.ANON);
		if (reifs != null) {
			this.removeReification(reifs);
		}
	}


	public void removeAnonResStat(final String uriSubj, final String uriPred, final String idObj) {
		if ((uriSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		this.removeStatement(subj, pred, obj);
	}


	public void removeAnonResStatOnAnonSubj(final String idSubj, final String uriPred, final String idObj) {
		if ((idSubj == null) || (uriPred == null) || (idObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		this.removeStatement(subj, pred, obj);
	}


	public void removeLitStat(final String uriSubj, final String uriPred, final String litObj) {
		if ((uriSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Literal lit = this.createTempLit(litObj);
		this.removeStatement(subj, pred, lit);
	}


	public void removeLitStatOnAnonSubj(final String idSubj, final String uriPred, final String litObj) {
		if ((idSubj == null) || (uriPred == null) || (litObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Literal obj = this.createTempLit(litObj);
		this.removeStatement(subj, pred, obj);
	}


	public void removeReifStat(final String statUri) {
		final ReifiedStatement reifs = this.getSpecifiedReifStat(statUri, ResourceTypes.NAMED);
		if (reifs != null) {
			this.removeReification(reifs);
		}
	}


	public void removeResStat(final String uriSubj, final String uriPred, final String uriObj) {
		if ((uriSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		this.removeStatement(subj, pred, obj);
	}


	public void removeResStatOnAnonSubj(final String idSubj, final String uriPred, final String uriObj) {
		if ((idSubj == null) || (uriPred == null) || (uriObj == null)) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj + BackEndJenaHelper.URI_PRED + uriPred + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		final Property pred = this.createTempProp(uriPred);
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		this.removeStatement(subj, pred, obj);
	}


	public void removeStatsOnAnonSubj(final String idSubj) {
		if (idSubj == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_SUBJ + idSubj);
		}
		final Resource subj = this.createTempRes(idSubj, ResourceTypes.ANON);
		this.removeStatement(subj, null, null);
	}


	public void removeStatsOnSubj(final String uriSubj) {
		if (uriSubj == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_SUBJ + uriSubj);
		}
		final Resource subj = this.createTempRes(uriSubj, ResourceTypes.NAMED);
		this.removeStatement(subj, null, null);
	}


	public void removeStatsWithLit(final String litObj) {
		if (litObj == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.LIT_OBJ + litObj);
		}
		final Literal lit = this.createTempLit(litObj);
		this.removeStatement(null, null, lit);
	}


	public void removeStatsWithPred(final String uriPred) {
		if (uriPred == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_PRED + uriPred);
		}
		final Property pred = this.createTempProp(uriPred);
		this.removeStatement(null, pred, null);
	}


	public void removeStatsWithRes(final String uriObj) {
		if (uriObj == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.URI_OBJ + uriObj);
		}
		final Resource obj = this.createTempRes(uriObj, ResourceTypes.NAMED);
		this.removeStatement(null, null, obj);
	}


	public void removeStatWithAnonRes(final String idObj) {
		if (idObj == null) {
			throw new RDFHelperException(BackEndJenaHelper.PARAMS_NULL + BackEndJenaHelper.ID_OBJ + idObj);
		}
		final Resource obj = this.createTempRes(idObj, ResourceTypes.ANON);
		this.removeStatement(null, null, obj);
	}


	/**
	 * @param prefix
	 *            A prefix to be used in RDF/XML.
	 * @param uri
	 *            The uri associated with this prefix.
	 * @return The full map of prefixes after the addition of <code>prefix</code> and <code>uri</code>.
	 */
	public Map<String, String> setNSPrefix(final String prefix, final String uri) {
		if ((this.model != null) && (prefix != null) && (uri != null) && (!prefix.isEmpty()) && (!uri.isEmpty())) {
			final Map<?, ?> map = this.model.setNsPrefix(prefix, uri).getNsPrefixMap();
			return BackEndJenaHelper.convertNSMap(map);
		}
		return this.getNSPrefixes();

	}


	/**
	 * @param node
	 *            The <code>RDFNode</code> to test presence in <code>model</code>. It should have been created into <code>tempMode</code>.
	 * @return Whether or not <code>node</code> is defined in <code>model</code> .
	 */
	protected boolean containsResource(final RDFNode node) {
		return this.model.containsResource(node);
	}


	/**
	 * Test whether any statement matching <tt>(S,P,O)</tt> is present in the model. <code>null</code> values are working as wildcard.
	 *
	 * @param subj
	 *            The subject of the statement to create; it might be either or named or not.
	 * @param pred
	 *            The predicate of the statement to create.
	 * @param obj
	 *            The object of the statement to create. It might either be a literal or a resource.
	 * @return true if the statement exist as a normal or reified one
	 */
	protected boolean containsStatement(final Resource subj, final Property pred, final RDFNode obj) {
		if (!this.model.contains(subj, pred, obj)) {
			if ((subj != null) && (pred != null) && (obj != null)) {
				final Statement statement = this.buildTempStatement(subj, pred, obj);
				return this.model.isReified(statement);
			}
			return false;
		}
		return true;
	}


	/**
	 * @param reifs
	 *            A map of predicate URIs and objects (literal values, internal ID or URI of resources).
	 * @param objType
	 *            The type of the second string in the map (literal values, internal ID or URI of resources).
	 * @return The map to be used for reification.
	 */
	protected Map<Property, RDFNode> convertReifMap(final Map<String, String> reifs, final RDFNodeTypes objType) {
		if (reifs == null) {
			return new HashMap<>();
		}
		final Map<Property, RDFNode> result = new HashMap<>(reifs.size());
		if (objType.equals(RDFNodeTypes.LITERAL)) {
			for (final Entry<String, String> entry : reifs.entrySet()) {
				result.put(this.createTempProp(entry.getKey()), this.createTempLit(entry.getValue()));
			}
		} else if (objType.equals(RDFNodeTypes.NAMED_R)) {
			for (final Entry<String, String> entry : reifs.entrySet()) {
				result.put(this.createTempProp(entry.getKey()), this.createTempRes(entry.getValue(), ResourceTypes.NAMED));
			}
		} else if (objType.equals(RDFNodeTypes.ANON_R)) {
			for (final Entry<String, String> entry : reifs.entrySet()) {
				result.put(this.createTempProp(entry.getKey()), this.createTempRes(entry.getValue(), ResourceTypes.ANON));
			}
		}
		return result;
	}


	/**
	 * @param subj
	 *            The subject of the statement to be reified.
	 * @param pred
	 *            The predicate of the statement to be reified.
	 * @param obj
	 *            The object of the statement to be reified.
	 * @param reifs
	 *            Properties and values to be affected to the reified statement.
	 * @param uri
	 *            The uri (might be null) of the statement to create.
	 * @return the id of the uri of the created reified statement
	 */
	protected String createReifiedStatement(final Resource subj, final Property pred, final RDFNode obj, final Map<Property, RDFNode> reifs, final String uri) {
		final ReifiedStatement rst;
		if (uri == null) {
			rst = this.model.createReifiedStatement(this.createStatement(subj, pred, obj, false));
		} else {
			rst = this.model.createReifiedStatement(uri, this.createStatement(subj, pred, obj, false));
		}

		for (final Entry<Property, RDFNode> entry : reifs.entrySet()) {
			rst.addProperty(entry.getKey(), entry.getValue());
		}
		if (uri == null) {
			return rst.getId().getLabelString();
		}
		return rst.getURI();
	}


	/**
	 * @param subj
	 *            The subject of the statement to create; it might be either or named or not.
	 * @param pred
	 *            The predicate of the statement to create.
	 * @param obj
	 *            The object of the statement to create. It might either be a literal or a resource.
	 * @return thje created statement
	 */
	protected Statement createStatement(final Resource subj, final Property pred, final RDFNode obj) {
		if (!this.containsStatement(subj, pred, obj)) {
			this.model.add(this.model.createStatement(subj, pred, obj));
		}
		return this.model.createStatement(subj, pred, obj);
	}


	/**
	 * @param subj
	 *            The subject of the statement to create; it might be either or named or not.
	 * @param pred
	 *            The predicate of the statement to create.
	 * @param obj
	 *            The object of the statement to create. It might either be a literal or a resource.
	 * @param add
	 *            Whether of not to add the created statement into the model.
	 * @return the created statement
	 */
	protected Statement createStatement(final Resource subj, final Property pred, final RDFNode obj, final boolean add) {
		if (!this.containsStatement(subj, pred, obj) && add) {
			this.model.add(this.model.createStatement(subj, pred, obj));
		}
		return this.model.createStatement(subj, pred, obj);
	}


	/**
	 * @param value
	 *            The value of the literal to be created in <code>tempModel</code>.
	 * @return A Jena temporary <code>Literal</code>
	 */
	protected Literal createTempLit(final String value) {
		return this.createTempLit(value, null);
	}


	/**
	 * @param value
	 *            The value of the literal to be created in <code>tempModel</code>.
	 * @param language
	 *            The language of the typed value
	 * @return A Jena temporary <code>Literal</code>
	 */
	protected Literal createTempLit(final String value, final String language) {
		return this.tempModel.createLiteral(value, language);
	}


	/**
	 * @param uri
	 *            The uri of the property to create
	 * @return A Jena temporary <code>Property</code>
	 */
	protected Property createTempProp(final String uri) {
		return this.tempModel.createProperty(uri);
	}


	/**
	 * @param uriOrId
	 *            URI or internal ID of the resource to be created in <code>tempModel</code>.
	 * @param resType
	 *            The type of the resource to be created, i.e. named or anonymous one. In case of named, <code>uriOrId</code> will be used as an URI, in case of
	 *            anonymous, <code>uriOrId</code> will be used as the internal ID for anonymous resources.
	 * @return A Jena temporary <code>Resource</code>
	 */
	protected Resource createTempRes(final String uriOrId, final ResourceTypes resType) {
		if (resType.equals(ResourceTypes.NAMED)) {
			return this.tempModel.createResource(uriOrId);
		} else if (resType.equals(ResourceTypes.ANON)) {
			final AnonId anon = AnonId.create(uriOrId);
			return this.tempModel.createResource(anon);
		}
		throw new RDFHelperException("Unexpected error. Using a type that is not defined: " + resType.name() + "-" + resType.ordinal());
	}


	@Override
	protected void finalize() throws Throwable {
		this.tempModel.close();
		// this.model.close();
		super.finalize();
	}


	/**
	 * @param subj
	 *            The subject. It can be either an anonymous resource, named resource or <code>null</code>. In case of <code>null</code>, only properties will
	 *            be used to search for objects.
	 * @param pred
	 *            The predicate.
	 * @param nodeType
	 *            The type of the objects to retrieve.
	 * @return A <code>List</code> of URIs, internal IDs or literal values that are object of statements having <code>pred</code> as predicate and <code>subj</code> as subject.
	 */
	protected List<String> getObjects(final Resource subj, final Property pred, final RDFNodeTypes nodeType) {
		return this.getObjects(subj, pred, nodeType, null);
	}


	/**
	 * @param language
	 *            The lang code to use for restriction (might be null)
	 * @param subj
	 *            The subject. It can be either an anonymous resource, named resource or <code>null</code>. In case of <code>null</code>, only properties will
	 *            be used to search for objects.
	 * @param pred
	 *            The predicate.
	 * @param nodeType
	 *            The type of the objects to retrieve.
	 * @return A <code>List</code> of URIs, internal IDs or literal values that are object of statements having <code>pred</code> as predicate and <code>subj</code> as subject.
	 */
	protected List<String> getObjects(final Resource subj, final Property pred, final RDFNodeTypes nodeType, final String language) {
		final List<Statement> stats = this.listStatements();
		if (subj != null) {
			final ListIterator<Statement> iterator = stats.listIterator();
			while (iterator.hasNext()) {
				final Statement stat = iterator.next();
				if (!stat.getSubject().equals(subj)) {
					iterator.remove();
				}
			}
		}
		final List<String> objs = new ArrayList<>();
		for (final Statement stat : stats) {
			if (pred.equals(stat.getPredicate())) {
				final RDFNode obj = stat.getObject();
				if (nodeType.equals(RDFNodeTypes.ANON_R) && obj.isAnon()) {
					objs.add(((Resource) obj).getId().getLabelString());
				} else if (nodeType.equals(RDFNodeTypes.LITERAL) && obj.isLiteral()) {
					if ((language != null) && ((Literal) obj).getLanguage().equals(language)) {
						objs.add(((Literal) obj).getLexicalForm());
					} else if (language == null) {
						objs.add(((Literal) obj).getLexicalForm());
					}
				} else if (nodeType.equals(RDFNodeTypes.NAMED_R) && obj.isURIResource()) {
					objs.add(((Resource) obj).getURI());
				}
			}
		}
		return objs;
	}


	/**
	 * @param subj
	 *            The subject. It can be either an anonymous resource or named resource.
	 * @return A <code>Set</code> URIs of the predicates contained by the model an refining <code>subj</code> if not <code>null</code>.
	 */
	protected Set<String> getPredicates(final Resource subj) {
		final List<Statement> stats = this.listStatements();
		final Set<String> preds = new HashSet<>();
		for (final Statement stat : stats) {
			if (subj.equals(stat.getSubject())) {
				preds.add(stat.getPredicate().getURI());
			}
		}
		return preds;
	}


	/**
	 * @param rs
	 *            The reified statement to look for object.
	 * @param type
	 *            The type of the object.
	 * @return The object (literal value, id or uri) or null if an error occurs
	 */
	protected static String getReifiedStatementObject(final ReifiedStatement rs, final RDFNodeTypes type) {
		final String toRet;
		final RDFNode obj = rs.getStatement().getObject();
		if (type.equals(RDFNodeTypes.ANON_R) && obj.isAnon()) {
			toRet = ((Resource) obj).getId().getLabelString();
		} else if (type.equals(RDFNodeTypes.LITERAL) && obj.isLiteral()) {
			toRet = ((Literal) obj).getLexicalForm();
		} else if (type.equals(RDFNodeTypes.NAMED_R) && obj.isURIResource()) {
			toRet = ((Resource) obj).getURI();
		} else {
			toRet = null;
		}
		return toRet;
	}


	/**
	 * @param reifs
	 *            The reified statement to look for predicate.
	 * @return The predicate uri or null if an error occurs
	 */
	protected static String getReifiedStatementPredicate(final ReifiedStatement reifs) {
		return reifs.getStatement().getPredicate().getURI();
	}


	/**
	 * @param rs
	 *            The reified statement to look for subject.
	 * @param type
	 *            The type of the subject.
	 * @return The subject (uri or id) or null if an error occurs
	 */
	protected static String getReifiedStatementSubject(final ReifiedStatement rs, final ResourceTypes type) {
		final String toRet;
		final Resource subj = rs.getStatement().getSubject();
		if (type.equals(ResourceTypes.ANON) && subj.isAnon()) {
			toRet = subj.getId().getLabelString();
		} else if (type.equals(ResourceTypes.NAMED) && subj.isURIResource()) {
			toRet = subj.getURI();
		} else {
			toRet = null;
		}
		return toRet;
	}


	/**
	 * @param subj
	 *            The subject of the statement that might be reified.
	 * @param pred
	 *            The predicate of the statement that might be reified.
	 * @param obj
	 *            The object of the statement that might be reified.
	 * @return A list that might be empty of reified statement representing the statement (<code>subj</code>, <code>pred</code>, <code>obj</code> ).
	 */
	protected List<ReifiedStatement> getReifStats(final Resource subj, final Property pred, final RDFNode obj) {
		final List<ReifiedStatement> reifs = new ArrayList<>();
		if (this.containsStatement(subj, pred, obj)) {
			final Statement statement = this.createStatement(subj, pred, obj, false);
			if (statement.isReified()) {
				final RSIterator rsit = statement.listReifiedStatements();
				while (rsit.hasNext()) {
					reifs.add(rsit.nextRS());
				}
				rsit.close();
			}
		}
		return reifs;
	}


	/**
	 * @param idOrUri
	 *            The internal ID or uri of the reified statement to retrieve
	 * @param type
	 *            The type of reified statements to retrieve, i.e. anonymous or named.
	 * @return The statement or <code>null</code>.
	 */
	protected ReifiedStatement getSpecifiedReifStat(final String idOrUri, final ResourceTypes type) {
		ReifiedStatement toRet = null;
		if (type.equals(ResourceTypes.ANON)) {
			for (final ReifiedStatement reifs : this.listReifStats(type)) {
				if (reifs.getId().getLabelString().equals(idOrUri)) {
					toRet = reifs;
					break;
				}
			}
		} else {
			for (final ReifiedStatement reifs : this.listReifStats(type)) {
				if (reifs.getURI().equals(idOrUri)) {
					toRet = reifs;
					break;
				}
			}
		}
		return toRet;
	}


	/**
	 * @param pred
	 *            The predicate.
	 * @param obj
	 *            The object. If <code>obj</code> is <code>null</code> only the predicate is used to search for subjects.
	 * @param resType
	 *            The type of the resources to retrieve, i.e. named or anonymous one.
	 * @return A <code>Set</code> of URIs or internal IDs resources that are subjects of statements having <code>pred</code> as predicate and <code>obj</code> as object.
	 */
	protected Set<String> getSubjects(final Property pred, final RDFNode obj, final ResourceTypes resType) {
		final List<Statement> stats = this.listStatements();
		if (obj != null) {
			final ListIterator<Statement> iterator = stats.listIterator();
			while (iterator.hasNext()) {
				final Statement stat = iterator.next();
				if (!stat.getObject().equals(obj)) {
					iterator.remove();
				}
			}
		}
		final Set<String> subjs = new HashSet<>();
		for (final Statement stat : stats) {
			if (pred.equals(stat.getPredicate())) {
				final Resource subj = stat.getSubject();
				if (resType.equals(ResourceTypes.NAMED) && subj.isURIResource()) {
					subjs.add(subj.getURI());
				} else if (resType.equals(ResourceTypes.ANON) && subj.isAnon()) {
					subjs.add(subj.getId().getLabelString());
				}
			}
		}
		return subjs;
	}


	/**
	 * @param resType
	 *            The type of the resources to retrieve, i.e. named or anonymous one.
	 * @return A <code>Set</code> of URIs or internal IDs resources that are subjects of statements in this model.
	 */
	protected Set<String> getSubjects(final ResourceTypes resType) {
		final List<Statement> stats = this.listStatements();
		final Set<String> subjs = new HashSet<>();
		for (final Statement stat : stats) {
			final Resource subj = stat.getSubject();
			if (resType.equals(ResourceTypes.NAMED) && subj.isURIResource()) {
				subjs.add(subj.getURI());
			} else if (resType.equals(ResourceTypes.ANON) && subj.isAnon()) {
				subjs.add(subj.getId().getLabelString());
			}
		}
		return subjs;
	}


	/**
	 * @param subj
	 *            The subject of the statement to test reification.
	 * @param pred
	 *            The predicate of the statement to test reification.
	 * @param obj
	 *            The object of the statement to test reification.
	 * @return A temporary statement
	 */
	protected boolean isReified(final Resource subj, final Property pred, final RDFNode obj) {
		final Statement statement = this.buildTempStatement(subj, pred, obj);
		return this.model.isReified(statement);
	}


	/**
	 * @return A list containing every statements including the one that are subject of reifications but not the reification implementation ones (subject,
	 *         object...).
	 */
	protected List<Statement> listStatements() {
		final List<Statement> stats = new ArrayList<>();
		final StmtIterator sit = this.model.listStatements();
		while (sit.hasNext()) {
			final Statement stat = sit.nextStatement();
			// Filtering reified statement

			if (!stat.getPredicate().equals(RDF.subject) && !stat.getPredicate().equals(RDF.predicate) && !stat.getPredicate().equals(RDF.object)) {
				if (stat.getPredicate().equals(RDF.type) && stat.getObject().isResource()) {
					if (!((Resource) stat.getObject()).equals(RDF.Statement)) {
						stats.add(stat);
					}
				} else {
					stats.add(stat);
				}
			}
		}
		sit.close();
		final RSIterator rsit = this.model.listReifiedStatements();
		while (rsit.hasNext()) {
			stats.add(rsit.nextRS().getStatement());
		}
		rsit.close();
		return stats;
	}


	/**
	 * @param reifs
	 *            The reified statement to look remove.
	 */
	protected void removeReification(final ReifiedStatement reifs) {
		this.removeStatement(reifs, null, null);
		this.model.removeReification(reifs);
	}


	/**
	 * Remove any statement matching <tt>(S,P,O)</tt> from the model. <code>null</code> values are working as wildcard.
	 *
	 * @param subj
	 *            The subject of the statement to create; it might be either or named or not.
	 * @param pred
	 *            The predicate of the statement to create.
	 * @param obj
	 *            The object of the statement to create. It might either be a literal or a resource.
	 */
	protected void removeStatement(final Resource subj, final Property pred, final RDFNode obj) {
		if ((subj == null) || (pred == null) || (obj == null)) {
			final StmtIterator sit = this.model.listStatements(subj, pred, obj);
			while (sit.hasNext()) {
				final Statement statement = sit.nextStatement();
				this.model.removeAllReifications(statement);
			}
			sit.close();
			this.model.removeAll(subj, pred, obj);
		} else {
			final Statement statement = this.buildTempStatement(subj, pred, obj);
			this.model.removeAllReifications(statement);
			this.model.remove(statement);
		}
		for (final Statement statement : this.listStatements()) {
			if (((subj == null) || statement.getSubject().equals(subj)) && ((pred == null) || statement.getPredicate().equals(pred)) && ((obj == null) || statement.getObject().equals(obj))) {
				if (statement.isReified()) {
					final RSIterator rsit = this.model.listReifiedStatements(statement);
					while (rsit.hasNext()) {
						final ReifiedStatement reifs = rsit.nextRS();
						this.model.removeAll(reifs, null, null);
						this.model.removeAll(null, null, reifs);
					}
					rsit.close();
				}
				this.model.removeAllReifications(statement);
				this.model.remove(statement);
			}
		}
	}


	/**
	 * @param subj
	 *            The subject of the statement to be build.
	 * @param pred
	 *            The predicate of the statement to be build.
	 * @param obj
	 *            The object of the statement to be build.
	 * @return A temporary statement
	 */
	private Statement buildTempStatement(final Resource subj, final Property pred, final RDFNode obj) {
		return this.tempModel.createStatement(subj, pred, obj);
	}


	/**
	 * @param type
	 *            The type of reified statements to retrieve, i.e. anonymous or named ones.
	 * @return Every reified statements of the model, having a type as <code>type</code>.
	 */
	private List<ReifiedStatement> listReifStats(final ResourceTypes type) {
		final RSIterator rsi = this.model.listReifiedStatements();
		final List<ReifiedStatement> rss = new ArrayList<>();
		if (type.equals(ResourceTypes.ANON)) {
			while (rsi.hasNext()) {
				final ReifiedStatement reifs = rsi.nextRS();
				if (reifs.isAnon()) {
					rss.add(reifs);
				}
			}
		} else if (type.equals(ResourceTypes.NAMED)) {
			while (rsi.hasNext()) {
				final ReifiedStatement reifs = rsi.nextRS();
				if (reifs.isURIResource()) {
					rss.add(reifs);
				}
			}
		} else {
			LogFactory.getLog(this.getClass()).warn("Error in listReifiedStatement ResourceType was neither an anon nor a named resource...");
		}
		rsi.close();
		return rss;
	}

}
