/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helper.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.ResourceHelper;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.utils.BackEndJenaHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is an implementation of the <code>ResourceHelper</code>. It loads every <code>Annotations</code> that are on this object and in the inner <code>Resources</code>. If the loaded
 * <code>Resource</code> is an <code>Annotation</code>, its content won't be loaded.
 *
 * More documentation is available on {@link ResourceHelper}.
 *
 * @see ResourceHelper
 * @author EADS WebLab Team
 */
public class JenaResourceHelper implements ResourceHelper {


	/**
	 * The current <code>Resource</code> object loaded.
	 */
	protected Resource resource;


	/**
	 * Map to remember the mapping between sub-resources and resource URIs
	 */
	protected Map<String, Resource> resourcesMap;


	/**
	 * An handler for the Jena model behind.
	 */
	protected BackEndJenaHelper bejh;


	protected final Logger logger = LoggerFactory.getLogger(this.getClass());


	/**
	 * @param res
	 *            The Resource to load inside this helper.
	 */
	public JenaResourceHelper(final Resource res) {
		this.setResource(res);
	}


	@Override
	public Resource getResource(final String uri) throws WebLabCheckedException {
		if (!this.isFullyDefinedResource(uri)) {
			throw new WebLabCheckedException("Cannot retrieve type for resource '" + uri + "'. Resource is not defined.");
		}
		return this.resourcesMap.get(uri);
	}


	@Override
	public <T extends Resource> T getSpecificResource(final String uri, final Class<T> specificClass) throws WebLabCheckedException {
		final Resource res = this.getResource(uri);
		if (specificClass.isInstance(res)) {
			return specificClass.cast(res);
		}
		throw new WebLabCheckedException("Cannot retrieve Resource '" + uri + "'",
				new ClassCastException("Unable to cast resource: " + res.getUri() + " into a " + specificClass.getName() + " since its a " + res.getClass().getName()));
	}


	@Override
	public Class<? extends Resource> getType(final String uri) throws WebLabCheckedException {
		final Resource res = this.getResource(uri);
		return res.getClass();
	}


	@Override
	public void setResource(final Resource resource) {
		if (this.bejh != null) {
			this.bejh.close();
		}
		this.bejh = new BackEndJenaHelper();
		this.resourcesMap = new HashMap<>();
		this.addResource(resource);
		this.resource = resource;
	}


	/**
	 * Method recursively called to add every resources in the model and in the map.
	 *
	 * @param res
	 *            The resource to add.
	 */
	protected void addResource(final Resource res) {
		this.resourcesMap.put(res.getUri(), res);

		for (final Resource subRes : ResourceUtil.getSubResources(res)) {
			this.resourcesMap.put(subRes.getUri(), subRes);
			if (subRes instanceof PieceOfKnowledge) {
				this.addPieceOfKnowledge((PieceOfKnowledge) subRes);
			}
		}
	}


	/**
	 * @param pok
	 *            pok
	 *            The pok to add.
	 */
	protected void addPieceOfKnowledge(final PieceOfKnowledge pok) {
		final String rdf = PoKUtil.getPoKData(pok);
		if (!rdf.trim().equals("")) {
			this.loadFromRdfXml(rdf, true);
		}
	}


	@Override
	public List<String> getLitsOnPredSubj(final String uriSubj, final String uriPred) {
		return this.bejh.getLitsOnPredSubj(uriSubj, uriPred);
	}


	@Override
	public Set<String> getPredsOnSubj(final String uriSubj) {
		return this.bejh.getPredsOnSubj(uriSubj);
	}


	@Override
	public List<String> getRessOnPredSubj(final String uriSubj, final String uriPred) {
		return this.bejh.getRessOnPredSubj(uriSubj, uriPred);
	}


	@Override
	public Set<String> getSubjs() {
		return this.bejh.getSubjs();
	}


	@Override
	public Set<String> getSubjsOnPred(final String uriPred) {
		return this.bejh.getSubjsOnPred(uriPred);
	}


	@Override
	public Set<String> getSubjsOnPredLit(final String uriPred, final String litObj) {
		return this.bejh.getSubjsOnPredLit(uriPred, litObj);
	}


	@Override
	public Set<String> getSubjsOnPredRes(final String uriPred, final String uriObj) {
		return this.bejh.getSubjsOnPredRes(uriPred, uriObj);
	}


	@Override
	public boolean hasLitStat(final String uriSubj, final String uriPred, final String litObj) {
		return this.bejh.hasLitStat(uriSubj, uriPred, litObj);
	}


	@Override
	public boolean hasResStat(final String uriSubj, final String uriPred, final String uriObj) {
		return this.bejh.hasResStat(uriSubj, uriPred, uriObj);
	}


	@Override
	public boolean isDefinedResource(final String uri) {
		return this.bejh.isDefinedResource(uri);
	}


	@Override
	public void loadFromRdfXml(final String rdfXml) {
		this.loadFromRdfXml(rdfXml, false);
	}


	/**
	 * @param rdfXml
	 *            An RDF/XML String to be loaded or added to teh current model
	 * @param add
	 *            Whether to add <code>rdfXml</code> to current model or to create a new model from scratch.
	 */
	protected void loadFromRdfXml(final String rdfXml, final boolean add) {
		if (add && (this.bejh != null)) {
			this.bejh.addToModel(rdfXml);
		} else {
			this.bejh = new BackEndJenaHelper(rdfXml);
		}
	}


	@Override
	public Resource getResource() {
		return this.resource;
	}


	@Override
	public boolean isFullyDefinedResource(final String uri) {
		return this.isDefinedResource(uri) && this.resourcesMap.containsKey(uri);
	}


	@Override
	public Set<String> getPreds() {
		return this.bejh.getPreds();
	}


	@Override
	public String getRdfXml() {
		if ((this.bejh != null) && (this.resource != null)) {
			return this.bejh.getRdfXml();
		}
		return "";
	}


	@Override
	public List<String> getLitsOnPredSubj(final String uriSubj, final String uriPred, final String language) {
		return this.bejh.getLitsOnPredSubj(uriSubj, uriPred, language);
	}


	/**
	 * Return the Jena model.
	 *
	 * No changes in this model will be reflected in the inner model.
	 * You should consider using it for reading purpose only.
	 *
	 * @return The Jena model that is used inside.
	 */
	public Model getInnerJenaModel() {
		return this.bejh.getModel();
	}


}