/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helper;

import java.util.Map;

/**
 * <p>
 * This class is an extract from former <code>AnnotationHelper</code>, in order to be separated from methods of <code>ResourceHelper</code>.
 * </p>
 * More comments are done in {@link PoKHelper}.
 * 
 * @author EADS WebLab Team
 * @see PoKHelper
 */
interface RDFWriter {


	/**
	 * Creates a statement having <code>uriSubj</code> as subject, <code>uriPred</code> as predicate and <code>litObj</code> as literal value of the object.<br />
	 * 
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @param litObj
	 *            The literal value of the object.
	 * @param language
	 *            The language of the literal value. See http://www.w3.org/TR/REC-xml/#sec-lang-tag for more information.
	 */
	public void createLitStat(final String uriSubj, final String uriPred, final String litObj, final String language);


	/**
	 * Creates a statement having <code>uriSubj</code> as subject, <code>uriPred</code> as predicate and <code>litObj</code> as literal value of the object.<br />
	 * 
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @param litObj
	 *            The literal value of the object.
	 */
	public void createLitStat(final String uriSubj, final String uriPred, final String litObj);


	/**
	 * Creates a statement having <code>uriSubj</code> as subject, <code>uriPred</code> as predicate and <code>uriObj</code> as URI of the object.<br />
	 * 
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @param uriObj
	 *            The URI of the object resource.
	 */
	public void createResStat(final String uriSubj, final String uriPred, final String uriObj);


	/**
	 * Removes every statements having <code>uriSubj</code> as subject, <code>uriPred</code> as predicate and <code>litObj</code> as literal value of the
	 * object.<br />
	 * 
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @param litObj
	 *            The literal value of the object.
	 */
	public void removeLitStat(final String uriSubj, final String uriPred, final String litObj);


	/**
	 * Removes every statements having <code>uriSubj</code> as subject, <code>uriPred</code> as predicate and <code>uriObj</code> as URI of the object.<br />
	 * 
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @param uriObj
	 *            The URI of the object resource.
	 */
	public void removeResStat(final String uriSubj, final String uriPred, final String uriObj);


	/**
	 * Removes every statements having <code>uriSubj</code> as subject whatever are predicate and object.<br />
	 * 
	 * @param uriSubj
	 *            The URI of the subject resource.
	 */
	public void removeStatsOnSubj(final String uriSubj);


	/**
	 * Removes every statements having <code>litObj</code> as object literal value whatever are subject and predicate.<br />
	 * 
	 * @param litObj
	 *            The literal value of the object.
	 */
	public void removeStatsWithLit(final String litObj);


	/**
	 * Removes every statements having <code>uriPred</code> as predicate whatever are subject and object.<br />
	 * 
	 * @param uriPred
	 *            The URI of the predicate resource.
	 */
	public void removeStatsWithPred(final String uriPred);


	/**
	 * Removes every statements having <code>uriObj</code> as object named resource URI value whatever are subject and predicate.<br />
	 * 
	 * @param uriObj
	 *            The URI of the object resource. The URI of the object resource.
	 */
	public void removeStatsWithRes(final String uriObj);


	/**
	 * Sets a prefix to be used to represent the given <code>URI</code> in the RDF/XML view of the <code>PieceOfKnowledge</code> data.<br />
	 * The prefixes are used in other RDF representations such as Turtle. For instance, <code>skos</code> may represent <code>http://www.w3.org/2004/02/skos/core#</code>. If <code>prefix</code> or
	 * <code>uri</code> are <code>null</code> nothing is done. If <code>prefix</code> is <code>&quot;&quot;</code>, <code>uri</code> will be the default namespace. If <code>prefix</code> is
	 * <code>&quot;&quot;</code> while <code>uri</code> is not, nothing is done.
	 * 
	 * @param prefix
	 *            The <code>String</code> to be used as namespace prefix in the RDF/XML of the <code>PieceOfKnowledge</code>.
	 * @param uri
	 *            The <code>String</code> to be represented by <code>prefix</code>
	 * @return The current the map of prefixes/uris used.
	 */
	public Map<String, String> setNSPrefix(final String prefix, final String uri);


	/**
	 * Retrieve the current map of prefixes and uris used in the <code>PieceOfKnowledge</code> contained by this <code>PoKHelperExtended</code>.
	 * 
	 * @return The current the map of prefixes/uris used.
	 */
	public Map<String, String> getNSPrefixes();

}
