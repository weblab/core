/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helper.impl;

import org.apache.jena.rdf.model.ResourceFactory;
import org.ow2.weblab.core.extended.ontologies.RDF;
import org.ow2.weblab.core.extended.ontologies.WebLabModel;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.LowLevelDescriptor;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.model.SpatialSegment;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.TrackSegment;
import org.ow2.weblab.core.model.Video;


/**
 * This is an implementation of the <code>JenaResourceHelper</code>.
 * It loads every <code>Annotations</code> that are on this object and in the inner <code>Resources</code>. It adds the structure statements in the generated inner model.
 * If the loaded <code>Resource</code> is an <code>Annotation</code>, its content won't be loaded.
 *
 * @author EADS WebLab Team
 */
public class JenaResourceStructureHelper extends JenaResourceHelper {


	/**
	 * Constructor
	 *
	 * @param res
	 *            The first resource to be represented as an Structure helper(RDF view of the resource with structure statements)
	 */
	public JenaResourceStructureHelper(final Resource res) {
		super(res);
	}


	/**
	 * @param query
	 *            The query to add.
	 */
	protected void addQuery(final Query query) {
		final org.apache.jena.rdf.model.Resource queryRes = ResourceFactory.createResource(query.getUri());
		if (query instanceof StringQuery) {
			this.bejh.getModel().add(queryRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(StringQuery.class)));
			if (((StringQuery) query).getRequest() != null) {
				this.bejh.getModel().add(queryRes, ResourceFactory.createProperty(WebLabModel.HAS_REQUEST), ((StringQuery) query).getRequest());
			}
			this.bejh.getModel().addLiteral(queryRes, ResourceFactory.createProperty(WebLabModel.HAS_WEIGHT), ((StringQuery) query).getWeight());
		} else if (query instanceof SimilarityQuery) {
			this.addSimilarityQuery((SimilarityQuery) query);
		} else if (query instanceof ComposedQuery) {
			this.addComposedQuery((ComposedQuery) query);
		} else {
			this.logger.warn("Unknown type of Query: " + query.getClass().getCanonicalName());
			this.bejh.getModel().add(queryRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(Query.class)));
		}
	}


	protected void addSimilarityQuery(final SimilarityQuery similarityQuery) {
		final org.apache.jena.rdf.model.Resource sqRes = ResourceFactory.createResource(similarityQuery.getUri());
		this.bejh.getModel().add(sqRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(SimilarityQuery.class)));
		for (final Resource theResource : similarityQuery.getResource()) {
			this.bejh.getModel().add(sqRes, ResourceFactory.createProperty(WebLabModel.IS_COMPOSED_BY_RESOURCE), ResourceFactory.createResource(theResource.getUri()));
			this.bejh.getModel().add(ResourceFactory.createResource(theResource.getUri()), ResourceFactory.createProperty(WebLabModel.IN_COMPOSED_RESOURCE), sqRes);
			this.addResource(theResource);
		}
	}


	protected void addComposedQuery(final ComposedQuery ccomposedQuery) {
		final org.apache.jena.rdf.model.Resource cqRes = ResourceFactory.createResource(ccomposedQuery.getUri());
		this.bejh.getModel().add(cqRes, ResourceFactory.createProperty(WebLabModel.HAS_OPERATOR), ccomposedQuery.getOperator().value());
		this.bejh.getModel().add(cqRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(ComposedQuery.class)));
		for (final Query query : ccomposedQuery.getQuery()) {
			final org.apache.jena.rdf.model.Resource qRes = ResourceFactory.createResource(query.getUri());
			this.bejh.getModel().add(cqRes, ResourceFactory.createProperty(WebLabModel.IS_COMPOSED_BY_QUERY), qRes);
			this.bejh.getModel().add(qRes, ResourceFactory.createProperty(WebLabModel.IN_COMPOSED_QUERY), cqRes);
			this.addResource(query);
		}
	}


	protected void addResultSet(final ResultSet resultSet) {
		final org.apache.jena.rdf.model.Resource rsRes = ResourceFactory.createResource(resultSet.getUri());
		this.bejh.getModel().add(rsRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(ResultSet.class)));
		for (final Resource r : resultSet.getResource()) {
			final org.apache.jena.rdf.model.Resource rRes = ResourceFactory.createResource(r.getUri());
			this.bejh.getModel().add(rsRes, ResourceFactory.createProperty(WebLabModel.IS_COMPOSED_BY_RESOURCE_RESULT_SET), rRes);
			this.bejh.getModel().add(rRes, ResourceFactory.createProperty(WebLabModel.IN_RESULT_SET), rsRes);
			this.addResource(r);
		}
		final PieceOfKnowledge pok = resultSet.getPok();
		if (pok != null) {
			this.bejh.createResStat(resultSet.getUri(), WebLabModel.HAS_POK, pok.getUri());
			this.addResource(pok);
		}
	}


	protected void addComposedResource(final ComposedResource composedQuery) {
		final org.apache.jena.rdf.model.Resource crRes = ResourceFactory.createResource(composedQuery.getUri());
		for (final Resource theResource : composedQuery.getResource()) {
			final org.apache.jena.rdf.model.Resource rRes = ResourceFactory.createResource(theResource.getUri());
			this.bejh.getModel().add(crRes, ResourceFactory.createProperty(WebLabModel.IS_COMPOSED_BY_RESOURCE), rRes);
			this.bejh.getModel().add(rRes, ResourceFactory.createProperty(WebLabModel.IN_COMPOSED_RESOURCE), crRes);
			this.addResource(theResource);
		}
		this.bejh.getModel().add(crRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(ComposedResource.class)));
	}


	@Override
	protected void addPieceOfKnowledge(final PieceOfKnowledge pok) {
		final String rdf = PoKUtil.getPoKData(pok);
		if (!rdf.trim().equals("")) {
			this.loadFromRdfXml(rdf, true);
		}
		this.bejh.createResStat(pok.getUri(), RDF.TYPE, WebLabModel.getUriFromResource(Annotation.class));
	}


	/**
	 * @param seg
	 *            The segment to add.
	 */
	protected void addSegment(final Segment seg) {
		final org.apache.jena.rdf.model.Resource sRes = ResourceFactory.createResource(seg.getUri());
		if (seg instanceof LinearSegment) {
			this.bejh.getModel().add(sRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromSegment(LinearSegment.class)));
			this.bejh.getModel().addLiteral(sRes, ResourceFactory.createProperty(WebLabModel.START_AT), ((LinearSegment) seg).getStart());
			this.bejh.getModel().addLiteral(sRes, ResourceFactory.createProperty(WebLabModel.END_AT), ((LinearSegment) seg).getEnd());
		} else if (seg instanceof TemporalSegment) {
			this.bejh.getModel().add(sRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromSegment(TemporalSegment.class)));
		} else if (seg instanceof SpatialSegment) {
			this.bejh.getModel().add(sRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromSegment(SpatialSegment.class)));
		} else if (seg instanceof TrackSegment) {
			this.bejh.getModel().add(sRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromSegment(TrackSegment.class)));
		} else {
			this.logger.warn("Unknown type of Segment: " + seg.getClass().getCanonicalName());
			this.bejh.getModel().add(sRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromSegment(Segment.class)));
		}
	}


	/**
	 * @param mediaUnit
	 *            The mediaUnit to add.
	 */
	protected void addMediaUnit(final MediaUnit mediaUnit) {
		final org.apache.jena.rdf.model.Resource muRes = ResourceFactory.createResource(mediaUnit.getUri());
		for (final Segment seg : (mediaUnit).getSegment()) {
			this.bejh.getModel().add(muRes, ResourceFactory.createProperty(WebLabModel.HAS_SEGMENT), ResourceFactory.createResource(seg.getUri()));
			this.addSegment(seg);
		}

		if (mediaUnit instanceof Document) {
			this.addDocument((Document) mediaUnit);
		} else if (mediaUnit instanceof Text) {
			this.bejh.getModel().add(muRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(Text.class)));
			if (((Text) mediaUnit).getContent() != null) {
				this.bejh.getModel().add(muRes, ResourceFactory.createProperty(WebLabModel.HAS_CONTENT), ((Text) mediaUnit).getContent());
			}
		} else if (mediaUnit instanceof Image) {
			this.bejh.getModel().add(muRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(Image.class)));
		} else if (mediaUnit instanceof Audio) {
			this.bejh.getModel().add(muRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(Audio.class)));
		} else if (mediaUnit instanceof Video) {
			this.bejh.getModel().add(muRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(Video.class)));
		} else {
			this.logger.warn("Unknown type of MediaUnit: " + mediaUnit.getClass().getCanonicalName());
			this.bejh.getModel().add(muRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(MediaUnit.class)));
		}
	}


	/**
	 * @param document
	 *            The document to be added to the back end model
	 */
	protected void addDocument(final Document document) {
		final org.apache.jena.rdf.model.Resource dRes = ResourceFactory.createResource(document.getUri());
		for (final MediaUnit mediaUnit : document.getMediaUnit()) {
			this.bejh.getModel().add(dRes, ResourceFactory.createProperty(WebLabModel.IS_COMPOSED_BY_MEDIA_UNIT), ResourceFactory.createResource(mediaUnit.getUri()));
			this.bejh.getModel().add(ResourceFactory.createResource(mediaUnit.getUri()), ResourceFactory.createProperty(WebLabModel.IN_DOCUMENT), dRes);
			this.addResource(mediaUnit);
		}
		this.bejh.getModel().add(dRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(Document.class)));
	}


	/**
	 * Method recursively called to add every resources in the model and in the map.
	 *
	 * @param res
	 *            The resource to add.
	 */
	@Override
	protected void addResource(final Resource res) {
		this.resourcesMap.put(res.getUri(), res);
		org.apache.jena.rdf.model.Resource rRes = ResourceFactory.createResource(res.getUri());
		for (final Annotation annot : res.getAnnotation()) {
			this.bejh.getModel().add(rRes, ResourceFactory.createProperty(WebLabModel.HAS_ANNOTATION), ResourceFactory.createResource(annot.getUri()));
			this.addResource(annot);
		}
		for (final LowLevelDescriptor lld : res.getDescriptor()) {
			this.bejh.getModel().add(rRes, ResourceFactory.createProperty(WebLabModel.HAS_LOW_LEVEL_DESCRIPTOR), ResourceFactory.createResource(lld.getUri()));
			this.addResource(lld);
		}

		if (res instanceof MediaUnit) {
			this.addMediaUnit((MediaUnit) res);
		} else if (res instanceof PieceOfKnowledge) {
			this.addPieceOfKnowledge((PieceOfKnowledge) res);
		} else if (res instanceof Query) {
			this.addQuery((Query) res);
		} else if (res instanceof ResultSet) {
			this.addResultSet((ResultSet) res);
		} else if (res instanceof ComposedResource) {
			this.addComposedResource((ComposedResource) res);
		} else if (res instanceof LowLevelDescriptor) {
			this.bejh.getModel().add(rRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(LowLevelDescriptor.class)));
		} else {
			if (!res.getClass().equals(Resource.class)) {
				this.logger.warn("Resource type unknown: " + res.getClass().getCanonicalName());
			}
			this.bejh.getModel().add(rRes, org.apache.jena.vocabulary.RDF.type, ResourceFactory.createResource(WebLabModel.getUriFromResource(Resource.class)));
		}

	}


}
