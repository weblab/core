/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helper;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.Resource;

/**
 * <p>
 * API to manipulate <code>Annotation</code> objects that are present on a <code>Resource</code> and its potential sub-resources.
 * </p>
 * <p>
 * Note that if the given <code>Resource</code> is it self an <code>PieceOfKnowledge</code>, it will not load its content but the content of every <code>Annotation</code> on this
 * <code>PieceOfKnowledge</code> (and recursively then for each <code>Annotation</code> loaded).
 * </p>
 * This class replaces <code>RecursiveAnnotationHelper</code>.
 *
 * In most the case, this component should be replaced by the annotator project.
 *
 * @author EADS WebLab Team
 */
public interface ResourceHelper extends RDFReader {


	/**
	 * Fixes the current <code>Resource</code> that is manipulated by the helper.
	 * 
	 * It also process <code>resource</code> to load every <code>Annotation</code> that are present in its <code>Resource</code> tree.
	 *
	 * @param resource
	 *            The <code>Resource</code> to be considered by this helper.
	 */
	public void setResource(final Resource resource);


	/**
	 * @return The upper level <code>Resource</code> currently handled by the helper.
	 */
	public Resource getResource();


	/**
	 * @param uri
	 *            The URI of a <code>Resource</code> that must be defined in RDF statements and in the <code>Resource</code> tree.
	 * @return A <code>Resource</code>, which is a sub-resource of the <code>Resource</code> behind this helper, and that its URI is <code>uri</code>.
	 * @throws WebLabCheckedException
	 *             If <code>uri</code> is not a valid <code>Resource</code> and a defined URI in RDF.
	 */
	public Resource getResource(final String uri) throws WebLabCheckedException;


	/**
	 * @param uri
	 *            An URI to test existence in RDF <b>and</b> in loaded sub-resources.
	 * @return Whether or not <code>uri</code> represents a WebLab <code>Resource</code> present in <code>Resource</code> tree and an RDF resource.
	 */
	public boolean isFullyDefinedResource(final String uri);


	/**
	 * @param <T>
	 *            The real class of the <code>Resource</code> to load.
	 * @param uri
	 *            The URI of a <code>Resource</code> that must be defined in RDF statements and in the <code>Resource</code> tree.
	 * @param resourceType
	 *            The <code>class</code> of the <code>Resource</code> represented by <code>uri</code>.
	 * @return A <code>Resource</code>, which is a sub-resource of the <code>Resource</code> behind this helper, and that its URI is <code>uri</code>.
	 * @throws WebLabCheckedException
	 *             If <code>uri</code> is not a valid <code>Resource</code> and a defined URI in RDF.
	 */
	public <T extends Resource> T getSpecificResource(final String uri, final Class<T> resourceType) throws WebLabCheckedException;


	/**
	 * @param uri
	 *            The URI of a <code>Resource</code> that must be defined in RDF statements and in the <code>Resource</code> tree.
	 * @return The <code>class</code> of the <code>Resource</code> represented by <code>uri</code>.
	 * @throws WebLabCheckedException
	 *             If <code>uri</code> is not a valid <code>Resource</code> and a defined URI in RDF.
	 */
	public Class<? extends Resource> getType(final String uri) throws WebLabCheckedException;

}
