/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helper.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.sparql.resultset.ResultSetMem;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A semantic resource implementation based on Jena ARQ.
 * A semantic resource can process SPARQL queries on the semantic description of the WebLab resource and, optionally, reason on external ontologies.
 *
 * @author emilienbondu
 */
public class SemanticResource {


	private static final String QUERY_EXEC_ERROR = "An error occurs when executing the query.";


	private static final String RDF_FORMAT = "RDF/XML";


	protected Model jenaModel;


	protected Resource resource;


	protected final FastResourceToJenaModel loader;


	protected final Set<String> prefix;


	protected final Logger logger = LoggerFactory.getLogger(this.getClass());


	/**
	 * To construct a semantic resource from a WebLab Resource.
	 * Use a JenaDefaultModel as model, and fill it with:
	 * <ul>
	 * <li>Statements from the WebLab Resource.</li>
	 * <li>Statements about the WebLab structure of the resource are also added in the model.</li>
	 * </ul>
	 * Set rdf and rdfs prefixes for SPARQL queries to:
	 * 
	 * <pre>
	 * {@code
	 * PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	 * PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
	 * }
	 * </pre>
	 *
	 * @param resource
	 *            The resource on which to open the semantic resource
	 */
	public SemanticResource(final Resource resource) {
		// setting the WebLab resource associated to this semantic resource
		this.resource = resource;

		// init loader & load Jena model for the WebLab resource
		final long start = System.currentTimeMillis();
		this.loader = new FastResourceToJenaModel(resource);
		this.logger.debug("creating loader for " + resource.getUri() + " in " + (System.currentTimeMillis() - start));
		this.jenaModel = this.loader.bejh.getModel();

		// init prefixes
		this.prefix = new HashSet<>();
		this.prefix.add("PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>");
		this.prefix.add("PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>");
	}


	/**
	 * To construct a semantic resource from a WebLab Resource.
	 * Use the given Jena Ontology Model Specification for the Jena model, and fill it with
	 * <ul>
	 * <li>Statements from the WebLab Resource.</li>
	 * <li>Statements about the WebLab structure of the resource are also added in the model.</li>
	 * <li>Classes, properties &amp; instances from ontologies.</li>
	 * </ul>
	 *
	 * Set rdf and rdfs prefixes for SPARQL queries to:
	 * 
	 * <pre>
	 * {@code PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	 * PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>}
	 * </pre>
	 *
	 * @param resource
	 *            The resource on which to open the semantic resource
	 * @param jenaOntModelSpec
	 *            The onto spec to be used as constraint
	 * @param ontologiesURLs
	 *            URL ontologies to be loaded
	 */
	public SemanticResource(final Resource resource, final OntModelSpec jenaOntModelSpec, final String... ontologiesURLs) {
		// setting the WebLab resource associated to this semantic resource
		this.resource = resource;

		// init loader & load Jena model for the WebLab resource
		this.loader = new FastResourceToJenaModel(resource);

		// add ontologies to the Jena model
		this.jenaModel = ModelFactory.createOntologyModel(jenaOntModelSpec, this.loader.bejh.getModel());
		this.loadOntologiesFromURL(ontologiesURLs);

		// init prefixes
		this.prefix = new HashSet<>();
		this.prefix.add("PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>");
		this.prefix.add("PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>");
	}


	/**
	 * @param ontologiesURLs
	 */
	private void loadOntologiesFromURL(final String... ontologiesURLs) {
		for (final String ontology : ontologiesURLs) {
			final URL url;
			try {
				url = new URL(ontology);
			} catch (final MalformedURLException murle) {
				final String errorMessage = "Unable to load ontology at " + ontology + ".";
				this.logger.error(errorMessage, murle);
				throw new WebLabUncheckedException(errorMessage, murle);
			}
			try (final InputStream stream = url.openStream()) {
				this.jenaModel.read(stream, null);
			} catch (final IOException ioe) {
				final String errorMessage = "Unable to load ontology at " + ontology + ".";
				this.logger.error(errorMessage, ioe);
				throw new WebLabUncheckedException(errorMessage, ioe);
			}
		}
	}


	/**
	 * To construct a semantic resource from a WebLab Resource.
	 * Use the given Jena Ontology Model Specification for the Jena model, and fill it with
	 * <ul>
	 * <li>Statements from the WebLab Resource.</li>
	 * <li>Statements about the WebLab structure of the resource are also added in the model.</li>
	 * <li>Classes, properties &amp; instances from ontologies.</li>
	 * </ul>
	 * Set rdf and rdfs prefixes for SPARQL queries to:
	 * 
	 * <pre>
	 * {@code
	 * PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	 * PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
	 * }
	 * </pre>
	 *
	 * @param resource
	 *            The resource on which to open the semantic resource
	 * @param schemaModel
	 *            The schema model to be used for inference or as ontology model
	 */
	public SemanticResource(final Resource resource, final Model schemaModel) {
		// setting the WebLab resource associated to this semantic resource
		this.resource = resource;

		// init loader & load Jena model for the WebLab resource
		this.loader = new FastResourceToJenaModel(resource);

		if (schemaModel instanceof InfModel) {
			this.jenaModel = ModelFactory.createInfModel(((InfModel) schemaModel).getReasoner(), schemaModel, this.loader.bejh.getModel());
		} else {
			this.jenaModel = ModelFactory.createOntologyModel();
			this.jenaModel.add(schemaModel);
		}

		// init prefixes
		this.prefix = new HashSet<>();
		this.prefix.add("PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>");
		this.prefix.add("PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>");
	}


	/**
	 * @return The jena model inside
	 */
	public Model getJenaModel() {
		return this.jenaModel;
	}


	/**
	 * Add a prefix to prefixes added to SPARQL queries on this semantic resource
	 *
	 * @param prefixes
	 *            The prefixes to be added
	 */
	public void addPrefixForSPARQLQueries(final String... prefixes) {
		if (prefixes != null) {
			// add prefix to the set
			for (final String pref : prefixes) {
				this.prefix.add(pref);
			}
		}
	}


	/**
	 * To change the default Jena Model on this semantic resource to an OntologyModel according
	 * the given Jena Ontology Model Specification and load given ontologies in it.
	 * The new Jena Model contains:
	 * <ul>
	 * <li>Statements of the existing model</li>
	 * <li>Classes, properties &amp; instances from ontologies.</li>
	 * </ul>
	 *
	 * @param jenaOntModelSpec
	 *            The ontology model specifications
	 * @param ontologiesURLs
	 *            The ontologies to be loaded
	 */
	public void loadAnInferWithOntologies(final OntModelSpec jenaOntModelSpec, final String... ontologiesURLs) {
		// add ontologies to the Jena model
		this.jenaModel = ModelFactory.createOntologyModel(jenaOntModelSpec, this.jenaModel);
		this.loadOntologiesFromURL(ontologiesURLs);
	}


	/**
	 * To execute a "select" SPARQL request on this semantic resource and get the response
	 * in a Jena ResultSet. RDF and RDFS prefixes have not to be defined in the SPARQL request because they are
	 * automatically added.
	 *
	 * A select can be used, for example, to get all images contained in this resource with the given request:
	 * 
	 * <pre>
	 * {@code
	 * SELECT ?image WHERE {
	 * ?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>
	 * }
	 * }
	 * </pre>
	 *
	 * In this case, resultSet will contains URIs of WebLab images of this semantic resource.
	 *
	 * Note that since the return is an instance of resultset, you MUST close the resulset once you are done with it.
	 *
	 * @param sparqlQuery
	 *            The select sparql query
	 * @return the JenaResultSet for this SPARQL query.
	 */
	public ResultSetMem selectAsJenaResultSet(final String sparqlQuery) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(this.getPrefix() + sparqlQuery);
		try (final QueryExecution qexec = QueryExecutionFactory.create(query, this.jenaModel)) {
			// exec select
			return new ResultSetMem(qexec.execSelect());
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
		return null;
	}


	/**
	 * To execute a "select" SPARQL request on this semantic resource and write the response
	 * in the output stream in the JSON form. RDF and RDFS prefixes have not to be defined
	 * in the SPARQL request because they are automatically added.
	 *
	 * A select can be used, for example, to get all images contained in this resource with the given request:
	 *
	 * <pre>
	 * {@code
	 * SELECT ?image WHERE {
	 * 		?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>
	 * }
	 * }
	 * </pre>
	 *
	 * In this case, the result to this query is wrote as JSON in the output stream like:
	 *
	 * <pre>
	 * {@code
	 * {
	 * 		"head": {
	 * 			"vars": [ "image" ]
	 * 		} ,
	 * 		"results": {
	 * 			"bindings": [
	 * 				{
	 * 					"image": { "type": "uri" , "value": "weblab://anImage" }
	 * 				}
	 * 			]
	 *		}
	 * }
	 * }
	 * </pre>
	 *
	 * @param sparqlQuery
	 *            The select sparql query
	 * @param outStream
	 *            The output stream into which to write results as JSON
	 */
	public void selectAsJSON(final String sparqlQuery, final OutputStream outStream) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(this.getPrefix() + sparqlQuery);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, this.jenaModel)) {
			// exec select and format resultSet
			ResultSetFormatter.outputAsJSON(outStream, qexec.execSelect());
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
	}




	/**
	 * To execute a "select" SPARQL request on this semantic resource and write the response
	 * in the output stream in the XML form. RDF and RDFS prefixes have not to be defined
	 * in the SPARQL request because they are automatically added.
	 *
	 * A select can be used, for example, to get all images contained in this resource with the given request:
	 *
	 * <pre>
	 * {@code
	 * SELECT ?image WHERE {
	 * 		?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>
	 * }
	 * }
	 * </pre>
	 *
	 * In this case, the result to this query is wrote as XML in the output stream like:
	 *
	 * <pre>
	 *  {@code
	 *   <?xml version="1.0"?>
	 * 	<sparql xmlns="http://www.w3.org/2005/sparql-results#">
	 * 		<head>
	 * 			<variable name="image"/>
	 * 		</head>
	 * 		<results>
	 * 			<result>
	 * 				<binding name="image">
	 * 					<uri>weblab://anImage</uri>
	 * 				</binding>
	 * 			</result>
	 * 		</results>
	 * 	</sparql>
	 * }
	 * </pre>
	 *
	 * @param sparqlQuery
	 *            The select sparql query
	 * @param outStream
	 *            The stream into which to write output as sparql XML
	 */
	public void selectAsXML(final String sparqlQuery, final OutputStream outStream) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(this.getPrefix() + sparqlQuery);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, this.jenaModel)) {
			// exec select and format resultSet
			final ResultSet res = qexec.execSelect();
			ResultSetFormatter.outputAsXML(outStream, res);
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
	}


	/**
	 * To execute a "select" SPARQL request on this semantic resource and write the response
	 * in the output stream in the CSV form. RDF and RDFS prefixes have not to be defined
	 * in the SPARQL request because they are automatically added.
	 *
	 * A select can be used, for example, to get all images contained in this resource with the given request:
	 *
	 * <pre>
	 * {@code
	 * SELECT ?image WHERE {
	 * 		?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>
	 * }
	 * }
	 * </pre>
	 *
	 * In this case, the result to this query is wrote as CSV in the output stream like:
	 *
	 * <pre>
	 * {@code
	 * image
	 * weblab://anImage
	 *}
	 * </pre>
	 *
	 * @param sparqlQuery
	 *            The select sparql query
	 * @param outStream
	 *            The stream to be written with CSV results
	 */
	public void selectAsCSV(final String sparqlQuery, final OutputStream outStream) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(this.getPrefix() + sparqlQuery);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, this.jenaModel)) {
			// exec select and format resultSet
			final ResultSet res = qexec.execSelect();
			ResultSetFormatter.outputAsCSV(outStream, res);
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
	}


	/**
	 * To execute a "select" SPARQL request on this semantic resource and get all solutions that
	 * point to a WebLab resource as a WebLab resources list.
	 * RDF and RDFS prefixes have not to be defined in the SPARQL request because they are
	 * automatically added.
	 *
	 * A select can be used, for example, to get all images, as WebLab resources,
	 * contained in this resource with the given request:
	 *
	 * <pre>
	 * {@code
	 * SELECT ?image WHERE {
	 * ?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>
	 * }
	 * }
	 * </pre>
	 *
	 * In this case, return a list that contains WebLab images contained in the semantic resource.
	 * Note that all variables where solutions does not point to a WebLab resource a ignored in response.
	 *
	 * @param sparqlQuery
	 *            The select sparql query
	 * @return the list of WebLab resources
	 */
	public List<Resource> selectAsWeblabResources(final String sparqlQuery) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(this.getPrefix() + sparqlQuery);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, this.jenaModel)) {
			// exec select and create resource list
			return this.listResourcesFromResultSet(qexec.execSelect());
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
		return null;
	}


	/**
	 * To execute a "describe" SPARQL request on this semantic resource and get the description as
	 * a news semantic resource. The semantic resource returned is composed by a PieceOfKnowledge and
	 * a default Jena Model about data of the PieceOfKnowledge.
	 * RDF and RDFS prefixes have not to be defined in the SPARQL request because they are
	 * automatically added.
	 *
	 * A describe can be used, for example, to get the complete description about all images
	 * contained in this resource with the given request:
	 *
	 * <pre>
	 * {@code
	 * DESCRIBE ?image WHERE {
	 * ?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>
	 * }}
	 * </pre>
	 *
	 * In this case, return a new semantic resource composed by a PieceOfKnowledge that describe all images
	 * contained in the semantic resource and also the corresponding Jena model.
	 *
	 * @param sparqlQuery
	 *            The describe sparql query
	 * @return the new semantic resource
	 */
	public SemanticResource describeAsSemanticResource(final String sparqlQuery) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(sparqlQuery);
		try (QueryExecution qexec = QueryExecutionFactory.create(this.getPrefix() + query, this.jenaModel)) {
			// creating a new PieceOfKnowledge
			final PieceOfKnowledge pok = WebLabResourceFactory.createResource("POK", String.valueOf(System.nanoTime()), PieceOfKnowledge.class);

			// writing pok data
			final StringWriter writer = new StringWriter();
			qexec.execDescribe().write(writer, SemanticResource.RDF_FORMAT);
			PoKUtil.setPoKData(pok, writer.getBuffer().toString());

			// creating semantic resource from pok
			return new SemanticResource(pok);
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
		return null;
	}


	/**
	 * To execute a "describe" SPARQL request on this semantic resource and write the description as
	 * RDF/XML in the output stream.
	 * RDF and RDFS prefixes have not to be defined in the SPARQL request because they are
	 * automatically added.
	 *
	 * A describe can be used, for example, to get the complete description about all images
	 * contained in this resource with the given request:
	 *
	 * <pre>
	 * {@code
	 * DESCRIBE ?image WHERE {
	 * 	?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>
	 * }
	 * }
	 * </pre>
	 *
	 * In this case, the result to this query is wrote as RDF/XML in the output stream like:
	 *
	 * <pre>
	 * {@code
	 * <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	 * 			xmlns:j.0="http://weblab.ow2.org/ontology/structure#"
	 * 			xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
	 * 			xmlns:j.2="http://weblab.ow2.org/core/1.2/ontology/processing#" >
	 *
	 * 	 <rdf:Description rdf:about="weblab://anImage">
	 * 		<j.0:inComposedUnit rdf:resource="weblab://theComposedUnitforThisImage"/>
	 * 		<j.0:hasAnnotation rdf:resource="weblab://anWebLabAnnotationOnThisImage"/>
	 * 		<j.2:hasNativeContent rdf:resource="weblab://nativeContentForThisImage"/>
	 * 		<j.2:isExposedAs>http://example/anImage.jpg</j.2:isExposedAs>
	 * 		<rdf:type rdf:resource="http://weblab.ow2.org/core/1.2/ontology/model#Image"/>
	 *   </rdf:Description>
	 * </rdf:RDF>
	 * }
	 * </pre>
	 *
	 * @param sparqlQuery
	 *            The describe sparql query
	 * @param outStream
	 *            The stream into which to write result as RDF/XML
	 */
	public void describeAsRDF(final String sparqlQuery, final OutputStream outStream) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(sparqlQuery);
		try (QueryExecution qexec = QueryExecutionFactory.create(this.getPrefix() + query, this.jenaModel)) {
			// exec describe and format resultSet
			qexec.execDescribe().write(outStream, SemanticResource.RDF_FORMAT);
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
	}


	/**
	 * To execute a "describe" SPARQL request on this semantic resource and get the response an
	 * a Jena model.
	 * RDF and RDFS prefixes have not to be defined in the SPARQL request because they are
	 * automatically added.
	 *
	 * A describe can be used, for example, to get the complete description about all images
	 * contained in this resource with the given request:
	 *
	 * <pre>
	 * {@code
	 * DESCRIBE ?image WHERE {
	 * 		?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>
	 * }
	 * }
	 * </pre>
	 *
	 * In this case, the result to this query is a Jena model on statements of solutions.
	 *
	 * @param sparqlQuery
	 *            The describe sparql query
	 * @return the Jena model
	 */
	public Model describeAsJenaModel(final String sparqlQuery) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(sparqlQuery);
		try (QueryExecution qexec = QueryExecutionFactory.create(this.getPrefix() + query, this.jenaModel)) {
			// exec describe and return resultSet
			return qexec.execDescribe();
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
		return null;
	}


	/**
	 * To execute a "construct" SPARQL request on this semantic resource and get the description as
	 * a news semantic resource. The semantic resource returned is composed by a PieceOfKnowledge and
	 * a default Jena Model about data of the PieceOfKnowledge. Data of the PieceOfKnowledge contains
	 * statements returned in solutions.
	 * RDF and RDFS prefixes have not to be defined in the SPARQL request because they are
	 * automatically added.
	 *
	 * A construct can be used, for example, to get "isExposedAs" property values about all images
	 * contained in this resource with the given request:
	 *
	 * <pre>
	 * {@code
	 * CONSTRUCT {
	 * ?image <http://weblab.ow2.org/core/1.2/ontology/processing#isExposedAs> ?exposed
	 * } WHERE {
	 * ?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>.
	 * ?image <http://weblab.ow2.org/core/1.2/ontology/processing#isExposedAs> ?exposed
	 * }
	 * }
	 * </pre>
	 *
	 * In this case, return a new semantic resource composed by a PieceOfKnowledge that contains statements
	 * of solutions and also the corresponding Jena model.
	 *
	 * @param sparqlQuery
	 *            The construct sparql query
	 * @return the new semantic resource
	 */
	public SemanticResource constructAsSemanticResource(final String sparqlQuery) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(sparqlQuery);
		final StringWriter writer = new StringWriter();
		try (QueryExecution qexec = QueryExecutionFactory.create(this.getPrefix() + query, this.jenaModel)) {
			// writing pok data
			qexec.execConstruct().write(writer, SemanticResource.RDF_FORMAT);
			// creating a new PieceOfKnowledge
			final PieceOfKnowledge pok = WebLabResourceFactory.createResource("POK", String.valueOf(System.nanoTime()), PieceOfKnowledge.class);
			PoKUtil.setPoKData(pok, writer.getBuffer().toString());
			// creating semantic resource from pok
			return new SemanticResource(pok);
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
		return null;
	}


	/**
	 * To execute a "construct" SPARQL request on this semantic resource and get the description as
	 * a news semantic resource. The semantic resource returned is composed by a PieceOfKnowledge and
	 * a default Jena Model about data of the PieceOfKnowledge. Data of the PieceOfKnowledge contains
	 * statements returned in solutions.
	 * RDF and RDFS prefixes have not to be defined in the SPARQL request because they are
	 * automatically added.
	 *
	 * A construct can be used, for example, to get "isExposedAs" property values about all images
	 * contained in this resource with the given request:
	 *
	 * <pre>
	 * {@code
	 * CONSTRUCT {
	 * 		?image <http://weblab.ow2.org/core/1.2/ontology/processing#isExposedAs> ?exposed
	 * } WHERE {
	 * 		?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>.
	 * 		?image <http://weblab.ow2.org/core/1.2/ontology/processing#isExposedAs> ?exposed
	 * }
	 * }
	 * </pre>
	 *
	 * In this case, the result to this query is wrote as RDF/XML in the output stream like:
	 *
	 * <pre>
	 * {@code
	 * <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	 * 			xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
	 * 			xmlns:j.0="http://weblab.ow2.org/core/1.2/ontology/processing#" >
	 *
	 * 	 <rdf:Description rdf:about="weblab://anImage">
	 * 		<j.0:isExposedAs>http://example/anImage.jpg</j.2:isExposedAs>
	 *   </rdf:Description>
	 *
	 * </rdf:RDF>
	 * }
	 * </pre>
	 *
	 * @param sparqlQuery
	 *            The construct sparql query
	 * @param outStream
	 *            The output stream into which to write graph as RDF/XML
	 */
	public void constructAsRDF(final String sparqlQuery, final OutputStream outStream) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(this.getPrefix() + sparqlQuery);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, this.jenaModel)) {
			// exec construct and format resultSet
			qexec.execConstruct().write(outStream, SemanticResource.RDF_FORMAT);
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
	}


	/**
	 * To execute a "construct" SPARQL request on this semantic resource and get the response as a
	 * jena model. The returned jena model contains statements returned in solutions.
	 * RDF and RDFS prefixes have not to be defined in the SPARQL request because they are
	 * automatically added.
	 *
	 * A construct can be used, for example, to get "isExposedAs" property values about all images
	 * contained in this resource with the given request:
	 *
	 * <pre>
	 * {@code
	 * CONSTRUCT {
	 * ?image <http://weblab.ow2.org/core/1.2/ontology/processing#isExposedAs> ?exposed
	 * } WHERE {
	 * ?image rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>.
	 * ?image <http://weblab.ow2.org/core/1.2/ontology/processing#isExposedAs> ?exposed
	 * }
	 * }
	 * </pre>
	 *
	 * In this case, return a Jena model that contains statements of solutions.
	 *
	 * @param sparqlQuery
	 *            The construct sparql query
	 * @return the Jena model
	 */
	public Model constructAsJenaModel(final String sparqlQuery) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(sparqlQuery);
		try (QueryExecution qexec = QueryExecutionFactory.create(this.getPrefix() + query, this.jenaModel)) {
			// exec construct and return Jena model
			return qexec.execConstruct();
		} catch (final Exception e) {
			this.logger.error(SemanticResource.QUERY_EXEC_ERROR, e);
		}
		return null;
	}


	/**
	 * To execute a "ask" SPARQL request on this semantic resource and get the response as a boolean.
	 * RDF and RDFS prefixes have not to be defined in the SPARQL request because they are
	 * automatically added.
	 *
	 * A ask can be used, for example, to know if a resource is WebLab image with the given request:
	 *
	 * <pre>
	 * {@code
	 * 	ASK {<weblab://anImage rdf:type <http://weblab.ow2.org/core/1.2/ontology/model#Image>}
	 * }
	 * </pre>
	 *
	 * In this case, return true if weblab://anImage is a WebLab image, false otherwise.
	 *
	 * @param sparqlQuery
	 *            The ask sparql query
	 * @return The boolean answer of the ask query
	 */
	public boolean ask(final String sparqlQuery) {
		// prepare a Jena Query & Jena QueryExecution
		final Query query = QueryFactory.create(this.getPrefix() + sparqlQuery);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, this.jenaModel)) {
			// exec ask and return boolean
			return qexec.execAsk();
		}
	}


	/**
	 * To retrieve WebLab resources describe in the Jena ResultSet as a list
	 *
	 * @param res
	 *            the Jena resultSet
	 * @return the list of WebLab resources.
	 */
	private List<Resource> listResourcesFromResultSet(final ResultSet res) {
		// prepare the return list
		final List<Resource> ret = new ArrayList<>();

		// iteration in resultset
		while (res.hasNext()) {
			final QuerySolution solution = res.next();
			final Iterator<String> varIterator = solution.varNames();

			// iteration on variables
			while (varIterator.hasNext()) {
				final String varName = varIterator.next();
				if (solution.get(varName).isURIResource()) {

					// variable value for this result is a resource
					final String resURI = solution.getResource(varName).getURI();
					if (this.loader.isDefinedResource(resURI)) {

						// the resource corresponds to a WebLab resource
						final Resource resWL = this.loader.resourcesMap.get(resURI);
						if (resWL != null) {

							// add the WebLab resource to the list
							ret.add(resWL);
						}
					}
				}
			}
		}
		// return the list
		return ret;
	}


	/**
	 * To get all added prefixes
	 *
	 * @return The list of prefixes
	 */
	private String getPrefix() {
		final StringBuffer prefixes = new StringBuffer();
		for (final String pref : this.prefix) {
			// concat prefix
			prefixes.append(pref);
		}
		return prefixes.toString();
	}

}
