/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helper.impl;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdfxml.xmlinput.DOM2Model;
import org.ow2.weblab.core.extended.ontologies.RDF;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.w3c.dom.Node;
import org.xml.sax.SAXParseException;

/**
 * A fast Jena model loader for WebLab resources, based on DOM, that extends JenaResourceStructureHelperExtended. Using DOM object to add PieceOfKnowledge in Jena model is above 30% faster than using
 * String and indentation of the PoKUtil getPokData method.
 *
 * @author emilienbondu
 */
public class FastResourceToJenaModel extends JenaResourceStructureHelper {


	/**
	 * Constructor on the WebLab resource.
	 * 
	 * @param resource
	 *            The resource to be loaded inside
	 */
	public FastResourceToJenaModel(Resource resource) {
		super(resource);
	}


	@Override
	protected void addPieceOfKnowledge(final PieceOfKnowledge pok) {
		final Object object = pok.getData();
		if (object != null && !object.getClass().equals(Object.class)) {
			final Model newModel = ModelFactory.createDefaultModel();
			try {
				final DOM2Model domModel = DOM2Model.createD2M(RDF.NAMESPACE, newModel);
				domModel.load(((Node) object).getFirstChild());
				domModel.close();
			} catch (final SAXParseException exception) {
				this.logger.error("Could not add POK into current FastResourceToJenaModel.", exception);
			}
			this.bejh.getModel().add(newModel);
		}
	}

}
