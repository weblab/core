/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.core.helper;

import java.util.List;
import java.util.Set;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;

/**
 * <p>
 * This class is an extract from former <code>AnnotationHelper</code>, in order to be joint with <code>ResourceHelper</code> methods.
 * </p>
 *
 * @author ymombrun
 * @see ResourceHelper
 */
interface RDFReader {


	/**
	 * @return A valid RDF/XML <code>String</code> of the current changed made.
	 */
	public String getRdfXml();


	/**
	 * Loads an RDF/XML <code>String</code>, to be queried.
	 *
	 * @param rdfXml
	 *            A RDF/XML content to be loaded.
	 * @throws WebLabCheckedException
	 *             If the <code>rdfXml</code> is not an RDF/XML valid <code>String</code>.
	 */
	public void loadFromRdfXml(final String rdfXml) throws WebLabCheckedException;


	/**
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @return A <code>List</code> containing values of every literal that are object of statements having <code>uriPred</code> as predicate and <code>uriSubj</code> as subject in the
	 *         <code>PieceOfKnowledge</code>.
	 */
	public List<String> getLitsOnPredSubj(final String uriSubj, final String uriPred);


	/**
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @param language
	 *            The xml:lang attribute for this literal.
	 * @return A <code>List</code> containing values in language <code>language</code> of every literal that are object of statements having <code>uriPred</code> as predicate and <code>uriSubj</code>
	 *         as subject in the <code>PieceOfKnowledge</code>. For more information about language
	 *         attribute, see http://www.w3.org/TR/REC-xml/#sec-lang-tag.
	 */
	public List<String> getLitsOnPredSubj(final String uriSubj, final String uriPred, final String language);


	/**
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @return A <code>Set</code> containing URIs of every named resources that are predicate of any statement having <code>uriSubj</code> as subject in the <code>PieceOfKnowledge</code>.
	 */
	public Set<String> getPredsOnSubj(final String uriSubj);


	/**
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @return A <code>List</code> containing URIs of every named resources that are object of statements having <code>uriPred</code> as predicate and <code>uriSubj</code> as subject in the
	 *         <code>PieceOfKnowledge</code>.
	 */
	public List<String> getRessOnPredSubj(final String uriSubj, final String uriPred);


	/**
	 * @return A <code>Set</code> containing URIs of every named resources that are subject of any statement in the <code>PieceOfKnowledge</code>.
	 */
	public Set<String> getSubjs();


	/**
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @return A <code>Set</code> containing URIs of every named resources that are subject of statements having <code>uriPred</code> as predicate in the <code>PieceOfKnowledge</code>.
	 */
	public Set<String> getSubjsOnPred(final String uriPred);


	/**
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @param litObj
	 *            The literal value of the object.
	 * @return A <code>Set</code> containing URIs of every named resources that are subject of statements having <code>uriPred</code> as predicate and <code>litObj</code> as object literal value in
	 *         the <code>PieceOfKnowledge</code>.
	 */
	public Set<String> getSubjsOnPredLit(final String uriPred, final String litObj);


	/**
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @param uriObj
	 *            The URI of the object resource.
	 * @return A <code>Set</code> containing URIs of every named resources that are subject of statements having <code>uriPred</code> as predicate and <code>uriObj</code> as object named resource URI
	 *         in the <code>PieceOfKnowledge</code>.
	 */
	public Set<String> getSubjsOnPredRes(final String uriPred, final String uriObj);


	/**
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @param litObj
	 *            The literal value of the object.
	 * @return Whether or not the <code>PieceOfKnowledge</code> contains at least one statement having <code>uriSubj</code> as subject, <code>uriPred</code> as
	 *         predicate and <code>litObj</code> as literal value of the object.
	 */
	public boolean hasLitStat(final String uriSubj, final String uriPred, final String litObj);


	/**
	 * @param uriSubj
	 *            The URI of the subject resource.
	 * @param uriPred
	 *            The URI of the predicate resource.
	 * @param uriObj
	 *            The URI of the object resource.
	 * @return Whether or not the <code>PieceOfKnowledge</code> contains at least one statement having <code>uriSubj</code> as subject, <code>uriPred</code> as
	 *         predicate and <code>uriObj</code> as URI of the object.
	 */
	public boolean hasResStat(final String uriSubj, final String uriPred, final String uriObj);


	/**
	 * Tests if the given URI is defined as a named resource in the <code>PieceOfKnowledge</code>.
	 *
	 * @param uri
	 *            The URI to test existence in the <code>PieceOfKnowledge</code>.
	 * @return <code>true</code> if the URI is defined as a named resource URI in the <code>PieceOfKnowledge</code>.
	 */
	public boolean isDefinedResource(final String uri);


	/**
	 * @return A <code>Set</code> containing URIs of properties that are predicate of any statement in the <code>PieceOfKnowledge</code>.
	 */
	public Set<String> getPreds();

}
